﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.ShareTrxControl;
using Esol.ShareMemory;
using System.Text.RegularExpressions;
using System.Windows.Documents;
#endregion

namespace Esol.CIM
{
    public class CMSProcesser
    {
        #region Class Constants
        #endregion

        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_CimMessageProcessor.HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx ShareTrx
        {
            get
            {
                return m_CimMessageProcessor.ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory ShareMem
        {
            get
            {
                return m_CimMessageProcessor.ShareMem;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public CMSProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            InitializeCMS_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeCMS_Event()
        {

            Xgem.OnCMSReqCancelCarrier += new OnCMSReqCancelCarrier(S3F17_CancelCarrier);
            Xgem.OnCMSRspCancelCarrier += new OnCMSRspCancelCarrier(S3F18_CancelCarrier);
            Xgem.OnCMSReqCancelCarrierAtPort += new OnCMSReqCancelCarrierAtPort(S3F18_CancelCarrierAtPort);
            Xgem.OnCMSRspProceedCarrier += new OnCMSRspProceedCarrier(OnCMSRspProceedCarrier);

            Xgem.OnCMSCarrierVerifySucceeded += new OnCMSCarrierVerifySucceeded(OnCMSCarrierVerifySucceeded);


            //CMS
            Xgem.OnCMSCarrierIDStatusChanged += new OnCMSCarrierIDStatusChanged(S3F17_CarrierIdDown);
            Xgem.OnCMSSlotMapStatusChanged += new OnCMSSlotMapStatusChanged(S3F17_SlotMapDown);
            Xgem.OnCMSReqChangeAccess += new OnCMSReqChangeAccess(S3F27);
            Xgem.OnCMSAssociationStateChanged += new OnCMSAssociationStateChanged(AssociationStateChangeEvent);
            Xgem.OnCMSReservationStateChanged += new OnCMSReservationStateChanged(ReservationStateChangeEvent);
            Xgem.OnCMSAccessModeStateChanged += new OnCMSAccessModeStateChanged(AccessModeStateChanged);
            Xgem.OnCMSRspBind += new OnCMSRspBind(RspBind);
            Xgem.OnCMSRspCancelBind += new OnCMSRspCancelBind(RspCancelBind);
            Xgem.OnCMSReqCarrierIn += new OnCMSReqCarrierIn(OnCMSReqCarrierIn);
            Xgem.OnCMSReqCarrierRelease += new OnCMSReqCarrierRelease(OnCMSReqCarrierRelease);
            Xgem.OnCMSServiceReceived += new OnCMSServiceReceived(ServiceReceived);
            Xgem.OnCMSCancelAllCarrierOut += new OnCMSCancelAllCarrierOut(CancelAllCarrierOut);
            Xgem.OnCMSRspCarrierIn += new OnCMSRspCarrierIn(RspCarrierIn);

        }
        #endregion

        #region Class CMS Evnet

        private void S3F17_CarrierIdDown(string sLocID, long nState, string sCarrierID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSCarrierIDStatusChanged");

            HsmsLogControl.AddMessage("sLocID : {0} / nState : {1} / sCarrierID : {2}", sLocID, nState, sCarrierID);
            if (nState == 3)
            {
                byte iPortNo = Convert.ToByte(sLocID.Replace("LP", string.Empty));
                List<PlcAddr> listCarrierDatas = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_CARRIER_DATA_DOWNLOAD_STRUCT];

                ShareMem.SetByte(listCarrierDatas[0], 1);
                ShareMem.SetByte(listCarrierDatas[1], iPortNo);
                ShareMem.SetByte(listCarrierDatas[2], 2);
                ShareMem.SetAscii(listCarrierDatas[3], sCarrierID);
                ShareMem.SetAscii(listCarrierDatas[4], string.Empty);
                ShareMem.SetAscii(listCarrierDatas[5], string.Empty);

                PlcAddr CarrierCommand = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_CARRIER_DATA_DOWN_COMMAND];
                ShareMem.SetBit(CarrierCommand, true);
            }

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCMSRspProceedCarrier(string sLocID, string sCarrierID, long nCount, string[] psLotID, string[] psSubstrateID, string sUsage, long nResult)
        {
            byte iPortNo = Convert.ToByte(sLocID.Replace("LP", string.Empty));
            string sMaskID = GetSlotMap(iPortNo);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSRspProceedCarrier");

            HsmsLogControl.AddMessage("sLocID : {0} / sCarrierID : {1} / nCount : {2} / sUsage : {3} / nResult : {4}", sLocID, sCarrierID, nCount, sUsage, nResult);
            for (int i = 0; i < nCount; i++)
                HsmsLogControl.AddMessage("[{0}] psLotID : {1} / psSubstrateID : {2}", i, psLotID[i], psSubstrateID[i]);

            List<PlcAddr> listCarrierDatas = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_CARRIER_DATA_DOWNLOAD_STRUCT];
            ShareMem.SetByte(listCarrierDatas[0], 2);
            ShareMem.SetByte(listCarrierDatas[1], iPortNo);
            ShareMem.SetByte(listCarrierDatas[2], 3);
            ShareMem.SetAscii(listCarrierDatas[3], sCarrierID);
            ShareMem.SetAscii(listCarrierDatas[4], psSubstrateID[0]);
            ShareMem.SetAscii(listCarrierDatas[5], psLotID[0]);

            PlcAddr CarrierCommand = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_CARRIER_DATA_DOWN_COMMAND];
            ShareMem.SetBit(CarrierCommand, true);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S3F17_SlotMapDown(string sLocID, long nState, string sCarrierID)
        {

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSSlotMapStatusChanged");

            HsmsLogControl.AddMessage("sLocID : {0} / nState : {1} / sCarrierID : {2}", sLocID, nState, sCarrierID);

            if (nState == 3)
            {
                byte iPortNo = Convert.ToByte(sLocID.Replace("LP", string.Empty));
                string sMaskID = GetSlotMap(iPortNo);

                List<PlcAddr> listCarrierDatas = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_CARRIER_DATA_DOWNLOAD_STRUCT];
                ShareMem.SetByte(listCarrierDatas[0], 2);
                ShareMem.SetByte(listCarrierDatas[1], iPortNo);
                ShareMem.SetByte(listCarrierDatas[2], 4);
                ShareMem.SetAscii(listCarrierDatas[3], sCarrierID);
                ShareMem.SetAscii(listCarrierDatas[4], sMaskID);
                ShareMem.SetAscii(listCarrierDatas[5], string.Empty);

                PlcAddr CarrierCommand = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_CARRIER_DATA_DOWN_COMMAND];
                ShareMem.SetBit(CarrierCommand, true);
            }

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S3F17_CancelCarrier(long nMsgId, string sLocID, string sCarrierID)
        {
            List<object> list = new List<object>() { nMsgId, sLocID, sCarrierID };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSReqCancelCarrier");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S3F17"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S3F18_CancelCarrierAtPort(long nMsgId, string sLocID)
        {
            //Cancel Carrier 
            //CarrierID 와 상관없이 LoadPort에 존재하는 Carrier 를 Cancel 처리
            List<object> list = new List<object>() { nMsgId, sLocID };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSReqCancelCarrierAtPort");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S3F17"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S3F18_CancelCarrier(string sLocID, string sCarrierID, long nResult)
        {

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSRspCancelCarrier");

            HsmsLogControl.AddMessage("sLocID : {0} / sCarrierID : {1} / nResult : {2}", sLocID, sCarrierID, nResult);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S3F27(long nMsgId, long nMode, long nCount, string[] psLocID)
        {
            //AccessMode Change Command
            List<object> list = new List<object>() { nMsgId, nMode, nCount, psLocID };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSReqChangeAccess");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S3F27"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCMSReqCarrierRelease(long nMsgId, string sLocID, string sCarrierID)
        {
            long nReturn = 0;

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSReqCarrierRelease");

            HsmsLogControl.AddMessage("nMsgId : {0} / sLocID : {1} / sCarrierID : {2}", nMsgId, sLocID, sCarrierID);

            //Send Response Message : S3F18
            nReturn = Xgem.CMSRspCarrierRelease(nMsgId, sLocID, sCarrierID, 0, 0, null, null);
            if (nReturn == 0)
                HsmsLogControl.AddMessage("[EQ ==> XGEM] Return OK");
            else
                HsmsLogControl.AddMessage("[EQ ==> XGEM] Return Fail [{0}]", nReturn);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCMSReqCarrierIn(long nMsgId, string sLocID, string sCarrierID)
        {
            long nReturn = 0;

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSReqCarrierIn");

            HsmsLogControl.AddMessage("nMsgId : {0} / sLocID : {1} / sCarrierID : {2}", nMsgId, sLocID, sCarrierID);

            //Send Response Message : S3F18
            nReturn = Xgem.CMSRspCarrierIn(nMsgId, sLocID, sCarrierID, 0, 0, null, null);
            if (nReturn == 0)
                HsmsLogControl.AddMessage("[EQ ==> XGEM] Return OK");
            else
                HsmsLogControl.AddMessage("[EQ ==> XGEM] Return Fail [{0}]", nReturn);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void AssociationStateChangeEvent(string sLocID, long nState)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSAssociationStateChanged");
            HsmsLogControl.AddMessage("sLocID : {0} / nState : {1}", sLocID, nState);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void ReservationStateChangeEvent(string sLocID, long nState)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSReservationStateChanged");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sLocID : {0} / nState : {1}", sLocID, nState);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void AccessModeStateChanged(string sLocID, long nState)
        {

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSAccessModeStateChanged");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sLocID : {0} / nState : {1}", sLocID, nState == 1 ? "Auto" : "Manual");
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void RspBind(string sLocID, string sCarrierID, long nResult)
        {

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSRspBind");
            HsmsLogControl.AddMessage(string.Format("sLocID : {0} / sCarrierID : {1} / nResult : {2}", sLocID, sCarrierID, nResult));
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void RspCancelBind(string sLocID, string sCarrierID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSRspCancelBind");
            HsmsLogControl.AddMessage(string.Format("sLocID : {0} / sCarrierID : {1} / nResult : {2}", sLocID, sCarrierID, nResult));
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void ServiceReceived(long nService, string sLocID, string sCarrierID, string sSlotMap, long nCount, string[] psLotID, string[] psSubstrateID, string sUsage)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);

            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSServiceReceived");
            HsmsLogControl.AddMessage(string.Format("nService : {0} / sLocID : {1} / sCarrierID : {2} / sSlotMap : {3} / nCount : {4} / sUsage : {5}", nService, sLocID, sCarrierID, sSlotMap, nCount, sUsage)); 
            for (int i = 0; i < nCount; i++)
                HsmsLogControl.AddMessage(string.Format("[{0}] psLotID : {0} / psSubstrateID : {1}", i, psLotID[i], psSubstrateID[i]));
            
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void CancelAllCarrierOut()
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSCancelAllCarrierOut");
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void RspCarrierIn(string sLocID, string sCarrierID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSRspCarrierIn");
            HsmsLogControl.AddMessage(string.Format("sLocID : {0} / sCarrierID : {1} / nResult : {2}", sLocID, sCarrierID, nResult));
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCMSCarrierVerifySucceeded(long nVerifyType, string sLocID, string sCarrierID, string sSlotMap, long nCount, string[] psLotID, string[] psSubstrateID, string sUsage)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCMSCarrierVerifySucceeded");

            HsmsLogControl.AddMessage(string.Format("nVerifyType : {0} / sLocID : {1} / sCarrierID : {2} / sSlotMap : {3} / nCount : {4} / sUsage : {5}", nVerifyType, sLocID, sCarrierID, sSlotMap, nCount, sUsage));
            for (int i = 0; i < nCount; i++)
                HsmsLogControl.AddMessage(string.Format("[{0}] psLotID : {0} / psSubstrateID : {1}", i, psLotID[i], psSubstrateID[i]));

            byte iPortNo = Convert.ToByte(sLocID.Replace("LP", string.Empty));
            List<PlcAddr> listCarrierDatas = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_CARRIER_DATA_DOWNLOAD_STRUCT];
            byte iState = nVerifyType == 0 ? (byte)1 : (byte)2;
            byte iResult = iState == 1 ? (byte)1 : (byte)3;

            ShareMem.SetByte(listCarrierDatas[0], iState);
            ShareMem.SetByte(listCarrierDatas[1], iPortNo);
            ShareMem.SetByte(listCarrierDatas[2], iResult);
            ShareMem.SetAscii(listCarrierDatas[3], sCarrierID);
            ShareMem.SetAscii(listCarrierDatas[4], psSubstrateID[0]);
            ShareMem.SetAscii(listCarrierDatas[5], psLotID[0]);

            PlcAddr CarrierCommand = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_CARRIER_DATA_DOWN_COMMAND];
            ShareMem.SetBit(CarrierCommand, true);

            if (!m_CimMessageProcessor.SelectCarrierID.Contains(sCarrierID))
                m_CimMessageProcessor.SelectCarrierID.Add(sCarrierID);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private string GetSlotMap(byte iPortNo)
        {
            long nReturn = 0;
            long nObjID = 0;
            long nCount = 0;
            string _locID = string.Format("LP{0}", iPortNo);
            long nCarrContentsMapCnt = 0;
            string[] saLocID = null;
            string[] saSubstID = null;
            string sSubId = string.Empty;

            nReturn = Xgem.CMSGetAllCarrierInfo(ref nObjID, ref nCount);
            if (nReturn == 0)
            {
                string _tempPortID = string.Empty;
                for (int _iPos = 0; _iPos < nCount; _iPos++)
                {
                    Xgem.GetCarrierLocID(nObjID, _iPos, ref _tempPortID);

                    if (_tempPortID != _locID)
                        continue;

                    nReturn = Xgem.GetCarrierContentsMapCount(nObjID, _iPos, ref nCarrContentsMapCnt);
                    HsmsLogControl.AddMessage(string.Format("GetCarrierContentsMapCount Successfully  ({0}) ", nCarrContentsMapCnt));

                    if (nReturn == 0)
                    {
                        if (nCarrContentsMapCnt > 0)
                        {
                            saLocID = new string[nCarrContentsMapCnt];
                            saSubstID = new string[nCarrContentsMapCnt];
                        }

                        nReturn = Xgem.GetCarrierContentsMap(nObjID, _iPos, nCarrContentsMapCnt, ref saLocID, ref saSubstID);

                        if (nReturn == 0)
                        {
                            HsmsLogControl.AddMessage(string.Format("GetCarrierContentsMap Successfully  (Index :{0} CarrContentsMapCnt:{1})({2}) ", _iPos, nCarrContentsMapCnt, nReturn));
                            for (int i = 0; i < nCarrContentsMapCnt; i++)
                            {
                                HsmsLogControl.AddMessage(string.Format("LocID:{0}, SubstID:{1} ", saLocID[i], saSubstID[i]));
                            }

                            sSubId = saSubstID[0];
                        }
                        else
                            HsmsLogControl.AddMessage(string.Format("Fail to GetCarrierContentsMap (Index : {0} )({1}) ", _iPos, nReturn));

                    }
                    else
                        HsmsLogControl.AddMessage("Fail to GetCarrierContentsMap Count");
                }
                Xgem.GetCarrierClose(nObjID);
            }
            else
                HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to Get All Carrier Information");

            return sSubId;
        }

        #endregion
    }
}
