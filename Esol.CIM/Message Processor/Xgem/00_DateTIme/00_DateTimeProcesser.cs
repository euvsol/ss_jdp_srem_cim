﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.ShareTrxControl;
using Esol.ShareMemory;
using System.Text.RegularExpressions;
#endregion

namespace Esol.CIM
{
    public class DateTimeProcesser
    {
        #region Class Constants
        #endregion

        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public DateTimeProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            InitializeDateTime_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDateTime_Event()
        {
            //DateTime
            Xgem.OnGEMReqGetDateTime += new OnGEMReqGetDateTime(S2F17_HOST);
            Xgem.OnGEMRspGetDateTime += new OnGEMRspGetDateTime(S2F18);
            Xgem.OnGEMReqDateTime += new OnGEMReqDateTime(S2F31);
        }
        #endregion

        #region Class Date Time Event
        /// <summary>
        /// 
        /// </summary>
        private void S2F17_HOST(long nMsgId)
        {
            List<object> list = new List<object>() { nMsgId };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqGetDateTime");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S2F17"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void S2F18(string sSystemTime)
        {
            List<object> list = new List<object>() { sSystemTime };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMRspGetDateTime");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S2F18"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        /// <summary>
        /// S2F31 Date Time Set
        /// </summary>
        private void S2F31(long nMsgId, string sSystemTime)
        {
            List<object> list = new List<object>() { nMsgId, sSystemTime };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqDateTime");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S2F31"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion
    }
}
