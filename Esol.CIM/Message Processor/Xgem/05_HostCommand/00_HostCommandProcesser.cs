﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.LogControl;
using System.Text;
#endregion

namespace Esol.CIM
{
    public class HostCommandProcesser
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_CimMessageProcessor.LogManager;
            }
        }


        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public HostCommandProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            InitializeHostCommand_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeHostCommand_Event()
        {
            //Control State Command
            Xgem.OnGEMReqOffline += new OnGEMReqOffline(S1F15);
            Xgem.OnGEMReqOnline += new OnGEMReqOnline(S1F17);

            //RCMD
            Xgem.OnGEMReqRemoteCommand += new OnGEMReqRemoteCommand(S2F41);
        }
        #endregion

        #region Class Control State Host Command
        /// <summary>
        /// 
        /// </summary>
        private void S1F15(long nMsgId, long nFromState, long nToState)
        {
            List<object> list = new List<object>() { nMsgId, nFromState, nToState };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqOffline");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S1F15"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void S1F17(long nMsgId, long nFromState, long nToState)
        {
            //Online Host Command
            List<object> list = new List<object>() { nMsgId, nFromState, nToState };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqOnline");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S1F17"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion


        #region Class RCMD Event
        /// <summary>
        /// 
        /// </summary>
        private void S2F41(long nMsgId, string sRcmd, long nCount, string[] psNames, string[] psVals)
        {
            List<object> list = new List<object>() { nMsgId, sRcmd, nCount, psNames, psVals };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqRemoteCommand");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S2F41"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion
    }
}
