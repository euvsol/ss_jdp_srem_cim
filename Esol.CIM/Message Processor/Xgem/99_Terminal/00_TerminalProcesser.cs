﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.ShareTrxControl;
using Esol.ShareMemory;
using System.Text.RegularExpressions;
#endregion

namespace Esol.CIM
{
    public class TerminalProcesser
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public TerminalProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            InitializeTerminal_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeTerminal_Event()
        {

            //Terminal
            Xgem.OnGEMTerminalMessage += new OnGEMTerminalMessage(S10F3);
            Xgem.OnGEMTerminalMultiMessage += new OnGEMTerminalMultiMessage(S10F5);
        }
        #endregion

        #region Class Terminal Message Event
        /// <summary>
        /// S10F3 Terminal Message Single
        /// </summary>
        private void S10F3(long nTid, string sMsg)
        {
            List<object> list = new List<object>() { nTid, sMsg };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMTerminalMessage");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S10F3"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        /// <summary>
        /// S10F5 Terminal Message Multi
        /// </summary>
        private void S10F5(long nTid, long nCount, string[] psMsg)
        {
            List<object> list = new List<object>() { nTid, nCount };

            foreach (string sMsg in psMsg)
                list.Add(sMsg);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMTerminalMultiMessage");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S10F5"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion
    }
}
