﻿#region Class using
using GEM_XGem300Pro;
#endregion

namespace Esol.CIM
{
    public class ControlJobData
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private string m_sControlJobId;
        /// <summary>
        /// 
        /// </summary>
        private long m_nControlJobState;
        /// <summary>
        /// 
        /// </summary>
        private long m_nAutoStart;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sArrayPJJobIds;
        /// <summary>
        /// 
        /// </summary>
        private ProcessJobData m_ProcessJobData;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public string ControlJobID
        {
            get
            {
                return m_sControlJobId;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long AutoStart
        {
            get
            {
                return m_nAutoStart;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long ControlJobState
        {
            get
            {
                return m_nControlJobState;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] ProcessJogIds
        {
            get
            {
                return m_sArrayPJJobIds;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ProcessJobData ProcessJobData
        {
            get
            {
                return m_ProcessJobData;
            }
        }
        #endregion

        #region Initialization
        /// <summary>
        /// 
        /// </summary>
        public ControlJobData()
        {
            m_sArrayPJJobIds = new string[1];
            m_ProcessJobData = new ProcessJobData();
        }
        /// <summary>
        /// 
        /// </summary>
        public ControlJobData GetControlJobData(string sControlJobId, XGem300ProNet Xgem)
        {
            long nObjID = 0;
            long nCJobCount = 0;
            string sCJobID = string.Empty;
            m_sControlJobId = sControlJobId;

            long nReturn = Xgem.CJGetAllJobInfo(ref nObjID, ref nCJobCount);
            if (nReturn == 0)
            {
                for (int i = 0; i < nCJobCount; i++)
                {
                    nReturn = Xgem.GetCtrlJobID(nObjID, i, ref sCJobID);

                    if (sCJobID == sControlJobId)
                    {
                        nReturn = Xgem.GetCtrlJobState(nObjID, i, ref m_nControlJobState);

                        nReturn = Xgem.GetCtrlJobStartMethod(nObjID, i, ref m_nAutoStart);

                        nReturn = Xgem.GetCtrlJobPRJobIDs(nObjID, i, 1, ref m_sArrayPJJobIds);

                        foreach (string sProcessJobId in m_sArrayPJJobIds)
                        {
                            m_ProcessJobData.GetProcessJobData(sProcessJobId, Xgem);
                            m_sArrayPJJobIds[0] = sProcessJobId;
                        }
                    }

                }
                Xgem.GetCtrlJobClose(nObjID);
            }
            else
                return null;

            return this;
        }
        #endregion
    }
}
