﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.ShareTrxControl;
using Esol.ShareMemory;
using System.Windows.Documents;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using static System.Windows.Forms.AxHost;
using System.Security.Cryptography;
#endregion

namespace Esol.CIM
{
    public class ControlJobProcesser
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        long m_iCjJobCount = 0;
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, List<string>> m_hashCjToPjs;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_CimMessageProcessor.HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx ShareTrx
        {
            get
            {
                return m_CimMessageProcessor.ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory ShareMem
        {
            get
            {
                return m_CimMessageProcessor.ShareMem;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public ControlJobProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            m_hashCjToPjs = new Dictionary<string, List<string>>();

            IntializeCJ_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        public void IntializeCJ_Event()
        {
            Xgem.OnCJReqVerify += new OnCJReqVerify(S14F9);
            Xgem.OnCJReqCommand += new OnCJReqCommand(S16F27);  //CJ Command
            Xgem.OnCJRspCommand += new OnCJRspCommand(S16F28);  //CJ Command ACK
            Xgem.OnCJRspGetJob += new OnCJRspGetJob(OnCJRspGetJob);
            Xgem.OnCJRspGetAllJobID += new OnCJRspGetAllJobID(OnCJRspGetAllJobID);
            Xgem.OnCJRspCreate += new OnCJRspCreate(OnCJRspCreate);
            Xgem.OnCJCreated += new OnCJCreated(OnCJCreated);
            Xgem.OnCJStateChanged += new OnCJStateChanged(OnCJStateChanged);
            Xgem.OnCJManualStartDisplay += new OnCJManualStartDisplay(OnCJManualStartDisplay);
            Xgem.OnCJRspSelect += new OnCJRspSelect(OnCJRspSelect);
            Xgem.OnCJDeleted += new OnCJDeleted(OnCJDeleted);
            Xgem.OnCJRspHOQJob += new OnCJRspHOQJob(OnCJRspHOQJob);
            Xgem.OnCJGetHOQJob += new OnCJGetHOQJob(OnCJGetHOQJob);
            Xgem.OnCJHOQJobChanged += new OnCJHOQJobChanged(OnCJHOQJobChanged);
        }
        #endregion
        #region Class ControlJob

        private void S16F27(long nMsgId, string sCJobID, long nCommand, string sCPName, string sCPVal)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJReqCommand");

            m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgId : {0} / sCJobID : {1} / nCommand : {2} / sCPName : {3} / sCPVal : {4}", nMsgId, sCJobID, nCommand, sCPName, sCPVal);

            if (nCommand == 8)
            {
                Xgem.CJRspCommand(nMsgId, sCJobID, nCommand, 0, 0, string.Empty);
                return;
            }

            List<PlcAddr> listWriteDatas = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_CONTROL_JOB_CMD_STRUCT];
            PlcAddr CommandBit = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROLJOB_CMD_COMMAND];

            ShareMem.SetAscii(listWriteDatas[0], sCJobID);
            ShareMem.SetByte(listWriteDatas[1], (byte)nCommand);
            ShareMem.SetByte(listWriteDatas[2], GetPortIDByCjID(sCJobID));
            ShareMem.SetBit(CommandBit, true);


            Xgem.CJRspCommand(nMsgId, sCJobID, nCommand, 1, 0, string.Empty);

            CheckCJ(string.Empty, sCJobID, true);
            //Xgem.CJReqSelect(sCJobID);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S14F9(long nMsgID, string sCJobID, long nCarrierCount, string[] psCarrierID, long nPJobCount, string[] psPJobID, long nProcessOrderMgmt, long nStartMethod)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJReqVerify");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgID : {0} / sCJobID : {1} / nCarrierCount : {2} / nPJobCount : {3} / nProcessOrderMgmt : {4} / nStartMethod : {5}", nMsgID, sCJobID, nCarrierCount, nPJobCount, nProcessOrderMgmt, nStartMethod);
            for (int i = 0; i < nCarrierCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psCarrierID : {1}", i, psCarrierID[i]);
            for (int i = 0; i < nPJobCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psPJobID : {1}", i, psPJobID[i]);



            // Configuration Tool 에서 Xgem 탭 -> Equipment-> Selected Module -> Equipment Verifies Job = TRUE 상태 일때
            long nResult = 0;
            long nErrorCount = 0;
            long[] nArrayErrorCode = new long[1];
            string[] sArrayErrorText = new string[1];
            /*
                0 - ok
                1 - unknown object
                2 - unknown class
                3 - unknown object instance
                4 - unknown attribute type
                5 - read-only attribute
                6 - unknown class
                7 - invalid attribute value
                8 - syntax error
                9 - verification error
                10 - validation error
                11 - object ID in use
                12 - improper parameters
                13 - missing parameters
                14 - unsupported option requested
                15 - busy
                16 - unavailable
                17 - command not valid in current state
                18 - no material altered
                19 - partially processed
                20 - all material processed
                21 - recipe specification error
                22 - failure when processing
                23 - failure when not processing
                24 - lack of material
                25 - job aborted
                26 - job stopped
                27 - job cancelled
                28 - cannot change selected recipe
                29 - unknown event
                30 - duplicate report ID
                31 - unknown data report
                32 - data report not linked
                33 - unknown trace report
                34 - duplicate trace ID
                35 - too many reports
                36 - invalid sample period
                37 - group size too large
                38 - recovery action invalid
                39 - busy with previous recovery
                40 - no active recovery
                41 - recovery failed
                42 - recovery aborted
                43 - invalid table element
                44 - unknown table element
                45 - cannot delete predefined
                46 - invalid token
                47 - invalid parameter
                48 - Load port does not exist
                49 - Load port is busy
                50 - missing carrier
                32768 - deferred for later initiation
                32769 - can not be performed now
                32770 - failure from errors
                32771 - invalid command
                32772 - client alarm
                32773 - duplicate clientID
                32774 - invalid client type
                32776 - unknown clientID
                32777 - Unsuccessful completion
                32779 - detected obstacle
                32780 - material not sent
                32781 - material not received
                32782 - material lost
                32783 - hardware error
                32784 - transfer cancelled
             */
            //S14F10
            m_CimMessageProcessor.CimConfigurator.ControlJobID = sCJobID;


            long nReturn = Xgem.CJRspVerify(nMsgID, sCJobID, nResult, nErrorCount, nArrayErrorCode, sArrayErrorText);

            if (nResult == 0)
                HsmsLogControl.AddMessage("CJ Verify OK");
            else
                HsmsLogControl.AddMessage("CJ Verify Fail");

            ControlJobData cjd = new ControlJobData();
            cjd.GetControlJobData(sCJobID, Xgem);
            foreach (string sCarrierId in cjd.ProcessJobData.CarrierInfos.Keys)
            {
                if (m_CimMessageProcessor.SelectCarrierID.Contains(sCarrierId))
                {
                    CheckCJ(sCarrierId, sCJobID, nStartMethod == 0 ? false : true);
                    m_CimMessageProcessor.SelectCarrierID.Remove(sCarrierId);
                }
            }

            string sProcessJobID = m_CimMessageProcessor.CimConfigurator.ProcessJobID;

            string[] sVidValues = new string[1];
            long[] sVids = new long[1];

            Xgem.CJReqSelect(cjd.ControlJobID);

            //추후 지우기 Recipe SelectsVids = new long[1];
            sVids[0] = 4016;
            sVidValues[0] = m_CimMessageProcessor.CimConfigurator.ProcessRecipe;
            m_CimMessageProcessor.CimConfigurator.SetVids(sVids, sVidValues);

            //nReturn = Xgem.GEMSetEvent(10);

            if (nReturn == 0)
                HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : {0} successfully ({1})", 10, nReturn);
            else
                HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", 10, nReturn);
            //m_CimMessageProcessor.RecipeControl.UpdateNotificationBoard(CimConfigurator.ProcessRecipe);

            HsmsLogControl.AddMessage(string.Format("Select RecipeName : {0}", m_CimMessageProcessor.CimConfigurator.ProcessRecipe));

            //Xgem.PJSetState(sProcessJobID, 1);

            if (nStartMethod == 0)
                Xgem.PJSetState(sProcessJobID, 2);


            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJHOQJobChanged(string sCJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJHOQJobChanged");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0}", sCJobID);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJGetHOQJob(string sCJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJGetHOQJob");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0}", sCJobID);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJRspHOQJob(string sCJobID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJRspHOQJob");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / nResult : {1}", sCJobID, nResult);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJDeleted(string sCJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJDeleted");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0}", sCJobID);

            //CJ Delete
            //Delete ShareMemory Interface 추가 필요
            List<PlcAddr> CommandStruct = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_CONTROL_JOB_CMD_STRUCT];
            PlcAddr CommandBit = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROLJOB_CMD_COMMAND];

            ShareMem.SetAscii(CommandStruct[0], sCJobID);
            ShareMem.SetByte(CommandStruct[1], 9);
            ShareMem.SetByte(CommandStruct[2], 1/* GetPortIDByCjID(sCJobID)*/);
            ShareMem.SetBit(CommandBit, true);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJRspSelect(string sCJobID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJRspSelect");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / nResult : {1}", sCJobID, nResult);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJManualStartDisplay(string sCJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJManualStartDisplay");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / sCJobID : {1}", sCJobID, sCJobID);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJStateChanged(string sCJobID, long nState)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJStateChanged");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / nState : {1}", sCJobID, nState);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJCreated(string sCjJobID, long nStartMethod, long nCountPRJob, string[] psPRJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJCreated");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCjJobID : {0} / nStartMethod : {1} / nCountPRJob : {2}", sCjJobID, nStartMethod, nCountPRJob);
            for (int i = 0; i < nCountPRJob; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psPRJobID : {1}", i, psPRJobID[i]);


            if (m_CimMessageProcessor.CurrentControlJobIDs.Count > 0)
                nStartMethod = 0;

            byte _cjPortID = GetPortIDByCjID(sCjJobID);
            if (!m_CimMessageProcessor.CurrentControlJobIDs.ContainsKey(sCjJobID))
                m_CimMessageProcessor.CurrentControlJobIDs.Add(sCjJobID, _cjPortID);
            else
            {
                m_CimMessageProcessor.CurrentControlJobIDs.Remove(sCjJobID);
                m_CimMessageProcessor.CurrentControlJobIDs.Add(sCjJobID, _cjPortID);
                HsmsLogControl.AddMessage("Control Recipe 중복!!!!! -> 덮어쓰기 진행");
            }

            //JHKIM 220524
            //ShareMemory Interface 추가
            List<PlcAddr> listCj = ShareTrx.hashShareStruct[ShareNames.DEF_HOST_CONTROL_JOB_DOWNLOAD_STRUCT];
            PlcAddr WriteCommand = ShareTrx.hashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_JOB_DOWNLOAD_COMMAND];

            ShareMem.SetAscii(listCj[0], sCjJobID);
            ShareMem.SetAscii(listCj[1], psPRJobID[0]);
            ShareMem.SetByte(listCj[3], _cjPortID);
            ShareMem.SetByte(listCj[2], Convert.ToByte(nStartMethod));

            ShareMem.SetBit(WriteCommand, true);

            m_CimMessageProcessor.CimConfigurator.ControlJobID = sCjJobID;

            Xgem.CJReqGetAllJobID();
            
        }
        private void OnCJRspCreate(string sCJobID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJRspCreate");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / nResult : {1}", sCJobID, nResult);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJRspGetAllJobID(long nCountCJob, string[] psCJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJRspGetAllJobID");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nCountCJob : {0}", nCountCJob);
            for (int i = 0; i < nCountCJob; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psCJobID : {1}", i, psCJobID[i]);

            m_hashCjToPjs.Clear();
            m_iCjJobCount = nCountCJob;
            if (nCountCJob > 0)
                foreach (string sCjJobid in psCJobID)
                    Xgem.CJReqGetJob(sCjJobid);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnCJRspGetJob(string sCJobID, long nStartMethod, long nCountPRJob, string[] psPRJobID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJRspGetJob");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / nStartMethod : {1} / nCountPRJob : {2} / nResult : {3}", sCJobID, nStartMethod, nCountPRJob, nResult);
            for (int i = 0; i < nCountPRJob; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psPRJobID : {1}", i, psPRJobID[i]);

            string sPRJobID = string.Empty;
            if (nCountPRJob > 0)
            {
                List<string> listPjs = new List<string>();
                for (int i = 0; i < nCountPRJob; i++)
                {
                    sPRJobID = psPRJobID[i];
                    listPjs.Add(sPRJobID);
                    HsmsLogControl.AddMessage(string.Format("PRJobID={0}", psPRJobID[i]));
                }

                if (!m_hashCjToPjs.ContainsKey(sCJobID))
                    m_hashCjToPjs.Add(sCJobID, listPjs);
            }

            if (m_iCjJobCount == m_hashCjToPjs.Count)
                m_CimMessageProcessor.JobControl.ControlJobChange(m_hashCjToPjs);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S16F28(string sCJobID, long nCommand, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnCJStateChanged");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sCJobID : {0} / nCommand : {1} / nResult : {2}", sCJobID, nCommand, nResult);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion

        #region Class Public Mehtods
        /// <summary>
        /// 
        /// </summary>
        public void CheckCJ(string sCarrierID, string sCjName = "", bool bStart = false)
        {
            long pnObjID = 0;
            long pnCount = 0;
            string sCJobID = string.Empty;
            long nReturn = Xgem.CJGetAllJobInfo(ref pnObjID, ref pnCount);

            List<ControlJobData> listCJDatas = new List<ControlJobData>();
            for (int i = 0; i < pnCount + 1; i++)
            {
                nReturn = Xgem.GetCtrlJobID(pnObjID, i, ref sCJobID);
                if (nReturn == 0)
                {
                    ControlJobData cjd = new ControlJobData();
                    cjd = cjd.GetControlJobData(sCJobID, Xgem);
                    listCJDatas.Add(cjd);
                }
            }

            foreach (ControlJobData data in listCJDatas)
            {
                if (data.ControlJobID == sCjName)
                {
                    if (bStart)
                    {
                        //Xgem.CJSetJobInfo(data.ControlJobID, 1, data.AutoStart,
                                //data.ProcessJogIds.Length, data.ProcessJogIds);

                        Xgem.CJSetJobInfo(data.ControlJobID, 3, data.AutoStart,
                                data.ProcessJogIds.Length, data.ProcessJogIds);
                        Xgem.PJSetState(data.ProcessJogIds[0], (long)PRJobState.Pj_SettingUp);
                        Xgem.PJSetState(data.ProcessJogIds[0], (long)PRJobState.Pj_WaintingForStart);
                        Xgem.PJSetState(data.ProcessJogIds[0], (long)PRJobState.Pj_Processing);

                        Xgem.GEMSetEvent(7005);
                        return;
                    }
                    else
                    {
                        //Xgem.CJSetJobInfo(data.ControlJobID, 1, data.AutoStart,
                        //        data.ProcessJogIds.Length, data.ProcessJogIds);
                        //
                        Xgem.CJSetJobInfo(data.ControlJobID, 2, data.AutoStart,
                                data.ProcessJogIds.Length, data.ProcessJogIds);
                        Xgem.PJSetState(data.ProcessJogIds[0], (long)PRJobState.Pj_SettingUp);
                        Xgem.PJSetState(data.ProcessJogIds[0], (long)PRJobState.Pj_WaintingForStart);
                    }
                }
                else if (data.ProcessJobData.CarrierInfos.ContainsKey(sCarrierID))
                {    
                    //Control Job Select
                    Xgem.CJReqSelect(data.ControlJobID);

                    if (data.AutoStart == 0)
                    {
                        //Control Job Waiting For Host
                        Xgem.CJSetJobInfo(data.ControlJobID, 2, data.AutoStart,
                            data.ProcessJogIds.Length, data.ProcessJogIds);
                    }
                }
            }
        }

        /// <summary>
        /// Delete / Complete 후에는 불가능!!!!!!
        /// </summary>
        public byte GetPortIDByCjID(string cjID)
        {
            try
            {
                ControlJobData _datas = new ControlJobData();
                _datas.GetControlJobData(cjID, Xgem);

                ////////////////////////

                return Convert.ToByte(m_CimMessageProcessor.ProcessJobProcesser.GetPortIDByPjID(_datas.ProcessJogIds[0].Replace("LP", string.Empty)));
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}
