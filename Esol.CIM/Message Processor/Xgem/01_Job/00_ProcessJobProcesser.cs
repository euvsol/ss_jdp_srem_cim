﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.ShareTrxControl;
using Esol.ShareMemory;
using System.Windows.Documents;
using static System.Windows.Forms.AxHost;
using System.Reflection;
using System.Security.Cryptography;
using System.Windows.Interop;
#endregion

namespace Esol.CIM
{
    public class ProcessJobProcesser
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, ProcessJobData> m_hashProcessJobIdToData;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_CimMessageProcessor.HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx ShareTrx
        {
            get
            {
                return m_CimMessageProcessor.ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory ShareMem
        {
            get
            {
                return m_CimMessageProcessor.ShareMem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, ProcessJobData> HashProcessJobIdToData
        {
            get
            {
                return m_hashProcessJobIdToData;
            }
            set
            {
                m_hashProcessJobIdToData = value;
            }
        }


        /// <summary>
        /// Delete / Complete 후에는 불가능!!!!!!
        /// </summary>
        public byte GetPortIDByPjID(string pjID)
        {
            try
            {
                long _objID = 0;
                long _processJobCount = 0;
                ProcessJobData _datas = new ProcessJobData();

                Xgem.PJGetAllJobInfo(ref _objID, ref _processJobCount);
                for (int _iPos = 0; _iPos < _processJobCount; _iPos++)
                {
                    string _tempProcessJobID = string.Empty;
                    long _tempCarrierCount = 0;
                    string[] _tempMaterialID = new string[25];
                    string[] _tempSlotInfo = new string[25];

                    Xgem.GetPRJobID(_objID, _iPos, ref _tempProcessJobID);

                    Xgem.GetPRJobCarrierCount(_objID, _iPos, ref _tempCarrierCount);
                    Xgem.GetPRJobCarrier(_objID, _iPos, _tempCarrierCount, ref _tempMaterialID, ref _tempSlotInfo);

                    ProcessJobData data = new ProcessJobData()
                    {
                        ProcessJobID = _tempProcessJobID,
                    };
                    for (int jPos = 0; jPos < _tempCarrierCount; jPos++)
                        data.CarrierInfos.Add(_tempMaterialID[jPos], _tempSlotInfo[jPos]);

                    if (pjID == data.ProcessJobID)
                    {
                        _datas = data;
                        break;
                    }
                }
                Xgem.GetPRJobClose(_objID);

                ////////////////////////

                _objID = 0;
                long _CmsCount = 0;
                string _tempID = string.Empty;
                string _tempPortID = string.Empty;
                Xgem.CMSGetAllCarrierInfo(ref _objID, ref _CmsCount);
                for (int _iPos = 0; _iPos < _CmsCount; _iPos++)
                {
                    Xgem.GetCarrierID(_objID, _iPos, ref _tempID);

                    foreach (string key in _datas.CarrierInfos.Keys)
                    {
                        if (key == _tempID)
                        {
                            Xgem.GetCarrierLocID(_objID, _iPos, ref _tempPortID);

                            Xgem.GetCarrierClose(_objID);
                            return Convert.ToByte(_tempPortID.Replace("LP", string.Empty));
                        }
                    }

                }

                return 0;
            }
            catch
            {
                return 0;
            }
        }

        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public ProcessJobProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            m_hashProcessJobIdToData = new Dictionary<string, ProcessJobData>();

            InitializePJ_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializePJ_Event()
        {
            Xgem.OnPJReqCommand += new OnPJReqCommand(S16F5);
            Xgem.OnPJRspCommand += new OnPJRspCommand(S16F6);
            Xgem.OnPJReqVerify += new OnPJReqVerify(S16F11_F15);
            Xgem.OnPJRspGetJob += new OnPJRspGetJob(OnPJRspGetJob);
            Xgem.OnPJRspGetAllJobID += new OnPJRspGetAllJobID(OnPJRspGetAllJobID);
            Xgem.OnPJRspCreate += new OnPJRspCreate(OnPJRspCreate);
            Xgem.OnPJCreated += new OnPJCreated(OnPJCreated);
            Xgem.OnPJStateChanged += new OnPJStateChanged(OnPJStateChanged);
            Xgem.OnPJManualStartDisplay += new OnPJManualStartDisplay(OnPJManualStartDisplay);
            Xgem.OnPJSettingUpStart += new OnPJSettingUpStart(OnPJSettingUpStart);
            Xgem.OnPJDeleted += new OnPJDeleted(OnPJDeleted);
            Xgem.OnPJReqSetRecipeVariable += new OnPJReqSetRecipeVariable(OnPJReqSetRecipeVariable);
            Xgem.OnPJReqSetStartMethod += new OnPJReqSetStartMethod(OnPJReqSetStartMethod);
            Xgem.OnPJReqSetMtrlOrder += new OnPJReqSetMtrlOrder(OnPJReqSetMtrlOrder);
        }
        #endregion

        #region Class ProcessJob Evnet
        private void S16F6(string sPJobID, long nCommand, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMTerminalMessage");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0} / nCommand : {1} / nResult : {2}", sPJobID, nCommand, nResult);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJRspGetJob(string sPJobID, long nMtrlFormat, long nAutoStart, long nMtrlOrder, long nMtrlCount, string[] psMtrlID, string[] psSlotInfo, long nRcpMethod, string sRcpID, long nRcpParCount, string[] psRcpParName, string[] psRcpParValue)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJRspGetJob");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0} / nMtrlFormat : {1} / nAutoStart : {2} / nMtrlOrder : {3} / nMtrlCount : {4} / nRcpMethod : {5} / sRcpID : {6} / nRcpParCount : {7}", 
                sPJobID, nMtrlFormat, nAutoStart, nMtrlOrder, nMtrlCount, nRcpMethod, sRcpID, nRcpParCount);

            for (int i = 0; i < nMtrlCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psMtrlID : {1} / psSlotInfo : {2}", i, psMtrlID[i], psSlotInfo[i]);
            for (int i = 0; i < nRcpParCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psRcpParName : {1} / psRcpParValue : {2}", i, psRcpParName[i], psRcpParValue[i]);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJReqSetMtrlOrder(long nMsgID, long nMtrlOrder)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqSetMtrlOrder");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgID : {0} / nMtrlOrder : {1}", nMsgID, nMtrlOrder);

            Xgem.PJRspSetMtrlOrder(nMsgID, nMtrlOrder);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJReqSetStartMethod(long nMsgID, long nPJobCount, string[] psPJobID, long nProcessStart)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqSetStartMethod");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgID : {0} / nPJobCount : {1} / nProcessStart : {2}", nMsgID, nPJobCount, nProcessStart);
            for(int i = 0;i< nPJobCount;i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psPJobID : {0}", i, psPJobID[i]);

            Xgem.PJRspSetStartMethod(nMsgID, nPJobCount, psPJobID, 1, 0, new long[0], new string[0]);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJReqSetRecipeVariable(long nMsgID, string sPJobID, long nRcpCount, string[] psRcpParName, string[] psRcpParValue)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqSetStartMethod");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgID : {0} / sPJobID : {1} / nRcpCount : {2}", nMsgID, sPJobID, nRcpCount);
            for (int i = 0; i < nRcpCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psRcpParName : {0}", i, psRcpParName[i], psRcpParValue[i]);

            Xgem.PJRspSetRcpVariable(nMsgID, sPJobID, 1, 0, new long[0], new string[0]);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJDeleted(string sPJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJDeleted");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0}", sPJobID);

            //Delete ShareMemory Interface 추가 필요

            List<PlcAddr> CommandStruct = m_CimMessageProcessor.HashShareStruct[ShareNames.DEF_HOST_PROCESS_JOB_CMD_STRUCT];
            PlcAddr CommandBit = m_CimMessageProcessor.HashShareKeyToShareData[ShareNames.DEF_HOST_PROCESSJOB_CMD_COMMAND];

            ShareMem.SetAscii(CommandStruct[0], sPJobID);
            ShareMem.SetByte(CommandStruct[1], 7);
            ShareMem.SetByte(CommandStruct[2], m_CimMessageProcessor.CurrentProcessJobIDs[sPJobID]);
            ShareMem.SetBit(CommandBit, true);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJSettingUpStart(string sPJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJSettingUpStart");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0}", sPJobID);

            Xgem.PJSettingUpStart(sPJobID);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJManualStartDisplay(string sPJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJManualStartDisplay");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0}", sPJobID);

            if (System.Windows.Forms.DialogResult.OK == System.Windows.Forms.MessageBox.Show(string.Format("Do you want process job ({0})s manual start?", sPJobID)))
            {
                //System.Windows.Forms.DlgDisplay dlg = new DlgDisplay();
                //dlg.SetDisplayType(sMsg);
                //dlg.ShowDialog();
                //
                //if (dlg.bReulst == true)
                //{
                //    Xgem.PJReqCommand(1, sPJobID);
                //    AddMessage(string.Format("Send Manual start ok, {0} sPJobID={1}, command={2}", "PJReqCommand", sPJobID, 1));
                //}
                //else
                //{
                //    Xgem.PJReqCommand(6, sPJobID);
                //    AddMessage(string.Format("Send Manual start cancel, {0} sPJobID={1}, command={2}", "PJReqCommand", sPJobID, 6));
                //}
            }

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S16F5(long nMsgID, string sPJobID, long nCommand)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqCommand");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgID : {0} / sPJobID : {1} / nCommand : {2} / PjCommandUse : {3}", nMsgID, sPJobID, nCommand, m_CimMessageProcessor.CimConfigurator.PjCommandUse.ToString());

            if (!m_CimMessageProcessor.CimConfigurator.PjCommandUse)
            {
                Xgem.PJRspCommand(nMsgID, nCommand, sPJobID, 0, 1, new long[1] { 16 }, new string[1] { "unavailable" });
            }
            else
            {
                List<object> list = new List<object>() { nMsgID, sPJobID, nCommand };

                m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S16F5"), list);
            }

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJStateChanged(string sPJobID, long nState)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJStateChanged");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0} / nState : {1}", sPJobID, nState);

            switch (nState)
            {
                case (long)PRJobState.Pj_SettingUp:
                    Xgem.PJSettingUpCompt(sPJobID);
                    break;
            }

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJCreated(string sPJobID, long nMtrlFormat, long nAutoStart, long nMtrlOrder, long nMtrlCount, string[] psMtrlID, string[] psSlotInfo, long nRcpMethod, string sRcpID, long nRcpParCount, string[] psRcpParName, string[] psRcpParValue)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJCreated");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0} / nMtrlFormat : {1} / nAutoStart : {2} / nMtrlOrder : {3} / nMtrlCount : {4} / nRcpMethod : {5} / sRcpID : {6} / nRcpParCount : {7}",
                sPJobID, nMtrlFormat, nAutoStart, nMtrlOrder, nMtrlCount, nRcpMethod, sRcpID, nRcpParCount);
            for (int i = 0; i < nMtrlCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psMtrlID : {1} / psSlotInfo : {2}", i, psMtrlID[i], psSlotInfo[i]);
            for (int i = 0; i < nRcpParCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] psRcpParName : {1} / psRcpParValue : {2}", i, psRcpParName[i], psRcpParValue[i]);

            byte _pjPortID = m_CimMessageProcessor.ProcessJobProcesser.GetPortIDByPjID(sPJobID);
            if (!m_CimMessageProcessor.CurrentProcessJobIDs.ContainsKey(sPJobID))
                m_CimMessageProcessor.CurrentProcessJobIDs.Add(sPJobID, _pjPortID);
            else
            {
                m_CimMessageProcessor.CurrentProcessJobIDs.Remove(sPJobID);
                m_CimMessageProcessor.CurrentProcessJobIDs.Add(sPJobID, _pjPortID);
                HsmsLogControl.AddMessage("Process Recipe 중복!!!!! -> 덮어쓰기 진행");
            }


            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S16F11_F15(long nMsgID, long nPJobCount, string[] psPJobID, long[] pnMtrlFormat, long[] pnAutoStart, long[] pnMtrlOrder, long[] pnMtrlCount,
            string[] psMtrlID, string[] psSlotInfo, long[] pnRcpMethod, string[] psRcpID, long[] pnRcpParCount, string[] psRcpParName, string[] psRcpParValue)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqVerify");

            List<object> list = new List<object>()
            {
                nMsgID, nPJobCount, psPJobID, pnMtrlFormat, pnAutoStart, pnMtrlOrder, pnMtrlCount,
                psMtrlID, psSlotInfo, pnRcpMethod, psRcpID, pnRcpParCount,psRcpParName,psRcpParValue
            };

            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S16F11_15"), list);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJRspCreate(string sPJobID, long nResult)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqVerify");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("sPJobID : {0} / nResult : {1}", sPJobID, nResult);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void OnPJRspGetAllJobID(long nPJobCount, string[] psPJobID)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnPJReqVerify");
            m_CimMessageProcessor.HsmsLogControl.AddMessage("nPJobCount : {0}", nPJobCount);

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }

        #endregion
    }
}
