﻿#region Class using
using GEM_XGem300Pro;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class ProcessJobData
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessJobId;
        /// <summary>
        /// 
        /// </summary>
        private long m_nProcessJobState;
        /// <summary>
        /// 
        /// </summary>
        private long m_nAutoStart;
        /// <summary>
        /// 
        /// </summary>
        private long m_nMtrlFormat;
        /// <summary>
        /// 
        /// </summary>
        private long m_nMtrlOrder;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_saMtrlID;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_saSlotInfo;
        /// <summary>
        /// 
        /// </summary>
        private long m_nRcpMethod;
        /// <summary>
        /// 
        /// </summary>
        private string m_sRecipeName;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessRecipeName;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_saRcpParName;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_saRcpParValue;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> m_hashCarreirInfos;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> m_hashRecipeParams;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public string ProcessJobID
        {
            get
            {
                return m_sProcessJobId;
            }
            set
            {
                m_sProcessJobId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long AutoStart
        {
            get
            {
                return m_nAutoStart;
            }
            set
            {
                m_nAutoStart = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long ProcessJobState
        {
            get
            {
                return m_nProcessJobState;
            }
            set
            {
                m_nProcessJobState = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long MtrlFormat
        {
            get
            {
                return m_nMtrlFormat;
            }
            set
            {
                m_nMtrlFormat = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long MtrlOrder
        {
            get
            {
                return m_nMtrlOrder;
            }
            set
            {
                m_nMtrlOrder = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] MtrlIds
        {
            get
            {
                return m_saMtrlID;
            }
            set
            {
                m_saMtrlID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] SlotInfos
        {
            get
            {
                return m_saSlotInfo;
            }
            set
            {
                m_saSlotInfo = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public long RcpMethod
        {
            get
            {
                return m_nRcpMethod;
            }
            set
            {
                m_nRcpMethod = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeName
        {
            get
            {
                return m_sRecipeName;
            }
            set
            {
                m_sRecipeName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessRecipeName
        {
            get
            {
                return m_sProcessRecipeName;
            }
            set
            {
                m_sProcessRecipeName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] RcpParNames
        {
            get
            {
                return m_saRcpParName;
            }
            set
            {
                m_saRcpParName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] RcpParValues
        {
            get
            {
                return m_saRcpParValue;
            }
            set
            {
                m_saRcpParValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> CarrierInfos
        {
            get
            {
                return m_hashCarreirInfos;
            }
            set
            {
                m_hashCarreirInfos = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> RecipeParams
        {
            get
            {
                return m_hashRecipeParams;
            }
            set
            {
                m_hashRecipeParams = value;
            }
        }
        #endregion

        #region Initialization
        /// <summary>
        /// 
        /// </summary>
        public ProcessJobData()
        {
            m_hashCarreirInfos = new Dictionary<string, string>();
            m_hashRecipeParams = new Dictionary<string, string>();
        }
        /// <summary>
        /// 
        /// </summary>
        public ProcessJobData GetProcessJobData(string sProcessJobId, XGem300ProNet Xgem)
        {
            m_sProcessJobId = sProcessJobId;
            long nReturn = 0;

            long nObjID = 0;
            long nPJobCount = 0;

            string sPRJobID = string.Empty;
            long nCarrierCount = 0;
            string[] saMaterialID = new string[25];
            string[] saSlotInfo = new string[25];

            long nRcpParamCount = 0;
            string[] saRecipeParamName = new string[25];
            string[] saRecipeParamValue = new string[25];

            nReturn = Xgem.PJGetAllJobInfo(ref nObjID, ref nPJobCount);
            if (nReturn == 0)
            {

                for (int i = 0; i < nPJobCount; i++)
                {
                    Xgem.GetPRJobID(nObjID, i, ref sPRJobID);

                    if (m_sProcessJobId == sPRJobID)
                    {
                        Xgem.GetPRJobState(nObjID, i, ref m_nProcessJobState);

                        Xgem.GetPRJobAutoStart(nObjID, i, ref m_nAutoStart);

                        Xgem.GetPRJobMtrlFormat(nObjID, i, ref m_nMtrlFormat);

                        Xgem.GetPRJobMtrlOrder(nObjID, i, ref m_nMtrlOrder);

                        Xgem.GetPRJobCarrierCount(nObjID, i, ref nCarrierCount);

                        Xgem.GetPRJobCarrier(nObjID, i, nCarrierCount, ref saMaterialID, ref saSlotInfo);

                        for (int k = 0; k < nCarrierCount; k++)
                            m_hashCarreirInfos.Add(saMaterialID[k], saSlotInfo[k]);

                        Xgem.GetPRJobRcpID(nObjID, i, ref m_sRecipeName);

                        Xgem.GetPRJobRcpParamCount(nObjID, i, ref nRcpParamCount);

                        Xgem.GetPRJobRcpParam(nObjID, i, nRcpParamCount, ref saRecipeParamName, ref saRecipeParamValue);

                        for (int k = 0; k < nRcpParamCount; k++)
                            m_hashRecipeParams.Add(saRecipeParamName[k], saRecipeParamValue[k]);
                    }
                }

                Xgem.GetPRJobClose(nObjID);
            }
            else
                return null;

            return this;
        }
        #endregion
    }
}
