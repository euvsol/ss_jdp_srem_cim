﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.LogControl;
using System.Text;
#endregion

namespace Esol.CIM
{
    public class RMSProcesser
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_CimMessageProcessor.LogManager;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public RMSProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            InitializeRMS_Event();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeRMS_Event()
        {
            Xgem.OnGEMReqPPList += new OnGEMReqPPList(S7F19);
            Xgem.OnGEMReqPPFmt += new OnGEMReqPPFmt(S7F25);
        }
        #endregion

        #region Class RMS 
        /// <summary>
        /// 
        /// </summary>
        private void S7F19(long nMsgId)
        {
            List<object> list = new List<object>() { nMsgId };

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqPPList");
            m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S7F19"), list);
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        private void S7F25(long nMsgId, string sPpid)
        {
            string sMdln = "";
            string sSoftRev = "";
            long nCount = 0;
            string[] saCCodes = new string[1];
            long[] naPCount = new long[1];
            string[] saPNames;
            string[] psParamValues;

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqPPFmt");

            if (m_CimMessageProcessor.RecipeManager.BasicRecipeNameToData.ContainsKey(sPpid))
            {
                BasicRecipeData mrd = m_CimMessageProcessor.RecipeManager.BasicRecipeNameToData[sPpid];
                int iParamCount = mrd.HashParamNameToValue.Count;
                sMdln = "SREM330";
                sSoftRev = mrd.RecipeVersion;
                nCount = 1;
                saCCodes[0] = "1";
                naPCount[0] = iParamCount;
                saPNames = new string[iParamCount];
                psParamValues = new string[iParamCount];

                int iCount = 0;
                foreach (string sKey in mrd.HashParamNameToValue.Keys)
                {
                    saPNames[iCount] = sKey;
                    psParamValues[iCount++] = mrd.HashParamNameToValue[sKey];
                }

                m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] GEMRspPPFmt2");
                m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgId : {0} / sPpid : {1} / sMdln : {2} / sSoftRev : {3} / nCount : {4} / saCCodes : {5} / naPCount : {6}", nMsgId, sPpid, sMdln, sSoftRev, nCount, saCCodes[0], naPCount[0]);
                for (int i = 0; i < nCount; i++)
                    m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] saPNames : {1} / psParamValues : {2}", i, saPNames[i], psParamValues[i]);

                this.Xgem.GEMRspPPFmt2(nMsgId, sPpid, sMdln, sSoftRev, nCount, saCCodes, naPCount, saPNames, psParamValues);
            }
            else if (m_CimMessageProcessor.RecipeManager.ProcessRecipeNameToData.ContainsKey(sPpid))
            {
                ProcessRecipeData prd = m_CimMessageProcessor.RecipeManager.ProcessRecipeNameToData[sPpid];
                int iParamCount = prd.ParameterNameToValue.Count;
                sMdln = "SREM330";
                sSoftRev = prd.RecipeVersion;
                nCount = 1;
                saCCodes[0] = "1";
                naPCount[0] = iParamCount;
                saPNames = new string[iParamCount];
                psParamValues = new string[iParamCount];

                int iCount = 0;
                foreach (string sKey in prd.ParameterNameToValue.Keys)
                {
                    saPNames[iCount] = sKey;
                    psParamValues[iCount++] = prd.ParameterNameToValue[sKey];
                }

                m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] GEMRspPPFmt2");
                m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgId : {0} / sPpid : {1} / sMdln : {2} / sSoftRev : {3} / nCount : {4} / saCCodes : {5} / naPCount : {6}", nMsgId, sPpid, sMdln, sSoftRev, nCount, saCCodes[0], naPCount[0]);
                for (int i = 0; i < nCount; i++)
                    m_CimMessageProcessor.HsmsLogControl.AddMessage("[{0}] saPNames : {1} / psParamValues : {2}", i, saPNames[i], psParamValues[i]);

                this.Xgem.GEMRspPPFmt2(nMsgId, sPpid, sMdln, sSoftRev, nCount, saCCodes, naPCount, saPNames, psParamValues);
            }
            else
            {
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] GEMRspPPFmt");
                m_CimMessageProcessor.HsmsLogControl.AddMessage("nMsgId : {0} / sPpid : {1}", nMsgId, sPpid);

                this.Xgem.GEMRspPPFmt(nMsgId, sPpid, string.Empty, string.Empty, 0, new string[0], new long[0], new string[0]);
            }
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
   
        #endregion
    }
}
