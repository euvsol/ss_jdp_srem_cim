﻿#region Class Using
using System;
using System.Collections.Generic;
using GEM_XGem300Pro;
using Esol.LogControl;
using System.Windows.Documents;
#endregion

namespace Esol.CIM
{
    public class EcidProcesser
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<long, string> m_hashEcidDatas;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_CimMessageProcessor.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_CimMessageProcessor.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<long, string> HashEcidDatas
        {
            get
            {
                return m_hashEcidDatas;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public EcidProcesser(CimMessageProcessor cimMessageProcessor)
        {
            m_CimMessageProcessor = cimMessageProcessor;

            m_hashEcidDatas = new Dictionary<long, string>();

            InitializeEcid_Event();

        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeEcid_Event()
        {
            //ECID
            Xgem.OnGEMReqChangeECV += new OnGEMReqChangeECV(S2F15);
            Xgem.OnGEMECVChanged += new OnGEMECVChanged(S6F11_EcidChange);
            Xgem.OnGEMRspAllECInfo += new OnGEMRspAllECInfo(OnGEMRspAllECInfo);
        }
        #endregion

        #region Class Ecid Event
        /// <summary>
        /// 
        /// </summary>
        private void S2F15(long nMsgId, long nCount, long[] pnEcids, string[] psVals)
        {
            try
            {
                List<object> list = new List<object>() { nMsgId, nCount, pnEcids, psVals };

                m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
                m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMReqChangeECV");
                m_CimMessageProcessor.HsmsHandler(m_CimMessageProcessor.GetAppropriateHandler("S2F15"), list);
                m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void S6F11_EcidChange(long nCount, long[] pnEcids, string[] psVals)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMECVChanged");

            m_CimMessageProcessor.HsmsLogControl.AddMessage("Count : {0}", nCount);
            for (int i = 0; i < nCount; i++)
                m_CimMessageProcessor.HsmsLogControl.AddMessage(string.Format("[{0}] Ecid : {0} / Value : {1}", i, pnEcids[i], psVals[i]));

            Xgem.GEMReqAllECInfo();

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnGEMRspAllECInfo(long lCount, long[] plVid, string[] psName, string[] psValue, string[] psDefault, string[] psMin, string[] psMax, string[] psUnit)
        {
            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
            m_CimMessageProcessor.HsmsLogControl.AddMessage("[XGEM ==> EQ] OnGEMRspAllECInfo");

            m_CimMessageProcessor.EcidControl.OnEcidChange(lCount, plVid, psName, psValue, psDefault, psMin, psMax, psUnit);

            m_CimMessageProcessor.HsmsLogControl.AddMessage("Count : {0}", lCount);
            for (int i = 0; i < lCount; i++)
            {
                m_CimMessageProcessor.HsmsLogControl.AddMessage(string.Format("[{0}] Vid:{0}, Name:{1}, Value:{2}, Default:{3}, Min:{4}, Max:{5}, Unit:{6}", i, plVid[i], psName[i], psValue[i], psDefault[i], psMin[i], psMax[i], psUnit[i]));

                if (!m_hashEcidDatas.ContainsKey(plVid[i]))
                    m_hashEcidDatas.Add(plVid[i], psValue[i]);
                else
                    m_hashEcidDatas[plVid[i]] = psValue[i];
            }

            m_CimMessageProcessor.HsmsLogControl.AddMessage(null);
        }
        #endregion
    }
}
