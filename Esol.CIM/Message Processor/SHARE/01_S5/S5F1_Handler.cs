﻿#region Class Using
using System;
using System.Collections.Generic;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    /// <summary>
    /// 
    /// </summary>
    public class S5F1_Handler : BaseHandler
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private const string DEF_TOTAL_KEYS = "Group";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_AlarmTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S5F1_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_AlarmTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (m_AlarmTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> ReadMessage = MainControl.HashShareStruct[m_AlarmTrx.ReadItem[0]];

                UInt16 uSetCode = ShareMem.GetByte(ReadMessage[0]);
                int iALCD = ShareMem.GetByte(ReadMessage[1]);
                UInt64 uALID = ShareMem.GetUInt64(ReadMessage[2]);

                if (!(uSetCode == 0 || uSetCode == 1))
                    return;

                string sALTX = string.Empty;

                if (CimConfigurator.AlarmManager.HashAlarmIdToData.ContainsKey(uALID))
                    sALTX = CimConfigurator.AlarmManager.HashAlarmIdToData[uALID].ALTX;

                ConvertAlarmData(uSetCode, iALCD, uALID, sALTX);

                //중알람만 상위 보고
                if (iALCD == 1 && HsmsState != ControlState.Offline)
                {
                    long nReturn = 1;

                    //Alarm Enalbe이 True면
                    if (CimConfigurator.AlarmManager.HashAlarmIdToData.ContainsKey(uALID) && CimConfigurator.AlarmManager.HashAlarmIdToData[uALID].USE)
                    {
                        nReturn = Xgem.GEMSetAlarm(Convert.ToInt32(uALID), Convert.ToInt32(uSetCode));

                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMSetAlarm => ID:{0}, State:{1} ({2})",
                        iALCD, uSetCode, nReturn));
                    }
                    else
                    {
                        nReturn = Xgem.GEMSetAlarm(1, Convert.ToInt32(uSetCode));

                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMSetAlarm => ID:{0}, State:{1} ({2})",
                        iALCD, uSetCode, nReturn));
                    }

                    if (nReturn == 0)
                    {
                        Esol.XmlControl.XmlDataWriter xmlWriter = new Esol.XmlControl.XmlDataWriter(CimConfigurator.ActiveAlarmPath)
                        {
                            OverWrite = true
                        };

                        
                        if (uSetCode == 1)
                        {
                            string[] sAttributeKeys = new string[7];
                            sAttributeKeys[0] = "id";
                            sAttributeKeys[1] = "set";
                            sAttributeKeys[2] = "code";
                            sAttributeKeys[3] = "text";
                            sAttributeKeys[4] = "time";
                            sAttributeKeys[5] = "tick";
                            sAttributeKeys[6] = string.Empty;

                            string[] sAttributeValues = new string[7];
                            sAttributeValues[0] = uALID.ToString();
                            sAttributeValues[1] = uSetCode.ToString();
                            sAttributeValues[2] = "1";
                            sAttributeValues[3] = sALTX;
                            sAttributeValues[4] = Common.GenerateCurrentTime();
                            sAttributeValues[5] = DateTime.Now.Ticks.ToString();
                            sAttributeValues[6] = string.Empty;


                            xmlWriter.XmlAttributeElementWrite(DEF_TOTAL_KEYS, "Message", sAttributeKeys, sAttributeValues);
                        }
                        else
                            xmlWriter.XmlElementDelete(DEF_TOTAL_KEYS, "id", uALID.ToString(), string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ConvertAlarmData(UInt16 uSetCode, int iALCD, UInt64 uALID, string sALTX)
        {
            try
            {
                long[] naALID = new long[1];
                long[] naALCD = new long[1];
                string[] naALTEXT = new string[1];

                naALID[0] = Convert.ToInt64(uALID);

                if (string.IsNullOrEmpty(sALTX.Trim()))
                {
                    Xgem.GEMGetAlarmInfo(1, naALID, ref naALCD, ref naALTEXT);
                    sALTX = naALTEXT[0];
                }

                AlarmData data = new AlarmData()
                {
                    ALCD = iALCD,
                    ALID = uALID,
                    ALTX = sALTX,
                    OccurTime = DateTime.Now.ToString("yyyyMMddHHmmss")
                };

                CimConfigurator.AlarmManager.AddMessage(uSetCode == 1 ? true : false, data);

                LogManager.WriteLog(LogType.ALARM.ToString(), string.Format("ALID : {0}, SetCode : {1}, ALCD : {2}, ALTX : {3}", uALID, uSetCode, iALCD, sALTX));
            }
            catch(Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
