﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    public class S6F11_ControlState_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_ControlStateTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_ControlState_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_ControlStateTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (m_ControlStateTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                PlcAddr listReadMessage = MainControl.HashShareKeyToShareData[m_ControlStateTrx.ReadItem[0]];
                byte iControlState = ShareMem.GetByte(listReadMessage);
                byte iAck = 1;

                switch (iControlState)
                {
                    case 1:
                        if (HsmsState == ControlState.Offline)
                            iAck = 2;
                        break;
                    case 2:
                        if (HsmsState == ControlState.Local)
                            iAck = 2;
                        break;
                    case 3:
                        if (HsmsState == ControlState.Remote)
                            iAck = 2;
                        break;
                }

                if (!MainControl.NotificationBoard.ConnectState)
                    iAck = 2;

                if (iAck != 1)
                    return;

                switch (iControlState)
                {
                    case 1:
                        MainControl.NotificationBoard.HsmsStateChange = ClassNames.DEF_OFFLINE_TEXT;
                        break;
                    case 2:
                        MainControl.NotificationBoard.HsmsStateChange = ClassNames.DEF_LOCAL_TEXT;
                        break;
                    case 3:
                        MainControl.NotificationBoard.HsmsStateChange = ClassNames.DEF_REMOTE_TEXT;
                        break;
                }

                long[] naVidName = new long[1] { 4 };
#if false       //20190614 jkseo, ICM->CIM (Offline:1, Local:2, Remote:3) CIM->TC (Offline:3, Local:4, Remote:5)
                string[] saVidValue = new string[1] { iControlState.ToString() };
#else
                string[] saVidValue = new string[1] { (iControlState + 2).ToString() };
#endif

                CimConfigurator.SetVids(naVidName, saVidValue);

                PlcAddr ControlStateAck = MainControl.HashShareKeyToShareData[ShareNames.DEF_CONTROL_STATE_ACK];
                ShareMem.SetByte(ControlStateAck, iAck);

                PlcAddr ControlStateReply = MainControl.HashShareKeyToShareData[ShareNames.DEF_CONTROL_STATE_RELAY];
                ShareMem.SetBit(ControlStateReply, true);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }

#endregion
    }
}
