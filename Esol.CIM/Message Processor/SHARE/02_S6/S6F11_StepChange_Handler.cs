﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_StepChange_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_StepChangeTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_StepChange_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_StepChangeTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (m_StepChangeTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                PlcAddr listReadMessage = MainControl.HashShareKeyToShareData[m_StepChangeTrx.ReadItem[0]];
                byte iStepState = ShareMem.GetByte(listReadMessage);
                long nReturn = 0;
                long nMode = 0;
                switch (iStepState)
                {
                    case 1: //OM Align Start
                        nReturn = Xgem.GEMSetEvent(30);
                        nMode = 30;
                        break;
                    case 2: //OM Align End
                        nReturn = Xgem.GEMSetEvent(31);
                        nMode = 31;
                        break;
                    case 3: //EUV Align Start
                        nReturn = Xgem.GEMSetEvent(32);
                        nMode = 32;
                        break;
                    case 4:  //EUV Align End
                        nReturn = Xgem.GEMSetEvent(33);
                        nMode = 33;
                        break;
                    case 5: //Inspection Start
                        nReturn = Xgem.GEMSetEvent(34);
                        nMode = 34;
                        break;
                    case 6: //Inspection End
                        nReturn = Xgem.GEMSetEvent(35);
                        nMode = 35;
                        break;
                    case 7: //Scan Start
                        nReturn = Xgem.GEMSetEvent(36);
                        nMode = 36;
                        break;
                    case 8: //Scan End
                        nReturn = Xgem.GEMSetEvent(37);
                        nMode = 37;
                        break;
                    case 9: //TxTy Start
                        nReturn = Xgem.GEMSetEvent(60);
                        nMode = 60;
                        break;
                    case 10: //TxTy End
                        nReturn = Xgem.GEMSetEvent(61);
                        nMode = 61;
                        break;
                    case 11: //Z Interlock Start
                        nReturn = Xgem.GEMSetEvent(62);
                        nMode = 62;
                        break;
                    case 12: //Z Interlock End
                        nReturn = Xgem.GEMSetEvent(63);
                        nMode = 63;
                        break;
                    case 13: //Cailbration Start
                        nReturn = Xgem.GEMSetEvent(64);
                        nMode = 64;
                        break;
                    case 14: //Cailbration End
                        nReturn = Xgem.GEMSetEvent(65);
                        nMode = 65;
                        break;
                    case 15: //Contamination Start
                        nReturn = Xgem.GEMSetEvent(66);
                        nMode = 66;
                        break;
                    case 16: //Contamination End
                        nReturn = Xgem.GEMSetEvent(67);
                        nMode = 67;
                        break;
                }

                if (nReturn == 0)
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : {0} successfully ({1})", nMode, nReturn);
                else
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", nMode, nReturn);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
