﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_Transfer_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_TransferTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_Transfer_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_TransferTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_TransferTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_TransferTrx.ReadItem[0]];

                byte iForm = ShareMem.GetByte(listReadMessage[0]);
                byte iTo = ShareMem.GetByte(listReadMessage[1]);
                string sControlJogID = ShareMem.GetAscii(listReadMessage[2]);
                string sProcessJobID = ShareMem.GetAscii(listReadMessage[3]);
                string[] sVidValues = new string[2] { iForm.ToString(), iTo.ToString() };
                long[] sVids = new long[2] { 19, 20 };

                CimConfigurator.SetVids(sVids, sVidValues);

                long nReturn =Xgem.GEMSetEvent(99);

                if (nReturn == 0)
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : {0} successfully ({1})", 99, nReturn);
                else
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", 99, nReturn);

                if (CimConfigurator.TestMode)
                {
                    sControlJogID = m_MainControl.CimConfigurator.ControlJobID == null ? sControlJogID : m_MainControl.CimConfigurator.ControlJobID;
                    sProcessJobID = m_MainControl.CimConfigurator.ProcessJobID == null ? sProcessJobID : m_MainControl.CimConfigurator.ProcessJobID;
                }

                if (HsmsState == ControlState.Remote)
                {
                    if (iForm == 2 && iTo == 1)
                    {
                        //Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_ProcessComplete);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
