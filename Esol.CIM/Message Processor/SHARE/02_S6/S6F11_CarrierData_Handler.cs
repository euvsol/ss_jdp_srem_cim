﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_CarrierData_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_CarrierTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_CarrierData_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_CarrierTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_CarrierTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_CarrierTrx.ReadItem[0]];
                string sLocID = string.Format("LP{0}", ShareMem.GetByte(listReadMessage[0]));
                string sCarrierID = ShareMem.GetAscii(listReadMessage[1]).Trim();
                byte iSlotData = ShareMem.GetByte(listReadMessage[2]);
                long nReturn = 0;
                string sSlotMapValue = "0";
                long[] naVidName;
                string[] saVidValue;

                naVidName = new long[3] { 5006, 5202, 33 };//, 5023 };
                saVidValue = new string[3] { sCarrierID, sCarrierID, "1" };//, sSlotMapValue };

                CimConfigurator.SetVids(naVidName, saVidValue);

                switch (iSlotData)
                {
                    case 5:
                        //Carrier ID Read
                        CheckCarrierId(sCarrierID);

                        naVidName = new long[2] { 5007, 5202 };
                        saVidValue = new string[2] { "1", sCarrierID };

                        CimConfigurator.SetVids(naVidName, saVidValue);

                        nReturn = Xgem.GEMSetEvent(5046);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_5047", nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_5047", nReturn);

                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Carrier ID Read. LocID={0}, CarrierID={1}, Result={2}", sLocID, sCarrierID, 0));

                        nReturn = Xgem.CMSSetCarrierID(sLocID, sCarrierID, 0);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Carrier ID successfully. LocID={0}, CarrierID={1}, Result={2}", sLocID, sCarrierID, 0));
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Carrier ID ({0})", nReturn));

                        nReturn = Xgem.CMSSetLPInfo(sLocID, 1, 1, 0, 1, sCarrierID);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Job Reserved ({0})", nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to Job Reserved ({0})", nReturn);

                        break;
                    case 6:
                        //Carrier ID Not Read
                        naVidName = new long[1] { 5202 };
                        saVidValue = new string[1] { "" };

                        CimConfigurator.SetVids(naVidName, saVidValue);

                        nReturn = Xgem.CMSSetCarrierIDStatus(sCarrierID, 0);

                        if (nReturn == 0)
                        {
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Send Set Carrier ID Status successfully");

                            naVidName = new long[1] { 5007 };
                            saVidValue = new string[1] { "0" };

                            CimConfigurator.SetVids(naVidName, saVidValue);
                        }
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Carrier ID Status ({0})", nReturn));
                        break;
                    case 7:
                        //Slot Map Not Read
                        nReturn = Xgem.CMSSetSlotMapStatus(sCarrierID, 0);

                        if (nReturn == 0)
                        {
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Send Set Slot Map Status successfully");

                            naVidName = new long[1] { 5024 };
                            saVidValue = new string[1] { "0" };

                            CimConfigurator.SetVids(naVidName, saVidValue);
                        }
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Slot Map Status ({0})", nReturn));
                        break;
                    default:
                        //Slot Map Read
                        string sSlotMap = string.Format("{0}000000000000000000000000", ConvertSlotMap(iSlotData));
                        sSlotMapValue = ConvertSlotMap(iSlotData).ToString();


                        naVidName = new long[1] { 5024 };
                        saVidValue = new string[1] { "1" };

                        CimConfigurator.SetVids(naVidName, saVidValue);

                        long nResult = 0;
                        long nObjId = 0;
                        Xgem.MakeObject(ref nObjId);
                        Xgem.SetListItem(nObjId, 25);
                        for (int i = 0; i < 25; i++)
                        {
                            if (i == 0)
                                nReturn = Xgem.SetStringItem(nObjId, sSlotMapValue);
                            else
                                nReturn = Xgem.SetStringItem(nObjId, "0");
                        }
                        nReturn = Xgem.GEMSetVariables(nObjId, 5023);


                        nReturn = Xgem.GEMSetEvent(5047);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_5047", nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_5047", nReturn);

                        nReturn = Xgem.CMSSetSlotMap(sLocID, sSlotMap, sCarrierID, nResult);

                        if (nReturn == 0)
                        {
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set SlotMap successfully. LocID={0},SlotMap={1},CarrierID={2},Result={3}", sLocID, sSlotMap, sCarrierID, nResult));
                            CimMessageProcessor.SelectCarrierID.Add(sCarrierID);

                            nReturn = Xgem.GEMSetEvent(22);

                            if (nReturn == 0)
                                HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_22", nReturn);
                            else
                                HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_22", nReturn);
                        }
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set SlotMap ({0})", nReturn));
                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Util Methods
        /// <summary>
        /// 
        /// </summary>
        private void CheckCarrierId(string carrierID)
        {
            long nReturn = 0;

            long nObjID = 0;
            long nCount = 0;
            long nCarrIndex = 0;
            string sCarrierID = "";

            long[] nVids = new long[1];
            nVids[0] = 5006;
            string[] nVidValue = new string[1];
            nVidValue[0] = carrierID;

            CimConfigurator.SetVids(nVids, nVidValue);

            nReturn = Xgem.CMSGetAllCarrierInfo(ref nObjID, ref nCount);

            if (nReturn == 0)
            {
                if (nCarrIndex < nCount)
                {
                    for (int _iPos = 0; _iPos < nCount; _iPos++)
                    {
                        nReturn = Xgem.GetCarrierID(nObjID, _iPos, ref sCarrierID);
                        if (nReturn == 0 && sCarrierID == carrierID)
                        {
                            HsmsLogControl.AddMessage(string.Format("Get Carrier ID Successfully (Index :{0} CarrierID:{1})({2}) ", nCarrIndex, sCarrierID, nReturn));
           
                            Xgem.CMSDelCarrierInfo(sCarrierID);
                        }
                    }

                    Xgem.GetCarrierClose(nObjID);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private byte ConvertSlotMap(byte iSlotMap)
        {
            switch (iSlotMap)
            {
                case 0:
                    return 1;
                case 1:
                    return 3;
                case 2:
                    return 4;
                case 3:
                    return 5;
            }

            return iSlotMap;
        }
        #endregion
    }
}
