﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_EqState_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_EqStateTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_EqState_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_EqStateTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_EqStateTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                PlcAddr listReadMessage = MainControl.HashShareKeyToShareData[m_EqStateTrx.ReadItem[0]];
                byte iEqState = ShareMem.GetByte(listReadMessage);
                
                switch(iEqState)
                {
                    case 1:

                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
