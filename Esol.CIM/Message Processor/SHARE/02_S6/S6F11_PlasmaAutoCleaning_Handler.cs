﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_PlasmaAutoCleaning_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_PlasmaAutoCleaningTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_PlasmaAutoCleaning_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_PlasmaAutoCleaningTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_PlasmaAutoCleaningTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_PlasmaAutoCleaningTrx.ReadItem[0]];
                byte iMode = ShareMem.GetByte(listReadMessage[0]);
                byte iWatt = ShareMem.GetByte(listReadMessage[1]);
                UInt16 uDuration = ShareMem.GetUShort(listReadMessage[2]);

                long[] sVids;
                string[] sValues;
                long nReturn = 0;
                if (iMode == 0)
                {
                    sVids = new long[2] { 50, 51 };
                    sValues = new string[2] { iWatt.ToString(), uDuration.ToString() };

                    CimConfigurator.SetVids(sVids, sValues);

                    nReturn = Xgem.GEMSetEvent(40);

                    if (nReturn == 0)
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_40", nReturn);
                    else
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_40", nReturn);
                }
                else
                {
                    double dPressAvg = ShareMem.GetDouble(listReadMessage[3]);
                    double dPressMin = ShareMem.GetDouble(listReadMessage[4]);
                    double dPressMax = ShareMem.GetDouble(listReadMessage[5]);
                    byte iResult = ShareMem.GetByte(listReadMessage[6]);


                    sVids = new long[6] { 50, 51, 52, 53, 54, 55 };
                    sValues = new string[6] { iWatt.ToString(), uDuration.ToString(), dPressAvg.ToString(), dPressMin.ToString(), dPressMax.ToString(), iResult.ToString() };

                    CimConfigurator.SetVids(sVids, sValues);

                    nReturn = Xgem.GEMSetEvent(41);

                    if (nReturn == 0)
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_41", nReturn);
                    else
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_41", nReturn);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
