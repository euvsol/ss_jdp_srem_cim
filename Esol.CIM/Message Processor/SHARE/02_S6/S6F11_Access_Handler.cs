﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_Access_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_AccessTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_Access_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_AccessTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_AccessTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_AccessTrx.ReadItem[0]];
                byte iPortNo = ShareMem.GetByte(listReadMessage[0]);
                string sLocID = string.Format("LP{0}", iPortNo);
                long nMode = Convert.ToInt64(ShareMem.GetByte(listReadMessage[1]));
                long nReturn = 0;

                //if (iPortNo != 1)
                //    iPortNo = 0;

                long[] svid = new long[] { 5201 };
                string[] sValue = new string[] { nMode.ToString() };

                CimConfigurator.SetVids(svid, sValue);

                nReturn = Xgem.CMSReqChangeAccess(nMode, sLocID);

                if (nReturn == 0)
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] {0} Mode Change : {1}", sLocID, nMode == 1 ? "Auto" : "Manual"));
                else
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Request Change Access ({0})", nReturn));

            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
