﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_EcidChange_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_EcidTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_EcidChange_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_EcidTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_EcidTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                XmlDataReader xmlDataReader = new XmlDataReader(CimConfigurator.EqEcidPath, false);
                string[] sArrayAttributeKes = new string[] { "name", "value" };
                List<string[]> listEcidDatas = xmlDataReader.XmlReadList("Group/Ecids", sArrayAttributeKes);

                List<long> listEcidKey = new List<long>();
                List<string> listEcidValue = new List<string>();
                

                foreach(string[] sArrayEcid in listEcidDatas)
                {
                    long.TryParse(sArrayEcid[0], out long nEcidKey);
                    nEcidKey += 2000;
                    if (!CimMessageProcessor.EcidProcesser.HashEcidDatas.ContainsKey(nEcidKey))
                        continue;

                    string sTempOldEcidValue = CimMessageProcessor.EcidProcesser.HashEcidDatas[nEcidKey];

                    if(sTempOldEcidValue.Length > sArrayEcid[1].Length)
                        sTempOldEcidValue = sTempOldEcidValue.Substring(0, sArrayEcid[1].Length);
                    

                    if (sTempOldEcidValue != sArrayEcid[1])
                    {
                        listEcidKey.Add(nEcidKey);
                        listEcidValue.Add(sArrayEcid[1]);
                    }
                }

                if (listEcidKey.Count == 0)
                {
                    HsmsLogControl.AddMessage("EQ => Xgem Ecid Change List Count Zero");
                    return;
                }

                long nReturn = Xgem.GEMSetECVChanged(listEcidKey.Count, listEcidKey.ToArray(), listEcidValue.ToArray());
                
                if (nReturn == 0)
                    HsmsLogControl.AddMessage("EQ => Xgem Ecid Change Ok");
                else
                    HsmsLogControl.AddMessage("EQ => Xgem Ecid Change Fail");

                Xgem.GEMReqAllECInfo();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
