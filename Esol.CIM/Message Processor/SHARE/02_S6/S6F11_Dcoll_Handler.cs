﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_Dcoll_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_DcollTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_Dcoll_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_DcollTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_DcollTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_DcollTrx.ReadItem[0]];
                string sLocID = string.Format("LP{0}", ShareMem.GetShort(listReadMessage[0]));
                long nMode = Convert.ToInt64(ShareMem.GetShort(listReadMessage[1]));

                if (nMode != 1)
                    nMode = 0;

                long nReturn = Xgem.CMSReqChangeAccess(nMode, sLocID);

                if (nReturn == 0)
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Request Change Access successfully.Mode={0},LocID={1}", nMode, sLocID));

                else
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Request Change Access ({0})", nReturn));

            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
