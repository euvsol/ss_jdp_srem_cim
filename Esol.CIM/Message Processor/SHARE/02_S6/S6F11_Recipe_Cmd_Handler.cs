﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
using System.IO;
#endregion

namespace Esol.CIM
{
    public class S6F11_Recipe_Cmd_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_RecipeCmdTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_Recipe_Cmd_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_RecipeCmdTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_RecipeCmdTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_RecipeCmdTrx.ReadItem[0]];
                long uMode = Convert.ToInt64(ShareMem.GetByte(listReadMessage[0]));
                string sRecipeName = ShareMem.GetAscii(listReadMessage[1]);
                long[] sVids;
                string[] sVidValues;

                long nCount = 0;
                long nReturn = 0;

                switch (uMode)
                {
                    case 1: //Basic Recipe Create
                    case 2: //Basic Recipe Modify
                    case 4: //Process Recipe Create
                    case 5: //Process Reicpe Modify
                        //Formatted Recipe
                        // Description: Formatted Process Program이 생성, 수정, 삭제되었을 경우 사용함.
                        // 주의 사항 : psParamNames는 2차원 배열 형태 정보를 1차원 배열로 나열한 것입니다.
                        // nCount만큼 할당되어야 하는 인자 : psCCode, pnParamCount
                        // pnParamCount 배열 값을 모두 더한 만큼 할당되어야 하는 인자 : psParamNames

                        if (uMode == 1 && CimConfigurator.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName))
                            uMode = 2;
                        if (uMode == 2 && !CimConfigurator.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName))
                            uMode = 1;

                        if (uMode == 4 && CimConfigurator.RecipeManager.ProcessRecipeNameToData.ContainsKey(sRecipeName))
                            uMode = 5;

                        sVids = new long[3];
                        sVidValues = new string[3];
                        sVids[0] = 29;
                        sVids[1] = 30;
                        sVids[2] = 4014;
                        sVidValues[0] = sRecipeName;
                        sVidValues[1] = uMode.ToString();
                        sVidValues[2] = sRecipeName;


                        if (uMode == 4 || uMode == 5)
                        {
                            string sEqProcessPath = CimConfigurator.EqProcessRecipePath + "\\" + sRecipeName + ".xml";
                            string sHostProcessPath = CimConfigurator.HostProcessRecipePath + "\\" + sRecipeName + ".xml";
                            FileInfo fiProcessEQ = new FileInfo(sEqProcessPath);
                            FileInfo fiProcessHost = new FileInfo(sHostProcessPath);

                            switch (uMode)
                            {
                                case 4:
                                    uMode = fiProcessHost.Exists ? 2 : 1;
                                    break;
                                case 5:
                                    uMode = !fiProcessHost.Exists ? 1 : 2;
                                    break;
                            }

                            fiProcessEQ.CopyTo(sHostProcessPath, true);

                            //Recipe Manager Process Recipe Data 넣기 및 업데이트

                            ProcessRecipeData prd = CimConfigurator.RecipeManager.ReadProcessRecipe(fiProcessHost);

                            if (prd == null)
                            {
                                HsmsLogControl.AddMessage(string.Format("ProcessRecipeName : {0} {1} Fail", sRecipeName, uMode == 1 ? "Create" : "Modify"));
                                return;
                            }
                        }
                        else
                        {
                            string sEqBasicRecipe = CimConfigurator.EqRecipePath + "\\" + sRecipeName + ".xml";
                            string sHostBasicRecipe = CimConfigurator.HostBasicRecipePath + "\\" + sRecipeName + ".xml";
                            FileInfo fiBasicRecipe = new FileInfo(sEqBasicRecipe);

                            BasicRecipeData mrd = CimConfigurator.RecipeManager.ReadRecipeParamName(fiBasicRecipe, sRecipeName);

                            fiBasicRecipe.CopyTo(sHostBasicRecipe, true);

                            if (mrd == null)
                            {
                                HsmsLogControl.AddMessage(string.Format("RecipeName : {0} {1} Fail", sRecipeName, uMode == 1 ? "Create" : "Modify"));
                                return;
                            }
                        }
                        m_MainControl.CimConfigurator.SetVids(sVids, sVidValues);

                        nReturn = Xgem.GEMSetPPFmtChanged(uMode, sRecipeName, string.Empty, string.Empty, nCount, new string[1], new long[1], new string[1]);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("RecipeName : {0} {1} Success", sRecipeName, uMode == 1 ? "Create" : "Modify"));
                        else
                            HsmsLogControl.AddMessage(string.Format("RecipeName : {0} {1} Fail", sRecipeName, uMode == 1 ? "Create" : "Modify"));
                        break;
                    case 3: //Recipe Delete
                    case 6: //Process Recipe Delete
                        if (uMode == 3 && !CimConfigurator.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName))
                            return;
                        if (uMode == 6 && !CimConfigurator.RecipeManager.ProcessRecipeNameToData.ContainsKey(sRecipeName))
                            return;

                        //File 삭제 3: Basic Recipe 6:Process Recipe
                        //FileInfo fi;
                        if (uMode == 3 && CimConfigurator.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName))
                        {
                            //fi = new FileInfo(CimConfigurator.EqRecipePath + "\\" + sRecipeName + ".xml");
                            //
                            //DirectoryInfo di = new DirectoryInfo(CimConfigurator.EqRecipePath + "\\_BackUp");
                            //if (!di.Exists)
                            //    di.Create();

                            //fi.CopyTo(CimConfigurator.ProcessRecipeBackUpPath + sRecipeName + "_" + Common.GenerateFileTime(DateTime.Now) + ".xml", true);

                            //FileInfo BasicHostRecipe = new FileInfo(CimConfigurator.HostBasicRecipePath + "\\" + sRecipeName + ".xml");
                            //BasicHostRecipe.Delete();

                            CimConfigurator.RecipeManager.BasicRecipeNameToData.Remove(sRecipeName);
                        }
                        else if (uMode == 6 && CimConfigurator.RecipeManager.ProcessRecipeNameToData.ContainsKey(sRecipeName))
                        {
                            //fi = new FileInfo(CimConfigurator.HostProcessRecipePath + "\\" + sRecipeName + ".xml");

                            //if (!fi.Exists)
                            //    return;

                            //DirectoryInfo di = new DirectoryInfo(CimConfigurator.HostProcessRecipePath + "\\_BackUp");
                            //if (!di.Exists)
                            //    di.Create();
                            //
                            //fi.CopyTo(CimConfigurator.ProcessRecipeBackUpPath + "\\" + sRecipeName + "_" + Common.GenerateFileTime(DateTime.Now) + ".xml", true);
                            //FileInfo ProcessHostRecipe = new FileInfo(CimConfigurator.HostProcessRecipePath + "\\" + sRecipeName + ".xml");
                            //ProcessHostRecipe.Delete();
                            CimConfigurator.RecipeManager.ProcessRecipeNameToData.Remove(sRecipeName);

                        }
                        else
                            return;

                        sVids = new long[3];
                        sVidValues = new string[3];
                        sVids[0] = 29;
                        sVids[1] = 30;
                        sVids[2] = 4014;
                        sVidValues[0] = sRecipeName;
                        sVidValues[1] = "3";
                        sVidValues[2] = sRecipeName;
                        m_MainControl.CimConfigurator.SetVids(sVids, sVidValues);

                        nReturn = Xgem.GEMSetPPFmtChanged(3, sRecipeName, string.Empty, string.Empty, nCount, new string[1], new long[1], new string[1]);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Success", sRecipeName));
                        else
                            HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Fail", sRecipeName));
                        break;
                    case 7: //Current Process Recipe
                        if (!CimConfigurator.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName) && !CimConfigurator.RecipeManager.ProcessRecipeNameToData.ContainsKey(sRecipeName))
                            return;

                        sVids = new long[1];
                        sVidValues = new string[1];
                        sVids[0] = 4016;
                        sVidValues[0] = sRecipeName;
                        m_MainControl.CimConfigurator.SetVids(sVids, sVidValues);

                        Xgem.GEMSetEvent(10);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : {0} successfully ({1})", 10, nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", 10, nReturn);
                        CimConfigurator.CurrentRecipe = sRecipeName;
                        m_MainControl.RecipeControl.UpdateNotificationBoard(sRecipeName);

                        HsmsLogControl.AddMessage(string.Format("Select RecipeName : {0}", sRecipeName));
                        break;
                }

                m_MainControl.RecipeControl.MainRecipeListChenage();
                m_MainControl.RecipeControl.ProcessRecipeListChange();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
