﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_EuvPowerMonitoring_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_EuvPowerMonitoringTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_EuvPowerMonitoring_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_EuvPowerMonitoringTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_EuvPowerMonitoringTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_EuvPowerMonitoringTrx.ReadItem[0]];

                byte iMode = ShareMem.GetByte(listReadMessage[0]);
                long nReturn;
                if (iMode == 1)
                {
                    float fIrPower = ShareMem.GetFloat(listReadMessage[1]);
                    float fCenterWaveLength = ShareMem.GetFloat(listReadMessage[2]);
                    float fEuvPower = ShareMem.GetFloat(listReadMessage[3]);
                    float fCenterRoiX = ShareMem.GetFloat(listReadMessage[4]);
                    float fCenterRoiY = ShareMem.GetFloat(listReadMessage[5]);
                    float fDivergence = ShareMem.GetFloat(listReadMessage[6]);
                    UInt16 uChillerFilterTime = ShareMem.GetUShort(listReadMessage[7]);
                    byte bResult = ShareMem.GetByte(listReadMessage[8]);

                    long[] sVids = new long[8] { 60, 61, 62, 63, 64, 65, 66, 67 };
                    string[] sValues = new string[8] { fIrPower.ToString(), fCenterWaveLength.ToString(), fEuvPower.ToString(), fCenterRoiX.ToString(), fCenterRoiY.ToString(), fDivergence.ToString(), uChillerFilterTime.ToString(), bResult.ToString() };

                    CimConfigurator.SetVids(sVids, sValues);


                    nReturn = Xgem.GEMSetEvent(43);

                    if (nReturn == 0)
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_43", nReturn);
                    else
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_43", nReturn);
                }
                else
                {
                    nReturn = Xgem.GEMSetEvent(42);
                    if (nReturn == 0)
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_42", nReturn);
                    else
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_42", nReturn);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
