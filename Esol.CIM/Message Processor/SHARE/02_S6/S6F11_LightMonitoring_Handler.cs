﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S6F11_LightMonitoring_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_LightMonitoringTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_LightMonitoring_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_LightMonitoringTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_LightMonitoringTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_LightMonitoringTrx.ReadItem[0]];

                byte iMode = ShareMem.GetByte(listReadMessage[0]);
                long nReturn = 0;

                if (iMode == 1)
                {
                    double PhD1 = ShareMem.GetDouble(listReadMessage[1]);
                    double PhD1_std = ShareMem.GetDouble(listReadMessage[2]);
                    double PhD2 = ShareMem.GetDouble(listReadMessage[3]);
                    double PhD2_std = ShareMem.GetDouble(listReadMessage[4]);
                    double NbD1 = ShareMem.GetDouble(listReadMessage[5]);
                    double NbD1_std = ShareMem.GetDouble(listReadMessage[6]);
                    double NbD2 = ShareMem.GetDouble(listReadMessage[7]);
                    double NbD2_std = ShareMem.GetDouble(listReadMessage[8]);
                    byte bResult = ShareMem.GetByte(listReadMessage[9]);

                    long[] sVids = new long[9] { 70, 71, 72, 73, 74, 75, 76, 77, 78 };
                    string[] sValues = new string[9]
                    {
                        PhD1.ToString(), PhD1_std.ToString(), PhD2.ToString(), PhD2_std.ToString(),
                        NbD1.ToString(), NbD1_std.ToString(), NbD2.ToString(), NbD2_std.ToString(),
                        bResult.ToString()
                    };
                    CimConfigurator.SetVids(sVids, sValues);

                    Xgem.GEMSetEvent(45);

                    if (nReturn == 0)
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_43", nReturn);
                    else
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_43", nReturn);
                }
                else
                {
                    Xgem.GEMSetEvent(44);

                    if (nReturn == 0)
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", "CEID_43", nReturn);
                    else
                        HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", "CEID_43", nReturn);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
