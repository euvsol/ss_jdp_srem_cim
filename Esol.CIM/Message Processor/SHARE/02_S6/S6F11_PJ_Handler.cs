﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    public class S6F11_PJ_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_ProcStatemTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_PJ_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_ProcStatemTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_ProcStatemTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                PlcAddr ReadMessage = MainControl.HashShareKeyToShareData[m_ProcStatemTrx.ReadItem[0]];
                int iProcState = Convert.ToInt32(m_MainControl.ShareMem.GetShort(ReadMessage));
                ProcessingState eProcState = (ProcessingState)iProcState;
                ProcessingState eOldProcState = m_MainControl.NotificationBoard.ProcState;
                long nCeid = 0;

                if (eProcState == eOldProcState)
                    return;

                if ((eOldProcState == ProcessingState.SystemPowerUp) && (eProcState == ProcessingState.Empty))
                    nCeid = 101;
                else if ((eOldProcState == ProcessingState.Empty) && (eProcState == ProcessingState.Idle))
                    nCeid = 102;
                else if ((eOldProcState == ProcessingState.Idle) && (eProcState == ProcessingState.Standby))
                    nCeid = 103;
                else if ((eOldProcState == ProcessingState.Standby) && (eProcState == ProcessingState.Processing))
                    nCeid = 104;
                else if ((eOldProcState == ProcessingState.Standby) && (eProcState == ProcessingState.Idle))
                    nCeid = 105;
                else if ((eOldProcState == ProcessingState.Processing) && (eProcState == ProcessingState.HoldProcessing))
                    nCeid = 106;
                else if ((eOldProcState == ProcessingState.Processing) && (eProcState == ProcessingState.Idle))
                    nCeid = 107;
                else if ((eOldProcState == ProcessingState.Processing) && (eProcState == ProcessingState.Standby))
                    nCeid = 108;
                else if ((eOldProcState == ProcessingState.HoldProcessing) && (eProcState == ProcessingState.Processing))
                    nCeid = 109;
                else if ((eOldProcState == ProcessingState.HoldProcessing) && (eProcState == ProcessingState.Standby))
                    nCeid = 110;
                else if ((eOldProcState == ProcessingState.HoldProcessing) && (eProcState == ProcessingState.Idle))
                    nCeid = 111;
                else
                {
                    m_MainControl.NotificationBoard.ProcState = eProcState;
                    return;
                }

                m_MainControl.NotificationBoard.ProcState = eProcState;

                long nReturn = Xgem.GEMSetEvent(nCeid);

                if (nReturn == 0)
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] GEMSetEvent successfully ({0})", nReturn);
                else
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to GEMSetEvent ({0})", nReturn);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
