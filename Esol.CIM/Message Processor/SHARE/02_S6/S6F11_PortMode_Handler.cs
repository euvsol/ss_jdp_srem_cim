﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
using System.Threading;
#endregion

namespace Esol.CIM
{
    public class S6F11_PortMode_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_PortModeTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S6F11_PortMode_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_PortModeTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (m_PortModeTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methodsaaa
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadMessage = MainControl.HashShareStruct[m_PortModeTrx.ReadItem[0]];

                string sLocID = string.Format("LP{0}", ShareMem.GetByte(listReadMessage[0]));
                long nMode_1 = Convert.ToInt64(ShareMem.GetByte(listReadMessage[1]));
                long nMode_2 = Convert.ToInt64(ShareMem.GetByte(listReadMessage[2]));
                long nMode = sLocID == "LP1" ? nMode_1 : nMode_2;
                long nReturn = 0;
                long nState = 0;
                string sMode = string.Empty;
                long[] nVids = new long[1];
                string[] nVidValue = new string[1] { "1" };

                if (nMode != 5 && nMode != 99) //Out Of Service가 아닌경우
                {
                    //CMSReqChangeServiceStatus(string sLocID, long nState)
                    //Control State가 Offline 혹은 Online Local 시에 EQ에서 Service Status를 변경할 때 사용
                    //nState    0 : Out Of Service
                    //          1 : TransferBlocked (In Service)
                    //          2 : ReadyToLoad (In Service)
                    //          3 : ReadyToUnload (In Service)

                    nReturn = Xgem.CMSReqChangeServiceStatus(sLocID, 1);

                    if (nReturn == 0)
                    {
                        //PortTransferModeSetVid(nState.ToString());
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Request Change Service Status successfully.{0}{1}", sLocID, nState));
                    }
                    else
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Request Change Service Status ({0})", nReturn));
                }
                switch (nMode)
                {
                    case 1: //Arrived
                    case 2: //Removed
                        if (nMode == 1)
                            nState = 1;
                        else
                        {
                            nState = 0;

                            nVids = new long[1] { 5202 };
                            nVidValue = new string[1] { "" };

                            CimConfigurator.SetVids(nVids, nVidValue);
                        }

                        //Meterial Evnet
                        nReturn = Xgem.CMSSetCarrierOnOff(sLocID, nState);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Carrier OnOff successfully LocID={0}, State={1}", sLocID, nState));
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Carrier OnOff ({0})", nReturn));

                        //LoadPort State Event
                        nReturn = Xgem.CMSSetPresenceSensor(sLocID, nState);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Presence Sensor successfully. Loc:{0}, State:{1}", sLocID, nState));
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Presence Sensor ({0})", nReturn));


                        nVids = new long[1] { 5006 };
                        nVidValue = new string[1] { string.Empty };


                        CimConfigurator.SetVids(nVids, nVidValue);

                        if (nMode == 2)
                        {
                            //long nObjID = 0;
                            //long nCount = 0;
                            //long nCarrIndex = 0;
                            //string sCarrierID = "";

                            //nReturn = Xgem.CMSGetAllCarrierInfo(ref nObjID, ref nCount);

                            //if (nReturn == 0)
                            //{
                            //    if (nCarrIndex < nCount)
                            //    {
                            //        nReturn = Xgem.GetCarrierID(nObjID, nCarrIndex, ref sCarrierID);

                            //        if (nReturn == 0)
                            //        {
                            //            HsmsLogControl.AddMessage(string.Format("Get Carrier ID Successfully (Index :{0} CarrierID:{1})({2}) ", nCarrIndex, sCarrierID, nReturn));
                            //            Xgem.CMSDelCarrierInfo(sCarrierID);
                            //        }

                            //        Xgem.GetCarrierClose(nObjID);
                            //    }
                            //}

                            JobClear(sLocID, true);
                        }

                        //Associated Event
                        //if (nMode == 2)
                        //    return;
                        //
                        //long nLoadPortState = nState == 1 ? 1 : 2;
                        //long nAssociated = nState == 1 ? 1 : 0;
                        //nReturn = Xgem.CMSSetLPInfo(sLocID, nLoadPortState, 1, 1, nAssociated, string.Empty);
                        //
                        //if (nReturn == 0)
                        //    HsmsLogControl.AddMessage("[EQ ==> XGEM] Job Reserved ({0})", nReturn);
                        //else
                        //    HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to Job Reserved ({0})", nReturn);
                        return;
                    case 3:    //Read To Load
                        PortTransferModeSetVid("2");
                        nReturn = Xgem.CMSSetReadyToLoad(sLocID);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Ready To Load successfully. ({0})", sLocID));
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Ready To Load ({0})", nReturn));
                        return;
                    case 4:    //Read To Unload
                        PortTransferModeSetVid("3");
                        nReturn = Xgem.CMSSetReadyToUnload(sLocID);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Ready To Unload successfully {0}", sLocID));
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Ready To Unload ({0})", nReturn));
                        return;
                    case 5: //Out Of Service
                    case 6: //Transfer Blocked
                        PortTransferModeSetVid(nState.ToString());
                        nState = nMode == 5 ? 0 : 1;

                        nReturn = Xgem.CMSReqChangeServiceStatus(sLocID, nState);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Request Change Service Status successfully.{0}{1}", sLocID, nState));
                        else
                            HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Request Change Service Status ({0})", nReturn));
                        return;
                    case 7:     //OHT Loading PIO Start
                    case 8:     //OHT Loading PIO End 
                    case 9:     //OHT Unloading PIO Start
                    case 10:    //OHT Unloading PIO End 
                        nMode += 10;

                        switch (nMode)
                        {
                            case 17:

                                sMode = "OHT Loading PIO Start";

                                nReturn = Xgem.CMSReqChangeAccess(1, sLocID);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] {0} Mode Change : {1}", sLocID, nMode == 1 ? "Auto" : "Manual"));
                                else
                                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Request Change Access ({0})", nReturn));

                                break;
                            case 18:
                                sMode = "OHT Loading PIO End";
                                break;
                            case 19:
                                sMode = "OHT Unloading PIO Start";
                                break;
                            case 20:
                                sMode = "OHT Unloading PIO End";
                                /*
                                nReturn = Xgem.GEMSetEvent(5045);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage("[EQ ==> XGEM] LoadPort_ReservedToNotReserved ({0})", nReturn);
                                else
                                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to LoadPort_ReservedToNotReserved ({0})", nReturn);
                                */
                                nReturn = Xgem.GEMSetEvent(nMode);
                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", sMode, nReturn);
                                else
                                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", sMode, nReturn);

                                Thread.Sleep(300);

                                long nLoadPortState = nState == 1 ? 1 : 2;
                                long nAssociated = nState == 1 ? 1 : 0;
                                nReturn = Xgem.CMSSetLPInfo(sLocID, 1, 1, 0, 0, string.Empty);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Transfer Blocked ({0})", nReturn);
                                else
                                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Transfer Blocked Fail ({0})", nReturn);

                                // Transfer Block -> Ready To Load -> Transfer Block  현상 방지
                                //Thread.Sleep(300);

                                //nReturn = Xgem.CMSSetLPInfo(sLocID, 2, 1, 0, 0, string.Empty);

                                //if (nReturn == 0)
                                //    HsmsLogControl.AddMessage("[EQ ==> XGEM] Transfer Blocked ({0})", nReturn);
                                //else
                                //    HsmsLogControl.AddMessage("[EQ ==> XGEM] Transfer Blocked Fail ({0})", nReturn);
                                return;
                        }

                        nReturn = Xgem.GEMSetEvent(nMode);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", sMode, nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", sMode, nReturn);
                        return;
                    case 11: //POD OPEN START
                        nReturn = Xgem.GEMSetEvent(21);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID_{0} successfully ({1})", sMode, nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", sMode, nReturn);
                        return;
                    case 12: // POD OPEN END
                        return;
                    case 13: // POD CLOSE START
                        nReturn = Xgem.GEMSetEvent(23);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : 23 successfully ({0})", nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", sMode, nReturn);

                        nReturn = Xgem.CMSSetLPInfo(sLocID, 1, 1, 0, 0, string.Empty);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Job Reserved ({0})", nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to Job Reserved ({0})", nReturn);
                        return;
                    case 14: // POD CLOSE END
                        nReturn = Xgem.GEMSetEvent(24);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : 24 successfully ({0})", nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", sMode, nReturn);
                        return;
                    case 99: // JOB CLEAR
                        HsmsLogControl.AddMessage(string.Format("[EQ] Recv Job Clear From ICM LocID={0}, State={1}", sLocID, nState));
                        JobClear(sLocID, true);
                        break;
                    default:
                        nReturn = Xgem.GEMSetEvent(nMode);
                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] CEID : {0} successfully ({1})", nMode, nReturn);
                        else
                            HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to CEID_{0} ({1})", nMode, nReturn);
                        return;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void PortTransferModeSetVid(string sValue)
        {
            long[] naVidName = new long[2] { 5020, 5209 };
            string[] saVidValue = new string[2] { sValue, sValue };

            CimConfigurator.SetVids(naVidName, saVidValue);
        }

        private void JobClear(string locID, bool isCarrierClear)
        {
            try
            {

                long pjObjID = 0, pjCount = 0;
                Xgem.PJGetAllJobInfo(ref pjObjID, ref pjCount);
                long cjObjID = 0, cjCount = 0;
                Xgem.CJGetAllJobInfo(ref cjObjID, ref cjCount);

                Xgem.GetCtrlJobClose(cjObjID);
                Xgem.GetPRJobClose(pjObjID);

                if (pjCount != 0 || cjCount != 0)
                {
                    HsmsLogControl.AddMessage(string.Format("Abnormal Scenario : PJ Count : {0} / CJ Count : {1}", pjCount, cjCount));
                    HsmsLogControl.AddMessage(string.Format("Abnormal Scenario : Job Clear Start", pjCount, cjCount));

                    long nReturn;
                    long nState = 0;
                    nState = 0;
                    string pjID = string.Empty;
                    if (cjCount != 0)
                    {
                        long nObjID = 0;
                        long nCJobCount = 0;
                        string sCJobID = string.Empty;
                        string[] m_sArrayPJJobIds = new string[1];
                        if (Xgem.CJGetAllJobInfo(ref nObjID, ref nCJobCount) == 0)
                        {
                            for (int i = 0; i < nCJobCount; i++)
                            {
                                Xgem.GetCtrlJobID(nObjID, i, ref sCJobID);
                                Xgem.GetCtrlJobPRJobIDs(nObjID, i, 1, ref m_sArrayPJJobIds);

                                if (locID == string.Format("LP{0}", CimMessageProcessor.CurrentProcessJobIDs[m_sArrayPJJobIds[0]]))
                                {
                                    pjID = m_sArrayPJJobIds[0];
                                    Xgem.CJDelJobInfo(sCJobID);

                                    if (CimMessageProcessor.CurrentControlJobIDs.ContainsKey(sCJobID))
                                        CimMessageProcessor.CurrentControlJobIDs.Remove(sCJobID);

                                    CimMessageProcessor.JobControl.ControlJobChange();

                                    break;
                                }
                            }
                            Xgem.GetCtrlJobClose(nObjID);
                        }
                    }
                    if (pjCount != 0)
                    {
                        if (pjID != string.Empty)
                        {
                            if (locID == string.Format("LP{0}", CimMessageProcessor.CurrentProcessJobIDs[pjID]))
                            {
                                Xgem.PJDelJobInfo(pjID);

                                if (CimMessageProcessor.CurrentProcessJobIDs.ContainsKey(pjID))
                                    CimMessageProcessor.CurrentProcessJobIDs.Remove(pjID);


                                CimMessageProcessor.JobControl.ProcessJobChange();
                            }
                        }
                    }


                    nReturn = Xgem.CMSSetCarrierOnOff(locID, nState);
                    if (nReturn == 0)
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Carrier OnOff successfully LocID={0}, State={1}", locID, nState));
                    else
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Carrier OnOff ({0})", nReturn));

                    nReturn = Xgem.CMSSetPresenceSensor(locID, nState);
                    if (nReturn == 0)
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Send Set Presence Sensor successfully. Loc:{0}, State:{1}", locID, nState));
                    else
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Fail to Set Presence Sensor ({0})", nReturn));


                    //Xgem.CMSDelAllCarrierInfo();


                    Xgem.GEMSetEvent(5040);

                    HsmsLogControl.AddMessage(string.Format("Abnormal Scenario : Job Clear Ok", pjCount, cjCount));
                }
                else
                    HsmsLogControl.AddMessage(string.Format("Normal Scenario : Already Not Job"));
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
