﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class ControlJobState_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_ControlJobStateTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public ControlJobState_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_ControlJobStateTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_ControlJobStateTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadDatas = MainControl.HashShareStruct[m_ControlJobStateTrx.ReadItem[0]];
                string sControlJobID = ShareMem.GetAscii(listReadDatas[0]);
                byte iControlJobState = ShareMem.GetByte(listReadDatas[1]);
                long nReturn = 0;
                byte iAck = 0;
                string sCJobID = string.Empty;

                string[] saPRJobID = new string[1];
                long nAutoStart = 0;


                ControlJobData cjd = new ControlJobData();
                cjd.GetControlJobData(sControlJobID, Xgem);

                iAck = CheckInterlock(iControlJobState, cjd.ControlJobState);

                if (iAck == 1)
                {
                    if (iControlJobState == 1)
                    {
                        nReturn = Xgem.CJSetJobInfo(sControlJobID, 3, nAutoStart, 1, cjd.ProcessJogIds);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("CJ State Command OK");
                        else
                        {
                            HsmsLogControl.AddMessage("CJ State Command Fail");
                            iAck = 2;
                        }
                    }

                    else
                    {
                        nReturn = Xgem.CJReqCommand(sControlJobID, iControlJobState, string.Empty, string.Empty);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage("CJ State Command OK");
                        else
                        {
                            HsmsLogControl.AddMessage("CJ State Command Fail");
                            iAck = 2;
                        }
                    }
                }

                PlcAddr AckPJ = MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_JOB_ACK];
                PlcAddr StateReply = MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_JOB_REPLY];

                ShareMem.SetByte(AckPJ, iAck);
                ShareMem.SetBit(StateReply, true);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private byte CheckInterlock(byte iControlJobState, long nState)
        {
            /* nState
             *0: Queued
             *1: Selected
             *2: WaitingForStart
             *3: Executing
             *4: Paused
             *5: Completed
             *6: Canceled
             *7: Deselected
             *8: Stopped
             *9: Aborted
             *10: Deleted
             *11: HeadOfQueue
             *12: Created
             *13: Resume
            */
            /* Command
             * 1 : Start
             * 2 : Pause
             * 3 : Resume
             * 4 : Cancel
             * 5 : Deselect
             * 6 : STOP
             * 7 : Abort
             * 8 : HOQ
             */

            byte iAck = 0;

            switch (iControlJobState)
            {
                case 1:
                    switch (nState)
                    {
                        case 1:
                        case 2:
                            iAck = 1;
                            break;
                        default:
                            iAck = 2;
                            break;
                    }
                    break;
                case 2:
                    if (nState == 3)
                        iAck = 1;
                    else
                        iAck = 2;
                    break;
                case 3:
                    if (nState == 4)
                        iAck = 1;
                    else
                        iAck = 2;
                    break;
                case 4:
                    if (nState == 0)
                        iAck = 1;
                    else
                        iAck = 2;
                    break;
                case 5:
                    if (nState == 1)
                        iAck = 1;
                    else
                        iAck = 2;
                    break;
                case 6:
                case 7:
                    switch (nState)
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            iAck = 1;
                            break;
                        default:
                            iAck = 2;
                            break;
                    }
                    break;
            }

            return iAck;
        }
        #endregion
    }
}
