﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    public class Delete_Handler : BaseHandler
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_PROCESS_JOB_ACK = "L1_W_PROCESS_JOB_ACK";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_CONTROL_JOB_ACK = "L1_W_CONTROL_JOB_ACK";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_CARRIER_ACK = "L1_W_CARRIER_JOB_ACK";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_DeleteTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public Delete_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_DeleteTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_DeleteTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                PlcAddr ReadMessage = MainControl.HashShareKeyToShareData[m_DeleteTrx.ReadItem[0]];
                string sName = ShareMem.GetAscii(ReadMessage);
                long nReturn = 0;
                PlcAddr WriteMessage;

                switch (m_DeleteTrx.sTrxId)
                {
                    case ShareNames.DEF_PROCESS_JOB_DELETE_TRX:
                        nReturn = Xgem.PJDelJobInfo(sName);
                        WriteMessage = MainControl.HashShareKeyToShareData[DEF_PROCESS_JOB_ACK];

                        ShareMem.SetByte(WriteMessage, Convert.ToByte(nReturn == 0 ? 0 : 1));
                        break;
                    case ShareNames.DEF_CONTROL_JOB_DELETE_TRX:
                        nReturn = Xgem.CJDelJobInfo(sName);
                        WriteMessage = MainControl.HashShareKeyToShareData[DEF_CONTROL_JOB_ACK];

                        ShareMem.SetByte(WriteMessage, Convert.ToByte(nReturn == 0 ? 0 : 1));
                        break;
                    case ShareNames.DEF_CARRIER_DELETE_TRX:
                        nReturn = Xgem.CMSDelCarrierInfo(sName);
                        WriteMessage = MainControl.HashShareKeyToShareData[DEF_CARRIER_ACK];

                        ShareMem.SetByte(WriteMessage, Convert.ToByte(nReturn == 0 ? 0 : 1));
                        break;
                }
                foreach (string sKey in m_DeleteTrx.WriteItem.Keys)
                    ShareMem.SetBit(MainControl.HashShareKeyToShareData[sKey], m_DeleteTrx.WriteItem[sKey]);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
