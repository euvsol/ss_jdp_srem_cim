﻿#region Class Using
using System;
using System.Runtime.InteropServices;
#endregion

namespace Esol.CIM
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SYSTEMTIME
    {
        public UInt16 wYear;
        public UInt16 wMonth;
        public UInt16 wDayOfWeek;
        public UInt16 wDay;
        public UInt16 wHour;
        public UInt16 wMinute;
        public UInt16 wSecond;
        public UInt16 wMilliseconds;
    }
    public class DateTimeHandler
    {
        #region Class Constants
        #endregion

        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        [DllImport("kernel32.dll", SetLastError = true)]
        public extern static bool SetSystemTime(ref SYSTEMTIME lpSystemTime);
        #endregion

        #region Class Properties
        #endregion

        #region Class Initialization

        public bool TimeCheck(string sDateTime)
        {
            bool bTimeCheck;
            SYSTEMTIME sysTime = new SYSTEMTIME()
            {
                wYear = UInt16.Parse(sDateTime.Substring(0, 4)),
                wMonth = UInt16.Parse(sDateTime.Substring(4, 2)),
                wDay = UInt16.Parse(sDateTime.Substring(6, 2)),
                wHour = UInt16.Parse(sDateTime.Substring(8, 2)),
                wMinute = UInt16.Parse(sDateTime.Substring(10, 2)),
                wSecond = UInt16.Parse(sDateTime.Substring(12, 2)),
                wMilliseconds = UInt16.Parse(sDateTime.Substring(14, 2))
            };

            bTimeCheck = TimeInterlockCheck(sysTime);

            if (bTimeCheck)
            {
                ConculationDateTimeSet(ref sysTime);

                SetSystemTime(ref sysTime);
            }

            return bTimeCheck;
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private bool TimeInterlockCheck(SYSTEMTIME time)
        {
            if (time.wYear > 2098 || time.wYear < 2000 || time.wMonth > 12 || time.wMonth < 1 || time.wHour > 23 ||
                                time.wHour < 0 || time.wMinute > 59 || time.wMinute < 0 || time.wSecond > 59 || time.wSecond < 0)
                return false;

            UInt16 uLimitDay = GetDay(time.wYear, time.wMonth);

            if (time.wDay > uLimitDay || time.wDay < 1)
                return false;

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        private UInt16 GetDay(UInt16 year, UInt16 month)
        {
            if (month == 2)
            {
                if (year % 4 == 0 && (year % 100 == 0 || year % 400 != 0))
                    return 29;
                else return 28;
            }

            return (month == 7) ? (UInt16)31 : (UInt16)(30 + (month % 7) % 2);
        }
        /// <summary>
        /// 
        /// </summary>
        private void ConculationDateTimeSet(ref SYSTEMTIME sysTime)
        {
            if (sysTime.wHour > 8)
                sysTime.wHour -= 9;
            else
            {
                if (sysTime.wDay != 1)
                    sysTime.wDay -= 1;
                else
                {
                    if (sysTime.wMonth != 1)
                        sysTime.wMonth -= 1;
                    else
                    {
                        sysTime.wMonth = 12;
                        sysTime.wYear -= 1;
                    }
                    sysTime.wDay = GetDay(sysTime.wYear, sysTime.wMonth);
                }
                sysTime.wHour += 15;
            }
        }
        #endregion
    }
}
