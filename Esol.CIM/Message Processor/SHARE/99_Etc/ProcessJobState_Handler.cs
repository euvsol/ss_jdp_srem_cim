﻿#region Class Using
using System;
using Esol.ShareTrxControl;
using Esol.LogControl;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class ProcessJobState_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_ProcessJobStateTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public ProcessJobState_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_ProcessJobStateTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline || m_ProcessJobStateTrx == null)
                    return;

                SendInvitation();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SendInvitation()
        {
            try
            {
                List<PlcAddr> listReadDatas = MainControl.HashShareStruct[m_ProcessJobStateTrx.ReadItem[0]];
                string sProcessJobID = ShareMem.GetAscii(listReadDatas[0]);
                byte iProcessJobState = ShareMem.GetByte(listReadDatas[1]);
                long nReturn = 0;

                byte iAck = 0;
                long nObjID = 0;
                long nPJobCount = 0;
                long nPJState = 0;
               
                if (MainControl.Xgem.PJGetAllJobInfo(ref nObjID, ref nPJobCount) == 0)
                {
                    string sPRJobID = string.Empty;

                    for (int i = 0; i < nPJobCount; i++)
                    {
                        Xgem.GetPRJobID(nObjID, i, ref sPRJobID);

                        if (sPRJobID == sProcessJobID)
                        {
                            Xgem.GetPRJobState(nObjID, i, ref nPJState);
                            break;
                        }
                    }

                    MainControl.Xgem.GetPRJobClose(nObjID);
                }


                switch (iProcessJobState)
                {
                    case 1:
                        if (nPJState == 0)
                        {
                            iAck = 1;
                            nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_SettingUp);

                            if (nReturn == 0)
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_SettingUp));
                            else
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_SettingUp));
                        }
                        else
                            iAck = 2;
                        break;
                    case 2:
                        if (nPJState == 1)
                        {
                            iAck = 1;
                            nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_WaintingForStart);

                            if (nReturn == 0)
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_WaintingForStart));
                            else
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_WaintingForStart));
                        }
                        else
                            iAck = 2;
                        break;
                    case 3:
                        switch (nPJState)
                        {
                            case 1:
                            case 2:
                                iAck = 1;
                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Processing);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Processing));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Processing));
                                break;
                            default:
                                iAck = 2;
                                break;
                        }
                        break;
                    case 4:
                        if (nPJState == 3)
                        {
                            iAck = 1;
                            nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_ProcessComplete);

                            if (nReturn == 0)
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_ProcessComplete));
                            else
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_ProcessComplete));

                            // 내부 List 삭제
                            byte _CompletePortID = CimMessageProcessor.CurrentProcessJobIDs[sProcessJobID];
                            CimMessageProcessor.CurrentProcessJobIDs.Remove(sProcessJobID);
                            foreach (string _key in CimMessageProcessor.CurrentControlJobIDs.Keys)
                            {
                                if (_CompletePortID == CimMessageProcessor.CurrentControlJobIDs[_key])
                                {
                                    CimMessageProcessor.CurrentControlJobIDs.Remove(_key);
                                    break;
                                }

                            }

                            CimMessageProcessor.JobControl.ControlJobChange();
                            CimMessageProcessor.JobControl.ProcessJobChange();


                            // Next Job 시작 판단
                            if (CimMessageProcessor.CurrentProcessJobIDs.Count > 0 && CimMessageProcessor.CurrentControlJobIDs.Count > 0)
                            {
                                string _nextProcessID = string.Empty;
                                string _nextControlID = string.Empty;
                                byte _nextPortID = 0;

                                foreach (string _key in CimMessageProcessor.CurrentProcessJobIDs.Keys)
                                {
                                    if (_CompletePortID != CimMessageProcessor.CurrentProcessJobIDs[_key])
                                    {
                                        _nextPortID = CimMessageProcessor.CurrentProcessJobIDs[_key];
                                        _nextProcessID = _key;

                                        break;
                                    }
                                }
                                foreach (string _key in CimMessageProcessor.CurrentControlJobIDs.Keys)
                                {
                                    if (_nextPortID == CimMessageProcessor.CurrentControlJobIDs[_key])
                                    {
                                        _nextControlID = _key;

                                        break;
                                    }
                                }

                                List<PlcAddr> CommandStruct = MainControl.HashShareStruct[ShareNames.DEF_HOST_CONTROL_JOB_CMD_STRUCT];
                                PlcAddr CommandBit = MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROLJOB_CMD_COMMAND];

                                ShareMem.SetAscii(CommandStruct[0], _nextControlID);
                                ShareMem.SetByte(CommandStruct[1], 1);
                                ShareMem.SetByte(CommandStruct[2], _nextPortID);
                                ShareMem.SetBit(CommandBit, true);
                            }
                        }
                        else
                            iAck = 2;
                        break;
                    case 5:
                        if (nPJState == 6)
                        {
                            iAck = 1;
                            nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Paused);

                            if (nReturn == 0)
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Paused));
                            else
                                HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Paused));
                        }
                        else
                            iAck = 2;
                        break;
                    case 6:
                        switch (nPJState)
                        {
                            case 1:
                            case 2:
                            case 3:
                                iAck = 1;
                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Stopping);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Stopping));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Stopping));

                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Stopped);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Aborted));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Aborted));
                                break;
                            case 8:
                                iAck = 1;
                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Stopped);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Aborted));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Aborted));
                                break;
                            default:
                                iAck = 2;
                                break;
                        }
                        break;
                    case 7:
                        switch (nPJState)
                        {
                            case 1:
                            case 2:
                            case 3:
                            case 8:
                                iAck = 1;
                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Aborting);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Aborting));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Aborting));

                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Aborted);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Aborted));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Aborted));
                                break;
                            case 9:
                                iAck = 1;
                                nReturn = Xgem.PJSetState(sProcessJobID, (long)PRJobState.Pj_Aborted);

                                if (nReturn == 0)
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} ok", PRJobState.Pj_Aborted));
                                else
                                    HsmsLogControl.AddMessage(string.Format("PJSetState : {0} Fail", PRJobState.Pj_Aborted));
                                break;
                            default:
                                iAck = 2;
                                break;
                        }
                        break;
                    default:
                        iAck = 2;
                        break;
                }

                PlcAddr AckPJ = MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_PROCESS_JOB_ACK];
                PlcAddr StateReply = MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_PROCESS_JOB_REPLY];

                ShareMem.SetByte(AckPJ, iAck);
                ShareMem.SetBit(StateReply, true);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
