﻿#region Class Using
using System;
using Esol.ShareTrxControl;
#endregion

namespace Esol.CIM
{
    public class S2F17_Handler : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Trx m_DateTimeTrx;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S2F17_Handler(MainControl monitor)
            : base(monitor)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_DateTimeTrx = (Trx)m_ShareIncomingData.Clone();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (HsmsState == ControlState.Offline)
                    return;

                long nReturn = Xgem.GEMReqGetDateTime();

                HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMReqGetDateTime ({0})", nReturn));
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
