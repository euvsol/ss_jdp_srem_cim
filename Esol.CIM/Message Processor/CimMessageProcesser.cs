﻿#region Class Using
using System;
using System.Collections.Generic;
using System.Text;
using GEM_XGem300Pro;
using Esol.ShareMemory;
using Esol.ShareTrxControl;
using System.Threading;
using Esol.LogControl;
using System.Data.SqlTypes;
#endregion

namespace Esol.CIM
{
    public class CimMessageProcessor
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        public const int MAX_ARRAY = 7;
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SHARE_MEMORY = "Share";
        /// <summary>
        /// 
        /// </summary>
        private readonly int DEF_TIMER_SCAN_TIME = 1000;
        /// <summary>
        /// 
        /// </summary>
        private readonly int DEF_BIT_TIME_OUT = 3;
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private MainControl m_MainControl;
        /// <summary>
        /// 
        /// </summary>
        private ProcessJobProcesser m_ProcessJobProcesser;
        /// <summary>
        /// 
        /// </summary>
        private ControlJobProcesser m_ControlJobProcesser;
        /// <summary>
        /// 
        /// </summary>
        private CMSProcesser m_CMSProcesser;
        /// <summary>
        /// 
        /// </summary>
        private DateTimeProcesser m_DateTimeProcesser;
        /// <summary>
        /// 
        /// </summary>
        private TerminalProcesser m_TerminalProcesser;
        /// <summary>
        /// 
        /// </summary>
        private EcidProcesser m_EcidProcesser;
        /// <summary>
        /// 
        /// </summary>
        private RMSProcesser m_RMSProcesser;
        /// <summary>
        /// 
        /// </summary>
        private HostCommandProcesser m_HostCommandProcesser;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, DateTime> m_hashBitTime;
        /// <summary>
        /// 
        /// </summary>
        private Timer m_timer;
        /// <summary>
        /// 
        /// </summary>
        private readonly object m_syncObject;
        /// <summary>
        /// 
        /// </summary>
        private Logger m_Logger;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, Type> m_hashShareNameToHandler;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, Type> m_hashHsmsNameToHandler;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, DateTime> m_hashEqToAliveTime;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public VirtualMemory ShareMem
        {
            get
            {
                return m_MainControl.ShareMem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ShareTrx ShareTrx
        {
            get
            {
                return m_MainControl.ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_MainControl.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_MainControl.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_MainControl.HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public JobControl JobControl
        {
            get
            {
                return m_MainControl.JobControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ProcessJobProcesser ProcessJobProcesser
        {
            get
            {
                return m_ProcessJobProcesser;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ControlJobProcesser ControlJobProcesser
        {
            get
            {
                return m_ControlJobProcesser;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public EcidControl EcidControl
        {
            get
            {
                return m_MainControl.EcidControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public RecipeManager RecipeManager
        {
            get
            {
                return m_MainControl.CimConfigurator.RecipeManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public CimConfigurator CimConfigurator
        {
            get
            {
                return m_MainControl.CimConfigurator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, DateTime> HashEqToAliveTime
        {
            get
            {
                return m_hashEqToAliveTime;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<PlcAddr>> HashShareStruct
        {
            get
            {
                return m_MainControl.HashShareStruct;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, PlcAddr> HashShareKeyToShareData
        {
            get
            {
                return m_MainControl.HashShareKeyToShareData;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<string> SelectCarrierID
        {
            get
            {
                return m_MainControl.SelectCarrierID;
            }
            set
            {
                m_MainControl.SelectCarrierID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public EcidProcesser EcidProcesser
        {
            get
            {
                return m_EcidProcesser;
            }
        }

        public Dictionary<string, byte> CurrentProcessJobIDs;
        public Dictionary<string, byte> CurrentControlJobIDs;

        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public CimMessageProcessor(MainControl MainControl)
        {
            try
            {
                m_MainControl = MainControl;

                m_syncObject = new object();
                m_hashBitTime = new Dictionary<string, DateTime>();
                m_hashHsmsNameToHandler = new Dictionary<string, Type>();
                m_hashShareNameToHandler = new Dictionary<string, Type>();

                InitalizeMessage();

                m_timer = new Timer(new TimerCallback(Timer));
                m_timer.Change(Timeout.Infinite, Timeout.Infinite);
                ReChargeTimer();

                m_Logger = new Logger();
                m_Logger.Initialize(m_MainControl.CimConfigurator.LogPath, MainControl.ShareTrx);
                m_Logger.Start();

                InitizlizeXgemEvent();

                m_MainControl.ShareDataChanged += new MainControl.ShareEventHandler(OnShareDataChanged);
                m_hashEqToAliveTime = new Dictionary<string, DateTime>()
                {
                    { ClassNames.DEF_ICM, DateTimeClone() },
                    { ClassNames.DEF_ITS, DateTimeClone() },
                };


                CurrentProcessJobIDs = new Dictionary<string, byte>();
                CurrentControlJobIDs = new Dictionary<string, byte>();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitizlizeXgemEvent()
        {
            try
            {
                //State
                Xgem.OnXGEMStateEvent += new OnXGEMStateEvent(OnXGEMStateEvent);
                Xgem.OnGEMCommStateChanged += new OnGEMCommStateChanged(OnGEMCommStateChanged);
                Xgem.OnGEMControlStateChanged += new OnGEMControlStateChanged(OnGEMControlStateChanged);


                //ETC
                Xgem.OnGEMErrorEvent += new OnGEMErrorEvent(OnGEMErrorEvent);

                //Job Processer Initialze
                m_ProcessJobProcesser = new ProcessJobProcesser(this);
                m_ControlJobProcesser = new ControlJobProcesser(this);
                m_CMSProcesser = new CMSProcesser(this);
                m_DateTimeProcesser = new DateTimeProcesser(this);
                m_TerminalProcesser = new TerminalProcesser(this);
                m_EcidProcesser = new EcidProcesser(this);
                m_RMSProcesser = new RMSProcesser(this);
                m_HostCommandProcesser = new HostCommandProcesser(this);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void InitalizeMessage()
        {
            BuildShareHandlersMap();
            BuildHsmsHandlersMap();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            m_Logger.OnStop();
            m_MainControl.SvidControl.OnStop();
        }
        #endregion

        #region Class Share Memory Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnShareDataChanged(PlcAddr addr)
        {
            try
            {
                CimShareEventFilter(addr);

                if (addr.AddrName.Contains(ClassNames.DEF_CIM_LOCAL_NO))
                    RegisterPlcDataChange(addr);
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void CimShareEventFilter(PlcAddr PlcMessage)
        {
            try
            {
                string sKeyMessage = string.Format("{0},{1}", PlcMessage.AddrName, PlcMessage.vBit == true ? "1" : "0");

                if (!m_MainControl.HashShareTrx.ContainsKey(sKeyMessage))
                {
                    if (string.Format("{0},1", ShareNames.DEF_SVID_REPORT) == sKeyMessage)
                    {
                        //Thread로 Read
                        //m_MainControl.CimConfigurator.SvidManager.RegisterPlcDataChange();
                        return;
                    }

                    if (sKeyMessage.Contains("ALIVE"))
                    {
                        if (sKeyMessage.Contains("L2"))
                            m_hashEqToAliveTime[ClassNames.DEF_ICM] = DateTimeClone();
                        else
                            m_hashEqToAliveTime[ClassNames.DEF_ITS] = DateTimeClone();

                        return;
                    }

                    if (PlcMessage.AddrName == ShareNames.DEF_ALARM_EXIST && PlcMessage.vBit == false)
                        CimConfigurator.AlarmManager.AlarmExistBitOff();

                    HsmsLogControl.ShareLogWrite(m_Logger.LogWrite(PlcMessage));
                    return;
                }

                Trx trx = m_MainControl.HashShareTrx[sKeyMessage];

                ShareAddLog(trx);

                if (PlcMessage.vBit && m_hashShareNameToHandler.ContainsKey(trx.sTrxId))
                {
                    Type handlerType = m_hashShareNameToHandler[trx.sTrxId];

                    BaseHandler handler = (BaseHandler)Activator.CreateInstance(handlerType, m_MainControl);

                    handler.ShareIncomingData = trx;

                    handler.Execute();
                }

                foreach (string sKey in trx.WriteItem.Keys)
                {
                    //필요 구문에서 ACK 판단 후 Reply
                    if (sKey.Contains("ACK"))
                        continue;

                    PlcAddr plcAddr = m_MainControl.HashShareKeyToShareData[sKey];
                    m_MainControl.ShareMem.SetBit(plcAddr, trx.WriteItem[sKey]);
                }

                HsmsLogControl.ShareLogWrite(m_Logger.LogWrite(PlcMessage));
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ShareAddLog(Trx trx)
        {
            StringBuilder sb = new StringBuilder();
            string sTime = Common.GenerateCurrentTime();

            sb.AppendLine(string.Format("{0} : {1}", sTime, trx.sTrxId));
            PlcAddr addr = m_MainControl.HashShareKeyToShareData[trx.sTriggerId];
            sb.AppendLine(string.Format("{0} : [{1}] {2} = {3}", sTime, addr.Addr, trx.sTriggerId, trx.bTriggerValue.ToString()));

            foreach (string sKey in trx.ReadItem)
            {
                if (m_MainControl.HashShareKeyToShareData.ContainsKey(sKey))
                {
                    PlcAddr plcaddr = m_MainControl.HashShareKeyToShareData[sKey];

                    object oValue = PlcTypeSwitch(plcaddr);

                    sb.AppendLine(string.Format("{0} : [{1}]{2} = {3}", sTime, plcaddr.Addr, plcaddr.AddrName, oValue.ToString()));
                }
                else if (m_MainControl.HashShareStruct.ContainsKey(sKey))
                {
                    List<PlcAddr> hashdata = m_MainControl.HashShareStruct[sKey];

                    sb.AppendLine(string.Format("{0} : {1}", sTime, sKey));

                    foreach (PlcAddr plcaddr in hashdata)
                    {
                        object oValue = PlcTypeSwitch(plcaddr);

                        sb.AppendLine(string.Format("{0} : [{1}]{2} = {3}", sTime, plcaddr.Addr, plcaddr.AddrName, oValue.ToString()));
                    }
                }
            }
            HsmsLogControl.ShareLogWrite(sb);
        }
        #endregion

        #region Class Class Xgem Error Methdos
        /// <summary>
        /// 
        /// </summary>
        private void OnGEMErrorEvent(string sInterfaceName, long nErrorCode)
        {
            HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMErrorEvent : ErrorCode({0}) ErrorName({1})", nErrorCode, sInterfaceName));
        }
        #endregion

        #region Class StateChange Methdos
        /// <summary>
        /// 
        /// </summary>
        private void OnXGEMStateEvent(long nState)
        {
            string szState = null;

            if (nState == -1) { szState = "Unknown"; }
            else if (nState == 0) { szState = "Init"; }
            else if (nState == 1) { szState = "Idle"; }
            else if (nState == 2) { szState = "Setup"; }
            else if (nState == 3) { szState = "Ready"; }
            else if (nState == 4) { szState = "Execute"; }
            else { szState = "Unknown"; }


            HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnXGEMStateEvent:{0}", szState));

            if (nState == 4)
            {
                m_MainControl.NotificationBoard.HsmsComStateChange(ClassNames.DEF_ENALBE_TEXT);

                HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMSetEstablish"));
            }
        }
        #endregion

        #region Class BitTimeOut
        /// <summary>
        ///
        /// </summary>
        public void RegisterPlcDataChange(PlcAddr addr)
        {
            TimeOutBitCompare(addr);
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimeOutBitCompare(PlcAddr addr)
        {
            lock (m_syncObject)
            {
                if (ShareMem.GetBit(addr))  //Bit On일때 Time Out 추가
                    m_hashBitTime[addr.AddrName] = DateTimeClone();
                else if (m_hashBitTime.ContainsKey(addr.AddrName))  //Bit Off일때 List에 있으면 삭제
                    m_hashBitTime.Remove(addr.AddrName);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void Timer(object sender)
        {
            try
            {
                lock (m_syncObject)
                {
                    if (m_hashBitTime.Count == 0)
                        return;

                    string[] sKeys = new string[m_hashBitTime.Count];

                    m_hashBitTime.Keys.CopyTo(sKeys, 0);

                    for (int i = 0; i < sKeys.Length; i++)
                    {
                        DateTime bitTime = m_hashBitTime[sKeys[i]].AddSeconds(DEF_BIT_TIME_OUT);

                        if (DateTime.Now.Ticks - bitTime.Ticks >= 0)
                            ShareMem.SetBit(ShareTrx.hashShareKeyToShareData[sKeys[i]], false);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
            finally
            {
                ReChargeTimer();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReChargeTimer()
        {
            m_timer.Change(DEF_TIMER_SCAN_TIME, Timeout.Infinite);
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        public void HsmsHandler(BaseHandler handler, List<object> list)
        {
            if (handler != null)
            {
                handler.HsmsDatas = list;
                handler.Execute();

                m_MainControl.OnHsmsDataChanged();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void BuildShareHandlersMap()
        {
            //Eq ShareMemory Mapping
            m_hashShareNameToHandler = new Dictionary<string, Type>()
            {
                { ShareNames.DEF_DATE_TIME_SYNC_TRX, typeof(S2F17_Handler) },
                { ShareNames.DEF_ALARM_TRX, typeof(S5F1_Handler) },
                { ShareNames.DEF_PROC_STATE_TRX, typeof(S6F11_ProcState_Handler) },
                { ShareNames.DEF_ACCESS_TRX, typeof(S6F11_Access_Handler) },
                { ShareNames.DEF_CARRIER_DATA_TRX, typeof(S6F11_CarrierData_Handler) },
                { ShareNames.DEF_PORT_MODE_TRX, typeof(S6F11_PortMode_Handler) },
                { ShareNames.DEF_TRANSFER_TRX, typeof(S6F11_Transfer_Handler) },
                //Dcoll 대응 안함
                //{ ShareNames.DEF_DCOLL_TRX,typeof(S6F11_Dcoll_Handler) },
                { ShareNames.DEF_RECIPE_CMD_TRX, typeof(S6F11_Recipe_Cmd_Handler) },
                { ShareNames.DEF_PROCESS_JOB_DELETE_TRX, typeof(Delete_Handler) },
                { ShareNames.DEF_CONTROL_JOB_DELETE_TRX, typeof(Delete_Handler) },
                { ShareNames.DEF_CARRIER_DELETE_TRX, typeof(Delete_Handler) },
                { ShareNames.DEF_PROCESS_JOB_STATE_TRX, typeof(ProcessJobState_Handler) },
                { ShareNames.DEF_CONTROL_JOB_STATE_TRX, typeof(ControlJobState_Handler) },
                { ShareNames.DEF_CONTROL_STATE_TRX, typeof(S6F11_ControlState_Handler) },
                { ShareNames.DEF_STEP_CHANGE_TRX, typeof(S6F11_StepChange_Handler) },
                { ShareNames.DEF_PLASMA_AUTO_CLEANING_TRX, typeof(S6F11_PlasmaAutoCleaning_Handler) },
                { ShareNames.DEF_EUV_POWER_MONITORING_TRX, typeof(S6F11_EuvPowerMonitoring_Handler) },
                { ShareNames.DEF_LIGHT_MONITORING_TRX, typeof(S6F11_LightMonitoring_Handler) },
                { ShareNames.DEF_ECID_CHANGE_TRX, typeof(S6F11_EcidChange_Handler) }
            };
        }
        /// <summary>
        /// 
        /// </summary>
        private void BuildHsmsHandlersMap()
        {
            //Host Event Mapping
            m_hashHsmsNameToHandler = new Dictionary<string, Type>()
            {
                { "S1F15", typeof(S1F15_Message) }, //Host Offline Change
                { "S1F17", typeof(S1F17_Message) }, //Host Online Change
                { "S2F15", typeof(S2F15_Message) }, //Host Ecid Change
                { "S2F17", typeof(S2F17_Message) }, //Host DateTime Request
                { "S2F18", typeof(S2F18_Message) }, //Date Time Sync
                { "S2F31", typeof(S2F31_Message) }, //Host Date Time Set
                { "S2F41", typeof(S2F41_Message) }, //RCMD
                { "S3F17", typeof(S3F17_Message) }, //Cancel Carrier
                { "S3F27", typeof(S3F27_Message) }, //Port Mode Change
                { "S7F19", typeof(S7F19_Message) }, //Recipe List Request
                { "S10F3", typeof(S10F3_Message) }, //Terminal Message Single
                { "S10F5", typeof(S10F5_Message) }, //Terminal Message Multi
                { "S16F5", typeof(S16F5_Message) }, //Process Job Command
                { "S16F11_15", typeof(S16F11_15_Message) }, //ProcessJob Create
            };

        }
        /// <summary>
        /// 
        /// </summary>
        public BaseHandler GetAppropriateHandler(string sName)
        {
            m_MainControl.OnHsmsDataChanged();

            if (m_hashHsmsNameToHandler.ContainsKey(sName))
                return GetAppropriateHandler_Temp(sName);

            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        private BaseHandler GetAppropriateHandler_Temp(string sName)
        {
            BaseHandler handler = null;

            try
            {
                Type handlerType = m_hashHsmsNameToHandler[sName];

                handler = (BaseHandler)Activator.CreateInstance(handlerType, m_MainControl);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }

            return handler;
        }
        /// <summary>
        /// 
        /// </summary>
        private object PlcTypeSwitch(PlcAddr plcaddr)
        {
            switch (plcaddr.ValueType.ToString())
            {
                case "ASCII":
                    return (object)m_MainControl.ShareMem.GetAscii(plcaddr);
                case "BYTE":
                    return (object)m_MainControl.ShareMem.GetByte(plcaddr);
                case "SHORT":
                    return (object)m_MainControl.ShareMem.GetShort(plcaddr);
                case "INT32":
                    return (object)m_MainControl.ShareMem.GetInt32(plcaddr);
                case "USHORT":
                    return (object)m_MainControl.ShareMem.GetUShort(plcaddr);
                case "UINT32":
                    return (object)m_MainControl.ShareMem.GetUInt32(plcaddr);
                case "UINT64":
                    return (object)m_MainControl.ShareMem.GetUInt64(plcaddr);
                case "BIT":
                    return (object)m_MainControl.ShareMem.GetBit(plcaddr);
                case "FLOAT":
                    return (object)m_MainControl.ShareMem.GetFloat(plcaddr);
                case "DOUBLE":
                    return (object)m_MainControl.ShareMem.GetDouble(plcaddr);
                default:
                    object ob = new object();
                    return ob;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private DateTime DateTimeClone()
        {
            return DateTime.Now;
        }
        #endregion

        #region Class Xgem Event
        /// <summary>
        /// Xgem Hsms State Change
        /// </summary>
        /// 

        private void OnGEMCommStateChanged(long nState)
        {
            string szState = string.Empty;

            m_MainControl.NotificationBoard.ConnectState = false;

            switch (nState)
            {
                case 1:
                    szState = "Comm Disabled";
                    m_MainControl.NotificationBoard.HsmsDisConnectOffline();
                    break;
                case 2:
                    szState = "WaitCRFromHost";
                    break;
                case 3:
                    szState = "WaitDelay";
                    m_MainControl.NotificationBoard.HsmsDisConnectOffline();
                    break;
                case 4:
                    szState = "WaitCRA";
                    break;
                case 5:
                    szState = "Communicating";
                    m_MainControl.NotificationBoard.ConnectState = true;

                    switch (m_MainControl.NotificationBoard.HsmsState)
                    {
                        case ControlState.Remote:
                            m_MainControl.NotificationBoard.HsmsConnectChange(ClassNames.DEF_REMOTE_TEXT);
                            break;
                        default:
                            m_MainControl.NotificationBoard.HsmsConnectChange(ClassNames.DEF_LOCAL_TEXT);
                            //m_MainControl.NotificationBoard.HsmsConnectChange("REMOTE");
                            break;
                    }
                    break;
                default:
                    szState = "Comm Disabled";
                    break;

            }

            HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMCommStateChanged:{0}", szState));

            m_MainControl.HsmsStateChange(szState);

            m_MainControl.OnHsmsDataChanged();

            //if (nState == 5)
            //{
            //    m_MainControl.NotificationBoard.ConnectState = true;
            //    m_MainControl.NotificationBoard.HsmsStateChange = ClassNames.DEF_REMOTE_TEXT;
            //}
        }
        /// <summary>
        /// Host Hsms Change
        /// </summary>
        private void OnGEMControlStateChanged(long nState)
        {
            string szState = string.Empty;
            string sConnectState = string.Empty;

            m_MainControl.NotificationBoard.ConnectState = true;

            switch (nState)
            {
                case 1:
                    szState = "OffLine";
                    sConnectState = ClassNames.DEF_OFFLINE_TEXT;
                    break;
                case 2:
                    szState = "Attempt OnLine";
                    break;
                case 3:
                    szState = "Host OffLine";
                    sConnectState = ClassNames.DEF_HOST_OFFLINE_TEXT;
                    break;
                case 4:
                    szState = "Online-Local";
                    sConnectState = ClassNames.DEF_LOCAL_TEXT;
                    break;
                case 5:
                    szState = "Online-Remote";
                    sConnectState = ClassNames.DEF_REMOTE_TEXT;
                    break;
                default:
                    szState = "OffLine";
                    sConnectState = ClassNames.DEF_OFFLINE_TEXT;
                    break;
            }

            HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMControlStateChanged:{0}", szState));

            m_MainControl.HsmsStateChange(szState);

            //if (!string.IsNullOrEmpty(sConnectState))
            //    m_MainControl.NotificationBoard.HsmsConnectChange(szState.ToUpper());

            m_MainControl.OnHsmsDataChanged();
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnClosingControlStateChange()
        {
            //Offline State 변경 Command
            m_MainControl.NotificationBoard.ConnectState = true;

            string szState = "OffLine";

            HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMControlStateChanged:{0}", szState));

            m_MainControl.HsmsStateChange(szState);
        }
        #endregion

        public void SendCustomNack(long[] errCode, string[] errText)
        {
            try
            {
                List<PlcAddr> listWriteDatas = m_MainControl.HashShareStruct[ShareNames.DEF_CUSTOM_NACK_STRUCT];
                PlcAddr WriteBit = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_CUSTOM_NACK_COMMAND];

                ShareMem.SetUInt64(listWriteDatas[0], (ulong)errCode[0]);
                ShareMem.SetAscii(listWriteDatas[1], errText[0]);

                ShareMem.SetBit(WriteBit, true);
            }
            catch(Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
    }
}
