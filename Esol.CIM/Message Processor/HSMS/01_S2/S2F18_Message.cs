﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class S2F18_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private string m_sDateTime;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S2F18_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_sDateTime = m_listHsmsMessages[0].ToString();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                if (m_sDateTime.Length != 16)
                {
                    HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMRspGetDateTime : NG systemtime({0})", m_sDateTime));
                    return;
                }

                bool bTimeCheck;

                DateTimeHandler dtp = new DateTimeHandler();

                bTimeCheck = dtp.TimeCheck(m_sDateTime);

                if (bTimeCheck)
                    HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMRspGetDateTime : systemtime({0})", m_sDateTime));
                else
                    HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMRspGetDateTime : NG systemtime({0})", m_sDateTime));
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
