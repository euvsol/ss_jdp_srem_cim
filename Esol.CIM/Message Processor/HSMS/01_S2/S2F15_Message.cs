﻿#region Class Using
using System;
using Esol.XmlControl;
using System.Collections.Generic;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    public class S2F15_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgId;
        /// <summary>
        /// 
        /// </summary>
        private long m_nCount;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnEcids;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psVals;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S2F15_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsgId = (long)m_listHsmsMessages[0];
                m_nCount = (long)m_listHsmsMessages[1];
                m_pnEcids = (long[])m_listHsmsMessages[2];
                m_psVals = (string[])m_listHsmsMessages[3];

                string sLog = string.Format("[XGEM ==> EQ] OnGEMReqChangeECV");
                HsmsLogControl.AddMessage(sLog);

                int iCount = 0;
                foreach(long nEcid in m_pnEcids)
                {
                    sLog = string.Format("     Ecid:{0}, Value:{1}", nEcid, m_psVals[iCount++]);
                    HsmsLogControl.AddMessage(sLog);
                }
            }
            catch(Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                string[] naName = new string[m_nCount];
                string[] naDefault = new string[m_nCount];
                string[] naMin = new string[m_nCount];
                string[] naMax = new string[m_nCount];
                string[] naUnit = new string[m_nCount];

                long nReturn = 0;
                if (!CimConfigurator.TestMode && (!MainControl.AliveICM && !MainControl.AliveITS))
                {
                    nReturn = Xgem.GEMRspChangeECV(m_nMsgId, 1);
                    HsmsLogControl.AddMessage(string.Format("Fail ITS or ICM Not Alive CMSRspChangeAccess ({0}) Result : {1}", nReturn, 2));
                    return;
                }


                nReturn = Xgem.GEMGetECVInfo(m_nCount, m_pnEcids, ref naName, ref naDefault, ref naMin, ref naMax, ref naUnit);

                if (nReturn == 0)
                {

                    //Ecid Range Check
                    for(int i=0; i<m_nCount; i++)
                    {
                        if(m_pnEcids[i] == 125)
                        {
                            if(!CheckIpAddress(m_psVals[i]))
                            {
                                Xgem.GEMRspChangeECV(m_nMsgId, 3);
                                return;
                            }
                        }
                        else
                        {
                            try
                            {
                                //if (naMin[i] == string.Empty || naMax[i] == string.Empty)
                                //    continue;
                                //
                                //if ((Convert.ToInt64(naMin[i]) > Convert.ToInt64(m_psVals[i])) || (Convert.ToInt64(naMax[i]) < Convert.ToInt64(m_psVals[i])))
                                //{
                                //    Xgem.GEMRspChangeECV(m_nMsgId, 3);
                                //    return;
                                //}
                            }
                            catch
                            {
                                Xgem.GEMRspChangeECV(m_nMsgId, 3);
                                return;
                            }
                        }
                    }

                    string sHostEcidPath = CimConfigurator.HostEcidPath;

                    //XML Write & Share Memory Interface
                    XmlDataWriter xmlWriter = new XmlDataWriter(sHostEcidPath, "Group");
                    Dictionary<string, string> hashEcidDatas = new Dictionary<string, string>();

                    for (int i = 0; i < m_nCount; i++)
                    {
                        long nEcid = m_pnEcids[i] - 2000;
                        hashEcidDatas.Add(nEcid.ToString(), m_psVals[i]);
                    }

                    xmlWriter.EsolXmlElement("Group", "Ecids", hashEcidDatas);


                    Xgem.GEMRspChangeECV(m_nMsgId, 0);
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMRspChangeECV Ack = {0}", nReturn));

                    PlcAddr addr = CimConfigurator.HashShareKeyToShareData["L1_B_ECID_CHANGE_COMMAND"];
                    CimConfigurator.ShareMem.SetBit(addr);
                }
                else
                {
                    Xgem.GEMRspChangeECV(m_nMsgId, 1);
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMRspChangeECV NAck = {0}", nReturn));
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Priavate Methods
        /// <summary>
        /// 
        /// </summary>
        private bool CheckIpAddress(string sValue)
        {
            string[] saIpAddress = sValue.Split('.');

            if (saIpAddress.Length != 4)
                return false;

            int iCount = 0;

            foreach (string sIpAddress in saIpAddress)
            {
                try
                {
                    byte iConvertData = Convert.ToByte(sIpAddress);

                    if (iCount == 0)
                    {
                        if (iConvertData < 1 || iConvertData > 255)
                            return false;

                        iCount++;
                    }
                    else if (iConvertData < 0 || iConvertData > 255)
                        return false;
                }
                catch(Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}
