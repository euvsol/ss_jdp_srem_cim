﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class S2F31_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgId;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDateTime;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S2F31_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsgId = (long)m_listHsmsMessages[0];
                m_sDateTime = m_listHsmsMessages[1].ToString();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                string sLog = string.Format("[XGEM ==> EQ] OnGEMReqDateTime : systemtime({0})", m_sDateTime);
                HsmsLogControl.AddMessage(sLog);

                DateTimeHandler dtp = new DateTimeHandler();

                bool bTimeCheck = dtp.TimeCheck(m_sDateTime);

                if (bTimeCheck)
                {
                    Xgem.GEMRspDateTime(m_nMsgId, 0);
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMRspDateTime"));
                }
                else
                {
                    Xgem.GEMRspDateTime(m_nMsgId, 1);
                    HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] NG GEMRspDateTime"));
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
