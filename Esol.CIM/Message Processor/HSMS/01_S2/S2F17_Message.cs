﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class S2F17_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgId;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S2F17_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsgId = (long)m_listHsmsMessages[0];
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                HsmsLogControl.AddMessage(string.Format("[XGEM ==> EQ] OnGEMReqGetDateTime"));

                string sSystemTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                Xgem.GEMRspGetDateTime(m_nMsgId, sSystemTime);

                HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] GEMRspGetDateTime:{0}", sSystemTime));
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
