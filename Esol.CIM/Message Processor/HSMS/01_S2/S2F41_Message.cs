﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class S2F41_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgId;
        /// <summary>
        /// 
        /// </summary>
        private string m_sRcmd;
        /// <summary>
        /// 
        /// </summary>
        private long m_nCount;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psNames;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psValue;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S2F41_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsgId = (long)m_listHsmsMessages[0];
                m_sRcmd = (string)m_listHsmsMessages[1];
                m_nCount = (long)m_listHsmsMessages[2];
                m_psNames = (string[])m_listHsmsMessages[3];
                m_psValue = (string[])m_listHsmsMessages[4];
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                long nReturn = 0;
                long[] pnResult = new long[m_nCount];
                for (int i = 0; i < m_nCount; i++)
                {
                    if (!MainControl.AliveICM || !MainControl.AliveITS)
                        pnResult[i] = 1;
                    else
                        pnResult[i] = 0;
                }

                HsmsLogControl.AddMessage("nMsgId : {0} / sRcmd : {1} / nCount : {2}", m_nMsgId, m_sRcmd, m_nCount);
                for (int i = 0; i < m_nCount; i++)
                    HsmsLogControl.AddMessage("[{0}] psNames : {1} / psVals : {2} / pnResult {3}", i, m_psNames[i], m_psValue[i], pnResult[i]);
                
                nReturn = Xgem.GEMRspRemoteCommand(m_nMsgId, m_sRcmd, 0, m_nCount, m_psNames, pnResult);
                if (nReturn == 0)
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Return OK");
                else
                    HsmsLogControl.AddMessage("[EQ ==> XGEM] Return Fail [{0}]", nReturn);
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
