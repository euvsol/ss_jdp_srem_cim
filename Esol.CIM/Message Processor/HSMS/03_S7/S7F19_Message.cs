﻿#region Class Using
using System;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S7F19_Message : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgId;        
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S7F19_Message(MainControl maincontrol) 
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            m_nMsgId = (long)m_listHsmsMessages[0];
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                HsmsLogControl.AddMessage("nMsgId : {0}", m_nMsgId);

                long nCount = 0;
                string[] sArrayPpids = new string[CimConfigurator.RecipeManager.BasicRecipeNameToData.Count + CimConfigurator.RecipeManager.ProcessRecipeNameToData.Count];
                string[] sArrayBasicRecipeNames = new string[CimConfigurator.RecipeManager.BasicRecipeNameToData.Keys.Count];
                string[] sArrayProcessRecipe = new string[CimConfigurator.RecipeManager.ProcessRecipeNameToData.Keys.Count];

                CimConfigurator.RecipeManager.BasicRecipeNameToData.Keys.CopyTo(sArrayBasicRecipeNames, 0);
                CimConfigurator.RecipeManager.ProcessRecipeNameToData.Keys.CopyTo(sArrayProcessRecipe, 0);

                HsmsLogControl.AddMessage("nCount : {0}", nCount);
                foreach (string sRecipeName in sArrayBasicRecipeNames)
                {
                    sArrayPpids[nCount++] = sRecipeName;
                    HsmsLogControl.AddMessage(string.Format("Basic Recipe Name : {0}", sRecipeName));
                }
                foreach (string sProcessRecipeName in sArrayProcessRecipe)
                {
                    sArrayPpids[nCount++] = sProcessRecipeName;
                    HsmsLogControl.AddMessage(string.Format("Process Recipe Name : {0}", sProcessRecipeName));
                }

                Xgem.GEMRspPPList(m_nMsgId, nCount, sArrayPpids);
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
