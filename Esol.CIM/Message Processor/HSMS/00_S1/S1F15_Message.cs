﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class S1F15_Message : BaseHandler
    {
        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S1F15_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        { }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                HsmsLogControl.AddMessage("nMsgId:{0}, nFromState:{1}, nToState:{2}", m_listHsmsMessages[0], m_listHsmsMessages[1], m_listHsmsMessages[2]);

                Xgem.GEMRsqOffline((long)m_listHsmsMessages[0], 0);

                m_MainControl.NotificationBoard.HsmsStateChange = ClassNames.DEF_HOST_OFFLINE_TEXT;
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
