﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class S1F17_Message : BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private long m_iMsgId;
        /// <summary>
        /// 
        /// </summary>
        private long m_iFormState;
        /// <summary>
        /// 
        /// </summary>
        private long m_iToState;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S1F17_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            m_iMsgId = (long)m_listHsmsMessages[0];
            m_iFormState = (long)m_listHsmsMessages[1];
            m_iToState = (long)m_listHsmsMessages[2];
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                HsmsLogControl.AddMessage("nMsgId:{0}, nFromState:{1}, nToState:{2}", m_listHsmsMessages[0], m_listHsmsMessages[1], m_listHsmsMessages[2]);

                Xgem.GEMRspOnline((long)m_listHsmsMessages[0], 0);

                m_MainControl.NotificationBoard.HsmsStateChange = ClassNames.DEF_REMOTE_TEXT;
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
