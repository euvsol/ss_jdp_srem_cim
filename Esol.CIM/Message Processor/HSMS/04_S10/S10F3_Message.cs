﻿#region Class Using
using System;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    class S10F3_Message : BaseHandler
    {
        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S10F3_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        { }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                HsmsLogControl.AddMessage("TID : {0} / Msg : {1}", m_listHsmsMessages[0], m_listHsmsMessages[1]);
                CimConfigurator.TerminalManager.AddMessage(m_listHsmsMessages[0].ToString(), new List<string>() { m_listHsmsMessages[1].ToString() });
                LogManager.WriteLog(LogType.TERMINAL.ToString(), string.Format("Tid : {0} Text : {1}",m_listHsmsMessages[0].ToString(), m_listHsmsMessages[1].ToString()));
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
