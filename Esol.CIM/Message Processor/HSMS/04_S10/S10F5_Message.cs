﻿#region Class Using
using System;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    class S10F5_Message : BaseHandler
    {
        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S10F5_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        { }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                long nTid = Convert.ToInt64(m_listHsmsMessages[0]);
                int iCount = Convert.ToInt32(m_listHsmsMessages[1]);
                List<string> listText = new List<string>();
                string sLog = string.Format("TID : {0} / Count : {1}", nTid, iCount);
                m_MainControl.HsmsLogControl.AddMessage(sLog);

                m_listHsmsMessages.RemoveAt(0);
                m_listHsmsMessages.RemoveAt(0);
                foreach (object oMessage in m_listHsmsMessages)
                {
                    listText.Add(oMessage.ToString());

                    m_MainControl.HsmsLogControl.AddMessage("MSG : {0}", oMessage);
                    LogManager.WriteLog(LogType.TERMINAL.ToString(), string.Format("Tid : {0} Text : {1}", nTid, oMessage.ToString()));
                }

                CimConfigurator.TerminalManager.AddMessage(nTid.ToString(), listText, true);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
