﻿#region Class Using
using System;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S3F17_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsagId;
        /// <summary>
        /// 
        /// </summary>
        private string m_sLocID;
        /// <summary>
        /// 
        /// </summary>
        private string m_sCarrierID;
        /// <summary>
        /// 
        /// </summary>
        private readonly string[] m_sArrayLocalId = new string[10];
        /// <summary>
        /// 
        /// </summary>
        private readonly long[] m_nArrayErrorCode = new long[10];
        /// <summary>
        /// 
        /// </summary>
        private readonly string[] m_sArrayErrText = new string[10];
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S3F17_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_sCarrierID = string.Empty;
                m_nMsagId = (long)m_listHsmsMessages[0];
                m_sLocID = m_listHsmsMessages[1].ToString();
                if (m_listHsmsMessages.Count > 2)
                    m_sCarrierID = m_listHsmsMessages[2].ToString();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                //Cancel Carrier Command
                long nReturn = 0;
                HsmsLogControl.AddMessage("m_sCarrierID : {0} / m_nMsagId : {1} / m_sLocID : {2}", m_sCarrierID, m_nMsagId, m_sLocID);

                long nResult = 0;
                long nErrCount = 0;
                long[] naErrCode = new long[0];
                string[] saErrText = new string[0];
                
                if (!CimConfigurator.TestMode && (!MainControl.AliveICM || !MainControl.AliveITS || MainControl.NotificationBoard.ProcState != ProcessingState.Idle))
                {
                    nErrCount = 1;
                    naErrCode = new long[1] { 10 };
                    saErrText = new string[1] { "Fail Cancel Carrier" };

                    nReturn = Xgem.CMSRspCancelCarrierAtPort(m_nMsagId, m_sLocID, 1, nErrCount, naErrCode, saErrText);
                    HsmsLogControl.AddMessage(string.Format("Fail ITS or ICM Not Alive CMSRspCancelCarrierAtPort ({0}) Result : {1}", nReturn, 2));
                    CimMessageProcessor.SendCustomNack(naErrCode, saErrText);
                    return;
                }
                if(CimMessageProcessor.CurrentControlJobIDs.Count > 0 || CimMessageProcessor.CurrentProcessJobIDs.Count > 0)
                {
                    nErrCount = 1;
                    naErrCode = new long[1] { 10 };
                    saErrText = new string[1] { "Fail Cancel Carrier" };

                    nReturn = Xgem.CMSRspCancelCarrierAtPort(m_nMsagId, m_sLocID, 1, nErrCount, naErrCode, saErrText);
                    HsmsLogControl.AddMessage(string.Format("Fail ITS or ICM Not Alive CMSRspCancelCarrierAtPort ({0}) Result : {1}", nReturn, 2));
                    CimMessageProcessor.SendCustomNack(naErrCode, saErrText);
                    return;
                }

                switch (string.IsNullOrEmpty(m_sCarrierID))
                {
                    case true:
                        //Send Response Message : S3F18
                        nReturn = Xgem.CMSRspCancelCarrierAtPort(m_nMsagId, m_sLocID, nResult, nErrCount, new long[0], new string[0]);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("Send CMSRspCancelCarrierAtPort successfully"));
                        else
                            HsmsLogControl.AddMessage(string.Format("Fail to CMSRspCancelCarrierAtPort ({0})", nReturn));
                        break;
                    case false:
                        //Send Response Message : S3F18
                        nReturn = Xgem.CMSRspCancelCarrier(m_nMsagId, m_sLocID, m_sCarrierID, nResult, nErrCount, naErrCode, saErrText);

                        if (nReturn == 0)
                            HsmsLogControl.AddMessage(string.Format("Send CMSRspCancelCarrier successfully"));
                        else
                            HsmsLogControl.AddMessage(string.Format("Fail to CMSRspCancelCarrier ({0})", nReturn));
                        break;
                }

                if (nReturn != 0)
                    return;

                byte iPortNo = Convert.ToByte(m_sLocID.Replace("LP", string.Empty));
                List<PlcAddr> listCarrierDatas = m_MainControl.HashShareStruct[ShareNames.DEF_HOST_CARRIER_DATA_DOWNLOAD_STRUCT];
                ShareMem.SetByte(listCarrierDatas[0], 3);
                ShareMem.SetByte(listCarrierDatas[1], iPortNo);
                ShareMem.SetByte(listCarrierDatas[2], 0);
                ShareMem.SetAscii(listCarrierDatas[3], m_sCarrierID);
                ShareMem.SetAscii(listCarrierDatas[4], string.Empty);
                ShareMem.SetAscii(listCarrierDatas[5], string.Empty);

                PlcAddr CarrierCommand = MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CARRIER_DATA_DOWN_COMMAND];
                ShareMem.SetBit(CarrierCommand, true);
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
