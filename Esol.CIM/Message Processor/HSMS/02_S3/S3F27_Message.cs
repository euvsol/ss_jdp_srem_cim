﻿#region Class Using
using System;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S3F27_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsagId;
        /// <summary>
        /// 
        /// </summary>
        private long m_nMode;
        /// <summary>
        /// 
        /// </summary>
        private long m_nCount;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sArrayLocId;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sArrayLocalId;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_nArrayErrorCode;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sArrayErrText;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S3F27_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsagId = (long)m_listHsmsMessages[0];
                m_nMode = (long)m_listHsmsMessages[1];
                m_nCount = (long)m_listHsmsMessages[2];
                m_sArrayLocId = (string[])m_listHsmsMessages[3];

                m_sArrayLocalId = new string[m_nCount];
                m_nArrayErrorCode = new long[0];
                m_sArrayErrText = new string[0];
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                m_MainControl.HsmsLogControl.AddMessage(string.Format("Received {0}, Mode = {1}, Count = {2}", "CMSReqChangeAccess", m_nMode, m_nCount));
                long nReturn = 0;
                foreach (string _tempLocID in m_sArrayLocId)
                {
                    HsmsLogControl.AddMessage(string.Format(" LocID = {0}", _tempLocID));
                    m_sArrayLocalId[0] = _tempLocID;

                    if (!(m_nMode >= 0 && m_nMode < 2))
                    {
                        m_nArrayErrorCode = new long[1];
                        m_nArrayErrorCode[0] = 1;
                        m_sArrayErrText = new string[1];
                        m_sArrayErrText[0] = "Invalid command";
                        nReturn = Xgem.CMSRspChangeAccess(m_nMsagId, m_nMode, 1, 1, m_sArrayLocalId, m_nArrayErrorCode, m_sArrayErrText);
                        HsmsLogControl.AddMessage(string.Format("Fail to CMSRspChangeAccess ({0}) Result : {1}", nReturn, "1"));
                        CimMessageProcessor.SendCustomNack(m_nArrayErrorCode, m_sArrayErrText);
                        return;
                    }

                    if (!CimConfigurator.TestMode && (!MainControl.AliveICM || !MainControl.AliveITS))
                    {
                        m_nArrayErrorCode = new long[1];
                        m_nArrayErrorCode[0] = 2;
                        m_sArrayErrText = new string[1];
                        m_sArrayErrText[0] = "Cannot perform now";

                        nReturn = Xgem.CMSRspChangeAccess(m_nMsagId, m_nMode, 2, 1, m_sArrayLocalId, m_nArrayErrorCode, m_sArrayErrText);
                        HsmsLogControl.AddMessage(string.Format("Fail ITS or ICM Not Alive CMSRspChangeAccess ({0}) Result : {1}", nReturn, 2));
                        CimMessageProcessor.SendCustomNack(m_nArrayErrorCode, m_sArrayErrText);
                        return;
                    }

                    //Send Response Message : S3F28
                    nReturn = Xgem.CMSRspChangeAccess(m_nMsagId, m_nMode, 0, 0, m_sArrayLocalId, new long[0], new string[0]);
                    if (nReturn == 0)
                    {
                        HsmsLogControl.AddMessage(string.Format("Send CMSRspChangeAccess successfully"));

                        List<PlcAddr> listAccesss = m_MainControl.HashShareStruct[ShareNames.DEF_HOST_ACCESS_STRUCT];
                        PlcAddr AccessCommand = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_ACCESS_COMMAND];

                        for (int i = 0; i < listAccesss.Count; i++)
                        {
                            Xgem.CMSReqChangeAccess(m_nMode, m_sArrayLocId[i]);

                            if (i == 0)
                                ShareMem.SetByte(listAccesss[i], Convert.ToByte(m_nMode + 1));
                            else
                                ShareMem.SetByte(listAccesss[i], 0);
                        }
                        ShareMem.SetBit(AccessCommand, true);
                    }
                    else
                        HsmsLogControl.AddMessage(string.Format("Fail to CMSRspChangeAccess ({0})", nReturn));
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
