﻿#region Class Using
using System;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class S16F5_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgID;
        /// <summary>
        /// 
        /// </summary>
        private string m_sPJobID;
        /// <summary>
        /// 
        /// </summary>
        private long m_nCommand;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S16F5_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsgID = (long)m_listHsmsMessages[0];
                m_sPJobID = (string)m_listHsmsMessages[1];
                m_nCommand = (long)m_listHsmsMessages[2];
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                //S16F5
                long nReturn = 0;

                HsmsLogControl.AddMessage(string.Format("Received {0} sPJobID={1}, nCommand={2}", "PJReqCommand", m_sPJobID, m_nCommand));

                //1:START 2:PAUSE 3:RESUME 4:STOP 5:ABORT 6:CANCEL
                switch (m_nCommand)
                {
                    case 1:
                        nReturn = Xgem.PJSetState(m_sPJobID, (long)PRJobState.Pj_Pausing);
                        break;
                    case 2:
                        nReturn = Xgem.PJSetState(m_sPJobID, (long)PRJobState.Pj_Pausing);
                        break;
                    case 4:
                        nReturn = Xgem.PJSetState(m_sPJobID, (long)PRJobState.Pj_Stopping);
                        break;
                    case 5:
                        nReturn = Xgem.PJSetState(m_sPJobID, (long)PRJobState.Pj_Aborting);
                        break;
                    case 6:
                        nReturn = Xgem.PJSetState(m_sPJobID, (long)PRJobState.Pj_JobCanceled);
                        break;
                }

                if (nReturn == 0)
                {
                    List<PlcAddr> CommandStruct = m_MainControl.HashShareStruct[ShareNames.DEF_HOST_PROCESS_JOB_CMD_STRUCT];
                    PlcAddr CommandBit = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_PROCESSJOB_CMD_COMMAND];

                    ShareMem.SetAscii(CommandStruct[0], m_sPJobID);
                    ShareMem.SetByte(CommandStruct[1], Convert.ToByte(m_nCommand));
                    ShareMem.SetByte(CommandStruct[2], CimMessageProcessor.CurrentProcessJobIDs[m_sPJobID]);
                    ShareMem.SetBit(CommandBit, true);
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
