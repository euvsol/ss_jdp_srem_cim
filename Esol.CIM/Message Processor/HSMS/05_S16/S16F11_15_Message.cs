﻿#region Class Using
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    public class S16F11_15_Message : BaseHandler
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        private long m_nMsgID;
        /// <summary>
        /// 
        /// </summary>
        private long m_nPJobCount;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psPJobID;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnMtrlFormat;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnAutoStart;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnMtrlOrder;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnMtrlCount;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psMtrlID;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psSlotInfo;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnRcpMethod;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psRcpID;
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnRcpParCount;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psRcpParName;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psRcpParValue;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public S16F11_15_Message(MainControl maincontrol)
            : base(maincontrol)
        { }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ReadIncomingMessage()
        {
            try
            {
                m_nMsgID = (long)m_listHsmsMessages[0];
                m_nPJobCount = (long)m_listHsmsMessages[1];
                m_psPJobID = (string[])m_listHsmsMessages[2];
                m_pnMtrlFormat = (long[])m_listHsmsMessages[3];
                m_pnAutoStart = (long[])m_listHsmsMessages[4];
                m_pnMtrlOrder = (long[])m_listHsmsMessages[5];
                m_pnMtrlCount = (long[])m_listHsmsMessages[6];
                m_psMtrlID = (string[])m_listHsmsMessages[7];
                m_psSlotInfo = (string[])m_listHsmsMessages[8];
                m_pnRcpMethod = (long[])m_listHsmsMessages[9];
                m_psRcpID = (string[])m_listHsmsMessages[10];
                m_pnRcpParCount = (long[])m_listHsmsMessages[11];
                m_psRcpParName = (string[])m_listHsmsMessages[12];
                m_psRcpParValue = (string[])m_listHsmsMessages[13];
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void HandleMessage()
        {
            try
            {
                //S16F11 and S16F15
                int nMtrlCnt = 0;
                int nRcpParCnt = 0;
                int nIndex1 = 0;
                int nIndex2 = 0;
                long lResult = 0;
                long lErrCnt = 0;
                long[] plErrorCode = null;
                string[] psErrText = null;
                string sRffName = m_psRcpParValue == null ? string.Empty : m_psRcpParValue[0];
                string sRffFilePath = (Path.GetExtension(sRffName).ToUpper() == ".RFF") ?
                    string.Format(@"{0}\{1}", CimMessageProcessor.CimConfigurator.RffFilePath, sRffName) :
                    string.Format(@"{0}\{1}\{2}", CimMessageProcessor.CimConfigurator.RffFilePath, "POS", sRffName);

                if (m_psRcpID != null || m_psRcpID.Length == 1)
                    HsmsLogControl.AddMessage("S16F11 Recv Basic Recipe : {0}", m_psRcpID[0].Replace(".JOB", ""));
                else
                    HsmsLogControl.AddMessage("S16F11 Recv Basic Recipe is Null");

                HsmsLogControl.AddMessage("S16F11 Recv RFF : {0}", sRffFilePath);

                //ProcessJob Check
                if (m_psPJobID.Length != 1)
                {
                    plErrorCode = new long[1];
                    plErrorCode[0] = 16;
                    psErrText = new string[1];
                    psErrText[0] = "unavailable";

                    Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                    CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                    return;
                }

                //Recipe Check
                string sRecipeName = string.Empty;
                if (m_psRcpID != null || m_psRcpID.Length == 1)
                    sRecipeName = m_psRcpID[0].Replace(".JOB", "");
                else
                {
                    plErrorCode = new long[1];
                    plErrorCode[0] = 7;
                    psErrText = new string[1];
                    psErrText[0] = "invalid attribute value";
                    HsmsLogControl.AddMessage("S16F11 Fail Result : invalid attribute value");
                    Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                    CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                    return;
                }

                if (sRecipeName != CimConfigurator.DailyMonitoringFlag.ToUpper() &&
                    sRffName != CimConfigurator.DailyMonitoringFlag.ToUpper())
                {
                    if (!CimMessageProcessor.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName))
                    {
                        plErrorCode = new long[1];
                        plErrorCode[0] = 99;
                        psErrText = new string[1];
                        psErrText[0] = "Not Exist Recipe";
                        HsmsLogControl.AddMessage("S16F11 Fail Result : Not Exist Recipe");
                        Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                        CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                        return;
                    }

                    //Rff File Check
                    sRffFilePath.Replace("\\", @"\");
                    if (!File.Exists(sRffFilePath))
                    {
                        plErrorCode = new long[1];
                        plErrorCode[0] = 100;
                        psErrText = new string[1];
                        psErrText[0] = "Not Exist Rff File";
                        HsmsLogControl.AddMessage("S16F11 Fail Result : Not Exist Rff File");
                        Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                        CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                        return;
                    }

                    //SlotMap Check
                    foreach (string sSlotInfo in m_psSlotInfo)
                    {
                        MatchCollection matches = Regex.Matches(sSlotInfo, "1");

                        if (matches.Count != 24)
                        {
                            plErrorCode = new long[1];
                            plErrorCode[0] = 10;
                            psErrText = new string[1];
                            psErrText[0] = "validation error";
                            HsmsLogControl.AddMessage("S16F11 Fail Result : validation error");
                            Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                            CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                            return;
                        }
                    }
                }

                //Recipe Rff Summary
                RffSummary rffSummary = new RffSummary(CimMessageProcessor.CimConfigurator.HostProcessRecipePath, CimConfigurator);
                string sRecipeFilePath = CimMessageProcessor.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName) ?
                    CimMessageProcessor.RecipeManager.BasicRecipeNameToData[sRecipeName].RecipeFilePath : "MONITORING";

                //Recipe File Path Check
                if (string.IsNullOrEmpty(sRecipeFilePath))
                {
                    plErrorCode = new long[1];
                    plErrorCode[0] = 99;
                    psErrText = new string[1];
                    psErrText[0] = "Not Exist Recipe";
                    HsmsLogControl.AddMessage("S16F11 Fail Result : Not Exist Recipe");
                    Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                    CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                    return;
                }

                //20230602 jkseo, ICM 전송 및 Recipe File명에 붙일 DateTime 
                string sDateTime = Common.GenerateFileTime(DateTime.Now);

                ProcessRecipeData prd = rffSummary.SummaryRecipeToData(sRecipeFilePath, sRecipeName, sRffFilePath, m_psPJobID[0], sDateTime, EmAccessMode.Auto);
                if (prd == null)
                {
                    plErrorCode = new long[1];
                    plErrorCode[0] = 101;
                    psErrText = new string[1];
                    psErrText[0] = "Fail Create Process Recipe";
                    HsmsLogControl.AddMessage("S16F11 Fail Result : Fail Create Recipe");
                    Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                    CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                    return;
                }
                else if (prd.RecipeTime == "-1")
                {
                    plErrorCode = new long[1];
                    plErrorCode[0] = long.Parse(prd.RecipeTime);
                    psErrText = new string[1];
                    psErrText[0] = "Defect Point Error!!";
                    HsmsLogControl.AddMessage("S16F11 Fail Result : Fail Create Recipe");
                    Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, 1, 1, plErrorCode, psErrText);
                    CimMessageProcessor.SendCustomNack(plErrorCode, psErrText);
                }

                //ProcessRecipeCreate Command
                //List<PlcAddr> listProcessRecipeStruct = CimConfigurator.HashShareStruct["L1_PROCESS_RECIPE_CMD_STRUCT"];
                //ShareMem.SetAscii(listProcessRecipeStruct[0], prd.RecipeName);
                //ShareMem.SetByte(listProcessRecipeStruct[1], 1);
                //
                //ShareMem.SetBit(CimConfigurator.HashShareKeyToShareData["L1_B_RECIPE_CMD_COMMAND"], true);

                Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, lResult, lErrCnt, plErrorCode, psErrText);

                HsmsLogControl.AddMessage(string.Format("Received {0}", "PJReqVerify"));
                //S16F12 and S16F16
                //Xgem.PJRspVerify(m_nMsgID, m_nPJobCount, m_psPJobID, lResult, lErrCnt, plErrorCode, psErrText);
                //(string sPJobID, long nMtrlFormat, long nAutoStart, long nMtrlOrder, long nMtrlCount, string[] psMtrlID, string[] psSlotInfo, long nRcpMethod, string sRcpID, long nRcpParCount, string[] psRcpParName, string[] psRcpParValue);
                //Xgem.PJReqCreate(m_psPJobID[0], m_pnMtrlFormat[0], m_pnAutoStart[0], m_pnMtrlOrder[0], m_pnMtrlCount[0], m_psMtrlID,
                //    m_psSlotInfo, m_pnRcpMethod[0], m_psRcpID[0], 1, m_psRcpParName, m_psRcpParValue);

                for (int i = 0; i < m_nPJobCount; i++)
                {
                    HsmsLogControl.AddMessage(string.Format("PRJobID={0}", m_psPJobID[i]));
                    HsmsLogControl.AddMessage(string.Format("MtrlFormat={0}", m_pnMtrlFormat[i]));
                    HsmsLogControl.AddMessage(string.Format("AutoStart={0}", m_pnAutoStart[i]));
                    HsmsLogControl.AddMessage(string.Format("MtrlOrder={0}", m_pnMtrlOrder[i]));
                    HsmsLogControl.AddMessage(string.Format("MtrlCount={0}", m_pnMtrlCount[i]));


                    nMtrlCnt = (int)m_pnMtrlCount[i];
                    string sCarrierID = "";
                    string sSlotInfo = "";

                    for (int j = 0; j < nMtrlCnt; j++)
                    {
                        sCarrierID = m_psMtrlID[nIndex1];
                        sSlotInfo = m_psSlotInfo[nIndex1];

                        HsmsLogControl.AddMessage(string.Format("CarrierID={0}", sCarrierID));
                        HsmsLogControl.AddMessage(string.Format("SlotInfo={0}", sSlotInfo));

                        nIndex1++;
                    }

                    HsmsLogControl.AddMessage(string.Format("RcpMethod={0}", m_pnRcpMethod[i]));

                    nRcpParCnt = (int)m_pnRcpParCount[i];
                    string sParname = "";
                    string sParValue = "";

                    for (int k = 0; k < nRcpParCnt; k++)
                    {
                        sParname = m_psRcpParName[nIndex2];
                        sParValue = m_psRcpParValue[nIndex2];

                        HsmsLogControl.AddMessage(string.Format("Parname={0}", sParname));
                        HsmsLogControl.AddMessage(string.Format("ParValue={0}", sParValue));

                        nIndex2++;
                    }
                }
                CimMessageProcessor.JobControl.ProcessJobChange();
                CimMessageProcessor.CimConfigurator.ProcessJobID = m_psPJobID[0];
                string sProcessRecipeName = prd.RecipeName.Replace(".xml", string.Empty);

                FileInfo ProcessRecipe = new FileInfo(prd.RecipeFilePath);
                ProcessRecipeData rProcessRecipeData = CimConfigurator.RecipeManager.ReadProcessRecipe(ProcessRecipe);
                if (!CimConfigurator.RecipeManager.ProcessRecipeNameToData.ContainsKey(sProcessRecipeName))
                    CimConfigurator.RecipeManager.ProcessRecipeNameToData.Add(sProcessRecipeName, rProcessRecipeData);
                else
                    CimConfigurator.RecipeManager.ProcessRecipeNameToData[sProcessRecipeName] = rProcessRecipeData;

                System.Threading.Thread.Sleep(100);
                m_MainControl.RecipeControl.ProcessRecipeListChange();

                foreach (string _key in CimMessageProcessor.CurrentProcessJobIDs.Keys)
                {
                    if (CimMessageProcessor.CurrentProcessJobIDs[_key] != CimMessageProcessor.ProcessJobProcesser.GetPortIDByPjID(m_psPJobID[0]))
                    {
                        m_pnAutoStart[0] = 0;
                    }
                }

                List<PlcAddr> listWriteDatas = m_MainControl.HashShareStruct[ShareNames.DEF_HOST_PROCESS_JOB_DOWNLOAD_STRUCT];
                PlcAddr WriteBit = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_PROCESS_JOB_DOWNLOAD_COMMAND];

                ShareMem.SetAscii(listWriteDatas[0], m_psPJobID[0]);
                ShareMem.SetAscii(listWriteDatas[1], m_psMtrlID[0]);
                ShareMem.SetAscii(listWriteDatas[2], sProcessRecipeName);
                ShareMem.SetByte(listWriteDatas[3], Convert.ToByte(m_pnAutoStart[0]));
                double dThreadShold = 100;
                if (!double.TryParse(m_psRcpParValue[1], out dThreadShold))
                    HsmsLogControl.AddMessage("S16F11 Fail Result : Exception ThreadShold Value!!!!");
                ShareMem.SetDouble(listWriteDatas[4], dThreadShold);
                ShareMem.SetAscii(listWriteDatas[5], sDateTime);
                ShareMem.SetByte(listWriteDatas[6], CimMessageProcessor.ProcessJobProcesser.GetPortIDByPjID(m_psPJobID[0]));

                ShareMem.SetBit(WriteBit, true);

                CimConfigurator.ProcessRecipe = rProcessRecipeData.RecipeName;
            }
            catch (System.Exception ex)
            {
                HsmsLogControl.AddMessage("S16F11 Fail Result : {0}", ex.ToString());
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
