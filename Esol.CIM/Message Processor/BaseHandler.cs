﻿#region Class Using
using System.Collections.Generic;
using Esol.ShareMemory;
using Esol.ShareTrxControl;
using GEM_XGem300Pro;
using Esol.LogControl;
#endregion

namespace Esol.CIM
{
    public abstract class BaseHandler
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        protected Trx m_ShareIncomingData;
        /// <summary>
        /// 
        /// </summary>
        protected MainControl m_MainControl;
        /// <summary>
        /// 
        /// </summary>
        protected List<object> m_listHsmsMessages;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        protected MainControl MainControl
        {
            get
            {
                return m_MainControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected CimConfigurator CimConfigurator
        {
            get
            {
                return m_MainControl.CimConfigurator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected VirtualMemory ShareMem
        {
            get
            {
                return m_MainControl.ShareMem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected XGem300ProNet Xgem
        {
            get
            {
                return m_MainControl.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected CimMessageProcessor CimMessageProcessor
        {
            get
            {
                return m_MainControl.CimMessageProcessor;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected LogManager LogManager
        {
            get
            {
                return m_MainControl.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<object> HsmsDatas
        {
            get
            {
                return m_listHsmsMessages;
            }
            set
            {
                m_listHsmsMessages = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Trx ShareIncomingData
        {
            get
            {
                return m_ShareIncomingData;
            }
            set
            {
                m_ShareIncomingData = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_MainControl.HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected ControlState HsmsState
        {
            get
            {
                return m_MainControl.NotificationBoard.HsmsState;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public BaseHandler(MainControl maincontrol)
        {
            m_MainControl = maincontrol;
        }
        #endregion

        #region Class Protected
        /// <summary>
        /// 
        /// </summary>
        public virtual void Execute()
        {
            ReadIncomingMessage();
            HandleMessage();
        }
        /// <summary>
        /// 
        /// </summary>
        protected abstract void ReadIncomingMessage();
        /// <summary>
        /// 
        /// </summary>
        protected abstract void HandleMessage();
        #endregion
    }
}
