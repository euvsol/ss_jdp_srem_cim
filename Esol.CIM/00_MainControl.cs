﻿#region Class Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Esol.LogControl;
using Esol.Components;
using System.Threading;
using GEM_XGem300Pro;
using System.IO;
using System.Diagnostics;
using Esol.ShareMemory;
using Esol.ShareTrxControl;
using System.Runtime.InteropServices;
using System.Windows.Forms.VisualStyles;
#endregion

namespace Esol.CIM
{
    public partial class MainControl : Form
    {
        #region Class Struct
        /// <summary>
        /// 
        /// </summary>
        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }
        #endregion

        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private const string DEF_CONNECT_TEXT = "Connected";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_DISCONNECT_TEXT = "Disconnected";
        /// <summary>
        /// 
        /// </summary>
        private const string DEF_CLOSED_TEXT = "Closed";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_PROGRAM_EXIT_TEXT = "프로그램을 종료 하시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_START_UP_CONFIG_PATH = @"\_StartUp.xml";
        /// <summary>
        /// 
        /// </summary>
        private const int WM_COPYDATA = 0x4A;
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private object m_syncShare;
        /// <summary>
        /// 
        /// </summary>
        private object m_syncObject;
        /// <summary>
        /// 
        /// </summary>
        private object m_syncHsmsObject;
        /// <summary>
        /// 
        /// </summary>
        private object m_SyncShareObject;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sArrayBitKeys;
        /// <summary>
        /// 
        /// </summary>
        private Point mouse_Offset;
        /// <summary>
        /// 
        /// </summary>
        private int m_iFormIndex;
        /// <summary>
        /// 
        /// </summary>
        //private ImageList m_listImage;
        /// <summary>
        /// 
        /// </summary>
        private EcidControl m_EcidControl;
        /// <summary>
        /// 
        /// </summary>
        private SvidControl m_SvidControl;
        /// <summary>
        /// 
        /// </summary>
        private JobControl m_JobControl;
        /// <summary>
        /// 
        /// </summary>
        private AlarmControl m_AlarmControl;
        /// <summary>
        /// 
        /// </summary>
        private RecipeControl m_RecipeControl;
        /// <summary>
        /// 
        /// </summary>
        private ConfigControl m_ConfigControl;
        /// <summary>
        /// 
        /// </summary>
        private LoadingControl m_LoadingControl;
        /// <summary>
        /// 
        /// </summary>
        private HsmsLogControl m_HsmsLogControl;
        /// <summary>
        /// 
        /// </summary>
        private HistoryControl m_HistoryControl;
        /// <summary>
        /// 
        /// </summary>
        private System.Threading.Timer m_timerHsmsImageChange;
        /// <summary>
        /// 
        /// </summary>
        private System.Threading.Timer m_timerXgemImageChange;
        /// <summary>
        /// 
        /// </summary>
        private XGem300ProNet m_XGem = null;
        /// <summary>
        /// 
        /// </summary>
        private CimConfigurator m_Cimconfigurator;
        /// <summary>
        /// 
        /// </summary>
        private CimMessageProcessor m_CimMessageProcessor;
        /// <summary>
        /// 
        /// </summary>
        private NotificationBoard m_controlNotificationBoard;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadShareData;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadShareDataChanged;
        /// <summary>
        /// 
        /// </summary>
        private Queue<PlcAddr> m_queueShare;
        /// <summary>
        /// 
        /// </summary>
        private Queue<PlcAddr> m_queueShareDataBunchData;
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx m_ShareTrx;
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory m_Sharemem;
        /// <summary>
        /// 
        /// </summary>
        private PlcAddr m_AliveBit;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bAliveICM;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bAliveITS;
        #endregion

        #region Class Event Methods Membersh
        /// <summary>
        /// 
        /// </summary>
        private delegate void ParameterlessEvendHandler();
        /// <summary>
        /// 
        /// </summary>
        private ParameterlessEvendHandler m_showViewerHandler;
        /// <summary>
        /// 
        /// </summary>
        public event ShareEventHandler ShareDataChanged;
        /// <summary>
        /// 
        /// </summary>
        public delegate void ShareEventHandler(PlcAddr Addr);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadLabel(Label label, string str);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadRoundMultiUsePanelVisible(RoundPictureBox panel, bool visible);
        /// <summary>
        /// 
        /// </summary>
        public delegate void ReloadFormControlShow(FormControl form, bool bShow, bool bModal = false);
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_XGem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public CimMessageProcessor CimMessageProcessor
        {
            get
            {
                return m_CimMessageProcessor;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public RecipeControl RecipeControl
        {
            get
            {
                return m_RecipeControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, Trx> HashShareTrx
        {
            get
            {
                return m_ShareTrx.hashShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<PlcAddr>> HashShareStruct
        {
            get
            {
                return m_ShareTrx.hashShareStruct;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, PlcAddr> HashShareKeyToShareData
        {
            get
            {
                return m_ShareTrx.hashShareKeyToShareData;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public VirtualMemory ShareMem
        {
            get
            {
                return m_Sharemem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ShareTrx ShareTrx
        {
            get
            {
                return m_ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_Cimconfigurator.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TerminalControl TerminalControl
        {
            get
            {
                return m_Cimconfigurator.TerminalManager.TerminalControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public NotificationBoard NotificationBoard
        {
            get
            {
                return m_controlNotificationBoard;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public EcidControl EcidControl
        {
            get
            {
                return m_EcidControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public CimConfigurator CimConfigurator
        {
            get
            {
                return m_Cimconfigurator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public SvidControl SvidControl
        {
            get
            {
                return m_SvidControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HistoryControl HistoryControl
        {
            get
            {
                return m_HistoryControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public JobControl JobControl
        {
            get
            {
                return m_JobControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool AliveICM
        {
            get
            {
                return m_bAliveICM;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool AliveITS
        {
            get
            {
                return m_bAliveITS;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<string> SelectCarrierID
        {
            get;
            set;
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public MainControl()
        {
            try
            {
                InitializeComponent();
                this.DoubleBuffered = true;

                m_syncShare = new object();
                m_syncObject = new object();
                m_syncHsmsObject = new object();
                m_SyncShareObject = new object();
                m_bAliveICM = false;
                m_bAliveITS = false;

                m_XGem = new XGem300ProNet();
                m_queueShare = new Queue<PlcAddr>();
                m_queueShareDataBunchData = new Queue<PlcAddr>();
                m_showViewerHandler = new ParameterlessEvendHandler(TerminalShow);

                m_Cimconfigurator = new CimConfigurator(this);
                //Path고정 해제
                m_Cimconfigurator.Initialize(System.Windows.Forms.Application.StartupPath + DEF_START_UP_CONFIG_PATH);
                m_picHsmsTransfer.Visible = false;
                m_picXgemTransfer.Visible = false;
                m_iFormIndex = 1;
                SelectCarrierID = new List<string>();

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += BackgroundWorking;
                worker.RunWorkerCompleted += BackgroundCompleted;
                worker.RunWorkerAsync();

                InitializeTitleBarEvent();
                //ImageGalleryList imageGalleryList = new Esol.Components.ImageGalleryList();
                //m_listImage = Esol.Components.ImageGalleryList.SmallImageList;

                Initialize();

                InitializeDatas();

                OnButtonClick(null);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void Initialize()
        {
            /*
            Initi순서
            INITIALIZE
            CREATE_FORM,
            CREATE_TITLE,
            SHAREMEM_DRIVER,
            HSMS_DRIVER,
            INITIAL_FORM,
            CONFIG,
            THREAD,
            */
            m_timerHsmsImageChange = new System.Threading.Timer(TimeOutTimer_Hsms, null, Timeout.Infinite, Timeout.Infinite);
            m_timerXgemImageChange = new System.Threading.Timer(TimeOutTimer_Share, null, Timeout.Infinite, Timeout.Infinite);

            if (m_LoadingControl == null)
                System.Threading.Thread.Sleep(500);

            m_LoadingControl.LoadingStep = LoadingStep.INITIALIZE;

            CreateFormControl();
            m_LoadingControl.LoadingStep = LoadingStep.CREATE_FORM;

            InitializeNotificationBoard();
            m_LoadingControl.LoadingStep = LoadingStep.CREATE_TITLE;

            InitializeShareDriver();
            m_LoadingControl.LoadingStep = LoadingStep.SHAREMEM_DRIVER;
            InitializeXgem();
            m_LoadingControl.LoadingStep = LoadingStep.HSMS_DRIVER;

            InitializeFormControl();
            m_LoadingControl.LoadingStep = LoadingStep.INITIAL_FORM;
            m_ConfigControl.InitializeConfig();
            m_LoadingControl.LoadingStep = LoadingStep.CONFIG;

            InitializeCreateThread();

            m_LoadingControl.LoadingStep = LoadingStep.THREAD;

            m_threadShareDataChanged.Start();
            m_threadShareData.Start();
            m_AliveTimer.Start();

            m_Cimconfigurator.SvidManager.OnFdcThreadStart();

            m_Cimconfigurator.Xgem.GEMReqAllECInfo();
        }
        /// <summary>
        /// 
        /// </summary>
        private void LoadingControlShow()
        {
            try
            {
                m_LoadingControl = new LoadingControl();
                m_LoadingControl.ShowDialog();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDatas()
        {
            try
            {
                long[] sVids = new long[3] { 14, 15, 33 };
                string[] sValues = new string[3] { CimConfigurator.MDLN, CimConfigurator.SoftVer, "1" };
                CimConfigurator.SetVids(sVids, sValues);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void BackgroundWorking(object sender, DoWorkEventArgs e)
        {
            LoadingControlShow();
        }
        /// <summary>
        /// 
        /// </summary>
        private void BackgroundCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_LoadingControl.Close();
            m_LoadingControl.Dispose();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeTitleBarEvent()
        {
            try
            {
                m_roundTitleBar.MouseMoveChaged += OnMouseMoveChaged;
                m_roundTitleBar.MouseDownChaged += OnMouseDownChaged;
                m_roundTitleBar.FormWindowStateChanged += OnFormWindowsStateChanged;
                m_roundTitleBar.FormWondowCloseChanged += new Esol.Components.RoundTitleBar.FormWindowCloseEventHandler(OnFormWindowsClosed);

                this.Icon = m_roundTitleBar._Round_Icon;
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeXgem()
        {
            try
            {
                m_CimMessageProcessor = new CimMessageProcessor(this);

                OnXgemInitialize();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeNotificationBoard()
        {
            m_controlNotificationBoard = new NotificationBoard(this);
            m_controlNotificationBoard.Initialize();
            m_panelNotification.Controls.Add(m_controlNotificationBoard);
            m_controlNotificationBoard.Show();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeShareDriver()
        {
            try
            {
                m_Sharemem = new VirtualMemory(CimConfigurator.ShareMemoryName, CimConfigurator.ShareMemorySize);
                m_Sharemem.Open();

                m_ShareTrx = new ShareTrx(CimConfigurator.LogPath, m_Sharemem);
                //Path고정 해제
                m_ShareTrx.InitializeDataBit(CimConfigurator.DataBitPath);
                m_ShareTrx.InitializeDataWord(CimConfigurator.DataWordPath);
                m_ShareTrx.InitializeDataStruct(CimConfigurator.DataWrodStructPath);
                m_ShareTrx.InitializeDataTrx(CimConfigurator.DataTrxPath);

                List<string> listBitNames = new List<string>();

                foreach (PlcAddr addr in m_ShareTrx.hashShareKeyToShareData.Values)
                {
                    if (addr.ValueType == PlcValueType.BIT)
                    {
                        listBitNames.Add(addr.AddrName);

                        if (addr.AddrName.Contains(ClassNames.DEF_CIM_LOCAL_NO))
                            m_Sharemem.SetBit(addr, false);
                    }
                }

                m_sArrayBitKeys = new string[listBitNames.Count];
                listBitNames.CopyTo(m_sArrayBitKeys, 0);

                m_AliveBit = m_ShareTrx.hashShareKeyToShareData[ShareNames.DEF_HOST_ALIVE_NAME];

                PlcAddr ProcessState = HashShareKeyToShareData[ShareNames.DEF_PROC_STATE];
                m_controlNotificationBoard.ProcState = (ProcessingState)ShareMem.GetByte(ProcessState);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void CreateFormControl()
        {
            try
            {
                //각 Control roundWindow에 추가 JHKIM
                m_HsmsLogControl = new HsmsLogControl();
                m_HsmsLogControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_HsmsLogControl);

                m_RecipeControl = new RecipeControl();
                m_RecipeControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_RecipeControl);

                m_EcidControl = new EcidControl();
                m_EcidControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_EcidControl);

                m_SvidControl = new SvidControl();
                m_SvidControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_SvidControl);

                m_JobControl = new JobControl();
                m_JobControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_JobControl);

                m_AlarmControl = new AlarmControl();
                m_AlarmControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_AlarmControl);

                m_HistoryControl = new HistoryControl();
                m_HistoryControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_HistoryControl);

                m_ConfigControl = new ConfigControl();
                m_ConfigControl.Dock = DockStyle.Fill;
                m_roundWindow.AddControl(m_ConfigControl);

                foreach (object control in m_roundWindow.Controls)
                    if (control is FormControl fc)
                        fc.MainControl = this;
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeFormControl()
        {
            try
            {
                m_RecipeControl.Initialize();
                m_EcidControl.Initialize();
                m_SvidControl.Initialize();
                m_AlarmControl.Initialize();
                m_HistoryControl.Initialize();
                m_ConfigControl.Initialize();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeCreateThread()
        {
            try
            {
                m_queueShareDataBunchData = new Queue<PlcAddr>();

                m_threadShareDataChanged = new Thread(new ThreadStart(OnShareDataChanged))
                { IsBackground = true };

                m_threadShareData = new Thread(new ThreadStart(OnShareMessageProcessorThread))
                { IsBackground = true };
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void HsmsStateChange(string sText)
        {
            LabelCrossThread(m_lbHsmsConnectState, sText);
        }
        /// <summary>
        /// 
        /// </summary>
        public void TerminalShow()
        {
            try
            {
                if (TerminalControl.InvokeRequired)
                    TerminalControl.Invoke(m_showViewerHandler);
                else
                {
                    if (TerminalControl.Visible)
                        TerminalControl.Focus();
                    else
                        TerminalControl.Show();

                    TerminalControl.TopMost = true;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnHsmsDataChanged()
        {
            RoundMultiUsePanelCrossThread(m_picHsmsTransfer, true);
            m_timerHsmsImageChange.Change(300, Timeout.Infinite);
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnXgemDataChanged()
        {
            RoundMultiUsePanelCrossThread(m_picXgemTransfer, true);
            m_timerXgemImageChange.Change(300, Timeout.Infinite);
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            try
            {
                Thread.Sleep(1);

                switch (m.Msg)
                {
                    case WM_COPYDATA:
                        COPYDATASTRUCT cds = (COPYDATASTRUCT)m.GetLParam(typeof(COPYDATASTRUCT));
                        switch (cds.lpData)
                        {
                            case "APP_SHOW":
                                this.Show();
                                this.Focus();
                                break;
                            case "APP_HIDE":
                                this.Hide();
                                break;
                        }
                        break;
                    default:
                        base.WndProc(ref m);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            OfflineForm form = new OfflineForm(DEF_PROGRAM_EXIT_TEXT, CimConfigurator.Password);
            LogManager.ButtonWriteLog("Windows Form Closing");
            DialogResult res = form.ShowDialog();
            LogManager.ButtonWriteLog("Windows Form Closing : " + res);

            if (DialogResult.OK != res)
            {
                LogManager.ButtonWriteLog("OFFLINE PASSWORD NOT MATCH");
                return;
            }

            m_controlNotificationBoard.HsmsStateChange = ClassNames.DEF_OFFLINE_TEXT;

            Thread.Sleep(3000);

            PlcAddr addr = HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_STATE_COMMAND];
            ShareMem.SetBit(addr, false);

            m_CimMessageProcessor.OnClosingControlStateChange();

            HsmsLogControl.AddMessage("Windows Form Closed");

            m_CimMessageProcessor.Stop();

            Thread.Sleep(500);

            Application.ExitThread();
            Environment.Exit(Environment.ExitCode);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseMoveChaged(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    Point mousePos = Control.MousePosition;
                    mousePos.Offset(mouse_Offset.X, mouse_Offset.Y);

                    this.Location = mousePos;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseDownChaged(object sender, MouseEventArgs e)
        {
            mouse_Offset = new Point(-e.X - 34, -e.Y - 3);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFormWindowsStateChanged(FormWindowState state)
        {
            this.WindowState = state;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFormWindowsClosed()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        private void OnButtonClick(object sender)
        {
            try
            {
                RoundTitleButton button = (RoundTitleButton)sender;

                int iIndex = 0;

                if (sender != null)
                {
                    if (button._Round_Index == m_iFormIndex)
                        return;

                    foreach (object control in this.Controls)
                        if (control is RoundTitleButton rtb)
                            rtb._Round_Selected = false;

                    button._Round_Selected = true;
                    iIndex = button._Round_Index;
                }
                else
                {
                    foreach (object control in this.Controls)
                    {
                        if (control is RoundTitleButton rtb)
                        {
                            if (rtb._Round_Index == 0)
                            {
                                rtb._Round_Selected = true;
                                iIndex = 0;
                                break;
                            }
                            else
                                rtb._Round_Selected = false;
                        }
                    }
                }

                m_roundWindow.SendToBack();
                m_roundWindow.Show(iIndex);

                m_iFormIndex = iIndex;

                if (button != null)
                    LogManager.ButtonWriteLog("Main title : " + button._Round_TitleText);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimeOutTimer_Hsms(object state)
        {
            RoundMultiUsePanelCrossThread(m_picHsmsTransfer, false);
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimeOutTimer_Share(object state)
        {
            RoundMultiUsePanelCrossThread(m_picXgemTransfer, false);
        }
        #endregion

        #region Class Utility Methods
        /// <summary>
        /// 
        /// </summary>
        private object Clone(object oValue)
        {
            object oReturn = oValue;

            return oReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        private void LabelCrossThread(Label label, string str)
        {
            try
            {
                if (label.InvokeRequired)
                {
                    ReloadLabel reload = new ReloadLabel(LabelCrossThread);
                    this.Invoke(reload, new object[] { label, str });
                }
                else
                    label.Text = str;
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void RoundMultiUsePanelCrossThread(RoundPictureBox panel, bool bVisible)
        {
            try
            {
                if (panel.InvokeRequired)
                {
                    ReloadRoundMultiUsePanelVisible reload = new ReloadRoundMultiUsePanelVisible(RoundMultiUsePanelCrossThread);
                    this.Invoke(reload, new object[] { panel, bVisible });
                }
                else
                    panel.Visible = bVisible;
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static void FormControlShowCrossThread(FormControl form, bool bShow, bool bModal = false)
        {
            try
            {
                if (form.InvokeRequired)
                {
                    ReloadFormControlShow reload = new ReloadFormControlShow(FormControlShowCrossThread);
                    form.Invoke(reload, new object[] { form, bShow, bModal });
                }
                else
                {
                    if (bShow)
                    {
                        if (form.Visible)
                            form.Focus();
                        else
                        {
                            if (bModal)
                                form.ShowDialog();
                            else
                                form.Show();
                        }
                    }
                    else
                        form.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        #endregion

        #region Class Thread Handlers
        /// <summary>
        /// 
        /// </summary>
        private void OnShareDataChanged()
        {
            try
            {
                Dictionary<string, PlcAddr> hashPlcData = m_ShareTrx.hashShareKeyToShareData;

                while (true)
                {
                    Thread.Sleep(1);

                    for (int i = 0; i < m_sArrayBitKeys.Length; i++)
                    {
                        PlcAddr plcAddr = hashPlcData[m_sArrayBitKeys[i]];

                        bool bReadValue = (bool)Clone(m_Sharemem.GetBit(plcAddr));

                        if (plcAddr.vBit != bReadValue)
                        {
                            hashPlcData[m_sArrayBitKeys[i]].vBit = bReadValue;

                            if (plcAddr.AddrName == ShareNames.DEF_HOST_ALIVE_NAME)
                                continue;

                            m_queueShareDataBunchData.Enqueue(plcAddr);

                            lock (m_SyncShareObject)
                                Monitor.Pulse(m_SyncShareObject);

                            OnXgemDataChanged();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnShareMessageProcessorThread()
        {
            try
            {
                while (true)
                {
                    PlcAddr addr = null;

                    lock (m_SyncShareObject)
                    {
                        if (m_queueShareDataBunchData.Count > 0)
                            addr = m_queueShareDataBunchData.Dequeue();
                        else
                            Monitor.Wait(m_SyncShareObject);

                        if (addr != null) ShareDataChanged(addr);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnAliveTick(object sender, EventArgs e)
        {
            try
            {
                string[] arrayAliveKeys = new string[2];
                m_CimMessageProcessor.HashEqToAliveTime.Keys.CopyTo(arrayAliveKeys, 0);
                //EQ Alive Bit Check
                foreach (string sKey in arrayAliveKeys)
                {
                    DateTime bitTime = m_CimMessageProcessor.HashEqToAliveTime[sKey].AddSeconds(5);

                    if (DateTime.Now.Ticks - bitTime.Ticks >= 0)
                    {
                        switch (sKey)
                        {
                            case ClassNames.DEF_ICM:
                                m_bAliveICM = false;
                                break;
                            case ClassNames.DEF_ITS:
                                m_bAliveITS = false;
                                break;
                        }
                    }
                    else
                    {
                        switch (sKey)
                        {
                            case ClassNames.DEF_ICM:
                                m_bAliveICM = true;
                                break;
                            case ClassNames.DEF_ITS:
                                m_bAliveITS = true;
                                break;
                        }
                    }
                }

                //Host Alive Bit
                switch (m_Sharemem.GetByte(m_AliveBit))
                {
                    case 1:
                        m_Sharemem.SetBit(m_AliveBit, false);
                        break;
                    case 0:
                        m_Sharemem.SetBit(m_AliveBit, true);
                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnDataTimer(object sender, EventArgs e)
        {

        }
        #endregion

        #region Class Xgem mehtods
        /// <summary>
        /// 
        /// </summary>
        private void OnXgemInitialize()
        {
            try
            {
                long nReturn = 0;
                //Path고정 해제
                string sCfg = System.Windows.Forms.Application.StartupPath + "\\XgemConfig.cfg";

                if (!File.Exists(sCfg))
                {
                    MessageBox.Show("Xgem Config 파일이 없습니다. Xgm에 연결 할 수 없습니다.\n프로그램을 종료합니다.");
                    this.Close();
                    m_CimMessageProcessor.Stop();
                    Application.ExitThread();
                    Environment.Exit(Environment.ExitCode);
                    return;
                }

                if ((nReturn = m_XGem.Initialize(sCfg)) != 0)
                {
                    m_HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to initialize XGem ({0})", nReturn);
                    MessageBox.Show("Xgem Initialize가 실패 하였습니다.\n프로그램을 종료합니다.");
                    this.Close();
                    m_CimMessageProcessor.Stop();
                    Application.ExitThread();
                    Environment.Exit(Environment.ExitCode);
                    return;
                }

                LabelCrossThread(m_lbXgemConnectState, DEF_DISCONNECT_TEXT);
                m_HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem initialized successfully ({0})", nReturn);

                OnXgemStartStop(null, null);

                m_XGem.Start();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Xgem Event mehtods
        /// <summary>
        /// 
        /// </summary>
        private void OnXgemMouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button != MouseButtons.Right)
                    return;

                long nReturn = 0;

                Process[] processes = Process.GetProcessesByName("Xgem");

                if (processes.Length == 0)
                {
                    LogManager.ButtonWriteLog("XgemMouseDown Start");

                    if (MessageBox.Show("Xgem 프로그램을 실행 하시겠습니까?", "Xgem Process Start", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        InitializeXgem();
                        LogManager.ButtonWriteLog("Xgem Proces Start");
                    }
                }
                else
                {
                    LogManager.ButtonWriteLog("XgemMouseDown Close");
                    if (MessageBox.Show("Xgem 프로그램을 종료 하시겠습니까?", "Xgem Process Close", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        if ((nReturn = m_XGem.Close()) == 0)
                        {
                            LabelCrossThread(m_lbXgemConnectState, DEF_CLOSED_TEXT);
                            m_HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem closed successfully ({0})", nReturn);
                            LogManager.ButtonWriteLog("Xgem Proces Close");
                        }
                        else
                        {
                            m_HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to close XGem ({0})", nReturn);
                            LogManager.ButtonWriteLog("Xgem Proces Close Fail");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnXgemStartStop(object sender, EventArgs e)
        {
            try
            {
                LogManager.ButtonWriteLog("Xgem Driver Start Stop");

                switch (m_lbXgemConnectState.Text)
                {
                    case DEF_CLOSED_TEXT:
                        MessageBox.Show("Xgem Driver이 닫혀 있습니다.");
                        LogManager.ButtonWriteLog("Xgem Driver이 닫혀 있습니다.");
                        return;
                    case DEF_CONNECT_TEXT:
                        long nReturn = 0;
                        LogManager.ButtonWriteLog("Xgem Driver Stop");
                        if ((nReturn = m_XGem.Stop()) == 0)
                        {
                            LabelCrossThread(m_lbXgemConnectState, DEF_DISCONNECT_TEXT);
                            m_HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem stopped successfully ({0})", nReturn);
                            LogManager.ButtonWriteLog("Xgem Driver Stop Succese");
                        }
                        else
                        {
                            m_HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to stop XGem ({0})", nReturn);
                            LogManager.ButtonWriteLog("Xgem Driver Stop Fail");
                        }
                        return;
                    default:
                        LogManager.ButtonWriteLog("Xgem Driver Start");

                        if ((nReturn = m_XGem.Start()) == 0)
                        {
                            LabelCrossThread(m_lbXgemConnectState, DEF_CONNECT_TEXT);
                            m_HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem started successfully ({0})", nReturn);

                            LogManager.ButtonWriteLog("Xgem Driver Start Seccese");
                        }
                        else
                        {
                            m_HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to start XGem ({0})", nReturn);
                            LogManager.ButtonWriteLog("Xgem Driver Start Fail");
                        }
                        return;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            m_CimMessageProcessor.SendCustomNack(new long[1] { 99 }, new string[1] { "TEST COMDe" });
        }
    }
}
