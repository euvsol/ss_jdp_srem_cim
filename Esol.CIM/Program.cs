﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Esol.CIM
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Mutex dup = new Mutex(true, "ESOL.CIM", out bool createNew);

            if(createNew)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainControl());
            }
            else
                MessageBox.Show("이미 CIM 프로그램이 실행중 입니다.");
        }
    }
}