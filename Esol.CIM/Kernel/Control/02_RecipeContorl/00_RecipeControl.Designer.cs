﻿namespace Esol.CIM
{
    partial class RecipeControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecipeControl));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_lblCurrentRecipe = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.roundPictureBox1 = new Esol.Components.RoundPictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_lbViewRecipe = new System.Windows.Forms.Label();
            this.roundPictureBox2 = new Esol.Components.RoundPictureBox();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_dgvRecipeId = new System.Windows.Forms.DataGridView();
            this.RecipeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parameter = new System.Windows.Forms.DataGridViewButtonColumn();
            this.roundPanel2 = new Esol.Components.RoundPanel();
            this.m_dgvRecipeParameter = new System.Windows.Forms.DataGridView();
            this.ParameterName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParemeterValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundPanel3 = new Esol.Components.RoundPanel();
            this.m_btnProcessRecipeCreate = new System.Windows.Forms.Button();
            this.m_cbDefaultRecipe = new System.Windows.Forms.ComboBox();
            this.m_dgvProcessRecipeId = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.m_lbRffFileName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CheckFile = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.roundPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundPictureBox2)).BeginInit();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvRecipeId)).BeginInit();
            this.roundPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvRecipeParameter)).BeginInit();
            this.roundPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvProcessRecipeId)).BeginInit();
            this.SuspendLayout();
            // 
            // m_lblCurrentRecipe
            // 
            this.m_lblCurrentRecipe.AutoSize = true;
            this.m_lblCurrentRecipe.Font = new System.Drawing.Font("굴림", 9F);
            this.m_lblCurrentRecipe.Location = new System.Drawing.Point(191, 12);
            this.m_lblCurrentRecipe.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.m_lblCurrentRecipe.Name = "m_lblCurrentRecipe";
            this.m_lblCurrentRecipe.Size = new System.Drawing.Size(73, 18);
            this.m_lblCurrentRecipe.TabIndex = 8;
            this.m_lblCurrentRecipe.Text = "( none )";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(44, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Current Recipe :";
            // 
            // roundPictureBox1
            // 
            this.roundPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.roundPictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("roundPictureBox1.BackgroundImage")));
            this.roundPictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.roundPictureBox1.Location = new System.Drawing.Point(13, 12);
            this.roundPictureBox1.Name = "roundPictureBox1";
            this.roundPictureBox1.Size = new System.Drawing.Size(24, 24);
            this.roundPictureBox1.TabIndex = 9;
            this.roundPictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(1112, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "View Recipe :";
            // 
            // m_lbViewRecipe
            // 
            this.m_lbViewRecipe.AutoSize = true;
            this.m_lbViewRecipe.Font = new System.Drawing.Font("굴림", 9F);
            this.m_lbViewRecipe.Location = new System.Drawing.Point(1238, 12);
            this.m_lbViewRecipe.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.m_lbViewRecipe.Name = "m_lbViewRecipe";
            this.m_lbViewRecipe.Size = new System.Drawing.Size(73, 18);
            this.m_lbViewRecipe.TabIndex = 8;
            this.m_lbViewRecipe.Text = "( none )";
            // 
            // roundPictureBox2
            // 
            this.roundPictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.roundPictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("roundPictureBox2.BackgroundImage")));
            this.roundPictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.roundPictureBox2.Location = new System.Drawing.Point(1081, 12);
            this.roundPictureBox2.Name = "roundPictureBox2";
            this.roundPictureBox2.Size = new System.Drawing.Size(24, 24);
            this.roundPictureBox2.TabIndex = 9;
            this.roundPictureBox2.TabStop = false;
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "DEFAULT RECIPE LIST";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvRecipeId);
            this.roundPanel1.Location = new System.Drawing.Point(10, 40);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1070, 632);
            this.roundPanel1.TabIndex = 11;
            // 
            // m_dgvRecipeId
            // 
            this.m_dgvRecipeId.AllowUserToAddRows = false;
            this.m_dgvRecipeId.AllowUserToDeleteRows = false;
            this.m_dgvRecipeId.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvRecipeId.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RecipeId,
            this.Version,
            this.Time,
            this.Parameter});
            this.m_dgvRecipeId.Location = new System.Drawing.Point(10, 41);
            this.m_dgvRecipeId.Margin = new System.Windows.Forms.Padding(4);
            this.m_dgvRecipeId.Name = "m_dgvRecipeId";
            this.m_dgvRecipeId.ReadOnly = true;
            this.m_dgvRecipeId.RowTemplate.Height = 23;
            this.m_dgvRecipeId.Size = new System.Drawing.Size(1050, 576);
            this.m_dgvRecipeId.TabIndex = 16;
            this.m_dgvRecipeId.Tag = "1";
            this.m_dgvRecipeId.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnRecipeListCellClick);
            // 
            // RecipeId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.RecipeId.DefaultCellStyle = dataGridViewCellStyle1;
            this.RecipeId.Frozen = true;
            this.RecipeId.HeaderText = "DEFAULT RECIPE ID";
            this.RecipeId.Name = "RecipeId";
            this.RecipeId.ReadOnly = true;
            this.RecipeId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.RecipeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RecipeId.Width = 300;
            // 
            // Version
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.Version.DefaultCellStyle = dataGridViewCellStyle2;
            this.Version.Frozen = true;
            this.Version.HeaderText = "Ver";
            this.Version.Name = "Version";
            this.Version.ReadOnly = true;
            this.Version.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Version.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Version.Width = 70;
            // 
            // Time
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            this.Time.DefaultCellStyle = dataGridViewCellStyle3;
            this.Time.Frozen = true;
            this.Time.HeaderText = "TIME";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Time.Width = 150;
            // 
            // Parameter
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.Parameter.DefaultCellStyle = dataGridViewCellStyle4;
            this.Parameter.Frozen = true;
            this.Parameter.HeaderText = "Show Recipe Parameter";
            this.Parameter.Name = "Parameter";
            this.Parameter.ReadOnly = true;
            this.Parameter.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Parameter.Width = 150;
            // 
            // roundPanel2
            // 
            this.roundPanel2._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel2._Round_Cutting = true;
            this.roundPanel2._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel2._Round_TitleImage = null;
            this.roundPanel2._Round_TitleImageShow = false;
            this.roundPanel2._Round_TitleLabelText = "RECIPE PARAMETER";
            this.roundPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel2.Controls.Add(this.m_dgvRecipeParameter);
            this.roundPanel2.Location = new System.Drawing.Point(1085, 40);
            this.roundPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel2.Name = "roundPanel2";
            this.roundPanel2.Size = new System.Drawing.Size(1070, 1230);
            this.roundPanel2.TabIndex = 12;
            // 
            // m_dgvRecipeParameter
            // 
            this.m_dgvRecipeParameter.AllowUserToAddRows = false;
            this.m_dgvRecipeParameter.AllowUserToDeleteRows = false;
            this.m_dgvRecipeParameter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvRecipeParameter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterName,
            this.ParemeterValue});
            this.m_dgvRecipeParameter.Location = new System.Drawing.Point(10, 40);
            this.m_dgvRecipeParameter.Margin = new System.Windows.Forms.Padding(4);
            this.m_dgvRecipeParameter.Name = "m_dgvRecipeParameter";
            this.m_dgvRecipeParameter.ReadOnly = true;
            this.m_dgvRecipeParameter.RowTemplate.Height = 23;
            this.m_dgvRecipeParameter.Size = new System.Drawing.Size(1050, 1170);
            this.m_dgvRecipeParameter.TabIndex = 16;
            // 
            // ParameterName
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.ParameterName.DefaultCellStyle = dataGridViewCellStyle5;
            this.ParameterName.Frozen = true;
            this.ParameterName.HeaderText = "PARATETER NAME";
            this.ParameterName.Name = "ParameterName";
            this.ParameterName.ReadOnly = true;
            this.ParameterName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ParameterName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ParameterName.Width = 300;
            // 
            // ParemeterValue
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            this.ParemeterValue.DefaultCellStyle = dataGridViewCellStyle6;
            this.ParemeterValue.Frozen = true;
            this.ParemeterValue.HeaderText = "PARAMETER VALUE";
            this.ParemeterValue.Name = "ParemeterValue";
            this.ParemeterValue.ReadOnly = true;
            this.ParemeterValue.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ParemeterValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ParemeterValue.Width = 350;
            // 
            // roundPanel3
            // 
            this.roundPanel3._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel3._Round_Cutting = true;
            this.roundPanel3._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel3._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel3._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel3._Round_TitleImage = null;
            this.roundPanel3._Round_TitleImageShow = false;
            this.roundPanel3._Round_TitleLabelText = "PROCESS RECIPE LIST";
            this.roundPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel3.Controls.Add(this.label3);
            this.roundPanel3.Controls.Add(this.label4);
            this.roundPanel3.Controls.Add(this.m_lbRffFileName);
            this.roundPanel3.Controls.Add(this.m_dgvProcessRecipeId);
            this.roundPanel3.Controls.Add(this.m_cbDefaultRecipe);
            this.roundPanel3.Controls.Add(this.m_btnProcessRecipeCreate);
            this.roundPanel3.Location = new System.Drawing.Point(10, 680);
            this.roundPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel3.Name = "roundPanel3";
            this.roundPanel3.Size = new System.Drawing.Size(1070, 592);
            this.roundPanel3.TabIndex = 13;
            // 
            // m_btnProcessRecipeCreate
            // 
            this.m_btnProcessRecipeCreate.ForeColor = System.Drawing.Color.Black;
            this.m_btnProcessRecipeCreate.Location = new System.Drawing.Point(985, -4);
            this.m_btnProcessRecipeCreate.Name = "m_btnProcessRecipeCreate";
            this.m_btnProcessRecipeCreate.Size = new System.Drawing.Size(75, 30);
            this.m_btnProcessRecipeCreate.TabIndex = 20;
            this.m_btnProcessRecipeCreate.Text = "Create";
            this.m_btnProcessRecipeCreate.UseVisualStyleBackColor = true;
            this.m_btnProcessRecipeCreate.Click += new System.EventHandler(this.OnProcessRecipeCreate);
            // 
            // m_cbDefaultRecipe
            // 
            this.m_cbDefaultRecipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cbDefaultRecipe.FormattingEnabled = true;
            this.m_cbDefaultRecipe.Location = new System.Drawing.Point(566, 0);
            this.m_cbDefaultRecipe.Name = "m_cbDefaultRecipe";
            this.m_cbDefaultRecipe.Size = new System.Drawing.Size(172, 26);
            this.m_cbDefaultRecipe.TabIndex = 19;
            // 
            // m_dgvProcessRecipeId
            // 
            this.m_dgvProcessRecipeId.AllowUserToAddRows = false;
            this.m_dgvProcessRecipeId.AllowUserToDeleteRows = false;
            this.m_dgvProcessRecipeId.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvProcessRecipeId.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewButtonColumn1});
            this.m_dgvProcessRecipeId.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_dgvProcessRecipeId.Location = new System.Drawing.Point(16, 43);
            this.m_dgvProcessRecipeId.Margin = new System.Windows.Forms.Padding(4);
            this.m_dgvProcessRecipeId.Name = "m_dgvProcessRecipeId";
            this.m_dgvProcessRecipeId.ReadOnly = true;
            this.m_dgvProcessRecipeId.RowTemplate.Height = 23;
            this.m_dgvProcessRecipeId.Size = new System.Drawing.Size(1050, 542);
            this.m_dgvProcessRecipeId.TabIndex = 17;
            this.m_dgvProcessRecipeId.Tag = "1";
            this.m_dgvProcessRecipeId.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnRecipeListCellClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "PROCESS RECIPE ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Ver";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 70;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "TIME";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewButtonColumn1
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewButtonColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewButtonColumn1.Frozen = true;
            this.dataGridViewButtonColumn1.HeaderText = "Show Recipe Parameter";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.ReadOnly = true;
            this.dataGridViewButtonColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn1.Width = 150;
            // 
            // m_lbRffFileName
            // 
            this.m_lbRffFileName.AutoSize = true;
            this.m_lbRffFileName.Location = new System.Drawing.Point(805, 5);
            this.m_lbRffFileName.Name = "m_lbRffFileName";
            this.m_lbRffFileName.Size = new System.Drawing.Size(0, 18);
            this.m_lbRffFileName.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(744, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 18);
            this.label4.TabIndex = 18;
            this.label4.Text = "RFF / POS : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(423, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 18);
            this.label3.TabIndex = 18;
            this.label3.Text = "Default Recipe : ";
            // 
            // CheckFile
            // 
            this.CheckFile.Interval = 10000;
            this.CheckFile.Tick += new System.EventHandler(this.OnCheckFile);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(613, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // RecipeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.roundPanel3);
            this.Controls.Add(this.roundPanel2);
            this.Controls.Add(this.roundPanel1);
            this.Controls.Add(this.roundPictureBox2);
            this.Controls.Add(this.roundPictureBox1);
            this.Controls.Add(this.m_lbViewRecipe);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_lblCurrentRecipe);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RecipeControl";
            this.Text = "RealLogControl";
            this.VisibleChanged += new System.EventHandler(this.OnVisibleChange);
            ((System.ComponentModel.ISupportInitialize)(this.roundPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundPictureBox2)).EndInit();
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvRecipeId)).EndInit();
            this.roundPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvRecipeParameter)).EndInit();
            this.roundPanel3.ResumeLayout(false);
            this.roundPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvProcessRecipeId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label m_lblCurrentRecipe;
        private System.Windows.Forms.Label label1;
        private Components.RoundPictureBox roundPictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label m_lbViewRecipe;
        private Components.RoundPictureBox roundPictureBox2;
        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvRecipeId;
        private Components.RoundPanel roundPanel2;
        private System.Windows.Forms.DataGridView m_dgvRecipeParameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParemeterValue;
        private Components.RoundPanel roundPanel3;
        private System.Windows.Forms.DataGridView m_dgvProcessRecipeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewButtonColumn Parameter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox m_cbDefaultRecipe;
        private System.Windows.Forms.Button m_btnProcessRecipeCreate;
        private System.Windows.Forms.Timer CheckFile;
        private System.Windows.Forms.Label m_lbRffFileName;
        private System.Windows.Forms.Button button1;
    }
}