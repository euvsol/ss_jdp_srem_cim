﻿#region Using
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using Esol.ShareMemory;
#endregion

namespace Esol.CIM
{
    public partial class RecipeControl : FormControl
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private const string DEF_REICPE_TEXT = "Recipe";
        /// <summary>
        /// 
        /// </summary>
        private const string DEF_PROCESS_RECIPE_TEXT = "ProcessRecipe";
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public RecipeControl()
        {
            InitializeComponent();
            this.TopLevel = false;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            InitializeDataGridView();
            CheckFile.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            for (int i = 0; i < m_dgvRecipeId.Columns.Count; i++)
                m_dgvRecipeId.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvRecipeId.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;


            for (int i = 0; i < m_dgvRecipeParameter.Columns.Count; i++)
                m_dgvRecipeParameter.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvRecipeParameter.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;

            for (int i = 0; i < m_dgvProcessRecipeId.Columns.Count; i++)
                m_dgvProcessRecipeId.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvProcessRecipeId.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;


            DataGridViewMainRecipeTrhead(m_dgvRecipeId);
            DataGridViewProcessRecipeTrhead(m_dgvProcessRecipeId);
        }
        #endregion

        #region Class Public Methdos
        /// <summary>
        /// 
        /// </summary>
        public void UpdateNotificationBoard(string sRecipeName)
        {
            if (!string.IsNullOrEmpty(sRecipeName) &&
                !CimConfigurator.RecipeManager.BasicRecipeNameToData.ContainsKey(sRecipeName))
                return;

            LabelCrossThread(m_lblCurrentRecipe, sRecipeName);
        }
        /// <summary>
        /// 
        /// </summary>
        public void ProcessRecipeListChange()
        {
            if (this.Visible)
                DataGridViewProcessRecipeTrhead(m_dgvProcessRecipeId);
        }
        /// <summary>
        /// 
        /// </summary>
        public void MainRecipeListChenage()
        {
            if (this.Visible)
                DataGridViewMainRecipeTrhead(m_dgvRecipeId);
        }
        #endregion

        #region Class CorssThread Methods
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadLabelStatus(Label label, string str);
        /// <summary>
        /// 
        /// </summary>
        private void LabelCrossThread(Label label, string str)
        {
            try
            {
                if (label.InvokeRequired)
                {
                    ReloadLabelStatus reload = new ReloadLabelStatus(LabelCrossThread);
                    this.Invoke(reload, new object[] { label, str });
                }
                else
                    label.Text = str;
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadRecipeDGVRefresh(DataGridView dgv);
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewMainRecipeTrhead(DataGridView dgv)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadRecipeDGVRefresh reload = new ReloadRecipeDGVRefresh(DataGridViewMainRecipeTrhead);
                    this.Invoke(reload, new object[] { dgv });
                }
                else
                {
                    dgv.Rows.Clear();
                    m_cbDefaultRecipe.Items.Clear();

                    Dictionary<string, BasicRecipeData> hashData = CimConfigurator.RecipeManager.BasicRecipeNameToData;

                    foreach (var item in hashData.OrderBy(i => i.Key))
                    {
                        var value = item.Value;

                        dgv.Rows.Add(value.RecipeName, value.RecipeVersion, value.RecipeTime);
                        dgv.Rows[dgv.Rows.Count - 1].Tag = value.RecipeFilePath;
                        m_cbDefaultRecipe.Items.Add(value.RecipeName);
                    }
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadProcessRecipeDGVRefresh(DataGridView dgv);
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewProcessRecipeTrhead(DataGridView dgv)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadProcessRecipeDGVRefresh reload = new ReloadProcessRecipeDGVRefresh(DataGridViewProcessRecipeTrhead);
                    this.Invoke(reload, new object[] { dgv });
                }
                else
                {
                    dgv.Rows.Clear();

                     Dictionary<string, ProcessRecipeData> hashData = CimConfigurator.RecipeManager.ProcessRecipeNameToData;

                    foreach (var item in hashData.OrderBy(i => i.Key))
                    {
                        var value = item.Value;

                        if (string.IsNullOrEmpty(value.RecipeName))
                            continue;

                        dgv.Rows.Add(value.RecipeName, value.RecipeVersion, value.RecipeTime);
                        dgv.Rows[dgv.Rows.Count - 1].Tag = value.RecipeFilePath;
                    }
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadParameterDGVRefresh(DataGridView dgv, Dictionary<string, string> hashRecipeDatas);
        /// <summary>
        /// 
        /// </summary>
        private void ParameterDataGridViewTrhead(DataGridView dgv, Dictionary<string, string> hashRecipeDatas)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadParameterDGVRefresh reload = new ReloadParameterDGVRefresh(ParameterDataGridViewTrhead);
                    this.Invoke(reload, new object[] { dgv, hashRecipeDatas });
                }
                else
                {
                    dgv.Rows.Clear();

                    foreach (string sName in hashRecipeDatas.Keys)
                        if (!sName.ToUpper().Contains(CimConfigurator.Password))
                            dgv.Rows.Add(sName.ToUpper(), hashRecipeDatas[sName]);

                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnRecipeListCellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dataGridView = (DataGridView)sender;

            int iCol = dataGridView.CurrentCell.ColumnIndex;
            int iRow = dataGridView.CurrentCell.RowIndex;

            if (iCol != 3)
                return;

            //File Read 후 m_dgvRecipeParameter Data View
            string sSelectRecipeName = dataGridView.Rows[iRow].Cells[0].Value.ToString();

            if (dataGridView.Name.Contains("Process"))
                ParameterDataGridViewTrhead(m_dgvRecipeParameter, CimConfigurator.RecipeManager.ProcessRecipeNameToData[sSelectRecipeName].ParameterNameToValue);
            else
                ParameterDataGridViewTrhead(m_dgvRecipeParameter, CimConfigurator.RecipeManager.BasicRecipeNameToData[sSelectRecipeName].HashParamNameToValue);

            LabelCrossThread(m_lbViewRecipe, sSelectRecipeName);
            LogManager.ButtonWriteLog("Show Recipe Parameter : " + sSelectRecipeName);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnVisibleChange(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                DataGridViewMainRecipeTrhead(m_dgvRecipeId);
                DataGridViewProcessRecipeTrhead(m_dgvProcessRecipeId);

                //삼성 장준태 프로 요청으로 Diagram에서 File 선택하게 변경
                //ReadRffFile();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnProcessRecipeCreate(object sender, EventArgs e)
        {
            try
            {
                if (MainControl.NotificationBoard.HsmsState == ControlState.Remote)
                {
                    MessageBox.Show("Control State가 Remote일때 생성 불가능합니다.");
                    return;
                }

                if (m_cbDefaultRecipe.SelectedIndex == -1)
                {
                    MessageBox.Show("Default Recipe가 선택 되지 않았습니다.");
                    return;
                }

                //Rff File Check
                OpenFileDialog ofd = new OpenFileDialog()
                {
                    Title = "RFF / POS Open",
                    Filter = "RFF / POS (*.rff, *.POS)|*.rff;*.POS",
                    InitialDirectory = CimConfigurator.RffFilePath
                };

                DialogResult dr = ofd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    string sRecipeName = m_cbDefaultRecipe.SelectedItem.ToString();
                    RffSummary rffSummary = new RffSummary(CimConfigurator.HostProcessRecipePath, CimConfigurator);
                    string sRecipeFilePath = CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].RecipeFilePath;

                    string sRffPath = ofd.FileName;

                    //20230602 jkseo, ICM 전송 및 Recipe File명에 붙일 DateTime 
                    string sDateTime = Common.GenerateFileTime(DateTime.Now);

                    ProcessRecipeData prd = rffSummary.SummaryRecipeToData(sRecipeFilePath, sRecipeName, sRffPath, "Manual", sDateTime, EmAccessMode.Manual);

                    if (prd == null)
                    {
                        MessageBox.Show("Process Recipe File 생성에 실패 하였습니다.");
                        return;
                    }

                    LabelCrossThread(m_lbRffFileName, ofd.SafeFileName.Replace(".rff", string.Empty));
                    DataGridViewProcessRecipeTrhead(m_dgvProcessRecipeId);

                    MessageBox.Show(prd.RecipeName + " 성공적으로 생성 되었습니다.");
                }
                else
                {
                    LabelCrossThread(m_lbRffFileName, string.Empty);
                    MessageBox.Show("RFF 또는 POS 파일이 선택되지 않았습니다.");
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void BasicRecipeCreateRemove(INI ini)
        {
            try
            {
                if (CimConfigurator.RecipeManager.HashBasicRecipeCMD.Count == 0)
                    return;

                long[] sVids;
                string[] sVidValues;
                long nReturn = 0;
                string[] sArrayCreateRecipeNmaes = new string[CimConfigurator.RecipeManager.HashBasicRecipeCMD.Count];
                CimConfigurator.RecipeManager.HashBasicRecipeCMD.Keys.CopyTo(sArrayCreateRecipeNmaes, 0);

                foreach (string sRecipeName in sArrayCreateRecipeNmaes)
                {
                    int iMode = CimConfigurator.RecipeManager.HashBasicRecipeCMD[sRecipeName];

                    switch (iMode)
                    {
                        case 1:
                        case 2:
                            sVids = new long[3];
                            sVidValues = new string[3];
                            sVids[0] = 29;
                            sVids[1] = 30;
                            sVids[2] = 4014;
                            sVidValues[0] = sRecipeName;
                            sVidValues[1] = "1";
                            sVidValues[2] = sRecipeName;
                            CimConfigurator.SetVids(sVids, sVidValues);

                            nReturn = Xgem.GEMSetPPFmtChanged(iMode, sRecipeName, string.Empty, string.Empty, 0, new string[1], new long[1], new string[1]);

                            if (nReturn == 0)
                            {
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} {1} Success", sRecipeName, "Create"));
                                ini.SetPairBySection(DEF_REICPE_TEXT, sRecipeName, Common.GenerateFileTime(DateTime.Now));
                                CimConfigurator.RecipeManager.HashBasicRecipeCMD.Remove(sRecipeName);
                            }
                            else
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} {1} Fail", sRecipeName, "Create"));

                            Thread.Sleep(100);
                            break;
                        case 3:
                            sVids = new long[3];
                            sVidValues = new string[3];
                            sVids[0] = 29;
                            sVids[1] = 30;
                            sVids[2] = 4014;
                            sVidValues[0] = sRecipeName;
                            sVidValues[1] = "3";
                            sVidValues[2] = sRecipeName;

                            CimConfigurator.SetVids(sVids, sVidValues);


                            nReturn = CimConfigurator.Xgem.GEMSetPPFmtChanged(3, sRecipeName, string.Empty, string.Empty, 0, new string[0], new long[0], new string[0]);
                            if (nReturn == 0)
                            {
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Success", sRecipeName));
                                ini.DeleteKey(DEF_REICPE_TEXT, sRecipeName);
                                CimConfigurator.RecipeManager.HashBasicRecipeCMD.Remove(sRecipeName);
                                CimConfigurator.RecipeManager.BasicRecipeNameToData.Remove(sRecipeName);

                                FileInfo fiEq = new FileInfo(CimConfigurator.EqRecipePath + "\\" + sRecipeName + ".xml");
                                FileInfo fiHost = new FileInfo(CimConfigurator.HostBasicRecipePath + "\\" + sRecipeName + ".xml");

                                if (fiEq.Exists)
                                    fiEq.Delete();
                                if (fiHost.Exists)
                                    fiHost.Delete();
                            }
                            else
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Fail", sRecipeName));

                            Thread.Sleep(100);
                            break;
                    }
                }
                MainControl.RecipeControl.MainRecipeListChenage();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ProcessRecipeCreateRemove(INI ini)
        {
            try
            {
                if (CimConfigurator.RecipeManager.HashProcessRecipeCMD.Count == 0)
                    return;

                long[] sVids;
                string[] sVidValues;
                long nReturn = 0;
                string[] sArrayCreateRecipeNmaes = new string[CimConfigurator.RecipeManager.HashProcessRecipeCMD.Count];
                CimConfigurator.RecipeManager.HashProcessRecipeCMD.Keys.CopyTo(sArrayCreateRecipeNmaes, 0);

                foreach (string sRecipeName in sArrayCreateRecipeNmaes)
                {
                    int iMode = CimConfigurator.RecipeManager.HashProcessRecipeCMD[sRecipeName];

                    switch (iMode)
                    {
                        case 1:
                        case 2:
                            sVids = new long[3];
                            sVidValues = new string[3];
                            sVids[0] = 29;
                            sVids[1] = 30;
                            sVids[2] = 4014;
                            sVidValues[0] = sRecipeName;
                            sVidValues[1] = "1";
                            sVidValues[2] = sRecipeName;
                            CimConfigurator.SetVids(sVids, sVidValues);

                            nReturn = Xgem.GEMSetPPFmtChanged(iMode, sRecipeName, string.Empty, string.Empty, 0, new string[1], new long[1], new string[1]);

                            if (nReturn == 0)
                            {
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("ProcessRecipeName : {0} {1} Success", sRecipeName, "Create"));
                                ini.SetPairBySection(DEF_PROCESS_RECIPE_TEXT, sRecipeName, Common.GenerateFileTime(DateTime.Now));
                                CimConfigurator.RecipeManager.HashProcessRecipeCMD.Remove(sRecipeName);
                            }
                            else
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} {1} Fail", sRecipeName, "Create"));

                            Thread.Sleep(100);
                            break;
                        case 3:
                            sVids = new long[3];
                            sVidValues = new string[3];
                            sVids[0] = 29;
                            sVids[1] = 30;
                            sVids[2] = 4014;
                            sVidValues[0] = sRecipeName;
                            sVidValues[1] = "3";
                            sVidValues[2] = sRecipeName;

                            CimConfigurator.SetVids(sVids, sVidValues);


                            nReturn = CimConfigurator.Xgem.GEMSetPPFmtChanged(3, sRecipeName, string.Empty, string.Empty, 0, new string[0], new long[0], new string[0]);
                            if (nReturn == 0)
                            {
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("ProcessRecipeName : {0} Delete Success", sRecipeName));
                                ini.DeleteKey(DEF_PROCESS_RECIPE_TEXT, sRecipeName);
                                CimConfigurator.RecipeManager.HashProcessRecipeCMD.Remove(sRecipeName);
                                CimConfigurator.RecipeManager.ProcessRecipeNameToData.Remove(sRecipeName);

                                FileInfo fiEq = new FileInfo(CimConfigurator.EqProcessRecipePath + "\\" + sRecipeName + ".xml");
                                FileInfo fiHost = new FileInfo(CimConfigurator.HostProcessRecipePath + "\\" + sRecipeName + ".xml");

                                if (fiEq.Exists)
                                    fiEq.Delete();
                                if (fiHost.Exists)
                                    fiHost.Delete();
                            }
                            else
                                CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Fail", sRecipeName));

                            Thread.Sleep(100);
                            break;
                    }
                }
                MainControl.RecipeControl.ProcessRecipeListChange();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Timer
        /// <summary>
        /// 
        /// </summary>
        private void OnCheckFile(object sender, EventArgs e)
        {
            try
            {
                //20230713 jkseo, Recipe Delete & Backup 기능인데 추후 정리할 필요있음
                return;
                   
                if (MainControl.NotificationBoard.HsmsState == ControlState.Offline)// || !MainControl.AliveICM)
                    return;
                INI ini = new INI(CimConfigurator.RecipeManager.DEF_RECIPE_LIST_PAHT);
                PlcAddr EqRecipeReply = CimConfigurator.HashShareKeyToShareData["L2_B_RECIPE_CMD_REPLY"];

                string sBackUpPath = string.Format(@"{0}\BackUp",  CimConfigurator.RffFilePath);

                if (!Directory.Exists(sBackUpPath))
                    Directory.CreateDirectory(sBackUpPath);

                //Rff File Bakcup
                DirectoryInfo diRff = new DirectoryInfo(CimConfigurator.RffFilePath);

                foreach (FileInfo fi in diRff.GetFiles())
                {
                    DateTime de = fi.LastWriteTime;

                    int iDay = (DateTime.Now - de).Days;

                    if (iDay > CimConfigurator.ProcessRecipeDay)
                    {
                        string sBakcupFullPath = string.Format(@"{0}\{1}_{2}", sBackUpPath, Common.GenerateFileTime(DateTime.Now), fi.Name);
                        fi.MoveTo(sBakcupFullPath);
                    }
                }

                long[] sVids;
                string[] sVidValues;
                //Process Recipe Backup & Delete Report
                DirectoryInfo diProcess = new DirectoryInfo(CimConfigurator.HostProcessRecipePath);

                foreach (FileInfo fi in diProcess.GetFiles())
                {
                    DateTime de = fi.LastWriteTime;

                    int iDay = (DateTime.Now - de).Days;

                    if (!CimConfigurator.ShareMem.GetBit(EqRecipeReply))
                        continue;

                    if (iDay > CimConfigurator.ProcessRecipeDay)
                    {
                        while (true)
                        {
                            if (!CimConfigurator.ShareMem.GetBit(EqRecipeReply))
                                break;
                        }

                        string sDeleteRecipeName = fi.Name.ToUpper().Replace(".XML", string.Empty);

                        if (MainControl.NotificationBoard.HsmsState == ControlState.Offline)
                            return;

                        sVids = new long[2];
                        sVids[0] = 30;
                        sVids[1] = 4014;

                        sVidValues = new string[2];
                        sVidValues[0] = "3";
                        sVidValues[1] = sDeleteRecipeName;

                        CimConfigurator.SetVids(sVids, sVidValues);

                        long nReturn = CimConfigurator.Xgem.GEMSetPPFmtChanged(3, sDeleteRecipeName, string.Empty, string.Empty, 0, new string[0], new long[0], new string[0]);
                        if (nReturn == 0)
                        {
                            CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Success", sDeleteRecipeName));
                            ini.DeleteKey("ProcessRecipe", sDeleteRecipeName);
                        }
                        else
                            CimConfigurator.HsmsLogControl.AddMessage(string.Format("RecipeName : {0} Delete Fail", sDeleteRecipeName));



                        List<PlcAddr> listProcessRecipeStruct = CimConfigurator.HashShareStruct["L1_PROCESS_RECIPE_CMD_STRUCT"];
                        CimConfigurator.ShareMem.SetAscii(listProcessRecipeStruct[0], sDeleteRecipeName);
                        CimConfigurator.ShareMem.SetByte(listProcessRecipeStruct[1], 3);

                        CimConfigurator.ShareMem.SetBit(CimConfigurator.HashShareKeyToShareData["L1_B_RECIPE_CMD_COMMAND"], true);

                        //BackUp Folder Move
                        string sBackUpFullPath = string.Format(@"{0}\{1}_{2}", CimConfigurator.ProcessRecipeBackUpPath, Common.GenerateFileTime(DateTime.Now), fi.Name);
                        fi.MoveTo(sBackUpFullPath);


                        ProcessRecipeCreateRemove(ini);
                    }
                    //BasicRecipeCreateRemove(ini);

                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            MainControl.CimConfigurator.RecipeManager.InitializeProcessRecipeRead();
        }
    }
}
