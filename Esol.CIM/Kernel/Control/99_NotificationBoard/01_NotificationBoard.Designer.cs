﻿namespace Esol.CIM
{
    partial class NotificationBoard
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotificationBoard));
            this.p_m_lbHsmsConnection = new System.Windows.Forms.Label();
            this.p_m_rbtnHsmsRemote = new Esol.Components.RoundMultiUsePanel();
            this.p_m_rbtnHsmsOffline = new Esol.Components.RoundMultiUsePanel();
            this.p_m_rbtnHsmsLocal = new Esol.Components.RoundMultiUsePanel();
            this.p_m_lbEqState = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p_m_rbtnHsmsEnable = new Esol.Components.RoundMultiUsePanel();
            this.p_m_rbtnHsmsDisable = new Esol.Components.RoundMultiUsePanel();
            this.SuspendLayout();
            // 
            // p_m_lbHsmsConnection
            // 
            this.p_m_lbHsmsConnection.AutoSize = true;
            this.p_m_lbHsmsConnection.ForeColor = System.Drawing.Color.DodgerBlue;
            this.p_m_lbHsmsConnection.Location = new System.Drawing.Point(153, 71);
            this.p_m_lbHsmsConnection.Name = "p_m_lbHsmsConnection";
            this.p_m_lbHsmsConnection.Size = new System.Drawing.Size(26, 18);
            this.p_m_lbHsmsConnection.TabIndex = 47;
            this.p_m_lbHsmsConnection.Text = "||";
            // 
            // p_m_rbtnHsmsRemote
            // 
            this.p_m_rbtnHsmsRemote._Round_Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.p_m_rbtnHsmsRemote._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.p_m_rbtnHsmsRemote._Round_Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsRemote._Round_Effect = false;
            this.p_m_rbtnHsmsRemote._Round_Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.p_m_rbtnHsmsRemote._Round_Shape = Esol.Components.eShape.Round;
            this.p_m_rbtnHsmsRemote._Round_Theme = Esol.Components.eTheme.Black;
            this.p_m_rbtnHsmsRemote._Round_Title = "REMOTE";
            this.p_m_rbtnHsmsRemote._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("p_m_rbtnHsmsRemote._Round_TitleImage")));
            this.p_m_rbtnHsmsRemote._Round_VisibleLine = false;
            this.p_m_rbtnHsmsRemote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.p_m_rbtnHsmsRemote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsRemote.Location = new System.Drawing.Point(330, 59);
            this.p_m_rbtnHsmsRemote.Margin = new System.Windows.Forms.Padding(0);
            this.p_m_rbtnHsmsRemote.Name = "p_m_rbtnHsmsRemote";
            this.p_m_rbtnHsmsRemote.Size = new System.Drawing.Size(125, 32);
            this.p_m_rbtnHsmsRemote.TabIndex = 46;
            this.p_m_rbtnHsmsRemote.Tag = "3";
            this.p_m_rbtnHsmsRemote.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnHsmsConnectClick);
            // 
            // p_m_rbtnHsmsOffline
            // 
            this.p_m_rbtnHsmsOffline._Round_Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.p_m_rbtnHsmsOffline._Round_BackgoundControl = Esol.Components.eRoundControl.Background;
            this.p_m_rbtnHsmsOffline._Round_Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsOffline._Round_Effect = false;
            this.p_m_rbtnHsmsOffline._Round_Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.p_m_rbtnHsmsOffline._Round_Shape = Esol.Components.eShape.Round;
            this.p_m_rbtnHsmsOffline._Round_Theme = Esol.Components.eTheme.Black;
            this.p_m_rbtnHsmsOffline._Round_Title = "OFFLINE";
            this.p_m_rbtnHsmsOffline._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("p_m_rbtnHsmsOffline._Round_TitleImage")));
            this.p_m_rbtnHsmsOffline._Round_VisibleLine = false;
            this.p_m_rbtnHsmsOffline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.p_m_rbtnHsmsOffline.Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsOffline.Location = new System.Drawing.Point(14, 59);
            this.p_m_rbtnHsmsOffline.Margin = new System.Windows.Forms.Padding(0);
            this.p_m_rbtnHsmsOffline.Name = "p_m_rbtnHsmsOffline";
            this.p_m_rbtnHsmsOffline.Size = new System.Drawing.Size(125, 32);
            this.p_m_rbtnHsmsOffline.TabIndex = 48;
            this.p_m_rbtnHsmsOffline.Tag = "1";
            this.p_m_rbtnHsmsOffline.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnHsmsConnectClick);
            // 
            // p_m_rbtnHsmsLocal
            // 
            this.p_m_rbtnHsmsLocal._Round_Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.p_m_rbtnHsmsLocal._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.p_m_rbtnHsmsLocal._Round_Cursor = System.Windows.Forms.Cursors.Default;
            this.p_m_rbtnHsmsLocal._Round_Effect = false;
            this.p_m_rbtnHsmsLocal._Round_Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.p_m_rbtnHsmsLocal._Round_Shape = Esol.Components.eShape.Round;
            this.p_m_rbtnHsmsLocal._Round_Theme = Esol.Components.eTheme.Black;
            this.p_m_rbtnHsmsLocal._Round_Title = "LOCAL";
            this.p_m_rbtnHsmsLocal._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("p_m_rbtnHsmsLocal._Round_TitleImage")));
            this.p_m_rbtnHsmsLocal._Round_VisibleLine = false;
            this.p_m_rbtnHsmsLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.p_m_rbtnHsmsLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.p_m_rbtnHsmsLocal.Cursor = System.Windows.Forms.Cursors.Default;
            this.p_m_rbtnHsmsLocal.Location = new System.Drawing.Point(182, 59);
            this.p_m_rbtnHsmsLocal.Margin = new System.Windows.Forms.Padding(0);
            this.p_m_rbtnHsmsLocal.Name = "p_m_rbtnHsmsLocal";
            this.p_m_rbtnHsmsLocal.Size = new System.Drawing.Size(116, 32);
            this.p_m_rbtnHsmsLocal.TabIndex = 49;
            this.p_m_rbtnHsmsLocal.Tag = "2";
            this.p_m_rbtnHsmsLocal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnHsmsConnectClick);
            // 
            // p_m_lbEqState
            // 
            this.p_m_lbEqState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.p_m_lbEqState.AutoSize = true;
            this.p_m_lbEqState.ForeColor = System.Drawing.Color.DodgerBlue;
            this.p_m_lbEqState.Location = new System.Drawing.Point(301, 71);
            this.p_m_lbEqState.Name = "p_m_lbEqState";
            this.p_m_lbEqState.Size = new System.Drawing.Size(26, 18);
            this.p_m_lbEqState.TabIndex = 50;
            this.p_m_lbEqState.Text = "||";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(153, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 18);
            this.label1.TabIndex = 50;
            this.label1.Text = "||";
            // 
            // p_m_rbtnHsmsEnable
            // 
            this.p_m_rbtnHsmsEnable._Round_Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.p_m_rbtnHsmsEnable._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.p_m_rbtnHsmsEnable._Round_Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsEnable._Round_Effect = false;
            this.p_m_rbtnHsmsEnable._Round_Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.p_m_rbtnHsmsEnable._Round_Shape = Esol.Components.eShape.Round;
            this.p_m_rbtnHsmsEnable._Round_Theme = Esol.Components.eTheme.Black;
            this.p_m_rbtnHsmsEnable._Round_Title = "Enable";
            this.p_m_rbtnHsmsEnable._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("p_m_rbtnHsmsEnable._Round_TitleImage")));
            this.p_m_rbtnHsmsEnable._Round_VisibleLine = false;
            this.p_m_rbtnHsmsEnable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.p_m_rbtnHsmsEnable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsEnable.Location = new System.Drawing.Point(182, 12);
            this.p_m_rbtnHsmsEnable.Margin = new System.Windows.Forms.Padding(0);
            this.p_m_rbtnHsmsEnable.Name = "p_m_rbtnHsmsEnable";
            this.p_m_rbtnHsmsEnable.Size = new System.Drawing.Size(125, 32);
            this.p_m_rbtnHsmsEnable.TabIndex = 49;
            this.p_m_rbtnHsmsEnable.Tag = "3";
            this.p_m_rbtnHsmsEnable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnComState);
            // 
            // p_m_rbtnHsmsDisable
            // 
            this.p_m_rbtnHsmsDisable._Round_Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.p_m_rbtnHsmsDisable._Round_BackgoundControl = Esol.Components.eRoundControl.Background;
            this.p_m_rbtnHsmsDisable._Round_Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsDisable._Round_Effect = false;
            this.p_m_rbtnHsmsDisable._Round_Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.p_m_rbtnHsmsDisable._Round_Shape = Esol.Components.eShape.Round;
            this.p_m_rbtnHsmsDisable._Round_Theme = Esol.Components.eTheme.Black;
            this.p_m_rbtnHsmsDisable._Round_Title = "Disable";
            this.p_m_rbtnHsmsDisable._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("p_m_rbtnHsmsDisable._Round_TitleImage")));
            this.p_m_rbtnHsmsDisable._Round_VisibleLine = false;
            this.p_m_rbtnHsmsDisable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.p_m_rbtnHsmsDisable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_m_rbtnHsmsDisable.Location = new System.Drawing.Point(14, 12);
            this.p_m_rbtnHsmsDisable.Margin = new System.Windows.Forms.Padding(0);
            this.p_m_rbtnHsmsDisable.Name = "p_m_rbtnHsmsDisable";
            this.p_m_rbtnHsmsDisable.Size = new System.Drawing.Size(125, 32);
            this.p_m_rbtnHsmsDisable.TabIndex = 51;
            this.p_m_rbtnHsmsDisable.Tag = "1";
            this.p_m_rbtnHsmsDisable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnComState);
            // 
            // NotificationBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.Controls.Add(this.p_m_rbtnHsmsDisable);
            this.Controls.Add(this.p_m_lbHsmsConnection);
            this.Controls.Add(this.p_m_rbtnHsmsEnable);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.p_m_rbtnHsmsRemote);
            this.Controls.Add(this.p_m_rbtnHsmsOffline);
            this.Controls.Add(this.p_m_rbtnHsmsLocal);
            this.Controls.Add(this.p_m_lbEqState);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "NotificationBoard";
            this.Size = new System.Drawing.Size(470, 108);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label p_m_lbHsmsConnection;
        private Components.RoundMultiUsePanel p_m_rbtnHsmsRemote;
        private Components.RoundMultiUsePanel p_m_rbtnHsmsOffline;
        private Components.RoundMultiUsePanel p_m_rbtnHsmsLocal;
        private System.Windows.Forms.Label p_m_lbEqState;
        private System.Windows.Forms.Label label1;
        private Components.RoundMultiUsePanel p_m_rbtnHsmsEnable;
        private Components.RoundMultiUsePanel p_m_rbtnHsmsDisable;
    }
}
