﻿#region Class Using
using System;
using System.Drawing;
using System.Windows.Forms;
using Esol.Components;
using Esol.ShareMemory;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public partial class NotificationBoard : UserControl
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_OFFLINE_MESSAGE = "Offline시 Host로 정보를 보고하지 않습니다.";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_DISABLE_MESSAGE = "Disable시 Host로 정보를 보고하지 않습니다.";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadRoundMultiUsePanelTitleStatus(RoundMultiUsePanel panel, string str);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadRoundMultiUsePanelPropStatus(RoundMultiUsePanel panel, bool bLineVisible, Cursor cursor);
        /// <summary>
        /// 
        /// </summary>
        private MainControl m_MainControl;
        /// <summary>
        /// 
        /// </summary>
        private ControlState m_eControlState;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bConnectState;
        /// <summary>
        /// 
        /// </summary>
        private ProcessingState m_eProcState;

        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public bool ConnectState
        {
            get
            {
                return m_bConnectState;
            }
            set
            {
                m_bConnectState = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ControlState HsmsState
        {
            get
            {
                return m_eControlState;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HsmsStateChange
        {
            set
            {
                switch (value)
                {
                    case ClassNames.DEF_OFFLINE_TEXT:
                        HsmsDisConnectOffline(value);
                        break;
                    case ClassNames.DEF_LOCAL_TEXT:
                        HsmsConnectChange(value);
                        break;
                    case ClassNames.DEF_REMOTE_TEXT:
                        HsmsConnectChange(value);
                        break;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ProcessingState ProcState
        {
            get
            {
                return m_eProcState;
            }
            set
            {
                m_eProcState = value;
            }
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public NotificationBoard(MainControl mainControl)
        {
            InitializeComponent();

            m_MainControl = mainControl;

            m_bConnectState = false;

            m_eProcState = ProcessingState.SystemPowerUp;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            m_eControlState = ControlState.Offline;
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        public void HsmsDisConnectOffline(string sText = ClassNames.DEF_OFFLINE_TEXT)
        {
            if (sText == ClassNames.DEF_OFFLINE_TEXT && HsmsState != ControlState.Offline)
            {
                RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsOffline, false, Cursors.Default);
                RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsLocal, true, Cursors.Hand);
                RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsRemote, true, Cursors.Hand);

                if (ConnectState)
                {
                    m_eControlState = ControlState.Offline;
                    m_MainControl.Xgem.GEMReqOffline();
                }

                ControlStateCommand(1);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void HsmsConnectChange(string sHsmsConnect)
        {
            try
            {
                byte iCommand = 0;
                byte iControlState = 0;
                switch (sHsmsConnect)
                {
                    case ClassNames.DEF_OFFLINE_TEXT:
                        if (m_eControlState != ControlState.Offline)
                        {
                            OfflineForm form = new OfflineForm(DEF_OFFLINE_MESSAGE, m_MainControl.CimConfigurator.Password);

                            DialogResult res = form.ShowDialog();
                            m_MainControl.LogManager.ButtonWriteLog("Password Button : " + res);
                            if (res != DialogResult.OK)
                                return;


                            if (m_MainControl.Xgem.GEMReqOffline() == 0)
                            {
                                RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsOffline, false, Cursors.Default);
                                RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsLocal, true, Cursors.Hand);
                                RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsRemote, true, Cursors.Hand);

                                if (ConnectState)
                                    m_eControlState = ControlState.Offline;

                                iCommand = 1;
                                iControlState = 1;
                            }
                        }
                        break;
                    case ClassNames.DEF_LOCAL_TEXT:
                    case ClassNames.DEF_ONLINE_LOCAL_TEXT:
                        if (m_MainControl.Xgem.GEMReqLocal() == 0)
                        {
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsLocal, false, Cursors.Default);
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsOffline, true, Cursors.Hand);
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsRemote, true, Cursors.Hand);

                            if (ConnectState)
                                m_eControlState = ControlState.Local;

                            iCommand = 4;
                            iControlState = 2;
                        }
                        break;
                    case ClassNames.DEF_REMOTE_TEXT:
                    case ClassNames.DEF_ONLINE_RETMOE_TEXT:
                        if (m_MainControl.Xgem.GEMReqRemote() == 0)
                        {
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsRemote, false, Cursors.Default);
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsOffline, true, Cursors.Hand);
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsLocal, true, Cursors.Hand);

                            if (ConnectState)
                                m_eControlState = ControlState.Remote;

                            iCommand = 5;
                            iControlState = 3;
                        }
                        break;
                    case ClassNames.DEF_HOST_OFFLINE_TEXT:
                        iCommand = 3;
                        iControlState = 1;
                        HsmsDisConnectOffline();
                        break;
                }

                if (iCommand == 0)
                    return;

                long[] naVids = new long[1];
                naVids[0] = 4;
                string[] nsValues = new string[1];
                nsValues[0] = iCommand.ToString();

                m_MainControl.Xgem.GEMSetVariable(1, naVids, nsValues);

                ControlStateCommand(iControlState);
            }
            catch (Exception ex)
            {
                m_MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void HsmsComStateChange(string sComConnect)
        {
            try
            {
                long nReturn = 0;
                string sAck = string.Empty;
                switch (sComConnect)
                {
                    case ClassNames.DEF_DISABLE_TEXT:
                        OfflineForm form = new OfflineForm(DEF_DISABLE_MESSAGE, m_MainControl.CimConfigurator.Password);

                        DialogResult res = form.ShowDialog();

                        m_MainControl.LogManager.ButtonWriteLog("Password Button : " + res);

                        if (res != DialogResult.OK)
                            return;

                        //Argument : bState value(0: disable, 1: enable)
                        nReturn = m_MainControl.Xgem.GEMSetEstablish(0);
                        if (nReturn == 0)
                        {
                            m_MainControl.HsmsLogControl.AddMessage("[EQ ==> XGEM] GEMSetEstablish Disable successfully ({0})", nReturn);

                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsDisable, false, Cursors.Default);
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsEnable, true, Cursors.Hand);
                        }
                        else
                            m_MainControl.HsmsLogControl.AddMessage("[EQ ==> XGEM] Disable Fail to GEMSetEstablish ({0})", nReturn);

                        sAck = nReturn.ToString() == "0" ? "Disable Ack : ACK" : "Disable Ack : NACK";
                        break;
                    case ClassNames.DEF_ENALBE_TEXT:
                        nReturn = m_MainControl.Xgem.GEMSetEstablish(1);
                        if (nReturn == 0)
                        {
                            m_MainControl.HsmsLogControl.AddMessage("[EQ ==> XGEM] GEMSetEstablish Enable successfully ({0})", nReturn);

                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsEnable, false, Cursors.Default);
                            RoundMultiUsePanelPropCrossThread(p_m_rbtnHsmsDisable, true, Cursors.Hand);
                        }
                        else
                            m_MainControl.HsmsLogControl.AddMessage("[EQ ==> XGEM] Enable Fail to GEMSetEstablish ({0})", nReturn);

                        sAck = nReturn.ToString() == "0" ? "Enalbe Ack : ACK" : "Enalbe Ack : NACK";

                        //ReadData();
                        break;
                }
                m_MainControl.LogManager.ButtonWriteLog(sAck);
            }
            catch (Exception ex)
            {
                m_MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ControlStateCommand(byte iCommand)
        {
            PlcAddr ControlStateAddr = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_STATE];
            PlcAddr ControlStateCommandAddr = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_STATE_COMMAND];

            m_MainControl.ShareMem.SetByte(ControlStateAddr, iCommand);
            m_MainControl.ShareMem.SetBit(ControlStateCommandAddr, true);
        }
        #endregion

        #region Class Event Methods
        
        /// <summary>
        /// 
        /// </summary>
        private void OnHsmsConnectClick(object sender, MouseEventArgs e)
        {
            try
            {
                RoundMultiUsePanel panel = (RoundMultiUsePanel)sender;

                if (panel._Round_VisibleLine)
                    return;

                if (!m_bConnectState)
                {
                    MessageBox.Show("Host Not Connected");
                    return;
                }

                PlcAddr ControlStateCommandAddr = m_MainControl.HashShareKeyToShareData[ShareNames.DEF_HOST_CONTROL_STATE_COMMAND];

                if (m_MainControl.ShareMem.GetBit(ControlStateCommandAddr))
                {
                    MessageBox.Show("잠시 후 시도 해주세요");
                    return;
                }

                m_MainControl.LogManager.ButtonWriteLog(panel._Round_Title);
                HsmsConnectChange(panel._Round_Title);
            }
            catch (Exception ex)
            {
                m_MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnComState(object sender, MouseEventArgs e)
        {
            try
            {
                RoundMultiUsePanel panel = (RoundMultiUsePanel)sender;

                if (panel._Round_VisibleLine)
                    return;

                m_MainControl.LogManager.ButtonWriteLog(panel._Round_Title);
                HsmsComStateChange(panel._Round_Title);
            }
            catch (Exception ex)
            {
                m_MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
#endregion

#region Class Utility Methods
        /// <summary>
        /// 
        /// </summary>
        private void RoundMultiUsePanelTitleCrossThread(RoundMultiUsePanel panel, string str)
        {
            try
            {
                if (panel.InvokeRequired)
                {
                    ReloadRoundMultiUsePanelTitleStatus reload = new ReloadRoundMultiUsePanelTitleStatus(RoundMultiUsePanelTitleCrossThread);
                    this.Invoke(reload, new object[] { panel, str });
                }
                else
                {
                    if (string.Compare(str, "Error") == 0)
                        panel._Round_Title = "Init";
                    else
                        panel._Round_Title = str;
                }
            }
            catch (Exception ex)
            {
                m_MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void RoundMultiUsePanelPropCrossThread(RoundMultiUsePanel panel, bool bLineVisible, Cursor cursor)
        {
            try
            {
                if (panel.InvokeRequired)
                {
                    ReloadRoundMultiUsePanelPropStatus reload = new ReloadRoundMultiUsePanelPropStatus(RoundMultiUsePanelPropCrossThread);
                    this.Invoke(reload, new object[] { panel, bLineVisible, cursor });
                }
                else
                {
                    panel._Round_Cursor = cursor;

                    Font font = new Font(panel._Round_Font, bLineVisible ? FontStyle.Bold : FontStyle.Regular);
                    panel._Round_Font = font;

                    panel._Round_BackgoundControl = bLineVisible ? eRoundControl.RoundWindow : eRoundControl.Background;
                }
            }
            catch (Exception ex)
            {
                m_MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
#endregion
    }
}
