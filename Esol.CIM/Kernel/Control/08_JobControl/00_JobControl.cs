﻿#region Using
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using JHKIM.DataGridViewEx;
#endregion

namespace Esol.CIM
{
    public partial class JobControl : FormControl
    {
        #region Class Memebers
        /// <summary>
        /// 
        /// </summary>
        List<string> m_listHasPJ;
        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, List<string>> m_hashCjToPjs;
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadConrolJobRefresh(DataGridView dgv, Dictionary<string, List<string>> hashCjToPjs);
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<string>> CjToPjs
        {
            get
            {
                return m_hashCjToPjs;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public JobControl()
        {
            InitializeComponent();
            this.TopLevel = false;

            InitializeDataGridView();

            m_listHasPJ = new List<string>();

            m_hashCjToPjs = new Dictionary<string, List<string>>();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            for (int i = 0; i < m_dgvProcessJob.Columns.Count; i++)
                m_dgvProcessJob.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvProcessJob.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;

            for (int i = 0; i < m_dgvControlJob.Columns.Count; i++)
                m_dgvControlJob.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvControlJob.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void ProcessJobChanged()
        {
            ProcessJobDataGridViewTrhead(m_dgvProcessJob);
        }
        #endregion

        #region Class Public Methdos
        /// <summary>
        /// 
        /// </summary>
        public void ProcessJobChange()
        {
            if (this.Visible)
                ProcessJobDataGridViewTrhead(m_dgvProcessJob);
        }
        /// <summary>
        /// 
        /// </summary>
        public void ControlJobChange(Dictionary<string, List<string>> hashCjToPjs)
        {
            if (this.Visible)
                ControlJobDataGridViewTrhead(m_dgvControlJob, hashCjToPjs);
        }
        public void ControlJobChange()
        {
            if (this.Visible)
            {
                Dictionary<string, List<string>> hashCjToPjs = new Dictionary<string, List<string>>();

                long nObjID = 0;
                long nCJobCount = 0;

                if (MainControl.Xgem.CJGetAllJobInfo(ref nObjID, ref nCJobCount) == 0)
                {
                    string sCRJobID = string.Empty;
                    string[] sPRJobID =new string[1];

                    for (int i = 0; i < nCJobCount; i++)
                    {
                        MainControl.Xgem.GetCtrlJobID(nObjID, i, ref sCRJobID);
                        MainControl.Xgem.GetCtrlJobPRJobIDs(nObjID, i, 1, ref sPRJobID);

                        hashCjToPjs.Add(sCRJobID, new List<string>(sPRJobID));
                    }

                    MainControl.Xgem.GetPRJobClose(nObjID);
                }
                
                ControlJobDataGridViewTrhead(m_dgvControlJob, hashCjToPjs);
            }
        }
        #endregion

        #region Class CorssThread Methods
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadProcessJobRefresh(DataGridView dgv);
        /// <summary>
        /// 
        /// </summary>
        private void ProcessJobDataGridViewTrhead(DataGridView dgv)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadProcessJobRefresh reload = new ReloadProcessJobRefresh(ProcessJobDataGridViewTrhead);
                    this.Invoke(reload, new object[] { dgv });
                }
                else
                {
                    dgv.Rows.Clear();

                    long nObjID = 0;
                    long nPJobCount = 0;

                    if (MainControl.Xgem.PJGetAllJobInfo(ref nObjID, ref nPJobCount) == 0)
                    {
                        string sPRJobID = string.Empty;

                        for (int i = 0; i < nPJobCount; i++)
                        {
                            MainControl.Xgem.GetPRJobID(nObjID, i, ref sPRJobID);
                            dgv.Rows.Add(sPRJobID);
                        }

                        MainControl.Xgem.GetPRJobClose(nObjID);
                    }
                }
            }
            catch (System.Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ControlJobDataGridViewTrhead(DataGridView dgv, Dictionary<string, List<string>> hashCjToPjs)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadConrolJobRefresh reload = new ReloadConrolJobRefresh(ControlJobDataGridViewTrhead);
                    this.Invoke(reload, new object[] { dgv, hashCjToPjs });
                }
                else
                {
                    dgv.Rows.Clear();
                    m_listHasPJ.Clear();
                    m_hashCjToPjs = hashCjToPjs;

                    foreach (string sCjName in hashCjToPjs.Keys)
                    {
                        foreach (string sPjName in hashCjToPjs[sCjName])
                        {
                            dgv.Rows.Add(sCjName, sPjName);
                            if (!m_listHasPJ.Contains(sPjName))
                                m_listHasPJ.Add(sPjName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnProcessJobListCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView dataGridView = (DataGridView)sender;

                int iCol = dataGridView.CurrentCell.ColumnIndex;
                int iRow = dataGridView.CurrentCell.RowIndex;

                if (iRow == -1 || iCol != 1)
                    return;


                OfflineForm form = new OfflineForm("선택한 ProcessJob을 삭제 하시겠습니까?", CimConfigurator.Password);

                DialogResult res = form.ShowDialog();

                if (res != DialogResult.OK)
                    return;

                string sPjId = dataGridView[0, iRow].Value.ToString();

                if (m_listHasPJ.Contains(sPjId))
                {
                    MessageBox.Show("ControlJob에 포함되어 있는 ProcessJob은 삭제 할 수 없습니다.");
                    return;
                }

                if (MainControl.Xgem.PJDelJobInfo(sPjId) == 0)
                {
                    ProcessJobDataGridViewTrhead(m_dgvProcessJob);
                    MessageBox.Show(string.Format("Process Job : {0}이 정상적으로 삭제 되었습니다.", sPjId));
                }
                else
                    MessageBox.Show(string.Format("Process Job : {0}이 삭제 되지 않았습니다.", sPjId));
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ControlJobCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView dataGridView = (DataGridView)sender;

                int iCol = dataGridView.CurrentCell.ColumnIndex;
                int iRow = dataGridView.CurrentCell.RowIndex;

                if (iRow == -1 || iCol != 2)
                    return;

                OfflineForm form = new OfflineForm("선택한 ControlJob을 삭제 하시겠습니까?", CimConfigurator.Password);

                DialogResult res = form.ShowDialog();

                if (res != DialogResult.OK)
                    return;

                string sCjId = dataGridView[0, iRow].Value.ToString();

                if (MainControl.Xgem.CJDelJobInfo(sCjId) == 0)
                {
                    dataGridView.Rows.Remove(dataGridView.Rows[iRow]);

                    foreach (string sPj in m_hashCjToPjs[sCjId])
                        m_listHasPJ.Remove(sPj);

                    m_hashCjToPjs.Remove(sCjId);
                    MessageBox.Show(string.Format("Control Job : {0}이 정상적으로 삭제 되었습니다.", sCjId));
                }
                else
                    MessageBox.Show(string.Format("Control Job : {0}이 삭제 되지 않았습니다.", sCjId));
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnVisibleChange(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                ProcessJobDataGridViewTrhead(m_dgvProcessJob);

                MainControl.Xgem.CJReqGetAllJobID();
            }
        }
        #endregion
    }
}