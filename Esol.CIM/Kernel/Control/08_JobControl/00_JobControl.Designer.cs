﻿namespace Esol.CIM
{
    partial class JobControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_dgvProcessJob = new System.Windows.Forms.DataGridView();
            this.RecipeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parameter = new System.Windows.Forms.DataGridViewButtonColumn();
            this.roundPanel2 = new Esol.Components.RoundPanel();
            this.m_dgvControlJob = new System.Windows.Forms.DataGridView();
            this.ParameterName = new JHKIM.DataGridViewEx.DataGridViewTextBoxColumnEx();
            this.PjNames = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParemeterValue = new System.Windows.Forms.DataGridViewButtonColumn();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvProcessJob)).BeginInit();
            this.roundPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvControlJob)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "PROCESS JOB LIST";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvProcessJob);
            this.roundPanel1.Location = new System.Drawing.Point(9, 12);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel1.TabIndex = 11;
            // 
            // m_dgvProcessJob
            // 
            this.m_dgvProcessJob.AllowUserToAddRows = false;
            this.m_dgvProcessJob.AllowUserToDeleteRows = false;
            this.m_dgvProcessJob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvProcessJob.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RecipeId,
            this.Parameter});
            this.m_dgvProcessJob.Location = new System.Drawing.Point(7, 39);
            this.m_dgvProcessJob.Margin = new System.Windows.Forms.Padding(4);
            this.m_dgvProcessJob.Name = "m_dgvProcessJob";
            this.m_dgvProcessJob.ReadOnly = true;
            this.m_dgvProcessJob.RowTemplate.Height = 23;
            this.m_dgvProcessJob.Size = new System.Drawing.Size(1050, 1200);
            this.m_dgvProcessJob.TabIndex = 16;
            this.m_dgvProcessJob.Tag = "1";
            this.m_dgvProcessJob.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnProcessJobListCellClick);
            // 
            // RecipeId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.RecipeId.DefaultCellStyle = dataGridViewCellStyle1;
            this.RecipeId.Frozen = true;
            this.RecipeId.HeaderText = "ProcessJob Name";
            this.RecipeId.Name = "RecipeId";
            this.RecipeId.ReadOnly = true;
            this.RecipeId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.RecipeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RecipeId.Width = 300;
            // 
            // Parameter
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.Parameter.DefaultCellStyle = dataGridViewCellStyle2;
            this.Parameter.Frozen = true;
            this.Parameter.HeaderText = "Delete";
            this.Parameter.Name = "Parameter";
            this.Parameter.ReadOnly = true;
            this.Parameter.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Parameter.Width = 60;
            // 
            // roundPanel2
            // 
            this.roundPanel2._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel2._Round_Cutting = true;
            this.roundPanel2._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel2._Round_TitleImage = null;
            this.roundPanel2._Round_TitleImageShow = false;
            this.roundPanel2._Round_TitleLabelText = "CONTROL JOB LIST";
            this.roundPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel2.Controls.Add(this.m_dgvControlJob);
            this.roundPanel2.Location = new System.Drawing.Point(1088, 9);
            this.roundPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel2.Name = "roundPanel2";
            this.roundPanel2.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel2.TabIndex = 12;
            // 
            // m_dgvControlJob
            // 
            this.m_dgvControlJob.AllowUserToAddRows = false;
            this.m_dgvControlJob.AllowUserToDeleteRows = false;
            this.m_dgvControlJob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvControlJob.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterName,
            this.PjNames,
            this.ParemeterValue});
            this.m_dgvControlJob.Location = new System.Drawing.Point(16, 42);
            this.m_dgvControlJob.Margin = new System.Windows.Forms.Padding(4);
            this.m_dgvControlJob.Name = "m_dgvControlJob";
            this.m_dgvControlJob.ReadOnly = true;
            this.m_dgvControlJob.RowTemplate.Height = 23;
            this.m_dgvControlJob.Size = new System.Drawing.Size(1050, 1200);
            this.m_dgvControlJob.TabIndex = 16;
            this.m_dgvControlJob.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ControlJobCellClick);
            // 
            // ParameterName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            this.ParameterName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ParameterName.Frozen = true;
            this.ParameterName.HeaderText = "ControlJob Name";
            this.ParameterName.Name = "ParameterName";
            this.ParameterName.ReadOnly = true;
            this.ParameterName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ParameterName.Width = 300;
            // 
            // PjNames
            // 
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.PjNames.DefaultCellStyle = dataGridViewCellStyle4;
            this.PjNames.Frozen = true;
            this.PjNames.HeaderText = "ProcessJob Name";
            this.PjNames.Name = "PjNames";
            this.PjNames.ReadOnly = true;
            this.PjNames.Width = 300;
            // 
            // ParemeterValue
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.ParemeterValue.DefaultCellStyle = dataGridViewCellStyle5;
            this.ParemeterValue.Frozen = true;
            this.ParemeterValue.HeaderText = "Delete";
            this.ParemeterValue.Name = "ParemeterValue";
            this.ParemeterValue.ReadOnly = true;
            this.ParemeterValue.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ParemeterValue.Width = 60;
            // 
            // JobControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.roundPanel2);
            this.Controls.Add(this.roundPanel1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "JobControl";
            this.Text = "RealLogControl";
            this.VisibleChanged += new System.EventHandler(this.OnVisibleChange);
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvProcessJob)).EndInit();
            this.roundPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvControlJob)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvProcessJob;
        private Components.RoundPanel roundPanel2;
        private System.Windows.Forms.DataGridView m_dgvControlJob;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipeId;
        private System.Windows.Forms.DataGridViewButtonColumn Parameter;
        private JHKIM.DataGridViewEx.DataGridViewTextBoxColumnEx ParameterName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PjNames;
        private System.Windows.Forms.DataGridViewButtonColumn ParemeterValue;
    }
}