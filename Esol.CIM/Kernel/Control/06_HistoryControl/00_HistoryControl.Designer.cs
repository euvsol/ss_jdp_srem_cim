﻿namespace Esol.CIM
{
    partial class HistoryControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.roundPanel2 = new Esol.Components.RoundPanel();
            this.m_dgvAlarm = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_dgvTerminal = new System.Windows.Forms.DataGridView();
            this.sNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvAlarm)).BeginInit();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTerminal)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel2
            // 
            this.roundPanel2._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel2._Round_Cutting = true;
            this.roundPanel2._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel2._Round_TitleImage = null;
            this.roundPanel2._Round_TitleImageShow = false;
            this.roundPanel2._Round_TitleLabelText = "ALARM MESSAGE HISTORY";
            this.roundPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel2.Controls.Add(this.m_dgvAlarm);
            this.roundPanel2.Location = new System.Drawing.Point(1086, 9);
            this.roundPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel2.Name = "roundPanel2";
            this.roundPanel2.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel2.TabIndex = 0;
            // 
            // m_dgvAlarm
            // 
            this.m_dgvAlarm.AllowUserToAddRows = false;
            this.m_dgvAlarm.AllowUserToDeleteRows = false;
            this.m_dgvAlarm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvAlarm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.sCode,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.m_dgvAlarm.Location = new System.Drawing.Point(10, 40);
            this.m_dgvAlarm.Name = "m_dgvAlarm";
            this.m_dgvAlarm.ReadOnly = true;
            this.m_dgvAlarm.RowTemplate.Height = 30;
            this.m_dgvAlarm.Size = new System.Drawing.Size(1050, 1200);
            this.m_dgvAlarm.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Id";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // sCode
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.sCode.Frozen = true;
            this.sCode.HeaderText = "Code";
            this.sCode.Name = "sCode";
            this.sCode.ReadOnly = true;
            this.sCode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sCode.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "Text";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 400;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "Time";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "TERMINAL MESSAGE HISTORY";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvTerminal);
            this.roundPanel1.Location = new System.Drawing.Point(10, 10);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel1.TabIndex = 0;
            // 
            // m_dgvTerminal
            // 
            this.m_dgvTerminal.AllowUserToAddRows = false;
            this.m_dgvTerminal.AllowUserToDeleteRows = false;
            this.m_dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvTerminal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sNo,
            this.sTid,
            this.sText,
            this.sTime});
            this.m_dgvTerminal.Location = new System.Drawing.Point(10, 40);
            this.m_dgvTerminal.Name = "m_dgvTerminal";
            this.m_dgvTerminal.ReadOnly = true;
            this.m_dgvTerminal.RowTemplate.Height = 30;
            this.m_dgvTerminal.Size = new System.Drawing.Size(1050, 1200);
            this.m_dgvTerminal.TabIndex = 1;
            // 
            // sNo
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sNo.DefaultCellStyle = dataGridViewCellStyle4;
            this.sNo.Frozen = true;
            this.sNo.HeaderText = "No";
            this.sNo.Name = "sNo";
            this.sNo.ReadOnly = true;
            this.sNo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sNo.Width = 40;
            // 
            // sTid
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sTid.DefaultCellStyle = dataGridViewCellStyle5;
            this.sTid.Frozen = true;
            this.sTid.HeaderText = "Tid";
            this.sTid.Name = "sTid";
            this.sTid.ReadOnly = true;
            this.sTid.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sTid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sTid.Width = 30;
            // 
            // sText
            // 
            this.sText.Frozen = true;
            this.sText.HeaderText = "Text";
            this.sText.Name = "sText";
            this.sText.ReadOnly = true;
            this.sText.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sText.Width = 440;
            // 
            // sTime
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sTime.DefaultCellStyle = dataGridViewCellStyle6;
            this.sTime.Frozen = true;
            this.sTime.HeaderText = "Time";
            this.sTime.Name = "sTime";
            this.sTime.ReadOnly = true;
            this.sTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sTime.Width = 150;
            // 
            // HistoryControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.roundPanel2);
            this.Controls.Add(this.roundPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HistoryControl";
            this.Text = "RealLogControl";
            this.VisibleChanged += new System.EventHandler(this.OnVisibleChanged);
            this.roundPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvAlarm)).EndInit();
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTerminal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvTerminal;
        private Components.RoundPanel roundPanel2;
        private System.Windows.Forms.DataGridView m_dgvAlarm;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn sCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn sNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTid;
        private System.Windows.Forms.DataGridViewTextBoxColumn sText;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTime;
    }
}