﻿#region Class Using
using System;
using System.Drawing;
using System.Windows.Forms;
#endregion

namespace Esol.CIM
{
    public partial class HistoryControl : FormControl
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadTerminalDataGridView(DataGridView dgv, string[][] list);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadAlarmDataGridView(DataGridView dgv, string[][] list);
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public HistoryControl()
        {
            InitializeComponent();
            this.TopLevel = false;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            InitializeDataGridView();

            //Alarm History
            MainControl.CimConfigurator.AlarmManager.AlarmHistoryListChanged +=
                new AlarmManager.AlarmMessageEventHandler(AlarmUpdateList);

            string[][] listAlarm = MainControl.CimConfigurator.AlarmManager.GetHistoryMessagesList();

            if (listAlarm.Length != 0)
                AlarmUpdateList(listAlarm);

            //Terminal History
            MainControl.CimConfigurator.TerminalManager.TerminalHistoryListChanged +=
                new TerminalManager.TerminalMessageEventHandler(TerminalUpdateList);

            string[][] listTerminal = MainControl.CimConfigurator.TerminalManager.GetHistoryMessagesList();

            if (listTerminal.Length != 0)
                TerminalUpdateList(listTerminal);
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            //DataGridView 가운데 정렬
            for (int i = 0; i < m_dgvAlarm.Columns.Count; i++)
                m_dgvAlarm.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvAlarm.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;

            for (int i = 0; i < m_dgvTerminal.Columns.Count; i++)
                m_dgvTerminal.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvTerminal.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void AlarmUpdateList(string[][] list)
        {
            AlarmDataGridViewCrossThread(m_dgvAlarm, list);
        }
        /// <summary>
        /// 
        /// </summary>
        private void AlarmDataGridViewCrossThread(DataGridView dgv, string[][] list)
        {
            if (dgv.InvokeRequired)
            {
                ReloadAlarmDataGridView reload = new ReloadAlarmDataGridView(AlarmDataGridViewCrossThread);
                this.Invoke(reload, new object[] { dgv, list });
            }
            else
            {
                try
                {
                    if (list.Length == 0)
                        return;

                    m_dgvAlarm.Rows.Clear();

                    m_dgvAlarm.Rows.Add(list.Length);

                    int iRowCount = 0;

                    for (int i = list.Length - 1; i >= 0; i -= 1)
                    {
                        m_dgvAlarm.Rows[iRowCount].Cells[0].Value = list[i][0];
                        m_dgvAlarm.Rows[iRowCount].Cells[1].Value = list[i][3];
                        m_dgvAlarm.Rows[iRowCount].Cells[2].Value = list[i][1];
                        m_dgvAlarm.Rows[iRowCount++].Cells[3].Value = list[i][2];
                    }
                    m_dgvAlarm.CurrentCell = m_dgvAlarm.Rows[0].Cells[0];
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void TerminalUpdateList(string[][] list)
        {
            TeriminalDataGridViewCrossThread(m_dgvTerminal, list);
        }
        /// <summary>
        /// 
        /// </summary>
        private void TeriminalDataGridViewCrossThread(DataGridView dgv, string[][] list)
        {
            if (dgv.InvokeRequired)
            {
                ReloadTerminalDataGridView reload = new ReloadTerminalDataGridView(TeriminalDataGridViewCrossThread);
                this.Invoke(reload, new object[] { dgv, list });
            }
            else
            {
                try
                {
                    if (list.Length == 0)
                        return;

                    m_dgvTerminal.Rows.Clear();

                    m_dgvTerminal.Rows.Add(list.Length);

                    int iRowCount = 0;

                    for (int i = list.Length - 1; i >= 0; i -= 1)
                    {
                        m_dgvTerminal.Rows[iRowCount].Cells[0].Value = iRowCount + 1;
                        m_dgvTerminal.Rows[iRowCount].Cells[1].Value = list[i][0];
                        m_dgvTerminal.Rows[iRowCount].Cells[2].Value = list[i][1];
                        m_dgvTerminal.Rows[iRowCount++].Cells[3].Value = list[i][2];
                    }

                    m_dgvTerminal.CurrentCell = m_dgvTerminal.Rows[0].Cells[0];
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnVisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
                return;


            string[][] listTerminal = MainControl.CimConfigurator.TerminalManager.GetHistoryMessagesList();

            if (listTerminal.Length != 0)
                TerminalUpdateList(listTerminal);


            string[][] listAlarm = MainControl.CimConfigurator.AlarmManager.GetHistoryMessagesList();

            if (listAlarm.Length != 0)
                AlarmUpdateList(listAlarm);
        }
        #endregion
    }
}
