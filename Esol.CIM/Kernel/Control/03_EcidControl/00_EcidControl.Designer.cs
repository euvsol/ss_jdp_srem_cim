﻿namespace Esol.CIM
{
    partial class EcidControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_dgvEcid = new System.Windows.Forms.DataGridView();
            this.Vid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvEcid)).BeginInit();
            this.roundPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_dgvEcid
            // 
            this.m_dgvEcid.AllowUserToAddRows = false;
            this.m_dgvEcid.AllowUserToDeleteRows = false;
            this.m_dgvEcid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvEcid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Vid,
            this.ItemName,
            this.ItemValue,
            this.ItemDefault,
            this.ItemMin,
            this.ItemMax,
            this.ItemUnit});
            this.m_dgvEcid.Location = new System.Drawing.Point(10, 40);
            this.m_dgvEcid.Name = "m_dgvEcid";
            this.m_dgvEcid.ReadOnly = true;
            this.m_dgvEcid.RowTemplate.Height = 30;
            this.m_dgvEcid.Size = new System.Drawing.Size(2130, 1207);
            this.m_dgvEcid.TabIndex = 0;
            // 
            // Vid
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Vid.DefaultCellStyle = dataGridViewCellStyle1;
            this.Vid.Frozen = true;
            this.Vid.HeaderText = "Vid";
            this.Vid.Name = "Vid";
            this.Vid.ReadOnly = true;
            this.Vid.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Vid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Vid.Width = 150;
            // 
            // ItemName
            // 
            this.ItemName.Frozen = true;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 400;
            // 
            // ItemValue
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ItemValue.DefaultCellStyle = dataGridViewCellStyle2;
            this.ItemValue.Frozen = true;
            this.ItemValue.HeaderText = "Item Value";
            this.ItemValue.Name = "ItemValue";
            this.ItemValue.ReadOnly = true;
            this.ItemValue.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemValue.Width = 350;
            // 
            // ItemDefault
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ItemDefault.DefaultCellStyle = dataGridViewCellStyle3;
            this.ItemDefault.Frozen = true;
            this.ItemDefault.HeaderText = "Default";
            this.ItemDefault.Name = "ItemDefault";
            this.ItemDefault.ReadOnly = true;
            this.ItemDefault.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemDefault.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemMin
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ItemMin.DefaultCellStyle = dataGridViewCellStyle4;
            this.ItemMin.Frozen = true;
            this.ItemMin.HeaderText = "Min";
            this.ItemMin.Name = "ItemMin";
            this.ItemMin.ReadOnly = true;
            this.ItemMin.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemMin.Width = 150;
            // 
            // ItemMax
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ItemMax.DefaultCellStyle = dataGridViewCellStyle5;
            this.ItemMax.Frozen = true;
            this.ItemMax.HeaderText = "Max";
            this.ItemMax.Name = "ItemMax";
            this.ItemMax.ReadOnly = true;
            this.ItemMax.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemMax.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemMax.Width = 150;
            // 
            // ItemUnit
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ItemUnit.DefaultCellStyle = dataGridViewCellStyle6;
            this.ItemUnit.Frozen = true;
            this.ItemUnit.HeaderText = "Unit";
            this.ItemUnit.Name = "ItemUnit";
            this.ItemUnit.ReadOnly = true;
            this.ItemUnit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "ECID";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvEcid);
            this.roundPanel1.Location = new System.Drawing.Point(10, 10);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(2149, 1252);
            this.roundPanel1.TabIndex = 4;
            // 
            // EcidControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.roundPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EcidControl";
            this.Text = "RealLogControl";
            this.VisibleChanged += new System.EventHandler(this.OnVisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvEcid)).EndInit();
            this.roundPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView m_dgvEcid;
        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMin;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemUnit;
    }
}