﻿#region Class Using
using System;
using System.Drawing;
using System.Windows.Forms;
#endregion

namespace Esol.CIM
{
    public partial class EcidControl : FormControl
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataGridView(DataGridView dgv, long lCount, long[] plVid, string[] psName, string[] psValue, string[] psDefault, string[] psMin, string[] psMax, string[] psUnit);
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public EcidControl()
        {
            InitializeComponent();
            this.TopLevel = false;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            InitializeDataGridView();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            for (int i = 0; i < m_dgvEcid.Columns.Count; i++)
                m_dgvEcid.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvEcid.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnVisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
                MainControl.Xgem.GEMReqAllECInfo();
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnEcidChange(long lCount, long[] plVid, string[] psName, string[] psValue, string[] psDefault, string[] psMin, string[] psMax, string[] psUnit)
        {
            if (this.Visible)
                DataGridViewCrossThread(m_dgvEcid, lCount, plVid, psName, psValue, psDefault, psMin, psMax, psUnit);
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewCrossThread(DataGridView dgv, long lCount, long[] plVid, string[] psName, string[] psValue, string[] psDefault, string[] psMin, string[] psMax, string[] psUnit)
        {
            if (dgv.InvokeRequired)
            {
                ReloadDataGridView reload = new ReloadDataGridView(DataGridViewCrossThread);
                this.Invoke(reload, new object[] { dgv, lCount, plVid, psName, psValue, psDefault, psMin, psMax, psUnit });
            }
            else
            {
                try
                {
                    m_dgvEcid.Rows.Clear();

                    int iCount = 0;

                    foreach (string sName in psName)
                    {
                        m_dgvEcid.Rows.Add(lCount);
                        m_dgvEcid.Rows[iCount].Cells[0].Value = plVid[iCount];
                        m_dgvEcid.Rows[iCount].Cells[1].Value = psName[iCount];
                        m_dgvEcid.Rows[iCount].Cells[2].Value = psValue[iCount];
                        m_dgvEcid.Rows[iCount].Cells[3].Value = psDefault[iCount];
                        m_dgvEcid.Rows[iCount].Cells[4].Value = psMin[iCount];
                        m_dgvEcid.Rows[iCount].Cells[5].Value = psMax[iCount];
                        m_dgvEcid.Rows[iCount].Cells[6].Value = psUnit[iCount];

                        iCount++;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        #endregion
    }
}
