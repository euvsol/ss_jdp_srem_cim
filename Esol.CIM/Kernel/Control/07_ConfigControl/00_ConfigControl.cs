﻿#region Class Using
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using GEM_XGem300Pro;
#endregion

namespace Esol.CIM
{
    public partial class ConfigControl : FormControl
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> m_hashConfig;
        /// <summary>
        /// 
        /// </summary>
        private List<string> m_listConfigDiscription;
        /// <summary>
        /// 
        /// </summary>
        private List<string> m_listConfigItemNames;
        /// <summary>
        /// 
        /// </summary>
        private List<string> m_listConfigValues;
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public ConfigControl()
        {
            InitializeComponent();

            m_hashConfig = new Dictionary<string, string>();
            m_listConfigValues = new List<string>();

            this.TopLevel = false;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            InitializeDataGridView();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            for (int i = 0; i < m_dgvConfig.Columns.Count; i++)
                m_dgvConfig.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvConfig.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        /// <summary>
        /// 
        /// </summary>
        public void InitializeConfig()
        {
            m_listConfigItemNames = new List<string>()
            {
                "Device ID",
                "IP",
                "Port",
                "Active",
                "Link Test Interval",
                "T3",
                "T5",
                "T6",
                "T7",
                "T8",
                "Scan No"
            };

            m_listConfigDiscription = new List<string>()
            {
                "Host Connect Device ID",
                "Host IP Adress",
                "Host Port No",
                "False : Passive / True : Active",
                "Link Test Send Time",
                "Range : 1 ~ 120 / Default : 45",
                "Range : 1 ~ 240 / Default : 10",
                "Range : 1 ~ 240 / Default : 15",
                "Range : 1 ~ 240 / Default : 10",
                "Range : 1 ~ 120 / Default : 5",
                "RFF Scan No Setting ( 1 ~ 3 )"
            };

            foreach (ConfigItems oKey in Enum.GetValues(typeof(ConfigItems)))
            {
                string sKey = oKey.ToString().Replace('_', ' ');
                m_dgvConfig.Rows.Add();
                int iCount = m_dgvConfig.Rows.Count - 1;
                m_dgvConfig.Rows[iCount].Cells[0].Value = sKey;
                m_dgvConfig.Rows[iCount].Cells[2].Value = m_listConfigDiscription[iCount];

                m_hashConfig.Add(sKey, string.Empty);
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnVisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible && MainControl.Xgem.GetActive())
            {
                try
                {
                    string sValue = string.Empty;
                    m_listConfigValues = new List<string>();

                    for (int i = 0; i < m_listConfigItemNames.Count; i++)
                    {
                        if (m_listConfigItemNames[i] == "Scan No")
                        {
                            m_dgvConfig.Rows[i].Cells[1].Value = CimConfigurator.ScanNo;
                            m_listConfigValues.Add(sValue);
                        }
                        else
                        {
                            MainControl.Xgem.GEMGetParam(m_listConfigItemNames[i], ref sValue);
                            if (m_listConfigItemNames[i] == "Active")
                            {
                                DataGridViewComboBoxCell ccell = new DataGridViewComboBoxCell();
                                ccell.Items.Add("true");
                                ccell.Items.Add("false");
                                ccell.Value = sValue.ToLower();

                                m_dgvConfig.Rows[i].Cells[1] = ccell;
                            }
                            else
                                m_dgvConfig.Rows[i].Cells[1].Value = sValue;
                            m_listConfigValues.Add(sValue);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnSaveConfig(object sender, EventArgs e)
        {
            string sValue = string.Empty;
            m_listConfigValues = new List<string>();

            OfflineForm form = new OfflineForm("Config를 저장 하시겠습니까?", CimConfigurator.Password);

            DialogResult res = form.ShowDialog();

            if (res != DialogResult.OK)
                return;
            try
            {
                long lCheck = 0;
                for (int i = 0; i < m_listConfigItemNames.Count; i++)
                {
                    if (m_listConfigItemNames[i] == "Scan No")
                    {
                        if (m_dgvConfig.Rows[i].Cells[1].Value.ToString() != "1" &&
                            m_dgvConfig.Rows[i].Cells[1].Value.ToString() != "2" &&
                            m_dgvConfig.Rows[i].Cells[1].Value.ToString() != "3")
                        {
                            MessageBox.Show(string.Format("{0}값이 잘못되었습니다.", m_dgvConfig.Rows[i].Cells[0].Value.ToString()));
                            return;
                        }

                        CimConfigurator.SaveScanNoConfig(m_dgvConfig.Rows[i].Cells[1].Value.ToString());
                        m_listConfigValues.Add(sValue);
                    }
                    else
                    {
                        sValue = m_dgvConfig.Rows[i].Cells[1].Value.ToString();
                        lCheck = MainControl.Xgem.GEMSetParam(m_listConfigItemNames[i], sValue);
                        if (lCheck != 0)
                        {
                            MessageBox.Show(string.Format("{0}값이 잘못되었습니다.", m_dgvConfig.Rows[i].Cells[0].Value.ToString()));
                            return;
                        }

                        m_listConfigValues.Add(sValue);
                    }
                }

                MessageBox.Show("정상적으로 Update 되었습니다.");
                LogManager.ButtonWriteLog("Config 적용");
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>l
        private void OnCancelConfig(object sender, EventArgs e)
        {
            for (int i = 0; i < m_listConfigValues.Count; i++)
                m_dgvConfig.Rows[i].Cells[1].Value = m_listConfigValues[i];

            MessageBox.Show("이전 값으로 복원 되었습니다.");

            LogManager.ButtonWriteLog("Config 취소");
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnCarrierInfoDelete(object sender, EventArgs e)
        {
            if (MainControl.Xgem.CMSDelAllCarrierInfo() == 0)
                MessageBox.Show("모든 Carrier Info가 정상적으로 삭제 되었습니다.");
            else
                MessageBox.Show("Carrier Info가 삭제되지 않았습니다.");
        }
        #endregion

    }
}
