﻿namespace Esol.CIM
{
    partial class ConfigControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.m_btnSave = new System.Windows.Forms.Button();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_dgvConfig = new System.Windows.Forms.DataGridView();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_btnCarrierAllDelete = new System.Windows.Forms.Button();
            this.roundPanel2 = new Esol.Components.RoundPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvConfig)).BeginInit();
            this.roundPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.Location = new System.Drawing.Point(975, 1210);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(86, 30);
            this.m_btnCancel.TabIndex = 1;
            this.m_btnCancel.Text = "취소";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            this.m_btnCancel.Click += new System.EventHandler(this.OnCancelConfig);
            // 
            // m_btnSave
            // 
            this.m_btnSave.Location = new System.Drawing.Point(883, 1210);
            this.m_btnSave.Name = "m_btnSave";
            this.m_btnSave.Size = new System.Drawing.Size(86, 30);
            this.m_btnSave.TabIndex = 1;
            this.m_btnSave.Text = "적용";
            this.m_btnSave.UseVisualStyleBackColor = true;
            this.m_btnSave.Click += new System.EventHandler(this.OnSaveConfig);
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "CONFIG";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvConfig);
            this.roundPanel1.Controls.Add(this.m_btnCancel);
            this.roundPanel1.Controls.Add(this.m_btnSave);
            this.roundPanel1.Location = new System.Drawing.Point(10, 10);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel1.TabIndex = 2;
            // 
            // m_dgvConfig
            // 
            this.m_dgvConfig.AllowUserToAddRows = false;
            this.m_dgvConfig.AllowUserToDeleteRows = false;
            this.m_dgvConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvConfig.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemName,
            this.Value,
            this.Discription});
            this.m_dgvConfig.Location = new System.Drawing.Point(10, 40);
            this.m_dgvConfig.Name = "m_dgvConfig";
            this.m_dgvConfig.RowTemplate.Height = 30;
            this.m_dgvConfig.Size = new System.Drawing.Size(1050, 1150);
            this.m_dgvConfig.TabIndex = 16;
            // 
            // ItemName
            // 
            this.ItemName.Frozen = true;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 250;
            // 
            // Value
            // 
            this.Value.Frozen = true;
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            this.Value.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Discription
            // 
            this.Discription.Frozen = true;
            this.Discription.HeaderText = "Discription";
            this.Discription.Name = "Discription";
            this.Discription.ReadOnly = true;
            this.Discription.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Discription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Discription.Width = 300;
            // 
            // m_btnCarrierAllDelete
            // 
            this.m_btnCarrierAllDelete.Location = new System.Drawing.Point(214, 67);
            this.m_btnCarrierAllDelete.Name = "m_btnCarrierAllDelete";
            this.m_btnCarrierAllDelete.Size = new System.Drawing.Size(95, 28);
            this.m_btnCarrierAllDelete.TabIndex = 3;
            this.m_btnCarrierAllDelete.UseVisualStyleBackColor = true;
            this.m_btnCarrierAllDelete.Click += new System.EventHandler(this.OnCarrierInfoDelete);
            // 
            // roundPanel2
            // 
            this.roundPanel2._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel2._Round_Cutting = true;
            this.roundPanel2._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel2._Round_TitleImage = null;
            this.roundPanel2._Round_TitleImageShow = false;
            this.roundPanel2._Round_TitleLabelText = "COMMAND";
            this.roundPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel2.Controls.Add(this.label1);
            this.roundPanel2.Controls.Add(this.m_btnCarrierAllDelete);
            this.roundPanel2.Location = new System.Drawing.Point(1088, 9);
            this.roundPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel2.Name = "roundPanel2";
            this.roundPanel2.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel2.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(21, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "All Carrier Info Delete :";
            // 
            // ConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.roundPanel2);
            this.Controls.Add(this.roundPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConfigControl";
            this.Text = "RealLogControl";
            this.VisibleChanged += new System.EventHandler(this.OnVisibleChanged);
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvConfig)).EndInit();
            this.roundPanel2.ResumeLayout(false);
            this.roundPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button m_btnSave;
        private System.Windows.Forms.Button m_btnCancel;
        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvConfig;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn Discription;
        private System.Windows.Forms.Button m_btnCarrierAllDelete;
        private Components.RoundPanel roundPanel2;
        private System.Windows.Forms.Label label1;
    }
}