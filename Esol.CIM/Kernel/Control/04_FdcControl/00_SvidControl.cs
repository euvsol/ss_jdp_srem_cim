﻿#region Class Using
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
#endregion

namespace Esol.CIM
{
    public partial class SvidControl : FormControl
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ELEMENT_NAME = "SvidEntries";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ELEMENT_SVID_ENTRY = "Svid";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private long[] m_pnVids;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psMins;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psMaxs;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_psValues;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadFdc;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bInitRead;
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataGridView(DataGridView dgv, long[] pnVids, string[] pnValues, string[] pnMins, string[] pnMaxs);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataGridViewValue(DataGridView dgv, string[] pnValues);
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public SvidControl()
        {
            InitializeComponent();
            this.TopLevel = false;
            m_bInitRead = false;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            InitializeDataGridView();

            ThreadStart tsShareDataChange = new ThreadStart(ThreadFdc);
            m_threadFdc = new Thread(tsShareDataChange)
            { IsBackground = true };

            m_threadFdc.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            for (int i = 0; i < m_dgvSvid.Columns.Count; i++)
                m_dgvSvid.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvSvid.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnVisibleChanged(object sender, EventArgs e)
        {
            try
            {
                m_pnVids = MainControl.CimConfigurator.SvidManager.ListTotalKeys.ToArray();

                m_psValues = new string[m_pnVids.Length];

                if (!m_bInitRead)
                {
                    m_psMins = new string[m_pnVids.Length];
                    m_psMaxs = new string[m_pnVids.Length];

                    long nReturn = MainControl.Xgem.GEMGetSVInfo(m_pnVids.Length, m_pnVids, ref m_psMins, ref m_psMaxs);

                    if (nReturn == 0)
                        m_bInitRead = true;
                }

                long nReturnValue = MainControl.Xgem.GEMGetVariable(m_pnVids.Length, ref m_pnVids, ref m_psValues);

                if (nReturnValue == 0)
                    DataGridViewCrossThread(m_dgvSvid, m_pnVids, m_psValues, m_psMins, m_psMaxs);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ThreadFdc()
        {
            while (true)
            {
                Thread.Sleep(1000);

                if (!this.Visible)
                    continue;

                try
                {
                    //long nReturn = MainControl.Xgem.GEMGetVariable(m_pnVids.Length, ref m_pnVids, ref m_psValues);
                    CimConfigurator.SvidManager.HashPlcChannelNameToValue.Values.CopyTo(m_psValues, 0);

                    DataGridViewValueCrossThread(m_dgvSvid, m_psValues);
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewValueCrossThread(DataGridView dgv, string[] pnValues)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadDataGridViewValue reload = new ReloadDataGridViewValue(DataGridViewValueCrossThread);
                    this.Invoke(reload, new object[] { dgv, pnValues });
                }
                else
                {
                    int iCount = 0;

                    foreach (string sValue in pnValues)
                    {
                        m_dgvSvid.Rows[iCount].Cells[2].Value = pnValues[iCount];
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewCrossThread(DataGridView dgv, long[] pnVids, string[] pnValues, string[] pnMins, string[] pnMaxs)
        {
            if (dgv.InvokeRequired)
            {
                ReloadDataGridView reload = new ReloadDataGridView(DataGridViewCrossThread);
                this.Invoke(reload, new object[] { dgv, pnVids, pnValues, pnMins, pnMaxs });
            }
            else
            {
                try
                {
                    m_dgvSvid.Rows.Clear();
                    int iCount = 0;

                    foreach (string sValue in pnValues)
                    {
                        int iSvid = Convert.ToInt32(pnVids[iCount]);

                        m_dgvSvid.Rows.Add(1);
                        m_dgvSvid.Rows[iCount].Cells[0].Value = pnVids[iCount];
                        m_dgvSvid.Rows[iCount].Cells[2].Value = pnValues[iCount];

                        if (CimConfigurator.SvidManager.SvidKeyToData.ContainsKey(iSvid))
                        {
                            m_dgvSvid.Rows[iCount].Cells[1].Value = CimConfigurator.SvidManager.SvidKeyToData[iSvid].SvidName;
                            m_dgvSvid.Rows[iCount].Cells[3].Value = CimConfigurator.SvidManager.SvidKeyToData[iSvid].SvidType;
                            m_dgvSvid.Rows[iCount].Cells[4].Value = CimConfigurator.SvidManager.SvidKeyToData[iSvid].SvidUnit;
                            m_dgvSvid.Rows[iCount].Cells[5].Value = CimConfigurator.SvidManager.SvidKeyToData[iSvid].SivdRange;
                        }
                        iCount++;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnStop()
        {
            if (m_threadFdc != null)
                m_threadFdc.Abort();
        }
        #endregion
    }
}
