﻿namespace Esol.CIM
{
    partial class SvidControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_dgvSvid = new System.Windows.Forms.DataGridView();
            this.SVID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SVIDNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SVIDVALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RANGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvSvid)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "SVID/FDC";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvSvid);
            this.roundPanel1.Location = new System.Drawing.Point(10, 10);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(2149, 1252);
            this.roundPanel1.TabIndex = 0;
            // 
            // m_dgvSvid
            // 
            this.m_dgvSvid.AllowUserToAddRows = false;
            this.m_dgvSvid.AllowUserToDeleteRows = false;
            this.m_dgvSvid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvSvid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SVID,
            this.SVIDNAME,
            this.SVIDVALUE,
            this.TYPE,
            this.UNIT,
            this.RANGE});
            this.m_dgvSvid.Location = new System.Drawing.Point(10, 40);
            this.m_dgvSvid.Name = "m_dgvSvid";
            this.m_dgvSvid.ReadOnly = true;
            this.m_dgvSvid.RowTemplate.Height = 30;
            this.m_dgvSvid.Size = new System.Drawing.Size(2130, 1200);
            this.m_dgvSvid.TabIndex = 16;
            this.m_dgvSvid.VisibleChanged += new System.EventHandler(this.OnVisibleChanged);
            // 
            // SVID
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SVID.DefaultCellStyle = dataGridViewCellStyle1;
            this.SVID.HeaderText = "SVID";
            this.SVID.Name = "SVID";
            this.SVID.ReadOnly = true;
            this.SVID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SVID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SVID.Width = 150;
            // 
            // SVIDNAME
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.SVIDNAME.DefaultCellStyle = dataGridViewCellStyle2;
            this.SVIDNAME.HeaderText = "SVID NAME";
            this.SVIDNAME.Name = "SVIDNAME";
            this.SVIDNAME.ReadOnly = true;
            this.SVIDNAME.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SVIDNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SVIDNAME.Width = 460;
            // 
            // SVIDVALUE
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SVIDVALUE.DefaultCellStyle = dataGridViewCellStyle3;
            this.SVIDVALUE.HeaderText = "SVID VALUE";
            this.SVIDVALUE.Name = "SVIDVALUE";
            this.SVIDVALUE.ReadOnly = true;
            this.SVIDVALUE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SVIDVALUE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SVIDVALUE.Width = 400;
            // 
            // TYPE
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TYPE.DefaultCellStyle = dataGridViewCellStyle4;
            this.TYPE.HeaderText = "TYPE";
            this.TYPE.Name = "TYPE";
            this.TYPE.ReadOnly = true;
            this.TYPE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TYPE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TYPE.Width = 140;
            // 
            // UNIT
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.UNIT.DefaultCellStyle = dataGridViewCellStyle5;
            this.UNIT.HeaderText = "UNIT";
            this.UNIT.Name = "UNIT";
            this.UNIT.ReadOnly = true;
            this.UNIT.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.UNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UNIT.Width = 140;
            // 
            // RANGE
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RANGE.DefaultCellStyle = dataGridViewCellStyle6;
            this.RANGE.HeaderText = "RANGE";
            this.RANGE.Name = "RANGE";
            this.RANGE.ReadOnly = true;
            this.RANGE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.RANGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RANGE.Width = 140;
            // 
            // SvidControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.roundPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SvidControl";
            this.Text = "RealLogControl";
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvSvid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvSvid;
        private System.Windows.Forms.DataGridViewTextBoxColumn SVID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SVIDNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SVIDVALUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn RANGE;
    }
}