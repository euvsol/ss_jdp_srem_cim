﻿#region Class Using
using System;
using System.Drawing;
using System.Runtime.InteropServices;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
#endregion

namespace Esol.CIM
{
    public partial class AlarmExportPopup : Form
    {
        #region Class Members
        ///// <summary>
        ///// 
        ///// </summary>
        //private static Excel.Application excelApp = null;
        ///// <summary>
        ///// 
        ///// </summary>
        //private static Excel.Workbook workBook = null;
        ///// <summary>
        ///// 
        ///// </summary>
        //private static Excel.Worksheet workSheet = null;
        ///// <summary>
        ///// 
        ///// </summary>
        //private MainControl m_MainControl;
        #endregion

        #region Class Initialization
        ///// <summary>
        ///// 
        ///// </summary>
        public AlarmExportPopup(MainControl maincontrol)
        {
            InitializeComponent();

            //m_MainControl = maincontrol;

            //for (int i = 0; i < m_dgvExportList.Columns.Count; i++)
            //    m_dgvExportList.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            //m_dgvExportList.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            //m_cbSelectAll.Checked = true;

            //m_dgvExportList.Rows.Clear();

            //foreach (AlarmData data in m_MainControl.CimConfigurator.AlarmManager.HashAlarmIdToData.Values)
            //    m_dgvExportList.Rows.Add(true, data.ALID, data.ALCD, data.ALTX, data.USE);
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnCancel(object sender, EventArgs e)
        {
            this.Hide();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnExportSave(object sender, EventArgs e)
        {
            //if (m_MainControl.NotificationBoard.HsmsState != ControlState.Offline)
            //{
            //    MessageBox.Show("Offline에서 시도 해주세요");
            //    return;
            //}

            //bool bCheck = false;
            //int iCount = 0;

            //string sFileName = string.Empty;

            //SaveFileDialog saveFileDialog = new SaveFileDialog()
            //{
            //    Title = "File Save Path",
            //    OverwritePrompt = false,
            //    Filter = "xls File(*.xls)|*.xls"
            //};

            //if (saveFileDialog.ShowDialog() == DialogResult.OK)
            //{
            //    sFileName = saveFileDialog.FileName;
            //    try
            //    {
            //        string path = sFileName;

            //        excelApp = new Excel.Application();
            //        workBook = excelApp.Workbooks.Add();
            //        workSheet = workBook.Worksheets.get_Item(1) as Excel.Worksheet;

            //        workSheet.Cells[1, 1] = "AlarmID";
            //        workSheet.Cells[1, 2] = "AlarmCode";
            //        workSheet.Cells[1, 3] = "AlarmText";
            //        workSheet.Cells[1, 4] = "Enabled";

            //        for (int i = 0; i < m_dgvExportList.Rows.Count; i++)
            //        {
            //            bool bSvaeCheck = (bool)m_dgvExportList.Rows[i].Cells[0].Value;

            //            if (!bSvaeCheck)
            //                continue;

            //            workSheet.Cells[2 + iCount, 1] = m_dgvExportList.Rows[i].Cells[1].Value.ToString();
            //            workSheet.Cells[2 + iCount, 2] = "129";
            //            workSheet.Cells[2 + iCount, 3] = m_dgvExportList.Rows[i].Cells[3].Value.ToString();
            //            workSheet.Cells[2 + iCount, 4] = m_dgvExportList.Rows[i].Cells[4].Value.ToString().ToUpper() == "TRUE" ? "1" : "0";
            //            iCount++;
            //        }

            //        workSheet.Columns.AutoFit();
            //        workBook.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal);
            //        workBook.Close(true);
            //        excelApp.Quit();
            //        bCheck = true;
            //    }
            //    catch (Exception ex)
            //    {
            //        workBook.Close(true);
            //        excelApp.Quit();
            //        m_MainControl.CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            //        MessageBox.Show(ex.ToString());
            //    }
            //    finally
            //    {
            //        ReleaseObject(workSheet);
            //        ReleaseObject(workBook);
            //        ReleaseObject(excelApp);
            //    }

            //    if (bCheck)
            //    {
            //        MessageBox.Show(string.Format("Alarm List Count : {0} 저장 완료 되었습니다.", iCount));
            //        this.Hide();
            //    }
            //}
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnCheckedChanged(object sender, EventArgs e)
        {
            //for (int i = 0; i < m_dgvExportList.Rows.Count; i++)
            //    m_dgvExportList.Rows[i].Cells[0].Value = m_cbSelectAll.Checked;
        }
        #endregion

        #region Class Util
        /// <summary>
        /// 
        /// </summary>
        //private static void ReleaseObject(object obj)
        //{
        //    try
        //    {
        //        if (obj != null)
        //        {
        //            Marshal.ReleaseComObject(obj);
        //            obj = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        obj = null;
        //        throw ex;
        //    }
        //    finally
        //    {
        //        GC.Collect();
        //    }
        //}
        #endregion
    }
}
