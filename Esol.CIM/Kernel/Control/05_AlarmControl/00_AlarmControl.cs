﻿#region Class Using
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
#endregion

namespace Esol.CIM
{
    public partial class AlarmControl : FormControl
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ALARM_ITEM_NOT_EXIST_ERROR = "Alarm Item Not Exist!";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, bool> m_hashAlarmListChange;
        /// <summary>
        /// 
        /// </summary>
        private AlarmExportPopup m_AlarmExportPopup;
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataDeleteGridViewValue(DataGridView dgv, AlarmData DeleteAlarmData);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataGridViewValue(DataGridView dgv, Dictionary<UInt64, AlarmData> hashAlarmDatas);
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public AlarmControl()
        {
            InitializeComponent();
            this.TopLevel = false;
            m_hashAlarmListChange = new Dictionary<string, bool>();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {            
            InitializeDataGridView();
            InitialzeAlarmList();
            m_AlarmExportPopup = new AlarmExportPopup(MainControl)
            { TopMost = true };
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            for (int i = 0; i < m_dgvAlarmList.Columns.Count; i++)
                m_dgvAlarmList.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvAlarmList.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;


            for (int i = 0; i < m_dgvEditAlarm.Columns.Count; i++)
                m_dgvEditAlarm.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvEditAlarm.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;


            for (int i = 0; i < m_dgvDeleteAlarm.Columns.Count; i++)
                m_dgvDeleteAlarm.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            m_dgvDeleteAlarm.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitialzeAlarmList()
        {
            DataGridAlarmListCrossThread(m_dgvAlarmList, CimConfigurator.AlarmManager.HashAlarmIdToData);
        }
        #endregion

        #region Class CrossThread Methods
        /// <summary>
        /// 
        /// </summary>
        private void DataGridAlarmListCrossThread(DataGridView dgv, Dictionary<UInt64, AlarmData> hashAlarmDatas)
        {
            if (dgv.InvokeRequired)
            {
                ReloadDataGridViewValue reload = new ReloadDataGridViewValue(DataGridAlarmListCrossThread);
                this.Invoke(reload, new object[] { dgv, hashAlarmDatas });
            }
            else
            {
                dgv.Rows.Clear();

                List<UInt64> listKeys = hashAlarmDatas.Keys.ToList();
                listKeys.Sort();

                foreach (UInt64 sAlarmKey in listKeys)
                {
                    AlarmData data = hashAlarmDatas[sAlarmKey];
                    dgv.Rows.Add(data.ALID, data.ALCD, data.ALTX, data.USE);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void DataGridDeleteAlarmListCrossThread(DataGridView dgv, AlarmData DeleteAlarmData)
        {
            if (dgv.InvokeRequired)
            {
                ReloadDataDeleteGridViewValue reload = new ReloadDataDeleteGridViewValue(DataGridDeleteAlarmListCrossThread);
                this.Invoke(reload, new object[] { dgv, DeleteAlarmData });
            }
            else
            {
                if (dgv.Name.ToUpper().Contains("EDIT"))
                {
                    DataGridViewComboBoxCell cCell = new DataGridViewComboBoxCell();
                    cCell.Items.Add("1");
                    cCell.Items.Add("2");
                    cCell.Value = DeleteAlarmData.ALCD;

                    dgv.Rows.Add(DeleteAlarmData.ALID, string.Empty, DeleteAlarmData.ALTX, DeleteAlarmData.USE);
                    cCell.Value = DeleteAlarmData.ALCD.ToString();
                    dgv.Rows[dgv.Rows.Count - 1].Cells[1] = cCell;
                }
                else
                    dgv.Rows.Add(DeleteAlarmData.ALID, DeleteAlarmData.ALCD.ToString(), DeleteAlarmData.ALTX, DeleteAlarmData.USE);
            }
        }
        #endregion

        #region Class Evenet Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnEditSave(object sender, EventArgs e)
        {
            OfflineForm form = new OfflineForm("Alarm을 추가 or 수정 하시겠습니까?", CimConfigurator.Password);

            DialogResult res = form.ShowDialog();

            if (res != DialogResult.OK)
                return;

            Dictionary<UInt64, AlarmData> hashEditAlarmdata = new Dictionary<UInt64, AlarmData>();

            for (int i = 0; i < m_dgvEditAlarm.Rows.Count; i++)
            {
                UInt64 iAlarmId = 0;

                try
                {
                    UInt64 iTempAlarmId = Convert.ToUInt64(m_dgvEditAlarm.Rows[i].Cells[0].Value);

                    if (iTempAlarmId == 0)
                    {
                        MessageBox.Show("Alarm ID가 0이거나 비어 있습니다.");
                        return;
                    }

                    iAlarmId = iTempAlarmId;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("AlarmID가 잘못 되었습니다.");
                    LogManager.ErrorWriteLog(ex.ToString());
                    return;
                }

                string sAlarmText = m_dgvEditAlarm.Rows[i].Cells[2].Value.ToString();
                if (string.IsNullOrEmpty(sAlarmText))
                {
                    MessageBox.Show("Alarm Text가 비어 있습니다.");
                    return;
                }

                if (CimConfigurator.AlarmManager.HashAlarmIdToData.ContainsKey(iAlarmId))
                    if (MessageBox.Show(string.Format("Alarm ID : {0}이(가) 존재 합니다. 덮어쓰시겠습니까?", iAlarmId), "", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                        continue;

                AlarmData data = new AlarmData();

                if (string.IsNullOrEmpty(m_dgvEditAlarm.Rows[i].Cells[0].Value.ToString()))
                    continue;

                data.ALID = Convert.ToUInt32(m_dgvEditAlarm.Rows[i].Cells[0].Value);
                data.ALCD = Convert.ToInt32(m_dgvEditAlarm.Rows[i].Cells[1].Value);
                data.ALTX = m_dgvEditAlarm.Rows[i].Cells[2].Value.ToString();
                data.USE = m_dgvEditAlarm.Rows[i].Cells[3].Value.ToString().ToUpper() == "TRUE" ? true : false;

                if (!hashEditAlarmdata.ContainsKey(data.ALID))
                    hashEditAlarmdata.Add(data.ALID, data);
                else
                    if (MessageBox.Show(string.Format("EditAlarmList에 ID : {0}이(가) 존재 합니다. 덮어쓰시겠습니까?", iAlarmId), "", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                        continue;
                    else
                        hashEditAlarmdata[data.ALID] = data;
            }

            if (hashEditAlarmdata.Count == 0)
                return;

            CimConfigurator.AlarmManager.AlarmListSave(hashEditAlarmdata);

            CimConfigurator.AlarmManager.AlarmListRefresh();

            DataGridAlarmListCrossThread(m_dgvAlarmList, CimConfigurator.AlarmManager.HashAlarmIdToData);

            m_dgvEditAlarm.Rows.Clear();

            m_dgvDeleteAlarm.Rows.Clear();

            string sMessage = "Alarm ID : ";

            foreach (UInt32 uKey in hashEditAlarmdata.Keys)
                sMessage += uKey.ToString() + " ";

            MessageBox.Show(string.Format("{0} 파일 저장이 완료 되었습니다.", sMessage));
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnDeleteSave(object sender, EventArgs e)
        {
            
            if (m_dgvDeleteAlarm.Rows.Count == 0)
            {
                MessageBox.Show(DEF_ALARM_ITEM_NOT_EXIST_ERROR);
                CimConfigurator.LogManager.ButtonWriteLog(DEF_ALARM_ITEM_NOT_EXIST_ERROR);
                return;
            }

            OfflineForm form = new OfflineForm("Alarm을 삭제 하시겠습니까?", CimConfigurator.Password);

            DialogResult res = form.ShowDialog();

            if (res != DialogResult.OK)
                return;


            List<UInt32> listDeleteAlarm = new List<uint>();

            for (int i = 0; i < m_dgvDeleteAlarm.Rows.Count; i++)
                listDeleteAlarm.Add(Convert.ToUInt32(m_dgvDeleteAlarm.Rows[i].Cells[0].Value));

            CimConfigurator.AlarmManager.DeleteAlarmlist(listDeleteAlarm);

            CimConfigurator.AlarmManager.AlarmListRefresh();

            DataGridAlarmListCrossThread(m_dgvAlarmList, CimConfigurator.AlarmManager.HashAlarmIdToData);

            m_dgvDeleteAlarm.Rows.Clear();
            m_dgvEditAlarm.Rows.Clear();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnEditCancel(object sender, EventArgs e)
        {
            m_dgvEditAlarm.Rows.Clear();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnDeleteCancel(object sender, EventArgs e)
        {
            m_dgvDeleteAlarm.Rows.Clear();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnSaveAlarmEnable(object sender, EventArgs e)
        {
            Dictionary<UInt64, AlarmData> hashAlarmData = CimConfigurator.AlarmManager.HashAlarmIdToData;

            OfflineForm form = new OfflineForm("Alarm을 저장 하시겠습니까?", CimConfigurator.Password);

            DialogResult res = form.ShowDialog();

            if (res != DialogResult.OK)
            {
                CimConfigurator.AlarmManager.AlarmListRefresh();

                DataGridAlarmListCrossThread(m_dgvAlarmList, CimConfigurator.AlarmManager.HashAlarmIdToData);
                return;
            }
                

            Dictionary<UInt64, AlarmData> hashTempAlarmData = new Dictionary<UInt64, AlarmData>();
            for (int i = 0; i < m_dgvAlarmList.Rows.Count - 1; i++)
            {
                UInt64 uAlarmId = Convert.ToUInt32(m_dgvAlarmList.Rows[i].Cells[0].Value);
                bool bUse = m_dgvAlarmList.Rows[i].Cells[3].Value.ToString().ToUpper() == "TRUE" ? true : false;

                if (hashAlarmData[uAlarmId].USE != bUse)
                {
                    hashAlarmData[uAlarmId].USE = bUse;
                    hashTempAlarmData.Add(uAlarmId, hashAlarmData[uAlarmId]);
                }
            }

            if (hashTempAlarmData.Count == 0)
                return;

            CimConfigurator.AlarmManager.AlarmListSave(hashTempAlarmData);

            CimConfigurator.AlarmManager.AlarmListRefresh();

            DataGridAlarmListCrossThread(m_dgvAlarmList, CimConfigurator.AlarmManager.HashAlarmIdToData);

            m_dgvEditAlarm.Rows.Clear();

            m_dgvDeleteAlarm.Rows.Clear();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnExportExcel(object sender, EventArgs e)
        {
            m_AlarmExportPopup.Initialize();

            if (m_AlarmExportPopup.Visible)
                m_AlarmExportPopup.Focus();
            else
                m_AlarmExportPopup.Show();
        }
        #endregion

        #region Class Private Mehtods
        /// <summary>
        /// 
        /// </summary>
        private void OnEditAdd(string sAlarmId)
        {
            UInt32 uAlarmId = Convert.ToUInt32(sAlarmId);

            bool bCheck = false;

            for (int i = 0; i < m_dgvEditAlarm.Rows.Count; i++)
            {
                bCheck = m_dgvEditAlarm.Rows[i].Cells[0].Value.ToString() == sAlarmId ? true : false;

                if(bCheck)
                {
                    MessageBox.Show("중복된 AlarmID가 존재 합니다.");
                    m_tbEditAlarmId.Text = string.Empty;
                    return;
                }
            }

            if (CimConfigurator.AlarmManager.HashAlarmIdToData.ContainsKey(uAlarmId))
                DataGridDeleteAlarmListCrossThread(m_dgvEditAlarm, CimConfigurator.AlarmManager.HashAlarmIdToData[uAlarmId]);
            else
            {
                AlarmData data = new AlarmData()
                {
                    ALID = uAlarmId,
                    ALCD = 1,
                    ALTX = string.Empty,
                    USE = true
                };

                DataGridDeleteAlarmListCrossThread(m_dgvEditAlarm, data);
            }

            m_tbEditAlarmId.Text = string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        private bool OnCheckAlarmId(DataGridView dgv, string sAlarmID)
        {

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnDeleteAdd(string sAlarmId)
        {
            UInt32 uAlarmId = Convert.ToUInt32(sAlarmId);
            bool bCheck = false;

            for (int i = 0; i < m_dgvDeleteAlarm.Rows.Count; i++)
            {
                bCheck = m_dgvDeleteAlarm.Rows[i].Cells[0].Value.ToString() == sAlarmId ? true : false;

                if (bCheck)
                {
                    MessageBox.Show("중복된 AlarmID가 존재 합니다.");
                    m_tbDeleteAlarmId.Text = string.Empty;
                    return;
                }
            }

            if (CimConfigurator.AlarmManager.HashAlarmIdToData.ContainsKey(uAlarmId))
                DataGridDeleteAlarmListCrossThread(m_dgvDeleteAlarm, CimConfigurator.AlarmManager.HashAlarmIdToData[uAlarmId]);
            else
                MessageBox.Show("존재 하지 않은 Alarm ID 입니다.");

            m_tbDeleteAlarmId.Text = string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFindAlarmd(string sFindAlarmId)
        {
            UInt64 uAlarmId = Convert.ToUInt64(sFindAlarmId);

            if(!CimConfigurator.AlarmManager.HashAlarmIdToData.ContainsKey(uAlarmId))
            {
                MessageBox.Show("존재하지 않은 Alarm ID 입니다.");
                return;
            }

            List<UInt64> listAlarmId = CimConfigurator.AlarmManager.HashAlarmIdToData.Keys.ToList();

            listAlarmId.Sort();

            int iRow = Array.IndexOf(listAlarmId.ToArray(), uAlarmId);

            DataGridViewCell _dgvCell = m_dgvAlarmList.Rows[iRow].Cells[0];
            m_dgvAlarmList.FirstDisplayedCell = _dgvCell;
            m_dgvAlarmList.CurrentCell = _dgvCell;
        }
        #endregion

        #region Class Util Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) || e.KeyChar == Convert.ToChar(Keys.Enter)))
            {
                e.Handled = true;
                MessageBox.Show("Only Input Integer");
            }

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                TextBox tb = (TextBox)sender;

                switch (tb.Name)
                {
                    case "m_tbEditAlarmId":
                        OnEditAdd(m_tbEditAlarmId.Text);
                        break;
                    case "m_tbDeleteAlarmId":
                        OnDeleteAdd(m_tbDeleteAlarmId.Text);
                        break;
                    case "m_tbFineAlarmId":
                        OnFindAlarmd(m_tbFineAlarmId.Text);
                        m_tbFineAlarmId.Text = string.Empty;
                        break;
                }
            }
        }
        #endregion
    }
}
