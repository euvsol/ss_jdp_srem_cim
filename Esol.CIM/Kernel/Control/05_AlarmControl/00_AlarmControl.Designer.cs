﻿namespace Esol.CIM
{
    partial class AlarmControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.m_btnAlarmExport = new System.Windows.Forms.Button();
            this.m_dgvAlarmList = new System.Windows.Forms.DataGridView();
            this.AlarmID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmUse = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.roundPanel2 = new Esol.Components.RoundPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.m_tbEditAlarmId = new System.Windows.Forms.TextBox();
            this.m_btnEditCancel = new System.Windows.Forms.Button();
            this.m_btnEditSave = new System.Windows.Forms.Button();
            this.m_dgvEditAlarm = new System.Windows.Forms.DataGridView();
            this.EditAlarmID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditAlarmCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditAlarmText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditUse = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.roundPanel3 = new Esol.Components.RoundPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.m_tbDeleteAlarmId = new System.Windows.Forms.TextBox();
            this.m_btnDeleteCancel = new System.Windows.Forms.Button();
            this.m_btnDeleteSave = new System.Windows.Forms.Button();
            this.m_dgvDeleteAlarm = new System.Windows.Forms.DataGridView();
            this.DeleteAlarmId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteAlarmCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteAlarmText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteUse = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.m_tbFineAlarmId = new System.Windows.Forms.TextBox();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvAlarmList)).BeginInit();
            this.roundPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvEditAlarm)).BeginInit();
            this.roundPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvDeleteAlarm)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "ALARM LIST";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvAlarmList);
            this.roundPanel1.Controls.Add(this.m_btnAlarmExport);
            this.roundPanel1.Controls.Add(this.button1);
            this.roundPanel1.Controls.Add(this.m_tbFineAlarmId);
            this.roundPanel1.Controls.Add(this.label3);
            this.roundPanel1.Location = new System.Drawing.Point(10, 10);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(962, 1200);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 30);
            this.button1.TabIndex = 18;
            this.button1.Text = "SAVE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnSaveAlarmEnable);
            // 
            // m_btnAlarmExport
            // 
            this.m_btnAlarmExport.Location = new System.Drawing.Point(856, 1200);
            this.m_btnAlarmExport.Name = "m_btnAlarmExport";
            this.m_btnAlarmExport.Size = new System.Drawing.Size(100, 30);
            this.m_btnAlarmExport.TabIndex = 18;
            this.m_btnAlarmExport.Text = "EXPORT";
            this.m_btnAlarmExport.UseVisualStyleBackColor = true;
            this.m_btnAlarmExport.Click += new System.EventHandler(this.OnExportExcel);
            // 
            // m_dgvAlarmList
            // 
            this.m_dgvAlarmList.AllowUserToAddRows = false;
            this.m_dgvAlarmList.AllowUserToDeleteRows = false;
            this.m_dgvAlarmList.AllowUserToOrderColumns = true;
            this.m_dgvAlarmList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvAlarmList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AlarmID,
            this.AlarmCode,
            this.AlarmText,
            this.AlarmUse});
            this.m_dgvAlarmList.Location = new System.Drawing.Point(10, 40);
            this.m_dgvAlarmList.Name = "m_dgvAlarmList";
            this.m_dgvAlarmList.RowTemplate.Height = 30;
            this.m_dgvAlarmList.Size = new System.Drawing.Size(1050, 1150);
            this.m_dgvAlarmList.TabIndex = 16;
            // 
            // AlarmID
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AlarmID.DefaultCellStyle = dataGridViewCellStyle3;
            this.AlarmID.Frozen = true;
            this.AlarmID.HeaderText = "ALARM ID";
            this.AlarmID.Name = "AlarmID";
            this.AlarmID.ReadOnly = true;
            this.AlarmID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AlarmID.Width = 70;
            // 
            // AlarmCode
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AlarmCode.DefaultCellStyle = dataGridViewCellStyle4;
            this.AlarmCode.Frozen = true;
            this.AlarmCode.HeaderText = "CODE";
            this.AlarmCode.Name = "AlarmCode";
            this.AlarmCode.ReadOnly = true;
            this.AlarmCode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AlarmCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AlarmCode.Width = 70;
            // 
            // AlarmText
            // 
            this.AlarmText.Frozen = true;
            this.AlarmText.HeaderText = "ALARM TEXT";
            this.AlarmText.Name = "AlarmText";
            this.AlarmText.ReadOnly = true;
            this.AlarmText.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AlarmText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AlarmText.Width = 470;
            // 
            // AlarmUse
            // 
            this.AlarmUse.Frozen = true;
            this.AlarmUse.HeaderText = "USE";
            this.AlarmUse.Name = "AlarmUse";
            this.AlarmUse.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AlarmUse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AlarmUse.Width = 70;
            // 
            // roundPanel2
            // 
            this.roundPanel2._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel2._Round_Cutting = true;
            this.roundPanel2._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel2._Round_TitleImage = null;
            this.roundPanel2._Round_TitleImageShow = false;
            this.roundPanel2._Round_TitleLabelText = "EDIT ALARM";
            this.roundPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel2.Controls.Add(this.m_dgvEditAlarm);
            this.roundPanel2.Controls.Add(this.m_btnEditSave);
            this.roundPanel2.Controls.Add(this.m_btnEditCancel);
            this.roundPanel2.Controls.Add(this.m_tbEditAlarmId);
            this.roundPanel2.Controls.Add(this.label1);
            this.roundPanel2.Location = new System.Drawing.Point(1085, 10);
            this.roundPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel2.Name = "roundPanel2";
            this.roundPanel2.Size = new System.Drawing.Size(1070, 645);
            this.roundPanel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(16, 617);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 18);
            this.label1.TabIndex = 20;
            this.label1.Text = "Add / Edit Alarm ID : ";
            // 
            // m_tbEditAlarmId
            // 
            this.m_tbEditAlarmId.ForeColor = System.Drawing.Color.Black;
            this.m_tbEditAlarmId.Location = new System.Drawing.Point(201, 611);
            this.m_tbEditAlarmId.Name = "m_tbEditAlarmId";
            this.m_tbEditAlarmId.Size = new System.Drawing.Size(100, 28);
            this.m_tbEditAlarmId.TabIndex = 19;
            this.m_tbEditAlarmId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // m_btnEditCancel
            // 
            this.m_btnEditCancel.Location = new System.Drawing.Point(965, 611);
            this.m_btnEditCancel.Name = "m_btnEditCancel";
            this.m_btnEditCancel.Size = new System.Drawing.Size(100, 30);
            this.m_btnEditCancel.TabIndex = 17;
            this.m_btnEditCancel.Text = "CANCEL";
            this.m_btnEditCancel.UseVisualStyleBackColor = true;
            this.m_btnEditCancel.Click += new System.EventHandler(this.OnEditCancel);
            // 
            // m_btnEditSave
            // 
            this.m_btnEditSave.Location = new System.Drawing.Point(859, 611);
            this.m_btnEditSave.Name = "m_btnEditSave";
            this.m_btnEditSave.Size = new System.Drawing.Size(100, 30);
            this.m_btnEditSave.TabIndex = 17;
            this.m_btnEditSave.Text = "SAVE";
            this.m_btnEditSave.UseVisualStyleBackColor = true;
            this.m_btnEditSave.Click += new System.EventHandler(this.OnEditSave);
            // 
            // m_dgvEditAlarm
            // 
            this.m_dgvEditAlarm.AllowUserToAddRows = false;
            this.m_dgvEditAlarm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvEditAlarm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditAlarmID,
            this.EditAlarmCode,
            this.EditAlarmText,
            this.EditUse});
            this.m_dgvEditAlarm.Location = new System.Drawing.Point(10, 40);
            this.m_dgvEditAlarm.Name = "m_dgvEditAlarm";
            this.m_dgvEditAlarm.RowTemplate.Height = 30;
            this.m_dgvEditAlarm.Size = new System.Drawing.Size(1058, 567);
            this.m_dgvEditAlarm.TabIndex = 18;
            // 
            // EditAlarmID
            // 
            this.EditAlarmID.Frozen = true;
            this.EditAlarmID.HeaderText = "ALARM ID";
            this.EditAlarmID.Name = "EditAlarmID";
            this.EditAlarmID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EditAlarmID.Width = 70;
            // 
            // EditAlarmCode
            // 
            this.EditAlarmCode.Frozen = true;
            this.EditAlarmCode.HeaderText = "CODE";
            this.EditAlarmCode.Name = "EditAlarmCode";
            this.EditAlarmCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EditAlarmCode.Width = 70;
            // 
            // EditAlarmText
            // 
            this.EditAlarmText.Frozen = true;
            this.EditAlarmText.HeaderText = "ALARM TEXT";
            this.EditAlarmText.Name = "EditAlarmText";
            this.EditAlarmText.Width = 470;
            // 
            // EditUse
            // 
            this.EditUse.Frozen = true;
            this.EditUse.HeaderText = "USE";
            this.EditUse.Name = "EditUse";
            this.EditUse.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EditUse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EditUse.Width = 70;
            // 
            // roundPanel3
            // 
            this.roundPanel3._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel3._Round_Cutting = true;
            this.roundPanel3._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel3._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel3._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel3._Round_TitleImage = null;
            this.roundPanel3._Round_TitleImageShow = false;
            this.roundPanel3._Round_TitleLabelText = "DELETE ALARM";
            this.roundPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel3.Controls.Add(this.m_dgvDeleteAlarm);
            this.roundPanel3.Controls.Add(this.m_btnDeleteSave);
            this.roundPanel3.Controls.Add(this.m_btnDeleteCancel);
            this.roundPanel3.Controls.Add(this.m_tbDeleteAlarmId);
            this.roundPanel3.Controls.Add(this.label2);
            this.roundPanel3.Location = new System.Drawing.Point(1085, 665);
            this.roundPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel3.Name = "roundPanel3";
            this.roundPanel3.Size = new System.Drawing.Size(1070, 625);
            this.roundPanel3.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(7, 615);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 18);
            this.label2.TabIndex = 20;
            this.label2.Text = "Delete Alarm ID : ";
            // 
            // m_tbDeleteAlarmId
            // 
            this.m_tbDeleteAlarmId.ForeColor = System.Drawing.Color.Black;
            this.m_tbDeleteAlarmId.Location = new System.Drawing.Point(158, 579);
            this.m_tbDeleteAlarmId.Name = "m_tbDeleteAlarmId";
            this.m_tbDeleteAlarmId.Size = new System.Drawing.Size(100, 28);
            this.m_tbDeleteAlarmId.TabIndex = 19;
            this.m_tbDeleteAlarmId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // m_btnDeleteCancel
            // 
            this.m_btnDeleteCancel.Location = new System.Drawing.Point(964, 579);
            this.m_btnDeleteCancel.Name = "m_btnDeleteCancel";
            this.m_btnDeleteCancel.Size = new System.Drawing.Size(100, 30);
            this.m_btnDeleteCancel.TabIndex = 19;
            this.m_btnDeleteCancel.Text = "CANCEL";
            this.m_btnDeleteCancel.UseVisualStyleBackColor = true;
            this.m_btnDeleteCancel.Click += new System.EventHandler(this.OnDeleteCancel);
            // 
            // m_btnDeleteSave
            // 
            this.m_btnDeleteSave.Location = new System.Drawing.Point(858, 579);
            this.m_btnDeleteSave.Name = "m_btnDeleteSave";
            this.m_btnDeleteSave.Size = new System.Drawing.Size(100, 30);
            this.m_btnDeleteSave.TabIndex = 18;
            this.m_btnDeleteSave.Text = "DELETE";
            this.m_btnDeleteSave.UseVisualStyleBackColor = true;
            this.m_btnDeleteSave.Click += new System.EventHandler(this.OnDeleteSave);
            // 
            // m_dgvDeleteAlarm
            // 
            this.m_dgvDeleteAlarm.AllowUserToAddRows = false;
            this.m_dgvDeleteAlarm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvDeleteAlarm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeleteAlarmId,
            this.DeleteAlarmCode,
            this.DeleteAlarmText,
            this.DeleteUse});
            this.m_dgvDeleteAlarm.Location = new System.Drawing.Point(10, 40);
            this.m_dgvDeleteAlarm.Name = "m_dgvDeleteAlarm";
            this.m_dgvDeleteAlarm.ReadOnly = true;
            this.m_dgvDeleteAlarm.Size = new System.Drawing.Size(1058, 523);
            this.m_dgvDeleteAlarm.TabIndex = 17;
            // 
            // DeleteAlarmId
            // 
            this.DeleteAlarmId.Frozen = true;
            this.DeleteAlarmId.HeaderText = "ALARM ID";
            this.DeleteAlarmId.Name = "DeleteAlarmId";
            this.DeleteAlarmId.ReadOnly = true;
            this.DeleteAlarmId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DeleteAlarmId.Width = 70;
            // 
            // DeleteAlarmCode
            // 
            this.DeleteAlarmCode.Frozen = true;
            this.DeleteAlarmCode.HeaderText = "CODE";
            this.DeleteAlarmCode.Name = "DeleteAlarmCode";
            this.DeleteAlarmCode.ReadOnly = true;
            this.DeleteAlarmCode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DeleteAlarmCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DeleteAlarmCode.Width = 70;
            // 
            // DeleteAlarmText
            // 
            this.DeleteAlarmText.Frozen = true;
            this.DeleteAlarmText.HeaderText = "ALARM TEXT";
            this.DeleteAlarmText.Name = "DeleteAlarmText";
            this.DeleteAlarmText.ReadOnly = true;
            this.DeleteAlarmText.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DeleteAlarmText.Width = 470;
            // 
            // DeleteUse
            // 
            this.DeleteUse.Frozen = true;
            this.DeleteUse.HeaderText = "USE";
            this.DeleteUse.Name = "DeleteUse";
            this.DeleteUse.ReadOnly = true;
            this.DeleteUse.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DeleteUse.Width = 70;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(7, 1200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Find Alarm ID : ";
            // 
            // m_tbFineAlarmId
            // 
            this.m_tbFineAlarmId.ForeColor = System.Drawing.Color.Black;
            this.m_tbFineAlarmId.Location = new System.Drawing.Point(140, 1200);
            this.m_tbFineAlarmId.Name = "m_tbFineAlarmId";
            this.m_tbFineAlarmId.Size = new System.Drawing.Size(100, 28);
            this.m_tbFineAlarmId.TabIndex = 19;
            this.m_tbFineAlarmId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // AlarmControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(2167, 1321);
            this.Controls.Add(this.roundPanel3);
            this.Controls.Add(this.roundPanel2);
            this.Controls.Add(this.roundPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlarmControl";
            this.Text = "RealLogControl";
            this.roundPanel1.ResumeLayout(false);
            this.roundPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvAlarmList)).EndInit();
            this.roundPanel2.ResumeLayout(false);
            this.roundPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvEditAlarm)).EndInit();
            this.roundPanel3.ResumeLayout(false);
            this.roundPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvDeleteAlarm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvAlarmList;
        private Components.RoundPanel roundPanel2;
        private Components.RoundPanel roundPanel3;
        private System.Windows.Forms.Button m_btnEditSave;
        private System.Windows.Forms.Button m_btnEditCancel;
        private System.Windows.Forms.Button m_btnDeleteCancel;
        private System.Windows.Forms.Button m_btnDeleteSave;
        private System.Windows.Forms.DataGridView m_dgvDeleteAlarm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeleteAlarmId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeleteAlarmCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeleteAlarmText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DeleteUse;
        private System.Windows.Forms.DataGridView m_dgvEditAlarm;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AlarmUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditAlarmID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditAlarmCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditAlarmText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EditUse;
        private System.Windows.Forms.Button m_btnAlarmExport;
        private System.Windows.Forms.TextBox m_tbEditAlarmId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_tbDeleteAlarmId;
        private System.Windows.Forms.TextBox m_tbFineAlarmId;
        private System.Windows.Forms.Label label3;
    }
}