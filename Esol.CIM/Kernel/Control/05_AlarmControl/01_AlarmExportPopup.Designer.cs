﻿namespace Esol.CIM
{
    partial class AlarmExportPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_btnExportSave = new System.Windows.Forms.Button();
            this.m_btnExportCancel = new System.Windows.Forms.Button();
            this.m_dgvExportList = new System.Windows.Forms.DataGridView();
            this.m_cbSelectAll = new System.Windows.Forms.CheckBox();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AlarmID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmUse = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvExportList)).BeginInit();
            this.SuspendLayout();
            // 
            // m_btnExportSave
            // 
            this.m_btnExportSave.Location = new System.Drawing.Point(1036, 1400);
            this.m_btnExportSave.Name = "m_btnExportSave";
            this.m_btnExportSave.Size = new System.Drawing.Size(100, 40);
            this.m_btnExportSave.TabIndex = 0;
            this.m_btnExportSave.Text = "SAVE";
            this.m_btnExportSave.UseVisualStyleBackColor = true;
            this.m_btnExportSave.Click += new System.EventHandler(this.OnExportSave);
            // 
            // m_btnExportCancel
            // 
            this.m_btnExportCancel.Location = new System.Drawing.Point(1142, 1399);
            this.m_btnExportCancel.Name = "m_btnExportCancel";
            this.m_btnExportCancel.Size = new System.Drawing.Size(100, 40);
            this.m_btnExportCancel.TabIndex = 0;
            this.m_btnExportCancel.Text = "CANCEL";
            this.m_btnExportCancel.UseVisualStyleBackColor = true;
            this.m_btnExportCancel.Click += new System.EventHandler(this.OnCancel);
            // 
            // m_dgvExportList
            // 
            this.m_dgvExportList.AllowUserToAddRows = false;
            this.m_dgvExportList.AllowUserToDeleteRows = false;
            this.m_dgvExportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvExportList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Check,
            this.AlarmID,
            this.AlarmCode,
            this.AlarmText,
            this.AlarmUse});
            this.m_dgvExportList.Location = new System.Drawing.Point(12, 12);
            this.m_dgvExportList.Name = "m_dgvExportList";
            this.m_dgvExportList.RowTemplate.Height = 30;
            this.m_dgvExportList.Size = new System.Drawing.Size(1230, 1381);
            this.m_dgvExportList.TabIndex = 1;
            // 
            // m_cbSelectAll
            // 
            this.m_cbSelectAll.AutoSize = true;
            this.m_cbSelectAll.Location = new System.Drawing.Point(12, 1417);
            this.m_cbSelectAll.Name = "m_cbSelectAll";
            this.m_cbSelectAll.Size = new System.Drawing.Size(134, 22);
            this.m_cbSelectAll.TabIndex = 2;
            this.m_cbSelectAll.Text = "SELECT ALL";
            this.m_cbSelectAll.UseVisualStyleBackColor = true;
            this.m_cbSelectAll.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // Check
            // 
            this.Check.Frozen = true;
            this.Check.HeaderText = "CHECK";
            this.Check.Name = "Check";
            this.Check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Check.Width = 70;
            // 
            // AlarmID
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AlarmID.DefaultCellStyle = dataGridViewCellStyle1;
            this.AlarmID.Frozen = true;
            this.AlarmID.HeaderText = "ALARM ID";
            this.AlarmID.Name = "AlarmID";
            this.AlarmID.ReadOnly = true;
            this.AlarmID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AlarmID.Width = 70;
            // 
            // AlarmCode
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AlarmCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.AlarmCode.Frozen = true;
            this.AlarmCode.HeaderText = "ALARM CODE";
            this.AlarmCode.Name = "AlarmCode";
            this.AlarmCode.ReadOnly = true;
            this.AlarmCode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AlarmCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AlarmCode.Width = 70;
            // 
            // AlarmText
            // 
            this.AlarmText.Frozen = true;
            this.AlarmText.HeaderText = "ALARM TEXT";
            this.AlarmText.Name = "AlarmText";
            this.AlarmText.ReadOnly = true;
            this.AlarmText.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AlarmText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AlarmText.Width = 470;
            // 
            // AlarmUse
            // 
            this.AlarmUse.Frozen = true;
            this.AlarmUse.HeaderText = "USE";
            this.AlarmUse.Name = "AlarmUse";
            this.AlarmUse.ReadOnly = true;
            this.AlarmUse.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // AlarmExportPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 1452);
            this.Controls.Add(this.m_cbSelectAll);
            this.Controls.Add(this.m_dgvExportList);
            this.Controls.Add(this.m_btnExportCancel);
            this.Controls.Add(this.m_btnExportSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlarmExportPopup";
            this.Text = "AlarmExportPopup";
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvExportList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnExportSave;
        private System.Windows.Forms.Button m_btnExportCancel;
        private System.Windows.Forms.DataGridView m_dgvExportList;
        private System.Windows.Forms.CheckBox m_cbSelectAll;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AlarmUse;
    }
}