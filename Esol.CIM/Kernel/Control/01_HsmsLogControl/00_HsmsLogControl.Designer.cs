﻿namespace Esol.CIM
{
    partial class HsmsLogControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HsmsLogControl));
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_cbFdcCheck = new System.Windows.Forms.CheckBox();
            this.m_cbXgemFiexd = new System.Windows.Forms.CheckBox();
            this.m_btnListBoxClear = new System.Windows.Forms.Button();
            this.m_listboxXgem = new System.Windows.Forms.ListBox();
            this.roundPanel2 = new Esol.Components.RoundPanel();
            this.m_cbShareLogFiexd = new System.Windows.Forms.CheckBox();
            this.m_btnShareLogClear = new System.Windows.Forms.Button();
            this.m_listboxShareMemory = new System.Windows.Forms.ListBox();
            this.roundPanel1.SuspendLayout();
            this.roundPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundPanel1._Round_TitleImage")));
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "XGEM LOG";
            this.roundPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_listboxXgem);
            this.roundPanel1.Controls.Add(this.m_btnListBoxClear);
            this.roundPanel1.Controls.Add(this.m_cbXgemFiexd);
            this.roundPanel1.Controls.Add(this.m_cbFdcCheck);
            this.roundPanel1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.roundPanel1.Location = new System.Drawing.Point(10, 10);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel1.TabIndex = 6;
            // 
            // m_cbFdcCheck
            // 
            this.m_cbFdcCheck.AutoSize = true;
            this.m_cbFdcCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.m_cbFdcCheck.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_cbFdcCheck.Location = new System.Drawing.Point(688, 1211);
            this.m_cbFdcCheck.Name = "m_cbFdcCheck";
            this.m_cbFdcCheck.Size = new System.Drawing.Size(147, 22);
            this.m_cbFdcCheck.TabIndex = 18;
            this.m_cbFdcCheck.Text = "FDC Log Write";
            this.m_cbFdcCheck.UseVisualStyleBackColor = false;
            // 
            // m_cbXgemFiexd
            // 
            this.m_cbXgemFiexd.AutoSize = true;
            this.m_cbXgemFiexd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.m_cbXgemFiexd.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_cbXgemFiexd.Location = new System.Drawing.Point(836, 1212);
            this.m_cbXgemFiexd.Name = "m_cbXgemFiexd";
            this.m_cbXgemFiexd.Size = new System.Drawing.Size(142, 22);
            this.m_cbXgemFiexd.TabIndex = 18;
            this.m_cbXgemFiexd.Text = "XgmLogFiexd";
            this.m_cbXgemFiexd.UseVisualStyleBackColor = false;
            // 
            // m_btnListBoxClear
            // 
            this.m_btnListBoxClear.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_btnListBoxClear.ForeColor = System.Drawing.Color.Black;
            this.m_btnListBoxClear.Location = new System.Drawing.Point(984, 1203);
            this.m_btnListBoxClear.Name = "m_btnListBoxClear";
            this.m_btnListBoxClear.Size = new System.Drawing.Size(75, 39);
            this.m_btnListBoxClear.TabIndex = 17;
            this.m_btnListBoxClear.Text = "Clear";
            this.m_btnListBoxClear.UseVisualStyleBackColor = true;
            this.m_btnListBoxClear.Click += new System.EventHandler(this.OnListBoxXgmClear);
            // 
            // m_listboxXgem
            // 
            this.m_listboxXgem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top) 
            | System.Windows.Forms.AnchorStyles.Left))));
            this.m_listboxXgem.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_listboxXgem.FormattingEnabled = true;
            this.m_listboxXgem.ItemHeight = 18;
            this.m_listboxXgem.Location = new System.Drawing.Point(10, 40);
            this.m_listboxXgem.Name = "m_listboxXgem";
            this.m_listboxXgem.Size = new System.Drawing.Size(1050, 1160);
            this.m_listboxXgem.TabIndex = 16;
            // 
            // roundPanel2
            // 
            this.roundPanel2._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel2._Round_Cutting = true;
            this.roundPanel2._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel2._Round_TitleImage = null;
            this.roundPanel2._Round_TitleImageShow = false;
            this.roundPanel2._Round_TitleLabelText = "SHARE MEMORY LOG";
            this.roundPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.roundPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel2.Controls.Add(this.m_listboxShareMemory);
            this.roundPanel2.Controls.Add(this.m_btnShareLogClear);
            this.roundPanel2.Controls.Add(this.m_cbShareLogFiexd);
            this.roundPanel2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.roundPanel2.Location = new System.Drawing.Point(1089, 9);
            this.roundPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel2.Name = "roundPanel2";
            this.roundPanel2.Size = new System.Drawing.Size(1070, 1250);
            this.roundPanel2.TabIndex = 7;
            // 
            // m_cbShareLogFiexd
            // 
            this.m_cbShareLogFiexd.AutoSize = true;
            this.m_cbShareLogFiexd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.m_cbShareLogFiexd.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_cbShareLogFiexd.Location = new System.Drawing.Point(817, 1212);
            this.m_cbShareLogFiexd.Name = "m_cbShareLogFiexd";
            this.m_cbShareLogFiexd.Size = new System.Drawing.Size(153, 22);
            this.m_cbShareLogFiexd.TabIndex = 18;
            this.m_cbShareLogFiexd.Text = "ShareLogFiexd";
            this.m_cbShareLogFiexd.UseVisualStyleBackColor = false;
            // 
            // m_btnShareLogClear
            // 
            this.m_btnShareLogClear.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_btnShareLogClear.ForeColor = System.Drawing.Color.Black;
            this.m_btnShareLogClear.Location = new System.Drawing.Point(986, 1203);
            this.m_btnShareLogClear.Name = "m_btnShareLogClear";
            this.m_btnShareLogClear.Size = new System.Drawing.Size(75, 39);
            this.m_btnShareLogClear.TabIndex = 17;
            this.m_btnShareLogClear.Text = "Clear";
            this.m_btnShareLogClear.UseVisualStyleBackColor = true;
            this.m_btnShareLogClear.Click += new System.EventHandler(this.OnShareLogClear);
            // 
            // m_listboxShareMemory
            // 
            this.m_listboxShareMemory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top) 
            | System.Windows.Forms.AnchorStyles.Left))));
            this.m_listboxShareMemory.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_listboxShareMemory.FormattingEnabled = true;
            this.m_listboxShareMemory.ItemHeight = 18;
            this.m_listboxShareMemory.Location = new System.Drawing.Point(10, 40);
            this.m_listboxShareMemory.Name = "m_listboxShareMemory";
            this.m_listboxShareMemory.Size = new System.Drawing.Size(1050, 1160);
            this.m_listboxShareMemory.TabIndex = 16;
            // 
            // HsmsLogControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            //this.ClientSize = new System.Drawing.Size(2167, 1321); 1483, 857
            this.ClientSize = new System.Drawing.Size(1483, 857); 
            this.Controls.Add(this.roundPanel2);
            this.Controls.Add(this.roundPanel1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HsmsLogControl";
            this.Text = "HsmsLogControl";
            this.roundPanel1.ResumeLayout(false);
            this.roundPanel1.PerformLayout();
            this.roundPanel2.ResumeLayout(false);
            this.roundPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.ListBox m_listboxXgem;
        private System.Windows.Forms.Button m_btnListBoxClear;
        private System.Windows.Forms.CheckBox m_cbXgemFiexd;
        private Components.RoundPanel roundPanel2;
        private System.Windows.Forms.ListBox m_listboxShareMemory;
        private System.Windows.Forms.Button m_btnShareLogClear;
        private System.Windows.Forms.CheckBox m_cbShareLogFiexd;
        private System.Windows.Forms.CheckBox m_cbFdcCheck;
    }
}