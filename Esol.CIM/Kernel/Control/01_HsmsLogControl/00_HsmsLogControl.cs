﻿#region Class Using
using System;
using System.Text;
using System.Windows.Forms;
#endregion

namespace Esol.CIM
{
    public partial class HsmsLogControl : FormControl
    {
        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public bool FdcLogWrite
        {
            get
            {
                return m_cbFdcCheck.Checked;
            }
        }
        #endregion

        #region Class Intialize
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl()
        {
            InitializeComponent();
            this.TopLevel = false;
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        public void AddMessage(string format, params object[] args)
        {
            try
            {
                if (format == null)
                    format = "-------------------------------------------";
                else
                    format = string.Format(format, args);


                StringBuilder sb = new StringBuilder();
                sb.Insert(0, Common.GenerateCurrentTime() + format);
                ListBoxCrossThread(m_listboxXgem, sb);

                LogManager.WriteLog(LogType.XGEM.ToString(), format);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnListBoxXgmClear(object sender, EventArgs e)
        {
            m_listboxXgem.Items.Clear();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnShareLogClear(object sender, EventArgs e)
        {
            m_listboxShareMemory.Items.Clear();
        }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void ShareLogWrite(StringBuilder sb)
        {
            ListBoxCrossThread(m_listboxShareMemory, sb);
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadListBox(ListBox listbox, StringBuilder sb);
        /// <summary>
        /// 
        /// </summary>
        private void ListBoxCrossThread(ListBox listbox, StringBuilder sb)
        {
            try
            {
                if (listbox.InvokeRequired)
                {
                    ReloadListBox reload = new ReloadListBox(ListBoxCrossThread);
                    this.Invoke(reload, new object[] { listbox, sb });
                }
                else
                {
                    string[] sValue = sb.ToString().Replace('\r', ' ').Split('\n');

                    foreach (string sData in sValue)
                    {
                        if (string.IsNullOrEmpty(sData))
                            continue;

                        listbox.Items.Add(sData);
                    }

                    if (listbox.Name.ToUpper().Contains(LogType.XGEM.ToString()) && !m_cbXgemFiexd.Checked)
                        m_listboxXgem.SelectedIndex = m_listboxXgem.Items.Count - 1;
                    else if (listbox.Name.Contains("Share") && !m_cbShareLogFiexd.Checked)
                        m_listboxShareMemory.SelectedIndex = m_listboxShareMemory.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
