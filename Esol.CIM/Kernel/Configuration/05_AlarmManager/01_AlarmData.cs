﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    public class AlarmData
    {
        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public int SetCode
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public UInt64 ALID
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public int ALCD
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string ALTX
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public bool USE
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string OccurTime
        {
            get;
            set;
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public AlarmData()
        { }
        #endregion
    }
}
