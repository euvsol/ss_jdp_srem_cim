﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using Esol.XmlControl;
using Esol.LogControl;
using System.Linq;
#endregion

namespace Esol.CIM
{
    /// <summary>
    /// 
    /// </summary>
    public class AlarmManager
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ROOT_ELEMENT = "Alarms";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_PARENT_ELEMENT = "Group";
        /// <summary>
        /// 
        /// </summary>
        private readonly int DEF_MAX_HISTORY_RECORDS_DAY = -1;
        /// <summary>
        /// 
        /// </summary>
        public const int DEF_MAX_HISTORY_RECORD_COUNT = 36;
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private CimConfigurator m_cimConfiguator;
        /// <summary>
        /// 
        /// </summary>
        private string m_sHistoryFile;
        /// <summary>
        /// 
        /// </summary>
        private XmlDataWriter m_xmlWriter;
        /// <summary>
        /// 
        /// </summary>
        private List<string[]> m_listHistoryMessages;
        /// <summary>
        /// 
        /// </summary>
        private delegate void ParameterlessEvendHandler();
        /// <summary>
        /// 
        /// </summary>
        private readonly object m_syncObject;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sHistoryKeys;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sAlarmKeys;
        /// <summary>
        /// 
        /// </summary>
        public delegate void AlarmMessageEventHandler(string[][] list);
        /// <summary>
        /// 
        /// </summary>
        private readonly string m_sAlarmListPath;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<UInt64, AlarmData> m_hashAlarmIdToData;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_cimConfiguator.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<UInt64, AlarmData> HashAlarmIdToData
        {
            get
            {
                return m_hashAlarmIdToData;
            }
        }
        #endregion

        #region Class Event Methodss
        /// <summary>
        /// 
        /// </summary>
        public event AlarmMessageEventHandler AlarmHistoryListChanged;
        /// <summary>
        /// 
        /// </summary>
        private void Fire_AlarmMessageHistoryListChanged()
        {
            if (null == AlarmHistoryListChanged || !m_cimConfiguator.HistoryControl.Visible)
                return;


            string[][] list = m_listHistoryMessages.ToArray();

            AlarmHistoryListChanged(list);
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public AlarmManager(CimConfigurator configurator)
        {
            if (null == configurator)
                return;

            m_cimConfiguator = configurator;
            m_sAlarmListPath = configurator.AlarmListPath;
            m_syncObject = new object();
            m_hashAlarmIdToData = new Dictionary<UInt64, AlarmData>();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            m_sHistoryKeys = Enum.GetNames(typeof(AlarmHistory));

            string sAlarmDirectory = string.Format(@"{0}\{1}\", m_cimConfiguator.LogPath, "ALARM");
            m_sHistoryFile = sAlarmDirectory + "Alarm_History.xml";

            DirectoryInfo di = new DirectoryInfo(sAlarmDirectory);
            if (!di.Exists) di.Create();

            m_xmlWriter = new XmlDataWriter(m_sHistoryFile, DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT)
            { OverWrite = false };

            ReadAlarmListFromFile();
            ReadAlarmHistoryMessagesFromFile();
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void ReadAlarmListFromFile()
        {
            try
            {
                m_hashAlarmIdToData = new Dictionary<UInt64, AlarmData>();
                XmlDataReader xmlDataReader = new XmlDataReader(m_sAlarmListPath, false);

                string sTotalKeys = string.Format("{0}/{1}", DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT);
                m_sAlarmKeys = Enum.GetNames(typeof(Alarm));
                List<string[]> listAlarmDatas = xmlDataReader.XmlReadList(sTotalKeys, m_sAlarmKeys);

                foreach (string[] sAlarmData in listAlarmDatas)
                {
                    try
                    {
                        AlarmData alarmdata = new AlarmData()
                        {
                            ALID = Convert.ToUInt32(sAlarmData[0]),
                            ALCD = Convert.ToInt32(sAlarmData[1]),
                            ALTX = sAlarmData[2],
                            USE = sAlarmData[3] == "1" ? true : false,
                        };

                        m_hashAlarmIdToData.Add(alarmdata.ALID, alarmdata);
                    }
                    catch (Exception ex)
                    {
                        LogManager.ErrorWriteLog(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Event Methods handlers
        /// <summary>
        /// 
        /// </summary>
        public string[][] GetHistoryMessagesList()
        {
            lock (m_syncObject)
            {
                return m_listHistoryMessages.ToArray();
            }
        }
        #endregion

        #region Class 'Read/Write' methods
        /// <summary>
        /// 
        /// </summary>
        private void ReadAlarmHistoryMessagesFromFile()
        {
            m_listHistoryMessages = new List<string[]>();
            ReadAlarmMessagesFromFile(m_sHistoryFile);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sPath"></param>
        private void ReadAlarmMessagesFromFile(string sPath)
        {
            try
            {
                lock (m_syncObject)
                {
                    XmlDataReader reader = new XmlDataReader(sPath, false);

                    m_listHistoryMessages = reader.XmlReadList(string.Format("{0}/{1}", DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT), m_sHistoryKeys);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void AddMessage(bool bSet, AlarmData alarm)
        {
            try
            {
                lock (m_syncObject)
                {
                    string[] Aryalarm = new string[m_sHistoryKeys.Length];
                    Aryalarm[(int)AlarmHistory.id] = alarm.ALID.ToString();
                    Aryalarm[(int)AlarmHistory.text] = alarm.ALTX.Trim();
                    Aryalarm[(int)AlarmHistory.time] = Common.GenerateGUITime(DateTime.Now);
                    Aryalarm[(int)AlarmHistory.code] = bSet.ToString();
                    Aryalarm[(int)AlarmHistory.tick] = DateTime.Now.Ticks.ToString();

                    m_listHistoryMessages.Add(Aryalarm);

                    DeleteAlarmHistory();

                    Fire_AlarmMessageHistoryListChanged();

                    Save(Aryalarm);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void Save(string[] alarm)
        {
            m_xmlWriter.XmlAttributeWrite(DEF_PARENT_ELEMENT, "Message", m_sHistoryKeys, alarm);
        }
        /// <summary>
        /// 
        /// </summary>
        private void DeleteAlarmHistory()
        {
            try
            {
                for (int i = 0; i < m_listHistoryMessages.Count; i++)
                {
                    long lMessageTicks = Convert.ToInt64(m_listHistoryMessages[0][(int)AlarmHistory.tick]);

                    long lDataTicks = DateTime.Now.AddDays(DEF_MAX_HISTORY_RECORDS_DAY).Ticks;

                    if (lDataTicks > lMessageTicks || m_listHistoryMessages.Count > DEF_MAX_HISTORY_RECORD_COUNT)
                    {
                        m_xmlWriter.XmlElementDelete(DEF_PARENT_ELEMENT, "tick", lMessageTicks.ToString(), true);
                        m_listHistoryMessages.RemoveAt(0);
                    }
                    else
                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void AlarmListSave(Dictionary<UInt64, AlarmData> hashEditAlarmdata)
        {
            try
            {
                XmlDataWriter xmlDataWriter = new XmlDataWriter(m_sAlarmListPath) { OverWrite = true };

                string sTotalKeys = string.Format("{0}/{1}", DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT);
                m_sAlarmKeys = Enum.GetNames(typeof(Alarm));

                List<UInt64> listKeys = hashEditAlarmdata.Keys.ToList();
                listKeys.Sort();


                foreach (UInt64 ukey in listKeys)
                {
                    AlarmData data = hashEditAlarmdata[ukey];
                    string[] sAlarmValue = new string[4];
                    sAlarmValue[0] = data.ALID.ToString();
                    sAlarmValue[1] = data.ALCD.ToString();
                    sAlarmValue[2] = data.ALTX;
                    sAlarmValue[3] = data.USE == true ? "1" : "0";

                    bool bCheck = xmlDataWriter.XmlAttributeWrite(DEF_PARENT_ELEMENT, "Alarm", m_sAlarmKeys, sAlarmValue);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void AlarmListRefresh()
        {
            ReadAlarmListFromFile();
        }
        /// <summary>
        /// 
        /// </summary>
        public void DeleteAlarmlist(List<UInt32> list)
        {
            XmlDataWriter xmlDataWriter = new XmlDataWriter(m_sAlarmListPath);

            foreach (UInt32 uKey in list)
            {
                bool bCheck = xmlDataWriter.XmlElementDelete(DEF_PARENT_ELEMENT, "id", uKey.ToString(), true);

                if (bCheck)
                    m_hashAlarmIdToData.Remove(uKey);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void AlarmExistBitOff()
        {
            string sActiveAlarmPath = m_cimConfiguator.ActiveAlarmPath;
            XmlDataReader xmlreader = new XmlDataReader(sActiveAlarmPath, false);
            string[] saReadKeys = new string[2]
            {
                "id",
                "text"
            };
            List<string[]> listActiveAlarm = xmlreader.XmlReadList("Alarms/Group", saReadKeys);

            foreach (string[] saActiveAlarmId in listActiveAlarm)
            {
                long nAlarmId = Convert.ToInt64(saActiveAlarmId[0]);
                long nReturn = m_cimConfiguator.Xgem.GEMSetAlarm(nAlarmId, 0);

                AlarmData data = new AlarmData()
                {
                    ALCD = 0,
                    ALID = Convert.ToUInt32(nAlarmId),
                    ALTX = saReadKeys[1],
                    OccurTime = DateTime.Now.ToString("yyyyMMddHHmmss")
                };

                AddMessage(false, data);
            }

            XmlDataWriter xmlWriter = new XmlDataWriter(sActiveAlarmPath);
            xmlWriter.XmlAllElementDelete(DEF_PARENT_ELEMENT);
        }
        #endregion
    }
}
