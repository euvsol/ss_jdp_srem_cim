﻿#region using
using System;
using System.IO;
using System.Collections.Generic;
using Esol.XmlControl;
using System.Linq;
#endregion

namespace Esol.CIM
{
    public class RecipeManager
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        public readonly string DEF_RECIPE_LIST_PAHT = @"_ConfigFiles\_alterable\HasRecipe.ini";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ELEMENT_KEY = "Recipe";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ATTRIBUTE_KEY = "Parameters";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ATTRIBUTE_INTERLOCK_KEY = "id";
        /// <summary>
        /// 
        /// </summary>
        private const string DEF_PROCESS_RECIPE_TEXT = "ProcessRecipe";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private CimConfigurator m_CimConfigurator;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, BasicRecipeData> m_hashRecipeNameToDatas;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, ProcessRecipeData> m_hashProcessRecipeNameToDatas;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, int> m_hashProcessRecipeCMD;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, int> m_hashBasicRecipeCMD;
        /// <summary>
        /// 
        /// </summary>
        private INI m_HasRecipeIni;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, BasicRecipeData> BasicRecipeNameToData
        {
            get
            {
                return m_hashRecipeNameToDatas;
            }
            set
            {
                m_hashRecipeNameToDatas = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, ProcessRecipeData> ProcessRecipeNameToData
        {
            get
            {
                return m_hashProcessRecipeNameToDatas;
            }
            set
            {
                m_hashProcessRecipeNameToDatas = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, int> HashBasicRecipeCMD
        {
            get
            {
                return m_hashBasicRecipeCMD;
            }
            set
            {
                m_hashBasicRecipeCMD = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, int> HashProcessRecipeCMD
        {
            get
            {
                return m_hashProcessRecipeCMD;
            }
            set
            {
                m_hashProcessRecipeCMD = value;
            }
        }
        #endregion

        #region Class Intialize
        /// <summary>
        /// 
        /// </summary>
        public RecipeManager(CimConfigurator cimConfigurator)
        {
            m_CimConfigurator = cimConfigurator;
            m_hashRecipeNameToDatas = new Dictionary<string, BasicRecipeData>();
            m_hashProcessRecipeNameToDatas = new Dictionary<string, ProcessRecipeData>();
            m_hashBasicRecipeCMD = new Dictionary<string, int>();
            m_hashProcessRecipeCMD = new Dictionary<string, int>();
            m_HasRecipeIni = new INI(DEF_RECIPE_LIST_PAHT);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            InitializeBasicRecipeRead();
            InitializeProcessRecipeRead();
        }
        /// <summary>
        /// 
        /// </summary>
        public void InitializeBasicRecipeRead()
        {
            try
            {
                m_hashRecipeNameToDatas = new Dictionary<string, BasicRecipeData>();

                List<string> listBasicList = new List<string>();

                //string[] sArrayBasicRecipeName = m_HasRecipeIni.GetPairsBySection("Recipe");
                //
                //foreach (string sRecipeName in sArrayBasicRecipeName)
                //{
                //    string[] sRecipeData = sRecipeName.Split('=');
                //    if (!listBasicList.Contains(sRecipeData[0]))
                //        listBasicList.Add(sRecipeData[0]);
                //}

                DirectoryInfo diEq = new DirectoryInfo(m_CimConfigurator.EqRecipePath);
                DirectoryInfo diHost = new DirectoryInfo(m_CimConfigurator.HostBasicRecipePath);
                Dictionary<string, BasicRecipeData> hashEqBasicRecipe = new Dictionary<string, BasicRecipeData>();
                Dictionary<string, BasicRecipeData> hashHostBasicRecipe = new Dictionary<string, BasicRecipeData>();

                if (!diEq.Exists)
                    diEq.Create();

                if (!diHost.Exists)
                    diHost.Create();

                FileInfo[] EqFileInfos = diEq.GetFiles("*.xml");

                foreach (FileInfo fi in EqFileInfos)
                {
                    string sRecipeName = fi.Name.Replace(".xml", string.Empty);

                    BasicRecipeData mrd = ReadRecipeParamName(fi, sRecipeName, true);

                    if (!hashEqBasicRecipe.ContainsKey(sRecipeName))
                        hashEqBasicRecipe.Add(sRecipeName, mrd);
                }

                FileInfo[] HostFileInfos = diHost.GetFiles("*.xml");

                foreach (FileInfo fi in HostFileInfos)
                {
                    string sRecipeName = fi.Name.Replace(".xml", string.Empty);

                    BasicRecipeData mrd = ReadRecipeParamName(fi, sRecipeName, false);

                    if (!hashHostBasicRecipe.ContainsKey(sRecipeName))
                        hashHostBasicRecipe.Add(sRecipeName, mrd);

                }

                //HashRecipeList에는 있으나 EqBasicRecipe에 List가 없으면 삭제 처리
                foreach (string sRecipeName in listBasicList)
                    if (!hashEqBasicRecipe.ContainsKey(sRecipeName))
                        m_hashBasicRecipeCMD.Add(sRecipeName, 3);

                foreach (BasicRecipeData mrd in hashEqBasicRecipe.Values)
                {
                    string sRecipeName = mrd.RecipeName;

                    if (!listBasicList.Contains(sRecipeName))
                    {
                        if (!hashHostBasicRecipe.ContainsKey(sRecipeName))
                            OnFileCopy(new FileInfo(mrd.RecipeFilePath), mrd.RecipeFilePath.Replace("\\Eq\\", "\\Host\\"));
                        else
                            OnFileCopy(new FileInfo(mrd.RecipeFilePath), hashHostBasicRecipe[sRecipeName].RecipeFilePath);
                        m_hashBasicRecipeCMD.Add(mrd.RecipeName, 1);
                    }
                    else if (hashHostBasicRecipe.ContainsKey(sRecipeName) && listBasicList.Contains(sRecipeName))
                    {
                        int.TryParse(mrd.RecipeVersion, out int iEqVersion);
                        int.TryParse(hashHostBasicRecipe[sRecipeName].RecipeVersion, out int iHostVersion);

                        if (iHostVersion != iEqVersion)
                        {
                            OnFileCopy(new FileInfo(mrd.RecipeFilePath), hashHostBasicRecipe[sRecipeName].RecipeFilePath);
                            m_hashBasicRecipeCMD.Add(mrd.RecipeName, 2);
                        }
                    }
                    else if (!hashHostBasicRecipe.ContainsKey(sRecipeName))
                        OnFileCopy(new FileInfo(mrd.RecipeFilePath), mrd.RecipeFilePath.Replace("\\Eq\\", "\\Host\\"));
                }

                foreach (FileInfo fi in HostFileInfos)
                {
                    string sRecipeName = fi.Name.Replace(".xml", string.Empty);

                    BasicRecipeData mrd = ReadRecipeParamName(fi, sRecipeName);
                }
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        private void OnFileCopy(FileInfo fi, string sNewPath)
        {
            fi.CopyTo(sNewPath, true);
        }
        /// <summary>
        /// 
        /// </summary>
        public void InitializeProcessRecipeRead()
        {
            try
            {
                m_hashProcessRecipeNameToDatas = new Dictionary<string, ProcessRecipeData>();

                List<string> listProcessList = new List<string>();

                //string[] sArrayProcessRecipeName = m_HasRecipeIni.GetPairsBySection(DEF_PROCESS_RECIPE_TEXT);
                //
                //foreach (string sProcessRecipeName in sArrayProcessRecipeName)
                //{
                //    string[] sProcessRecipeData = sProcessRecipeName.Split('=');
                //    if (!listProcessList.Contains(sProcessRecipeData[0]))
                //        listProcessList.Add(sProcessRecipeData[0]);
                //}

                Dictionary<string, ProcessRecipeData> hashEqProcessRecipeDatas = new Dictionary<string, ProcessRecipeData>();
                Dictionary<string, ProcessRecipeData> hashHostProcessRecipeDatas = new Dictionary<string, ProcessRecipeData>();

                DirectoryInfo diEq = new DirectoryInfo(m_CimConfigurator.EqProcessRecipePath);
                DirectoryInfo diHost = new DirectoryInfo(m_CimConfigurator.HostProcessRecipePath);

                FileInfo[] EqFileInfos = diEq.GetFiles("*.xml");
                FileInfo[] HostFileInfos = diHost.GetFiles("*.xml");

                foreach (FileInfo fi in EqFileInfos)
                {
                    ProcessRecipeData prd = ReadProcessRecipe(fi, false);

                    if (prd != null)
                    {
                        if (!hashEqProcessRecipeDatas.ContainsKey(prd.RecipeName))
                            hashEqProcessRecipeDatas.Add(prd.RecipeName, prd);
                    }
                }

                foreach (FileInfo fi in HostFileInfos)
                {
                    ProcessRecipeData prd = ReadProcessRecipe(fi, false);

                    if (prd != null)
                    {
                        if (!hashHostProcessRecipeDatas.ContainsKey(prd.RecipeName))
                            hashHostProcessRecipeDatas.Add(prd.RecipeName, prd);
                    }
                }

                //HashRecipeList에는 있으나 EqBasicRecipe에 List가 없으면 삭제 처리
                foreach (string sRecipeName in listProcessList)
                {
                    if (!hashEqProcessRecipeDatas.ContainsKey(sRecipeName))
                        m_hashProcessRecipeCMD.Add(sRecipeName, 3);
                }

                foreach (ProcessRecipeData prd in hashEqProcessRecipeDatas.Values)
                {
                    string sRecipeName = prd.RecipeName;

                    if (!listProcessList.Contains(sRecipeName))
                    {
                        if (!hashHostProcessRecipeDatas.ContainsKey(sRecipeName))
                            OnFileCopy(new FileInfo(prd.RecipeFilePath), prd.RecipeFilePath.Replace("\\Eq\\", "\\Host\\"));
                        else
                            OnFileCopy(new FileInfo(prd.RecipeFilePath), hashHostProcessRecipeDatas[sRecipeName].RecipeFilePath);
                        m_hashProcessRecipeCMD.Add(prd.RecipeName, 1);
                    }
                    else if (hashHostProcessRecipeDatas.ContainsKey(sRecipeName) && listProcessList.Contains(sRecipeName))
                    {
                        int.TryParse(prd.RecipeVersion, out int iEqVersion);
                        int.TryParse(hashHostProcessRecipeDatas[sRecipeName].RecipeVersion, out int iHostVersion);

                        if (iHostVersion != iEqVersion)
                        {
                            OnFileCopy(new FileInfo(prd.RecipeFilePath), hashHostProcessRecipeDatas[sRecipeName].RecipeFilePath);
                            m_hashProcessRecipeCMD.Add(prd.RecipeName, 2);
                        }
                    }
                    else if (!hashHostProcessRecipeDatas.ContainsKey(sRecipeName))
                        OnFileCopy(new FileInfo(prd.RecipeFilePath), prd.RecipeFilePath.Replace("\\Eq\\", "\\Host\\"));
                }

                foreach (FileInfo fi in HostFileInfos)
                {
                    string sRecipeName = fi.Name.Replace(".xml", string.Empty);

                    ProcessRecipeData mrd = ReadProcessRecipe(fi);
                }
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        public BasicRecipeData ReadRecipeParamName(FileInfo fi, string sRecipeName, bool bInit = true)
        {
            try
            {
                string sTotalKeys = string.Format("{0}/{1}", DEF_ELEMENT_KEY, DEF_ATTRIBUTE_KEY);

                BasicRecipeData mrd = new BasicRecipeData()
                {
                    RecipeName = sRecipeName,
                    RecipeFilePath = fi.FullName
                };

                XmlDataReader xmlDataReader = new XmlDataReader(fi.FullName, false);
                List<string[]> listInfo = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Info", m_CimConfigurator.AttriButeId);

                foreach (string[] sArrayReadDatas in listInfo)
                {
                    switch (sArrayReadDatas[0].ToUpper())
                    {
                        case "TIME":
                            mrd.RecipeTime = sArrayReadDatas[1].ToString();
                            break;
                        case "VERSION":
                            mrd.RecipeVersion = sArrayReadDatas[1].ToString();
                            break;
                    }
                }

                //230202 Basic Recipe 변경 사항 적용
                List<string[]> listInspection = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Inspection", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayReadDatas in listInspection)
                {
                    mrd.HashParamNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                    mrd.HashInspectionNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                }
                //230202 Basic Recipe 변경 사항 적용
                List<string[]> listAperture = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Aperture", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayReadDatas in listAperture)
                {
                    mrd.HashParamNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                    mrd.HashApertureNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                }

                List<string[]> listCalibration = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Calibration", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayReadDatas in listCalibration)
                {
                    mrd.HashParamNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                    mrd.HashCalibrationNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                }

                //230202 Basic Recipe 변경 사항 적용
                List<string[]> listMonitor = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Monitoring", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayReadDatas in listMonitor)
                {
                    mrd.HashParamNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                    mrd.HashMonitoringNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                }

                List<string[]> listContamination = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Contamination", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayReadDatas in listContamination)
                {
                    mrd.HashParamNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                    mrd.HahsContaminationNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                }

                List<string[]> listMeasurement = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Measurement", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayReadDatas in listMeasurement)
                {
                    mrd.HashParamNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                    mrd.HashMeasurementNameToValue.Add(sArrayReadDatas[0], sArrayReadDatas[1]);
                }

                if (bInit)
                {
                    if (!m_hashRecipeNameToDatas.ContainsKey(sRecipeName))
                    {
                        m_hashRecipeNameToDatas.Add(sRecipeName, mrd);
                        m_HasRecipeIni.SetPairBySection("Recipe", sRecipeName, Common.GenerateFileTime(DateTime.Now));
                    }
                    else
                        m_hashRecipeNameToDatas[sRecipeName] = mrd;
                }

                return mrd;
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ProcessRecipeData ReadProcessRecipe(FileInfo fi, bool bInit = true)
        {
            try
            {
                string sTotalKeys = string.Format("{0}/{1}", DEF_ELEMENT_KEY, DEF_ATTRIBUTE_KEY);

                XmlDataReader xmlDataReader = new XmlDataReader(fi.FullName, false);
                List<string[]> listInfo = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Info", m_CimConfigurator.AttriButeId);

                if (listInfo.Count != 3)
                    return null;

                ProcessRecipeData PRD = new ProcessRecipeData()
                {
                    RecipeName = fi.Name.Replace(".xml", string.Empty),
                    RecipeFilePath = fi.FullName,
                    RecipeTime = listInfo[0][1],
                    RecipeVersion = listInfo[1][1]
                };

                List<string[]> listSubstrate = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Substrate", m_CimConfigurator.AttriButeId);
                foreach (string[] sArraySubstrate in listSubstrate)
                    PRD.ParameterNameToValue.Add(sArraySubstrate[0], sArraySubstrate[1]);

                List<string[]> listOrigin = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Origin", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayOrigin in listOrigin)
                    PRD.ParameterNameToValue.Add(sArrayOrigin[0], sArrayOrigin[1]);

                //230202 Process Recipe 변경 사항 적용
                List<string[]> listInspection = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Inspection", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayInspection in listInspection)
                    PRD.ParameterNameToValue.Add(sArrayInspection[0], sArrayInspection[1]);

                //230202 Process Recipe 변경 사항 적용
                List<string[]> listAperture = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Aperture", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayAperture in listAperture)
                    PRD.ParameterNameToValue.Add(sArrayAperture[0], sArrayAperture[1]);

                List<string[]> listAlignment = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Alignment", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayAlignment in listAlignment)
                    PRD.ParameterNameToValue.Add(sArrayAlignment[0], sArrayAlignment[1]);

                List<string[]> listCalibration = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Calibration", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayCalibration in listCalibration)
                    PRD.ParameterNameToValue.Add(sArrayCalibration[0], sArrayCalibration[1]);

                //230202 Process Recipe 변경 사항 적용
                List<string[]> listMonitoring = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Monitoring", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayMonitoring in listMonitoring)
                    PRD.ParameterNameToValue.Add(sArrayMonitoring[0], sArrayMonitoring[1]);

                List<string[]> listContamination = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Contamination", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayContamination in listContamination)
                    PRD.ParameterNameToValue.Add(sArrayContamination[0], sArrayContamination[1]);

                List<string[]> listMeasurement = xmlDataReader.XmlReadList(sTotalKeys, DEF_ATTRIBUTE_INTERLOCK_KEY, "Measurement", m_CimConfigurator.AttriButeId);
                foreach (string[] sArrayMeasurement in listMeasurement)
                    PRD.ParameterNameToValue.Add(sArrayMeasurement[0], sArrayMeasurement[1]);

                if (!m_hashProcessRecipeNameToDatas.ContainsKey(PRD.RecipeName))
                    m_hashProcessRecipeNameToDatas.Add(PRD.RecipeName, PRD);
                else
                    m_hashProcessRecipeNameToDatas[PRD.RecipeName] = PRD;

                m_HasRecipeIni.SetPairBySection(DEF_PROCESS_RECIPE_TEXT, PRD.RecipeName, Common.GenerateFileTime(DateTime.Now));


                return PRD;
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());

                return null;
            }
        }
        #endregion

        #region Class Public Mehtods
        /// <summary>
        /// 
        /// </summary>
        public BasicRecipeData RecipeCreate(string sRecipePath, string sRecipeName)
        {
            try
            {
                FileInfo fi = new FileInfo(sRecipePath);

                BasicRecipeData mrd = ReadRecipeParamName(fi, sRecipeName);

                return mrd;
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void RecipeCreate(long uMode, string sRecipeName, BasicRecipeData mrd, ProcessRecipeData prd)
        {
            try
            {
                long nReturn = 0;
                string sMdln = "";
                string sSoftRev = "";
                long nCount = 0;

                string sRecipePath = string.Empty;

                if (mrd == null)
                {
                    if (!m_hashProcessRecipeNameToDatas.ContainsKey(sRecipeName))
                        m_hashProcessRecipeNameToDatas.Add(prd.RecipeName, prd);
                    else
                        m_hashProcessRecipeNameToDatas[prd.RecipeName] = prd;

                    sRecipePath = string.Format(@"{0}\{1}.xml", m_CimConfigurator.HostProcessRecipePath, sRecipeName);
                    mrd = RecipeCreate(sRecipePath, sRecipeName);
                }
                else
                    sRecipePath = string.Format(@"{0}\{1}.xml", m_CimConfigurator.EqRecipePath, sRecipeName);

                int iParamCount = prd != null ? prd.ParameterNameToValue.Count : mrd.HashParamNameToValue.Count;
                string[] saCCodes = new string[1];
                long[] naPCount = new long[1];
                string[] saPNames = new string[iParamCount];

                sMdln = "SREM";
                sSoftRev = mrd == null ? prd.RecipeVersion : mrd.RecipeVersion;
                nCount = 1;
                saCCodes[0] = "1";
                naPCount[0] = iParamCount;

                int iCount = 0;

                if (prd != null)
                    foreach (string sParamValues in prd.ParameterNameToValue.Values)
                        saPNames[iCount++] = sParamValues;
                else
                    foreach (string sParamValues in mrd.HashParamNameToValue.Values)
                        saPNames[iCount++] = sParamValues;

                if (prd != null)
                    sRecipeName = prd.RecipeName;

                long[] sVids = new long[2];
                string[] sVidValues = new string[2];
                sVids[0] = 4014;
                sVids[1] = 30;
                sVidValues[0] = sRecipeName;
                sVidValues[1] = "1";
                m_CimConfigurator.SetVids(sVids, sVidValues);

                //nMode : 1(Created), 2(Edited), 3(Deleted)
                nReturn = m_CimConfigurator.Xgem.GEMSetPPFmtChanged(uMode, sRecipeName, sMdln, sSoftRev, nCount, saCCodes, naPCount, saPNames);

                if (nReturn == 0)
                {
                    m_CimConfigurator.HsmsLogControl.AddMessage("[EQ ==> XGEM] GEMSetPPFmtChanged successfully");

                    if (prd != null)
                        m_CimConfigurator.RecipeControl.ProcessRecipeListChange();
                    else
                    {
                        m_CimConfigurator.RecipeManager.RecipeCreate(sRecipePath, sRecipeName);
                        m_CimConfigurator.RecipeControl.MainRecipeListChenage();
                    }
                }
                else
                    m_CimConfigurator.HsmsLogControl.AddMessage("[EQ ==> XGEM] Fail to GEMSetPPFmtChanged ({0})", nReturn);
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
