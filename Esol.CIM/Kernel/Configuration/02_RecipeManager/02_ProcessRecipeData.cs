﻿#region using
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class ProcessRecipeData
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private string m_sRecipeName;
        /// <summary>
        /// 
        /// </summary>
        private string m_sRecipeVersion;
        /// <summary>
        /// 
        /// </summary>
        private string m_sRecipeTime;
        /// <summary>
        /// 
        /// </summary>
        private string m_sRecipeFilePath;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> m_hashParameterNameToValue;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public string RecipeName
        {
            get
            {
                return m_sRecipeName;
            }
            set
            {
                m_sRecipeName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeVersion
        {
            get
            {
                return m_sRecipeVersion;
            }
            set
            {
                m_sRecipeVersion = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeTime
        {
            get
            {
                return m_sRecipeTime;
            }
            set
            {
                m_sRecipeTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeFilePath
        {
            get
            {
                return m_sRecipeFilePath;
            }
            set
            {
                m_sRecipeFilePath = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> ParameterNameToValue
        {
            get
            {
                return m_hashParameterNameToValue;
            }
            set
            {
                m_hashParameterNameToValue = value;
            }
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public ProcessRecipeData()
        {
            m_sRecipeName = string.Empty;

            m_sRecipeVersion = string.Empty;

            m_sRecipeTime = string.Empty;

            m_sRecipeFilePath = string.Empty;
            m_hashParameterNameToValue = new Dictionary<string, string>();
    }
        #endregion
    }
}
