﻿#region using
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public class BasicRecipeData
    {
        #region Members

        private string m_sRecipeName;

        private string m_sRecipeVersion;

        private string m_sRecipeTime;

        private string m_sRecipeFilePath;

        private Dictionary<string, string> m_hashMonitoringNameToValue;

        private Dictionary<string, string> m_hashInspectionNameToValue;

        private Dictionary<string, string> m_hashApertureNameToValue;

        private Dictionary<string, string> m_hashParamNameToValue;

        private Dictionary<string, string> m_hashCalibrationNameToValue;

        private Dictionary<string, string> m_hashContaminationNameToValue;

        private Dictionary<string, string> m_hashMeasurementNameToValue;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public string RecipeName
        {
            get
            {
                return m_sRecipeName;
            }
            set
            {
                m_sRecipeName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeVersion
        {
            get
            {
                return m_sRecipeVersion;
            }
            set
            {
                m_sRecipeVersion = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeTime
        {
            get
            {
                return m_sRecipeTime;
            }
            set
            {
                m_sRecipeTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RecipeFilePath
        {
            get
            {
                return m_sRecipeFilePath;
            }
            set
            {
                m_sRecipeFilePath = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashMonitoringNameToValue
        {
            get
            {
                return m_hashMonitoringNameToValue;
            }
            set
            {
                m_hashMonitoringNameToValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashInspectionNameToValue
        {
            get
            {
                return m_hashInspectionNameToValue;
            }
            set
            {
                m_hashInspectionNameToValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashApertureNameToValue
        {
            get
            {
                return m_hashApertureNameToValue;
            }
            set
            {
                m_hashApertureNameToValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashParamNameToValue
        {
            get
            {
                return m_hashParamNameToValue;
            }
            set
            {
                m_hashParamNameToValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashCalibrationNameToValue
        {
            get
            {
                return m_hashCalibrationNameToValue;
            }
            set
            {
                m_hashCalibrationNameToValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HahsContaminationNameToValue
        {
            get
            {
                return m_hashContaminationNameToValue;
            }
            set
            {
                m_hashContaminationNameToValue = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashMeasurementNameToValue
        {
            get
            {
                return m_hashMeasurementNameToValue;
            }
            set
            {
                m_hashMeasurementNameToValue = value;
            }
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public BasicRecipeData()
        {
            m_sRecipeName = string.Empty;

            m_sRecipeVersion = string.Empty;

            m_sRecipeTime = string.Empty;

            m_sRecipeFilePath = string.Empty;

            m_hashApertureNameToValue = new Dictionary<string, string>();
            m_hashCalibrationNameToValue = new Dictionary<string, string>();
            m_hashContaminationNameToValue = new Dictionary<string, string>();
            m_hashInspectionNameToValue = new Dictionary<string, string>();
            m_hashMeasurementNameToValue = new Dictionary<string, string>();
            m_hashMonitoringNameToValue = new Dictionary<string, string>();
            m_hashParamNameToValue = new Dictionary<string, string>();
        }
        #endregion
    }
}
