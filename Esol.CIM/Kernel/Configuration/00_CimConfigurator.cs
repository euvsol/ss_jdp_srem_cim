﻿#region Class Using
using System;
using System.Xml;
using GEM_XGem300Pro;
using System.Collections.Generic;
using Esol.ShareMemory;
using Esol.LogControl;
using System.IO;
using System.Linq;
#endregion

namespace Esol.CIM
{
    public class CimConfigurator
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_FILE_LOG = "log";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CIM_STARTUP_PATH = @"_ConfigFiles\";
        /// <summary>
        /// 
        /// </summary>
        public readonly string DEF_RFF_FILE_PATH = @"D:\rff";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ATTR_APPLICATIONNAME = "ApplicationName";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ATTR_SOFT_VER = "ver";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ATTR_LINE = "Line";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ATTR_PASSWORD = "Password";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ATTR_PATH = "path";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_SHAREMEMORY_NAME = "ShareMemoryName";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_SHAREMEMORY_SIZE = "ShareMemorySize";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_PJ_COMMAND_USE = "PjCommandUse";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_LOG_DAY = "LogDay";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_PROCESS_RECIPE_DAY = "PrDay";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private MainControl m_MainControl;
        /// <summary>
        /// 
        /// </summary>
        private SvidManager m_SvidManager;
        /// <summary>
        /// 
        /// </summary>
        private AlarmManager m_AlarmManager;
        /// <summary>
        /// 
        /// </summary>
        private RecipeManager m_RecipeManager;
        /// <summary>
        /// 
        /// </summary>
        private TerminalManager m_TerminalManager;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_LogManager;
        /// <summary>
        /// 
        /// </summary>
        private int m_iLogDays;
        /// <summary>
        /// 
        /// </summary>
        private string m_sConfigFile;
        /// <summary>
        /// 
        /// </summary>
        private string m_sApplicationName;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDataBitPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDataWordPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDataWordStructPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDataTrxPath;
        /// <summary>
        /// 
        /// </summary>
        private byte m_iLineNo;
        /// <summary>
        /// 
        /// </summary>
        private string m_sPassword;
        /// <summary>
        /// 
        /// </summary>
        private string m_sSoftVer;
        /// <summary>
        /// 
        /// </summary>
        private string m_sAlarmListPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sSvidConfigPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sAlarmPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sLogFilePath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sCurrentRecipe;
        /// <summary>
        /// 
        /// </summary>
        private string m_sShareMemoryName;
        /// <summary>
        /// 
        /// </summary>
        private int m_iShareMemorySize;
        /// <summary>
        /// 
        /// </summary>
        private string m_sControlJobID;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessJobID;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bTest;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bPjCommandUse;
        /// <summary>
        /// 
        /// </summary>
        private string m_sHostEcidPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sEqEcidPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sEqRecipePath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sHostRecipePath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sHostProcessRecipePath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sRffFilePath;
        private string scanNo;
        private List<string> scanNoFlag;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDailyMonitoringFlag;
        /// <summary>
        /// 
        /// </summary>
        private string m_sDailyMonitoringRecipePath;
        /// <summary>
        /// 
        /// </summary>
        private int m_iProcessRecipeDay;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessRecipeBackUpPath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sEqProcessReicpePath;
        /// <summary>
        /// 
        /// </summary>
        private string m_sEqProcessReicpeBackUpPath;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_saAttriButeId;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessRecipe;
        /// <summary>
        /// 
        /// </summary>
        private int m_iTerminalCount;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public XGem300ProNet Xgem
        {
            get
            {
                return m_MainControl.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public VirtualMemory ShareMem
        {
            get
            {
                return m_MainControl.ShareMem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, PlcAddr> HashShareKeyToShareData
        {
            get
            {
                return m_MainControl.HashShareKeyToShareData;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<PlcAddr>> HashShareStruct
        {
            get
            {
                return m_MainControl.HashShareStruct;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public SvidManager SvidManager
        {
            get
            {
                return m_SvidManager;
            }
        }
        /// <summary>        
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public AlarmManager AlarmManager
        {
            get
            {
                return m_AlarmManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public RecipeManager RecipeManager
        {
            get
            {
                return m_RecipeManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TerminalManager TerminalManager
        {
            get
            {
                return m_TerminalManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int LogDays
        {
            get
            {
                return m_iLogDays;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Password
        {
            get
            {
                return m_sPassword;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LogPath
        {
            get
            {
                return m_sLogFilePath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DataBitPath
        {
            get
            {
                return m_sDataBitPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DataWordPath
        {
            get
            {
                return m_sDataWordPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DataWrodStructPath
        {
            get
            {
                return m_sDataWordStructPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DataTrxPath
        {
            get
            {
                return m_sDataTrxPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AlarmListPath
        {
            get
            {
                return m_sAlarmListPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SoftVer
        {
            get
            {
                return m_sSoftVer;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MDLN
        {
            get
            {
                return m_sApplicationName;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CurrentRecipe
        {
            get
            {
                return m_sCurrentRecipe;
            }
            set
            {
                m_sCurrentRecipe = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ShareMemoryName
        {
            get
            {
                return m_sShareMemoryName;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ShareMemorySize
        {
            get
            {
                return m_iShareMemorySize;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ControlJobID
        {
            get
            {
                return m_sControlJobID;
            }
            set
            {
                m_sControlJobID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessJobID
        {
            get
            {
                return m_sProcessJobID;
            }
            set
            {
                m_sProcessJobID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HistoryControl HistoryControl
        {
            get
            {
                return m_MainControl.HistoryControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool TestMode
        {
            get
            {
                return m_bTest;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool PjCommandUse
        {
            get
            {
                return m_bPjCommandUse;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HostEcidPath
        {
            get
            {
                return m_sHostEcidPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string EqEcidPath
        {
            get
            {
                return m_sEqEcidPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string EqRecipePath
        {
            get
            {
                return m_sEqRecipePath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string EqProcessRecipePath
        {
            get
            {
                return m_sEqProcessReicpePath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HostProcessRecipePath
        {
            get
            {
                return m_sHostProcessRecipePath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessRecipeBackUpPath
        {
            get
            {
                return m_sProcessRecipeBackUpPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ActiveAlarmPath
        {
            get
            {
                return m_sAlarmPath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RffFilePath
        {
            get
            {
                return m_sRffFilePath;
            }
        }
        public string ScanNo
        {
            get
            {
                return scanNo;
            }
        }
        public List<string> ScanNoFlag
        {
            get
            {
                return scanNoFlag;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DailyMonitoringFlag
        {
            get
            {
                return m_sDailyMonitoringFlag;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DailyMonitoringRecipePath
        {
            get
            {
                return m_sDailyMonitoringRecipePath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ProcessRecipeDay
        {
            get
            {
                return m_iProcessRecipeDay;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HsmsLogControl HsmsLogControl
        {
            get
            {
                return m_MainControl.HsmsLogControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public RecipeControl RecipeControl
        {
            get
            {
                return m_MainControl.RecipeControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] AttriButeId
        {
            get
            {
                return m_saAttriButeId;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessRecipe
        {
            get
            {
                return m_sProcessRecipe;
            }
            set
            {
                m_sProcessRecipe = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HostBasicRecipePath
        {
            get
            {
                return m_sHostRecipePath;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int TerminalCount
        {
            get
            {
                return m_iTerminalCount;
            }
        }

        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public CimConfigurator(MainControl mainControl)
        {
            m_MainControl = mainControl;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize(string sConfigPath)
        {
            m_sCurrentRecipe = string.Empty;
            m_saAttriButeId = new string[2];
            m_saAttriButeId[0] = "name";
            m_saAttriButeId[1] = "value";
            //JHKIM Startup ini -> Xml 변경
            //INI ini = new INI(DEF_STARTUP_FILE_PATH);
            //m_iLogDays = ini.GetValueToInt("INFO", "LogDays", 30);
            InitializeConfig(sConfigPath);

            m_LogManager = new LogManager(LogPath, m_iLogDays);

            InitializeLogManager();

            m_SvidManager = new SvidManager(this);
            m_SvidManager.Initialize(m_sSvidConfigPath);

            m_AlarmManager = new AlarmManager(this);
            m_AlarmManager.Initialize();

            m_RecipeManager = new RecipeManager(this);
            m_RecipeManager.Initialize();

            m_TerminalManager = new TerminalManager(this);


        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeConfig(string sConfigPath)
        {
            try
            {
                m_sConfigFile = sConfigPath;

                XmlDocument doc = new XmlDocument();
                doc.Load(m_sConfigFile);

                ReadConfigLineNumber(sConfigPath);

                //Config Path 변수 저장
                ReadConfiguration(doc);

            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeLogManager()
        {
            m_LogManager.Initialize(LogType.ERROR.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
            m_LogManager.Initialize(LogType.BUTTON.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
            m_LogManager.Initialize(LogType.EVENT.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
            m_LogManager.Initialize(LogType.ALARM.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
            m_LogManager.Initialize(LogType.TERMINAL.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
            m_LogManager.Initialize(LogType.ECID.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
            m_LogManager.Initialize(LogType.XGEM.ToString(), string.Empty, DEF_FILE_LOG, eLogTimeType.Hour);
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        public void ReadConfigLineNumber(string sConfigPath)
        {
            try
            {
                XmlReader reader = new XmlTextReader(m_sConfigFile);
                reader.Read();

                m_sApplicationName = Common.ReadAttribute(reader, DEF_ATTR_APPLICATIONNAME);
                m_iLineNo = Convert.ToByte(Common.ReadAttribute(reader, DEF_ATTR_LINE));
                m_sPassword = Common.ReadAttribute(reader, DEF_ATTR_PASSWORD);
                m_sSoftVer = Common.ReadAttribute(reader, DEF_ATTR_SOFT_VER);
                m_iLogDays = string.IsNullOrEmpty(Common.ReadAttribute(reader, DEF_LOG_DAY)) ? 30 : Convert.ToInt32(Common.ReadAttribute(reader, DEF_LOG_DAY));
                m_iProcessRecipeDay = string.IsNullOrEmpty(Common.ReadAttribute(reader, DEF_PROCESS_RECIPE_DAY)) ? 30 : Convert.ToInt32(Common.ReadAttribute(reader, DEF_PROCESS_RECIPE_DAY));
                string sPjCommand = string.IsNullOrEmpty(Common.ReadAttribute(reader, DEF_PJ_COMMAND_USE))
                    ? "0" : Common.ReadAttribute(reader, DEF_PJ_COMMAND_USE);
                m_bPjCommandUse = sPjCommand == "0" ? false : true;

                m_sShareMemoryName = string.IsNullOrEmpty(Common.ReadAttribute(reader, DEF_SHAREMEMORY_NAME))
                    ? "ESOL_SHARE_MEM" : Common.ReadAttribute(reader, DEF_SHAREMEMORY_NAME);

                m_iShareMemorySize = Convert.ToInt32(string.IsNullOrEmpty(Common.ReadAttribute(reader, DEF_SHAREMEMORY_SIZE))
                    ? "10000" : Common.ReadAttribute(reader, DEF_SHAREMEMORY_SIZE));

                string sMode = string.IsNullOrEmpty(Common.ReadAttribute(reader, "Mode"))
                    ? "0" : Common.ReadAttribute(reader, "Mode");

                m_bTest = Convert.ToBoolean(sMode == "0" ? false : true);

                m_iTerminalCount = string.IsNullOrEmpty(Common.ReadAttribute(reader, "TerminalCount"))
                    ? 100 : Convert.ToInt32(Common.ReadAttribute(reader, "TerminalCount"));

                reader.Close();
            }
            catch (Exception ex)
            {
                string sMessage = string.Format(
                  "Error while reading [{0}]. {1}", sConfigPath, ex.Message);

                throw new Exception(sMessage);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadConfiguration(XmlDocument doc)
        {
            ReadRootElement(doc);

            //Manager Initialize
            string sXpath = "CIMServerSettings/Log";
            ReadManagerElement(doc, sXpath, ref m_sLogFilePath, false);

            sXpath = "CIMServerSettings/EqRecipe";
            ReadManagerElement(doc, sXpath, ref m_sEqRecipePath, false);

            sXpath = "CIMServerSettings/EqProcessRecipe";
            ReadManagerElement(doc, sXpath, ref m_sEqProcessReicpePath, false);

            sXpath = "CIMServerSettings/EqProcessRecipeBackUp";
            ReadManagerElement(doc, sXpath, ref m_sEqProcessReicpeBackUpPath, false);

            sXpath = "CIMServerSettings/HostRecipe";
            ReadManagerElement(doc, sXpath, ref m_sHostRecipePath, false);

            sXpath = "CIMServerSettings/HostProcessRecipe";
            ReadManagerElement(doc, sXpath, ref m_sHostProcessRecipePath, false);

            sXpath = "CIMServerSettings/HostProcessRecipeBackUp";
            ReadManagerElement(doc, sXpath, ref m_sProcessRecipeBackUpPath, false);

            sXpath = "CIMServerSettings/Alarm";
            ReadManagerElement(doc, sXpath, ref m_sAlarmPath, false);

            sXpath = "CIMServerSettings/AlarmList";
            ReadManagerElement(doc, sXpath, ref m_sAlarmListPath, false);

            sXpath = "CIMServerSettings/SVIDMapping";
            ReadManagerElement(doc, sXpath, ref m_sSvidConfigPath, false);

            sXpath = "CIMServerSettings/HostEcid";
            ReadManagerElement(doc, sXpath, ref m_sHostEcidPath, false);

            sXpath = "CIMServerSettings/EqEcid";
            ReadManagerElement(doc, sXpath, ref m_sEqEcidPath, false);

            sXpath = "CIMServerSettings/Rff";
            ReadManagerElement(doc, sXpath, ref m_sRffFilePath, false);
            
            sXpath = "CIMServerSettings/ScanNo";
            ReadManagerElement(doc, sXpath, ref scanNo, false);

            sXpath = "CIMServerSettings/ScanNoFlag";
            string tempString = string.Empty;
            ReadManagerElement(doc, sXpath, ref tempString, false);
            scanNoFlag = tempString.Split(',').ToList();
            
            sXpath = "CIMServerSettings/DailyMonitoringFlag";
            ReadManagerElement(doc, sXpath, ref m_sDailyMonitoringFlag, false);

            sXpath = "CIMServerSettings/DailyMonitoring";
            ReadManagerElement(doc, sXpath, ref m_sDailyMonitoringRecipePath, false);

            sXpath = "CIMServerSettings/DataBit";
            ReadManagerElement(doc, sXpath, ref m_sDataBitPath, false);

            sXpath = "CIMServerSettings/DataWord";
            ReadManagerElement(doc, sXpath, ref m_sDataWordPath, false);

            sXpath = "CIMServerSettings/DataStruct";
            ReadManagerElement(doc, sXpath, ref m_sDataWordStructPath, false);

            sXpath = "CIMServerSettings/DataTrx";
            ReadManagerElement(doc, sXpath, ref m_sDataTrxPath, false);

            CheckDirectory();
        }
        /// <summary>
        /// 
        /// </summary>
        private void CheckDirectory()
        {
            DirectoryInfo di = new DirectoryInfo(m_sRffFilePath);

            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(m_sEqProcessReicpePath);
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(m_sEqProcessReicpeBackUpPath);
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(m_sProcessRecipeBackUpPath);
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(m_sHostProcessRecipePath);
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(m_sEqRecipePath);
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(m_sHostRecipePath);
            if (!di.Exists)
                di.Create();
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadRootElement(XmlDocument doc)
        {
            string sXPath = "//CIMServerSettings";
            XmlNode node = doc.SelectSingleNode(sXPath);

            if (null == node)
            {
                if (null == node)
                {
                    string sMessage = string.Format("Couldn't find element [{0}] within [{1}].",
                      sXPath, m_sConfigFile);

                    throw new Exception(sMessage);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadManagerElement(XmlDocument doc,
            string sXPath, ref string sManagerConfigPath, bool bConfigPath = true)
        {
            XmlNode node = doc.SelectSingleNode(sXPath);

            if (null == node)
            {
                string sMessage = string.Format("Couldn't find element [{0}] within [{1}].",
                  sXPath, m_sConfigFile);

                throw new Exception(sMessage);
            }

            string sConfigPath =
              Common.ReadAttribute(node, DEF_ATTR_PATH);

            sManagerConfigPath = (bConfigPath) ?
              GetConfigFolderRelativePath(sConfigPath) : sConfigPath;

            if (string.IsNullOrEmpty(sManagerConfigPath))
            {
                string sMessage = string.Format(
                  "Couldn't find [{0}] attribute within [{1}] element.",
                  DEF_ATTR_PATH, sXPath);

                throw new Exception(sMessage);
            }
        }
        #endregion

        #region Class Utility Methods
        /// <summary>
        /// 
        /// </summary>
        protected internal string GetConfigFolderRelativePath(string sConfigFile)
        {
            return string.Format(@"{0}{1}", DEF_CIM_STARTUP_PATH, sConfigFile);
        }
        #endregion

        #region Public Mehtods
        /// <summary>
        /// 
        /// </summary>
        public void SetVids(long[] naVidName, string[] saVidValue, bool bSvid = false)
        {
            string sText = string.Empty;
            if (Xgem.GEMSetVariable(naVidName.Length, naVidName, saVidValue) == 0)
                if (!bSvid)
                    for (int i = 0; i < naVidName.Length; i++)
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Xgem Set vid : {0}, Value : {1}", naVidName[i], saVidValue[i]));
            else
                if (!bSvid)
                    for (int i = 0; i < naVidName.Length; i++)
                        HsmsLogControl.AddMessage(string.Format("[EQ ==> XGEM] Xgem Set Fail vid : {0}, Value : {1}", naVidName[i], saVidValue[i]));
        }


        public void SaveScanNoConfig(string scanNo)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(m_sConfigFile);
            ReadRootElement(doc);

            //Manager Initialize

            string sXpath = "CIMServerSettings/ScanNo";
            XmlNode node = doc.SelectSingleNode(sXpath);

            node.Attributes["path"].Value = scanNo;

            doc.Save(m_sConfigFile);

            this.scanNo = scanNo;
        }
        #endregion
    }
}
