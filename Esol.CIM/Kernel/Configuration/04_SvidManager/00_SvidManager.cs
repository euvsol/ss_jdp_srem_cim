﻿#region Class Using
using System;
using System.Collections.Generic;
using Esol.ShareMemory;
using Esol.LogControl;
using System.Threading;
using System.Diagnostics;
#endregion

namespace Esol.CIM
{
    public class SvidManager
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ELEMENT_NAME = "SvidEntries";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ELEMENT_SVID_ENTRY = "Svid";
        #endregion

        #region Class Memebers Methods
        /// <summary>
        /// 
        /// </summary>
        private object m_oSyncObject;
        /// <summary>
        /// 
        /// </summary>
        private string[] sSvidKey;
        /// <summary>
        /// 
        /// </summary>
        private CimConfigurator m_CimConfigurator;
        /// <summary>
        /// 
        /// </summary>
        private string m_sConfigPath;
        /// <summary>
        /// 
        /// </summary>
        private Esol.XmlControl.XmlDataReader m_XmlDataReader;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<int, string> m_hashSvidKeyToPlcChannelName;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, int> m_hashPlcChannelNameToSvidKey;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<int, SvidData> m_hashSvidKeyToData;
        /// <summary>
        /// 
        /// </summary>
        private List<long> m_listTotalKeys;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> m_hashPlcChannelNameToValue;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadFdc;
        #endregion

        #region Class Properties Mehtods
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> HashPlcChannelNameToValue
        {
            get
            {
                return m_hashPlcChannelNameToValue;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<long> ListTotalKeys
        {
            get
            {
                return m_listTotalKeys;
            }
            set
            {
                lock (m_oSyncObject)
                {
                    foreach (long iValue in value)
                    {
                        if (!m_listTotalKeys.Contains(iValue))
                            m_listTotalKeys.Add(iValue);
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, SvidData> SvidKeyToData
        {
            get
            {
                return m_hashSvidKeyToData;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected LogManager LogManager
        {
            get
            {
                return m_CimConfigurator.LogManager;
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public SvidManager(CimConfigurator cimconfigurator)
        {
            m_CimConfigurator = cimconfigurator;

            m_oSyncObject = new object();

            m_listTotalKeys = new List<long>();

            m_hashSvidKeyToData = new Dictionary<int, SvidData>();

            m_hashSvidKeyToPlcChannelName = new Dictionary<int, string>();

            m_hashPlcChannelNameToSvidKey = new Dictionary<string, int>();

            m_hashPlcChannelNameToValue = new Dictionary<string, string>();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize(string sConfigPath)
        {
            try
            {
                m_sConfigPath = sConfigPath;

                sSvidKey = Enum.GetNames(typeof(SVID));

                m_XmlDataReader = new Esol.XmlControl.XmlDataReader(m_sConfigPath, false);

                Dictionary<string, string[]> hashData = m_XmlDataReader.XmlReadDictionary(DEF_ELEMENT_NAME, sSvidKey);

                ReadDataToHash(hashData);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadDataToHash(Dictionary<string, string[]> hashData)
        {
            try
            {
                List<int> listSvidKeys = new List<int>();

                foreach (string sSvidKey in hashData.Keys)
                    listSvidKeys.Add(Convert.ToInt32(sSvidKey));

                listSvidKeys.Sort();

                foreach (int iSvid in listSvidKeys)
                {
                    ushort uKey = Convert.ToUInt16(iSvid);
                    string sPlcChannelName = hashData[iSvid.ToString()][(int)SVID.plcChannelName];
                    string sType = hashData[iSvid.ToString()][(int)SVID.Type];
                    string sUnit = hashData[iSvid.ToString()][(int)SVID.Unit];
                    string sRange = hashData[iSvid.ToString()][(int)SVID.Range]; ;
                    string sModuleID = hashData[iSvid.ToString()][(int)SVID.ModuleID];
                    string sPoint = hashData[iSvid.ToString()][(int)SVID.Point];


                    m_hashSvidKeyToPlcChannelName[uKey] = sPlcChannelName;
                    m_hashPlcChannelNameToSvidKey[sPlcChannelName] = uKey;
                    m_listTotalKeys.Add(Convert.ToInt64(uKey));

                    SvidData svidData = new SvidData()
                    {
                        SvidName = sPlcChannelName,
                        SvidType = sType,
                        SvidUnit = sUnit,
                        SivdRange = sRange,
                        SvidModuleId = sModuleID,
                        SvidPoint = sPoint
                    };

                    m_hashSvidKeyToData[uKey] = svidData;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnFdcThreadStart()
        {
            ThreadStart tsShareDataChange = new ThreadStart(RegisterPlcDataChange);
            m_threadFdc = new Thread(tsShareDataChange) { IsBackground = true };

            m_threadFdc.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        public void RegisterPlcDataChange()
        {
            List<PlcAddr> plcaddr = m_CimConfigurator.HashShareStruct[ShareNames.DEF_SVID_STRUCT];
            try
            {
                while (true)
                {
                    List<long> listVids = new List<long>();
                    List<string> listValues = new List<string>();

                    Stopwatch stopwatch = new Stopwatch(); //객체 선언
                    stopwatch.Start();

                    long[] m_pnVids = new long[] { 1, 4, 5, 14, 15 };
                    string[] m_psValues = new string[m_pnVids.Length];
                    m_CimConfigurator.Xgem.GEMGetVariable(m_pnVids.Length, ref m_pnVids, ref m_psValues);

                    m_hashPlcChannelNameToValue["Clock"] = m_psValues[0];
                    m_hashPlcChannelNameToValue["Control state"] = m_psValues[1];
                    m_hashPlcChannelNameToValue["EventsEnabled"] = m_psValues[2];
                    m_hashPlcChannelNameToValue["MDLN"] = m_psValues[3];
                    m_hashPlcChannelNameToValue["SOFTREV"] = m_psValues[4];

                    foreach (PlcAddr plcaddrItem in plcaddr)
                    {
                        if (!m_hashPlcChannelNameToSvidKey.ContainsKey(plcaddrItem.AddrName))
                            continue;

                        string sReadData = string.Empty;
                        long nSvidName = m_hashPlcChannelNameToSvidKey[plcaddrItem.AddrName];

                        switch (plcaddrItem.ValueType)
                        {
                            case PlcValueType.ASCII:
                                sReadData = m_CimConfigurator.ShareMem.GetAscii(plcaddrItem);
                                break;
                            case PlcValueType.INT32:
                                sReadData = m_CimConfigurator.ShareMem.GetInt32(plcaddrItem).ToString();
                                break;
                            case PlcValueType.INT64:
                                sReadData = m_CimConfigurator.ShareMem.GetInt64(plcaddrItem).ToString();
                                break;
                            case PlcValueType.BYTE:
                                sReadData = m_CimConfigurator.ShareMem.GetByte(plcaddrItem).ToString();
                                break;
                            case PlcValueType.SHORT:
                                sReadData = m_CimConfigurator.ShareMem.GetShort(plcaddrItem).ToString();
                                break;
                            case PlcValueType.UINT32:
                                sReadData = m_CimConfigurator.ShareMem.GetUInt32(plcaddrItem).ToString();
                                break;
                            case PlcValueType.UINT64:
                                sReadData = m_CimConfigurator.ShareMem.GetUInt64(plcaddrItem).ToString();
                                break;
                            case PlcValueType.USHORT:
                                sReadData = m_CimConfigurator.ShareMem.GetUShort(plcaddrItem).ToString();
                                break;
                            case PlcValueType.FLOAT:
                                sReadData = m_CimConfigurator.ShareMem.GetFloat(plcaddrItem).ToString();
                                break;
                            case PlcValueType.DOUBLE:
                                sReadData = m_CimConfigurator.ShareMem.GetDouble(plcaddrItem).ToString();
                                break;
                        }

                        listVids.Add(nSvidName);
                        listValues.Add(sReadData);
                        m_hashPlcChannelNameToValue[plcaddrItem.AddrName] = sReadData;
                    }


                    int iSleep = Convert.ToInt32(1000 - stopwatch.ElapsedMilliseconds);

                    if (listVids.Count == 0 || listValues.Count == 0)
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem FDC List Fail");

                        if (iSleep > 0)
                            Thread.Sleep(iSleep);

                        continue;
                    }

                    long[] sVids = listVids.ToArray();
                    string[] sValues = listValues.ToArray();

                    long nReturn = m_CimConfigurator.Xgem.GEMSetVariable(sVids.Length, sVids, sValues);
                    //FDC Log Flag 만들어서 Log 남길지 말지 결정하기
                    if (m_CimConfigurator.HsmsLogControl.FdcLogWrite)
                    {
                        if (nReturn == 0)
                        {
                            //항목저장할려면 추가 필요
                            m_CimConfigurator.HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem FDC successfully ({0})", nReturn);
                        }
                        else
                            m_CimConfigurator.HsmsLogControl.AddMessage("[EQ ==> XGEM] XGem FDC Fail ({0})", nReturn);
                    }
                    stopwatch.Stop();

                    iSleep = Convert.ToInt32(1000 - stopwatch.ElapsedMilliseconds);

                    if (iSleep > 0)
                        Thread.Sleep(iSleep);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());

                ThreadStart tsShareDataChange = new ThreadStart(RegisterPlcDataChange);
                m_threadFdc = new Thread(tsShareDataChange) { IsBackground = true };

                m_threadFdc.Start();
            }
        }
        #endregion
    }
}