﻿namespace Esol.CIM
{
    public class SvidData
    {
        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public string SvidName
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string SvidType
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string SvidUnit
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string SvidModuleId
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string SvidPoint
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string SivdRange
        {
            get;
            set;
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public SvidData() { }
        #endregion
    }
}
