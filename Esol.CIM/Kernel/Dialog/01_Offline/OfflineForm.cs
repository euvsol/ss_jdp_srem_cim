﻿#region Class Using
using System.Drawing;
using System.Windows.Forms;
#endregion

namespace Esol.CIM
{
    public partial class OfflineForm : Form
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private string m_sPassword;
        /// <summary>
        /// 
        /// </summary>
        private Point mouse_Offset;
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public OfflineForm(string sMessage, string sPassword)
        {
            InitializeComponent();

            if (!string.IsNullOrEmpty(sMessage))
                m_lbMessage.Text = sMessage;

            m_sPassword = sPassword;
        }
        #endregion

        #region Class Event Methods handlers
        /// <summary>
        /// 
        /// </summary>
        private void OnOKClick(object sender, MouseEventArgs e)
        {
            if (string.Compare(m_sPassword, m_tbPassWord.Text) == 0)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
                MessageBox.Show("Password가 틀렸습니다.", "경고창", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnCancelClick(object sender, MouseEventArgs e)
        {
            Close();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnPaint(object sender, PaintEventArgs e)
        {
            Color color = Color.SteelBlue;
            int iThickness = 3;
            ButtonBorderStyle style = ButtonBorderStyle.Solid;

            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, color, iThickness, style, color, iThickness, style, color, iThickness, style, color, iThickness, style);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            mouse_Offset = new Point(-e.X, -e.Y);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_Offset.X, mouse_Offset.Y);

                this.Location = mousePos;
            }
        }
        #endregion
    }
}
