﻿namespace Esol.CIM
{
    partial class OfflineForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnOk = new System.Windows.Forms.Button();
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.m_tbPassWord = new System.Windows.Forms.TextBox();
            this.m_lbMessage = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_btnOk
            // 
            this.m_btnOk.Font = new System.Drawing.Font("굴림", 9F);
            this.m_btnOk.Location = new System.Drawing.Point(93, 128);
            this.m_btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.m_btnOk.Name = "m_btnOk";
            this.m_btnOk.Size = new System.Drawing.Size(107, 34);
            this.m_btnOk.TabIndex = 0;
            this.m_btnOk.Text = "OK";
            this.m_btnOk.UseVisualStyleBackColor = true;
            this.m_btnOk.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnOKClick);
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.Font = new System.Drawing.Font("굴림", 9F);
            this.m_btnCancel.Location = new System.Drawing.Point(209, 128);
            this.m_btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(107, 34);
            this.m_btnCancel.TabIndex = 1;
            this.m_btnCancel.Text = "Cancel";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            this.m_btnCancel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnCancelClick);
            // 
            // m_tbPassWord
            // 
            this.m_tbPassWord.Location = new System.Drawing.Point(180, 87);
            this.m_tbPassWord.Margin = new System.Windows.Forms.Padding(4);
            this.m_tbPassWord.Name = "m_tbPassWord";
            this.m_tbPassWord.Size = new System.Drawing.Size(173, 28);
            this.m_tbPassWord.TabIndex = 2;
            // 
            // m_lbMessage
            // 
            this.m_lbMessage.AutoSize = true;
            this.m_lbMessage.Font = new System.Drawing.Font("굴림", 9F);
            this.m_lbMessage.ForeColor = System.Drawing.Color.Yellow;
            this.m_lbMessage.Location = new System.Drawing.Point(36, 15);
            this.m_lbMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.m_lbMessage.Name = "m_lbMessage";
            this.m_lbMessage.Size = new System.Drawing.Size(369, 18);
            this.m_lbMessage.TabIndex = 3;
            this.m_lbMessage.Text = "Are you sure you want to exit the program?";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(69, 92);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "PASSWORD";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(64, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(308, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "※ 담당자가 아니면 만지지 마십시오.";
            // 
            // OfflineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.ClientSize = new System.Drawing.Size(429, 186);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_lbMessage);
            this.Controls.Add(this.m_tbPassWord);
            this.Controls.Add(this.m_btnCancel);
            this.Controls.Add(this.m_btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(900, 500);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "OfflineForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OfflineForm";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnMouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnOk;
        private System.Windows.Forms.Button m_btnCancel;
        private System.Windows.Forms.TextBox m_tbPassWord;
        private System.Windows.Forms.Label m_lbMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}