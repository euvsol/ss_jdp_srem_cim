﻿namespace Esol.CIM
{
    partial class TerminalControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TerminalControl));
            this.roundPanel1 = new Esol.Components.RoundPanel();
            this.m_btnAllCheck = new System.Windows.Forms.Button();
            this.m_btnBuzzerOff = new System.Windows.Forms.Button();
            this.roundSinglePanel2 = new Esol.Components.RoundSinglePanel();
            this.label3 = new System.Windows.Forms.Label();
            this.m_lbText = new System.Windows.Forms.Label();
            this.roundSinglePanel1 = new Esol.Components.RoundSinglePanel();
            this.m_lbTid = new System.Windows.Forms.Label();
            this.m_lbTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_lbNo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_dgvTerminal = new System.Windows.Forms.DataGridView();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Clear = new System.Windows.Forms.DataGridViewButtonColumn();
            this.roundPanel1.SuspendLayout();
            this.roundSinglePanel2.SuspendLayout();
            this.roundSinglePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTerminal)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._Round_BackgoundControl = Esol.Components.eRoundControl.RoundWindow;
            this.roundPanel1._Round_Cutting = true;
            this.roundPanel1._Round_MouseEnterCursor = System.Windows.Forms.Cursors.Default;
            this.roundPanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundPanel1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundPanel1._Round_TitleImage = null;
            this.roundPanel1._Round_TitleImageShow = false;
            this.roundPanel1._Round_TitleLabelText = "Termina Message";
            this.roundPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundPanel1.Controls.Add(this.m_dgvTerminal);
            this.roundPanel1.Controls.Add(this.roundSinglePanel1);
            this.roundPanel1.Controls.Add(this.roundSinglePanel2);
            this.roundPanel1.Controls.Add(this.m_btnBuzzerOff);
            this.roundPanel1.Controls.Add(this.m_btnAllCheck);
            this.roundPanel1.Location = new System.Drawing.Point(0, 0);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(1145, 662);
            this.roundPanel1.TabIndex = 18;
            this.roundPanel1.MouseDownChanged += new Esol.Components.RoundPanel.MouseDownEventHandler(this.OnMouseDownChaged);
            this.roundPanel1.MouseMoveChanged += new Esol.Components.RoundPanel.MouseMoveEventHandler(this.OnMouseMoveChaged);
            // 
            // m_btnAllCheck
            // 
            this.m_btnAllCheck.Location = new System.Drawing.Point(1024, 597);
            this.m_btnAllCheck.Name = "m_btnAllCheck";
            this.m_btnAllCheck.Size = new System.Drawing.Size(96, 38);
            this.m_btnAllCheck.TabIndex = 19;
            this.m_btnAllCheck.Text = "All Check";
            this.m_btnAllCheck.UseVisualStyleBackColor = true;
            this.m_btnAllCheck.Click += new System.EventHandler(this.OnAllCheck);
            // 
            // m_btnBuzzerOff
            // 
            this.m_btnBuzzerOff.Location = new System.Drawing.Point(908, 597);
            this.m_btnBuzzerOff.Name = "m_btnBuzzerOff";
            this.m_btnBuzzerOff.Size = new System.Drawing.Size(110, 38);
            this.m_btnBuzzerOff.TabIndex = 19;
            this.m_btnBuzzerOff.Text = "Buzzer Off";
            this.m_btnBuzzerOff.UseVisualStyleBackColor = true;
            this.m_btnBuzzerOff.Visible = false;
            this.m_btnBuzzerOff.Click += new System.EventHandler(this.OnBuzzerOff);
            // 
            // roundSinglePanel2
            // 
            this.roundSinglePanel2._Round_BackgoundControl = Esol.Components.eRoundControl.Background;
            this.roundSinglePanel2._Round_Shape = Esol.Components.eShape.Round;
            this.roundSinglePanel2._Round_Theme = Esol.Components.eTheme.Gray;
            this.roundSinglePanel2._Round_VisibleLine = true;
            this.roundSinglePanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.roundSinglePanel2.Controls.Add(this.m_lbText);
            this.roundSinglePanel2.Controls.Add(this.label3);
            this.roundSinglePanel2.Location = new System.Drawing.Point(12, 449);
            this.roundSinglePanel2.Margin = new System.Windows.Forms.Padding(0);
            this.roundSinglePanel2.Name = "roundSinglePanel2";
            this.roundSinglePanel2.Size = new System.Drawing.Size(1108, 145);
            this.roundSinglePanel2.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Message";
            // 
            // m_lbText
            // 
            this.m_lbText.AutoSize = true;
            this.m_lbText.Location = new System.Drawing.Point(26, 42);
            this.m_lbText.Name = "m_lbText";
            this.m_lbText.Size = new System.Drawing.Size(0, 18);
            this.m_lbText.TabIndex = 8;
            // 
            // roundSinglePanel1
            // 
            this.roundSinglePanel1._Round_BackgoundControl = Esol.Components.eRoundControl.Background;
            this.roundSinglePanel1._Round_Shape = Esol.Components.eShape.Round;
            this.roundSinglePanel1._Round_Theme = Esol.Components.eTheme.Gray;
            this.roundSinglePanel1._Round_VisibleLine = true;
            this.roundSinglePanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.roundSinglePanel1.Controls.Add(this.label5);
            this.roundSinglePanel1.Controls.Add(this.label1);
            this.roundSinglePanel1.Controls.Add(this.m_lbNo);
            this.roundSinglePanel1.Controls.Add(this.label2);
            this.roundSinglePanel1.Controls.Add(this.m_lbTime);
            this.roundSinglePanel1.Controls.Add(this.m_lbTid);
            this.roundSinglePanel1.Location = new System.Drawing.Point(12, 395);
            this.roundSinglePanel1.Margin = new System.Windows.Forms.Padding(0);
            this.roundSinglePanel1.Name = "roundSinglePanel1";
            this.roundSinglePanel1.Size = new System.Drawing.Size(1108, 49);
            this.roundSinglePanel1.TabIndex = 19;
            // 
            // m_lbTid
            // 
            this.m_lbTid.AutoSize = true;
            this.m_lbTid.Location = new System.Drawing.Point(569, 20);
            this.m_lbTid.Name = "m_lbTid";
            this.m_lbTid.Size = new System.Drawing.Size(0, 18);
            this.m_lbTid.TabIndex = 6;
            // 
            // m_lbTime
            // 
            this.m_lbTime.AutoSize = true;
            this.m_lbTime.Location = new System.Drawing.Point(229, 20);
            this.m_lbTime.Name = "m_lbTime";
            this.m_lbTime.Size = new System.Drawing.Size(0, 18);
            this.m_lbTime.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(520, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tid :";
            // 
            // m_lbNo
            // 
            this.m_lbNo.AutoSize = true;
            this.m_lbNo.Location = new System.Drawing.Point(67, 20);
            this.m_lbNo.Name = "m_lbNo";
            this.m_lbNo.Size = new System.Drawing.Size(0, 18);
            this.m_lbNo.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "Time :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 18);
            this.label5.TabIndex = 5;
            this.label5.Text = "No :";
            // 
            // m_dgvTerminal
            // 
            this.m_dgvTerminal.AllowUserToAddRows = false;
            this.m_dgvTerminal.AllowUserToDeleteRows = false;
            this.m_dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvTerminal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Time,
            this.Tid,
            this.sText,
            this.Clear});
            this.m_dgvTerminal.Location = new System.Drawing.Point(12, 42);
            this.m_dgvTerminal.Name = "m_dgvTerminal";
            this.m_dgvTerminal.ReadOnly = true;
            this.m_dgvTerminal.Size = new System.Drawing.Size(1108, 350);
            this.m_dgvTerminal.TabIndex = 18;
            this.m_dgvTerminal.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellClick);
            // 
            // Time
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Time.DefaultCellStyle = dataGridViewCellStyle1;
            this.Time.Frozen = true;
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Time.Width = 180;
            // 
            // Tid
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Tid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Tid.Frozen = true;
            this.Tid.HeaderText = "Tid";
            this.Tid.Name = "Tid";
            this.Tid.ReadOnly = true;
            // 
            // sText
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.sText.DefaultCellStyle = dataGridViewCellStyle3;
            this.sText.Frozen = true;
            this.sText.HeaderText = "Text";
            this.sText.Name = "sText";
            this.sText.ReadOnly = true;
            this.sText.Width = 380;
            // 
            // Clear
            // 
            this.Clear.Frozen = true;
            this.Clear.HeaderText = "Clear";
            this.Clear.Name = "Clear";
            this.Clear.ReadOnly = true;
            this.Clear.Width = 50;
            // 
            // TerminalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 643);
            this.Controls.Add(this.roundPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TerminalControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.roundPanel1.ResumeLayout(false);
            this.roundSinglePanel2.ResumeLayout(false);
            this.roundSinglePanel2.PerformLayout();
            this.roundSinglePanel1.ResumeLayout(false);
            this.roundSinglePanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTerminal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundPanel roundPanel1;
        private System.Windows.Forms.DataGridView m_dgvTerminal;
        private Components.RoundSinglePanel roundSinglePanel1;
        private System.Windows.Forms.Label m_lbTid;
        private System.Windows.Forms.Label m_lbTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Components.RoundSinglePanel roundSinglePanel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label m_lbText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label m_lbNo;
        private System.Windows.Forms.Button m_btnAllCheck;
        private System.Windows.Forms.Button m_btnBuzzerOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tid;
        private System.Windows.Forms.DataGridViewTextBoxColumn sText;
        private System.Windows.Forms.DataGridViewButtonColumn Clear;
    }
}