﻿#region Class Using
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
#endregion

namespace Esol.CIM
{
    public partial class TerminalControl : Form
    {
        #region Class Constans
        /// <summary>
        /// 
        /// </summary>
        private readonly int DEF_NEXT_LINE_LENGTH = 120;
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataGridView(DataGridView dgv, string sTime, string sTid, string list);
        /// <summary>
        /// 
        /// </summary>
        private Point mouse_Offset;
        /// <summary>
        /// 
        /// </summary>
        private TerminalManager m_TerminalManager;
        /// <summary>
        /// 
        /// </summary>
        private string[][] m_listMessages;
        #endregion

        #region Class Initiailize
        /// <summary>
        /// 
        /// </summary>
        public TerminalControl(TerminalManager terminalManager)
        {
            InitializeComponent();

            InitializeDataGridView();

            m_TerminalManager = terminalManager;

        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            m_dgvTerminal.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;

            for (int i = 0; i < m_dgvTerminal.Columns.Count; i++)
                this.m_dgvTerminal.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewCrossThread(DataGridView dgv, string sTime, string sTid, string list)
        {
            if (dgv.InvokeRequired)
            {
                ReloadDataGridView reload = new ReloadDataGridView(DataGridViewCrossThread);
                this.Invoke(reload, new object[] { dgv, sTime, sTid, list });
            }
            else
                InsertOpCallMessageIntoDataGrid(sTime, sTid, list);
        }
        /// <summary>
        /// 
        /// </summary>
        private void InsertOpCallMessageIntoDataGrid(string sTime, string sTid, string nextMessage)
        {
            try
            {
                m_dgvTerminal.Rows.Add();

                int iRowIndex = m_dgvTerminal.Rows.Count - 1;

                m_dgvTerminal.Rows[iRowIndex].Cells[0].Value = sTime;
                m_dgvTerminal.Rows[iRowIndex].Cells[1].Value = sTid;
                m_dgvTerminal.Rows[iRowIndex].Cells[2].Value = nextMessage;


                m_lbNo.Text = m_dgvTerminal.Rows.Count.ToString();
                m_lbTime.Text = sTime;
                m_lbTid.Text = sTid;
                m_lbText.Text = StringNextLine(nextMessage);

                m_dgvTerminal.CurrentCell = m_dgvTerminal.Rows[0].Cells[0];
            }
            catch (Exception ex)
            {
                m_TerminalManager.CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private string StringNextLine(string sValue)
        {
            string sReturnValue = sValue;

            if (sValue.Length > DEF_NEXT_LINE_LENGTH)
            {
                int iCount = sValue.Length / DEF_NEXT_LINE_LENGTH;

                for (int i = iCount; i >= 1; i--)
                    sReturnValue = sReturnValue.Insert(DEF_NEXT_LINE_LENGTH * i, "\n");
            }

            return sReturnValue;
        }
        #endregion

        #region Class Event Methods
        /// <summary>
        /// 
        /// </summary>
        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            switch (e.ColumnIndex)
            {
                case 3:
                    m_dgvTerminal.Rows.RemoveAt(e.RowIndex);
                    m_TerminalManager.RemoveActiveMessage(e.RowIndex);
                    break;
                default:
                    int iRowIndex = e.RowIndex + 1;
                    m_lbNo.Text = iRowIndex.ToString();
                    m_lbTime.Text = m_dgvTerminal.Rows[e.RowIndex].Cells[0].Value.ToString();
                    m_lbTid.Text = m_dgvTerminal.Rows[e.RowIndex].Cells[1].Value.ToString();
                    m_lbText.Text = StringNextLine(m_dgvTerminal.Rows[e.RowIndex].Cells[2].Value.ToString());
                    break;
            }

            if (m_dgvTerminal.Rows.Count == 0)
                OnClosing();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            m_dgvTerminal.Rows.Clear();

            e.Cancel = true;

            base.OnClosing(e);

            OnClosing();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnClosing()
        {
            m_dgvTerminal.Rows.Clear();
            m_lbNo.Text = string.Empty;
            m_lbTime.Text = string.Empty;
            m_lbTid.Text = string.Empty;
            m_lbText.Text = string.Empty;

            m_TerminalManager.RemoveAllActiveMessages();

            this.Hide();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnAllCheck(object sender, EventArgs e)
        {
            OnClosing();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnBuzzerOff(object sender, EventArgs e)
        {
            //추후 Buzzer Interface 추가 후 기능 구현
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseMoveChaged(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    Point mousePos = Control.MousePosition;
                    mousePos.Offset(mouse_Offset.X, mouse_Offset.Y);

                    this.Location = mousePos;
                }
            }
            catch (Exception ex)
            {
                m_TerminalManager.CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseDownChaged(object sender, MouseEventArgs e)
        {
            mouse_Offset = new Point(-e.X - 34, -e.Y - 3);
        }
        #endregion

        #region Class Public
        /// <summary>
        /// 
        /// </summary>
        public void UpdateList(string[][] listMessages)
        {
            try
            {
                m_listMessages = listMessages;

                string sTime = (string)Common.GenerateCurrentTime().Clone();

                m_dgvTerminal.Rows.Clear();

                for (int i = 0, len = listMessages.Length; 0 < len; --len, i++)
                {
                    if (i == m_TerminalManager.TerminalViewCount)
                        m_TerminalManager.RemoveActiveMessage(m_TerminalManager.TerminalViewCount);
                    else
                    {
                        string[] nextMessage = listMessages[len - 1];

                        DataGridViewCrossThread(m_dgvTerminal, nextMessage[2], nextMessage[0], nextMessage[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                m_TerminalManager.CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
