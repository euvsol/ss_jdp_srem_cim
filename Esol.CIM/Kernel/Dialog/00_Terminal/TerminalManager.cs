﻿#region Class Using
using System;
using System.IO;
using System.Collections.Generic;
using Esol.XmlControl;
using Esol.LogControl;
#endregion

namespace Esol.CIM
{
    public class TerminalManager
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ROOT_ELEMENT = "TerminalMessages";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_PARENT_ELEMENT = "Group";
        /// <summary>
        /// 
        /// </summary>
        private readonly int DEF_MAX_ACTIVE_RECORDS_COUNT = 50;
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_MESSAGE_TEXT = "Message";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private readonly object m_syncObject;
        /// <summary>
        /// 
        /// </summary>
        private string[] m_sArrayKey;
        /// <summary>
        /// 
        /// </summary>
        private readonly string m_sActiveFile;
        /// <summary>
        /// 
        /// </summary>
        private readonly string m_sHistoryFile;
        /// <summary>
        /// 
        /// </summary>
        private List<string[]> m_listActiveMessages;
        /// <summary>
        /// 
        /// </summary>
        private List<string[]> m_listHistoryMessages;
        /// <summary>
        /// 
        /// </summary>
        private XmlDataWriter m_xmlActiveWriter;
        /// <summary>
        /// 
        /// </summary>
        private XmlDataWriter m_xmlHistoryWriter;
        /// <summary>
        /// 
        /// </summary>
        private CimConfigurator m_CimConfigurator;
        /// <summary>
        /// 
        /// </summary>
        private TerminalControl m_TerminalControl;
        /// <summary>
        /// 
        /// </summary>
        private FileSystemWatcher m_watcherActiveFile;
        /// <summary>
        /// 
        /// </summary>
        private readonly ParameterlessEvendHandler m_showViewerHandler;
        /// <summary>
        /// 
        /// </summary>
        private DateTime m_dateActiveFileLastModification;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public CimConfigurator CimConfigurator
        {
            get
            {
                return m_CimConfigurator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_CimConfigurator.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TerminalControl TerminalControl
        {
            get
            {
                return m_TerminalControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int TerminalViewCount
        {
            get
            {
                return m_CimConfigurator.TerminalCount;
            }
        }
        #endregion

        #region Class Event Methodss methods
        /// <summary>
        /// 
        /// </summary>
        private delegate void ParameterlessEvendHandler();
        /// <summary>
        /// 
        /// </summary>
        public delegate void TerminalMessageEventHandler(string[][] list);
        /// <summary>
        /// 
        /// </summary>
        public event TerminalMessageEventHandler TerminalHistoryListChanged;
        #endregion

        #region Class Initlaize
        /// <summary>
        /// 
        /// </summary>
        public TerminalManager(CimConfigurator cimConfigurator)
        {
            try
            {
                m_CimConfigurator = cimConfigurator;
                m_syncObject = new object();

                m_sArrayKey = Enum.GetNames(typeof(Terminal));

                string sTerminalDirectory = string.Format(@"{0}\{1}\", cimConfigurator.LogPath, "Terminal");

                m_sActiveFile = sTerminalDirectory + "Terminal_Active.xml";
                m_sHistoryFile = sTerminalDirectory + "Terminal_History.xml";

                DirectoryInfo di = new DirectoryInfo(sTerminalDirectory);
                if (!di.Exists) di.Create();

                m_xmlActiveWriter = new XmlDataWriter(m_sActiveFile, DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT);
                m_xmlHistoryWriter = new XmlDataWriter(m_sHistoryFile, DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT);

                m_watcherActiveFile = new FileSystemWatcher()
                {
                    Path = sTerminalDirectory,
                    Filter = Path.GetFileName(m_sActiveFile),
                    NotifyFilter = NotifyFilters.LastWrite,
                    EnableRaisingEvents = false,
                };
                m_watcherActiveFile.Changed += new FileSystemEventHandler(ActiveFileChanged);

                m_TerminalControl = new TerminalControl(this);

                m_showViewerHandler = new ParameterlessEvendHandler(ShowTermianlMessagesViewer);

                ReadTerminalActiveMessagesFromFile();
                ReadTerminalHistoryMessagesFromFile();

                ShowTermianlMessagesViewer();

                if (m_listActiveMessages.Count == 0)
                    m_TerminalControl.Hide();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void DeleteTerminalActive()
        {
            int iRange = m_listActiveMessages.Count - DEF_MAX_ACTIVE_RECORDS_COUNT;

            if (iRange > 0)
                m_listActiveMessages.RemoveRange(0, iRange);
        }
        /// <summary>
        /// 
        /// </summary>
        private void DeleteTerminalHistory()
        {
            try
            {
                for (int i = 0; i < m_listHistoryMessages.Count; i++)
                {
                    long lMessageTicks = Convert.ToInt64(m_listHistoryMessages[0][(int)Terminal.tick]);

                    int iLogDays = -m_CimConfigurator.LogDays;
                    long lDataTicks = DateTime.Now.AddDays(iLogDays).Ticks;

                    if (lDataTicks > lMessageTicks)
                    {
                        m_xmlHistoryWriter.XmlElementDelete(DEF_PARENT_ELEMENT, "tick", lMessageTicks.ToString(), true);
                        m_listHistoryMessages.RemoveAt(0);
                    }
                    else
                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ShowTermianlMessagesViewer()
        {
            string[][] listMessages = GetActiveMessagesList();

            if (0 > listMessages.Length)
                return;

            m_TerminalControl.UpdateList(listMessages);

            if (m_TerminalControl.Visible)
                m_TerminalControl.Focus();
            else
                m_TerminalControl.Show();

            m_TerminalControl.TopMost = true;
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadTerminalActiveMessagesFromFile()
        {
            m_listActiveMessages = new List<string[]>();
            XmlDataReader reader = new XmlDataReader(m_sActiveFile, false);

            m_listActiveMessages = reader.XmlReadList(string.Format("{0}/{1}", DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT), m_sArrayKey);
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadTerminalHistoryMessagesFromFile()
        {
            m_listHistoryMessages = new List<string[]>();
            XmlDataReader reader = new XmlDataReader(m_sHistoryFile, false);

            m_listHistoryMessages = reader.XmlReadList(string.Format("{0}/{1}", DEF_ROOT_ELEMENT, DEF_PARENT_ELEMENT), m_sArrayKey);
        }
        /// <summary>
        /// 
        /// </summary>
        private void Fire_TerminalMessagesHistoryListChanged()
        {
            if (null == TerminalHistoryListChanged || !m_CimConfigurator.HistoryControl.Visible)
                return;

            TerminalHistoryListChanged(m_listHistoryMessages.ToArray());
        }
        /// <summary>
        /// 
        /// </summary>
        private void SaveTerminalMessagesToFile(string[] sTerminal)
        {
            m_xmlActiveWriter.XmlAttributeWrite(DEF_PARENT_ELEMENT, DEF_MESSAGE_TEXT, m_sArrayKey, sTerminal);
            m_xmlHistoryWriter.XmlAttributeWrite(DEF_PARENT_ELEMENT, DEF_MESSAGE_TEXT, m_sArrayKey, sTerminal);
        }
        /// <summary>
        /// 
        /// </summary>
        private void ActiveFileChanged(object sender, FileSystemEventArgs ea)
        {
            try
            {
                DateTime time = File.GetLastWriteTime(m_sActiveFile);

                m_dateActiveFileLastModification = time;

                if (m_TerminalControl.InvokeRequired)
                    m_TerminalControl.Invoke(m_showViewerHandler);
                else
                    ShowTermianlMessagesViewer();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Public
        /// <summary>
        /// 
        /// </summary>
        public void RemoveAllActiveMessages()
        {
            try
            {
                lock (m_syncObject)
                {
                    m_listActiveMessages.Clear();

                    m_xmlActiveWriter.XmlAllElementDelete(DEF_PARENT_ELEMENT);

                    ShowTermianlMessagesViewer();
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[][] GetActiveMessagesList()
        {
            lock (m_syncObject)
            {
                return m_listActiveMessages.ToArray();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[][] GetHistoryMessagesList()
        {
            lock (m_syncObject)
            {
                return m_listHistoryMessages.ToArray();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void RemoveActiveMessage(int iIndex)
        {
            try
            {
                lock (m_syncObject)
                {
                    iIndex = m_listActiveMessages.Count - iIndex - 1;

                    if (iIndex < 0)
                        return;

                    string sTick = m_listActiveMessages[iIndex][(int)Terminal.tick];

                    m_listActiveMessages.RemoveAt(iIndex);

                    m_xmlActiveWriter.XmlElementDelete(DEF_PARENT_ELEMENT, Terminal.tick.ToString(), sTick, true);

                    ShowTermianlMessagesViewer();
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void AddMessage(string iId, List<string> listText, bool bMulti = false)
        {
            try
            {
                lock (m_syncObject)
                {
                    string sTime = Common.GenerateGUITime(DateTime.Now);

                    foreach (string sText in listText)
                    {
                        string[] message = new string[m_sArrayKey.Length];

                        message[(int)Terminal.id] = iId.ToString();
                        message[(int)Terminal.text] = sText;
                        message[(int)Terminal.tick] = DateTime.Now.Ticks.ToString();
                        message[(int)Terminal.time] = sTime;

                        DeleteTerminalActive();
                        DeleteTerminalHistory();

                        if (!bMulti)
                            m_listActiveMessages.Add(message);

                        m_listHistoryMessages.Add(message);

                        Fire_TerminalMessagesHistoryListChanged();

                        SaveTerminalMessagesToFile(message);
                    }

                    if (bMulti)
                    {
                        for (int i = listText.Count - 1; i >= 0; i -= 1)
                        {
                            string[] message = new string[m_sArrayKey.Length];

                            message[(int)Terminal.id] = iId.ToString();
                            message[(int)Terminal.text] = listText[i];
                            message[(int)Terminal.tick] = DateTime.Now.Ticks.ToString();
                            message[(int)Terminal.time] = sTime;

                            m_listActiveMessages.Add(message);
                        }
                    }
                }

                ActiveFileChanged(null, null);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
