﻿#region Class Using
using System;
using System.Xml;
#endregion

namespace Esol.CIM
{
    public class Common
    {
        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        public static string GenerateGUITime(DateTime time)
        {
            string sCurrentTime = string.Format("{0:yyyy}-{0:MM}-{0:dd} {0:HH}:{0:mm}:{0:ss}.{0:fff}", time);

            return sCurrentTime;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string GenerateFileTime(DateTime time)
        {
            string sCurrentTime = string.Format("{0:yy}{0:MM}{0:dd}_{0:HH}{0:mm}{0:ss}", time);

            return sCurrentTime;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string GenerateGetTime(DateTime time)
        {
            string sCurrentTime = string.Format("{0:yyyy}{0:MM}{0:dd}", time);

            return sCurrentTime;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string ReadAttribute(XmlReader reader, string sAttributeKey)
        {
            if (!reader.HasAttributes)
                return string.Empty;

            return reader.GetAttribute(sAttributeKey);
        }
        /// <summary>
        /// 
        /// </summary>
        public static string ReadAttribute(XmlNode node, string sName)
        {
            return ReadAttribute(node, sName, string.Empty);
        }
        /// <summary>
        /// 
        /// </summary>
        public static string ReadAttribute(XmlNode node, string sName, string sDefaultValue)
        {
            string str;
            try
            {
                str = node.Attributes[sName].Value;
            }
            catch
            {
                str = sDefaultValue;
            }
            return str;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string[] ReadArryAttribute(XmlReader reader, string sAttributekey)
        {
            string[] sReturnValue = new string[0];

            if (!reader.HasAttributes)
                return (string[])null;

            return sReturnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string SharePointValue(object oPlcValue, double dPoint)
        {
            double dValue = Convert.ToDouble(oPlcValue) * dPoint;

            string sValue = string.Format("{0:0.#########}", dValue);

            return sValue;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string GenerateCurrentTime()
        {
            string sCurrentTime = string.Format("[{0:yyyy}-{0:MM}-{0:dd}] [{0:HH}:{0:mm}:{0:ss}.{0:fff}]", DateTime.Now);

            return sCurrentTime;
        }
        #endregion
    }
}
