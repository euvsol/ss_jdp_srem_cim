﻿namespace Esol.CIM
{
    public class ShareNames
    {
        #region Class CIM

        #endregion

        #region Class CIM Bit
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_ALIVE_NAME = "L1_B_ALIVE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_STATE_COMMAND = "L1_B_CONTROL_STATE_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_ACCESS_COMMAND = "L1_B_PORT_MODE_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CARRIER_DATA_DOWN_COMMAND = "L1_B_CARRIER_DATA_DOWNLOAD_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_PROCESS_JOB_DOWNLOAD_COMMAND = "L1_B_PROCESS_JOB_DOWNLOAD_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_JOB_DOWNLOAD_COMMAND = "L1_B_CONTROL_JOB_DOWNLOAD_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_PROCESSJOB_CMD_COMMAND = "L1_B_PROCESS_JOB_CMD_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROLJOB_CMD_COMMAND = "L1_B_CONTROL_JOB_CMD_COMMAND";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_PROCESS_JOB_REPLY = "L1_B_PROCESS_JOB_STATE_REPLY";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_JOB_REPLY = "L1_B_CONTROL_JOB_STATE_REPLY";


        public const string DEF_CUSTOM_NACK_COMMAND = "L1_B_CUSTOM_NACK_COMMAND";
        #endregion

        #region Class EQ Bit
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SVID_REPORT = "L2_B_SVID_REPORT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ALARM_EXIST = "L2_B_ALARM_EXIST";
        #endregion

        #region Class CIM Word
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_STATE = "L1_W_CONTROL_STATE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_PROCESS_JOB_ACK = "L1_W_PROCESS_JOB_ACK";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_JOB_ACK = "L1_W_CONTROL_JOB_ACK";
        #endregion

        #region Class EQ Word
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROC_STATE = "L2_W_PROCESS_STATE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROCESS_JOB_STATE = "L2_W_PROCESS_JOB_STATE";
        #endregion

        #region Class CIM Struct
        /// <summary>
        ///
        /// </summary>
        public const string DEF_HOST_CARRIER_DATA_DOWNLOAD_STRUCT = "L1_CARRIER_MAPPING_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_PROCESS_JOB_DOWNLOAD_STRUCT = "L1_PROCESS_JOB_DOWNLOAD_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_JOB_DOWNLOAD_STRUCT = "L1_CONTROL_JOB_DOWNLOAD_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_PROCESS_JOB_CMD_STRUCT = "L1_PROCESS_JOB_CMD_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CONTROL_JOB_CMD_STRUCT = "L1_CONTROL_JOB_CMD_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_CJ_STRUCT = "L1_CONTROL_JOB_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_ACCESS_STRUCT = "L1_PORT_ACCESS_STRUCT";


        public const string DEF_CUSTOM_NACK_STRUCT = "L1_CUSTOM_NACK_STRUCT";
        #endregion

        #region Class EQ Struct
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SVID_STRUCT = "L2_SVID_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_EQ_PROCESSJOB_STATE_STRUCT = "L2_PROCESS_JOB_STATE_STRUCT";
        #endregion

        #region Class EQ Trx
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ALARM_TRX = "L2_AlarmTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROC_STATE_TRX = "L2_ProcStateTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_DATE_TIME_SYNC_TRX = "L2_DateTimeSyncTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ACCESS_TRX = "L2_AccessTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PORT_MODE_TRX = "L2_PortModeTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_TRANSFER_TRX = "L2_TransferDataTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_DCOLL_TRX = "L2_DcollTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_RECIPE_CMD_TRX = "L2_RecipeCmdTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROCESS_JOB_DELETE_TRX = "L2_ProcessDeleteTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CONTROL_JOB_DELETE_TRX = "L2_ControlJobDeleteTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CARRIER_DELETE_TRX = "L2_CarrierDeleteTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CARRIER_DATA_TRX = "L2_CarrierDataTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROCESS_JOB_STATE_TRX = "L2_ProcessJobStateTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CONTROL_JOB_STATE_TRX = "L2_ControlJobStateTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CONTROL_STATE_TRX = "L2_ControlStateTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_STEP_CHANGE_TRX = "L2_StepChangeTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CONTROL_STATE_ACK = "L1_W_CONTROL_STATE_ACK";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CONTROL_STATE_RELAY = "L1_B_CONTROL_STATE_REPLY";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PLASMA_AUTO_CLEANING_TRX = "L2_PlasmaAutoCleaningTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_EUV_POWER_MONITORING_TRX = "L2_EuvPowerMonitoringTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LIGHT_MONITORING_TRX = "L2_LightMonitoringTrx";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ECID_CHANGE_TRX = "L2_EcidChangeTrx";
        #endregion
    }
}
