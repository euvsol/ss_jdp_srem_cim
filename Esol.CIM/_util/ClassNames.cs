﻿namespace Esol.CIM
{
    public class ClassNames
    {
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_OFFLINE_TEXT = "OFFLINE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ONLINE_TEXT = "ONLINE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_DISABLE_TEXT = "Disable";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ENALBE_TEXT = "Enable";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CIM_LOCAL_NO = "L1";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_REMOTE_TEXT = "REMOTE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOCAL_TEXT = "LOCAL";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ONLINE_LOCAL_TEXT = "ONLINE-LOCAL";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ONLINE_RETMOE_TEXT = "ONLINE-REMOTE";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_HOST_OFFLINE_TEXT = "HOST OFFLINE";
        /// <summary>
        /// 
        /// </summary>
        public readonly string DEF_PASS_RECIPE_PARAMETER = "ALIGN_MARK_INFO";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ICM = "ICM";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ITS = "ITS";
    }
}
