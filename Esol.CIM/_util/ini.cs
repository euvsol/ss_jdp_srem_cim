﻿#region Class Using
using System;
using System.Runtime.InteropServices;
using System.IO;
#endregion

#region Public Basic
public interface PrivateINIBasic
{
    /// <summary>
    /// 
    /// </summary>
    string[] GetAllSectionNames();
    /// <summary>
    /// 
    /// </summary>
    string[] GetAllSectionValue(string SectionName);
    /// <summary>
    /// 
    /// </summary>
    string GetValue(string SectionName, string KeyName);
    /// <summary>
    /// 
    /// </summary>
    bool SetValue(string SectionName, string KeyName, string Value);
    /// <summary>
    /// 
    /// </summary>
    void DeleteSection(string SectionName);
    /// <summary>
    /// 
    /// </summary>
    bool DeleteKey(string SectionName, string KeyName);
    /// <summary>
    /// 
    /// </summary>
    bool SetPairBySection(string SectionName, string KeyName, string Value);
    /// <summary>
    /// 
    /// </summary>
    string[] GetPairsBySection(string SectionName);
}
#endregion

#region Class INIAPI
/// <summary>
/// 
/// </summary>
internal static class INIAPI
{
    /// <summary>
    /// 
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern bool WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
    /// <summary>
    /// 
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern bool WritePrivateProfileSection(string lpAppName, string lpString, string lpFileName);
    /// <summary>
    /// 
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern uint GetPrivateProfileInt(string lpAppName, string lpKeyName, int nDefalut, string lpFileName);
    /// <summary>
    /// 
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, byte[] lpReturnValue, uint nSize, string lpFileName);
    /// <summary>
    /// 
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern uint GetPrivateProfileSection(string lpAppName, byte[] lpPairVaules, uint nSize, string lpFileName);
    /// <summary>
    /// 
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern uint GetPrivateProfileSectionNames(byte[] lpSections, uint nSize, string lpFileName);
}
/// <summary>
/// 
/// </summary>
#endregion

#region PrivateBasic
class INI : PrivateINIBasic
{
    #region Class statics
    /// <summary>
    /// 
    /// </summary>
    public static uint SectionBufferSize = 1024;
    /// <summary>
    /// 
    /// </summary>
    public static uint DataBufferSize = 65534;
    /// <summary>
    /// 
    /// </summary>
    protected static string m_sFileName;
    #endregion

    #region Class initialization
    /// <summary>
    /// 
    /// </summary>
    public INI(string FileTotalName)
    {
        m_sFileName = FileTotalName;

        FileInfo fi = new FileInfo(m_sFileName);
        if (!fi.Directory.Exists)
            fi.Directory.Create();
    }
    #endregion

    #region Class Public Methods
    /// <summary>
    /// 
    /// </summary>
    public string[] GetAllSectionNames()
    {
        if (!FileExist()) throw (new FileNotFoundException());

        byte[] bSection = new byte[SectionBufferSize];

        if (INIAPI.GetPrivateProfileSectionNames(bSection, SectionBufferSize, m_sFileName) <= 0)
            return null;

        return System.Text.Encoding.Default.GetString(bSection).Split(new char[1] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
    }
    /// <summary>
    /// 
    /// </summary>
    public string[] GetAllSectionValue(string SectionName)
    {
        if (!FileExist()) throw (new FileNotFoundException());

        byte[] bData = new byte[DataBufferSize];

        if (INIAPI.GetPrivateProfileSection(SectionName, bData, DataBufferSize, m_sFileName) <= 0)
            return null;

        return System.Text.Encoding.Default.GetString(bData).Split(new char[1] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
    }
    /// <summary>
    /// 
    /// </summary>
    public string GetValue(string SectionName, string KeyName)
    {
        if (!FileExist()) throw (new FileNotFoundException());

        byte[] bPair = new byte[DataBufferSize];
        if (INIAPI.GetPrivateProfileString(SectionName, KeyName, "", bPair, DataBufferSize, m_sFileName) <= 0)
            return string.Empty;

        return System.Text.Encoding.Default.GetString(bPair).Trim('\0');
    }
    /// <summary>
    /// 
    /// </summary>
    public int GetValueToInt(string SectionName, string KeyName, int iDefault)
    {
        string sValue = GetValue(SectionName, KeyName);

        try
        {
            int iValue = iDefault;
            int.TryParse(sValue, out iValue);

            return iValue;
        }
        catch { return iDefault; }
    }
    /// <summary>
    /// 
    /// </summary>
    public double GetValueToDouble(string SectionName, string KeyName, double dDefault)
    {
        string sValue = GetValue(SectionName, KeyName);

        try
        {
            double dValue = dDefault;
            double.TryParse(sValue, out dValue);

            return dValue;
        }
        catch { return dDefault; }
    }
    /// <summary>
    /// 
    /// </summary>
    public bool SetValue(string SectionName, string KeyName, string Value)
    {
        if (!FileExist()) throw (new FileNotFoundException());

        return INIAPI.WritePrivateProfileString(SectionName, KeyName, Value, m_sFileName);
    }
    /// <summary>
    /// 
    /// </summary>
    public string[] GetPairsBySection(string SectionName)
    {
        if (!FileExist()) throw (new FileNotFoundException());
        byte[] bPair = new byte[DataBufferSize];
        if (INIAPI.GetPrivateProfileSection(SectionName, bPair, DataBufferSize, m_sFileName) <= 0)
            return null;
        return System.Text.Encoding.Default.GetString(bPair).Split(new char[1] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
    }
    /// <summary>
    /// 
    /// </summary>
    public bool SetPairBySection(string szSection, string szKey, string szValue)
    {
        return INIAPI.WritePrivateProfileString(szSection, szKey, szValue, m_sFileName);
    }
    /// <summary>
    /// 
    /// </summary>
    public bool DeleteKey(string szSection, string szKey)
    {
        return INIAPI.WritePrivateProfileString(szSection, szKey, null, m_sFileName);
    }
    /// <summary>
    /// 
    /// </summary>
    public void DeleteSection(string szSection)
    {
        INIAPI.WritePrivateProfileSection(szSection, null, m_sFileName);
    }
    /// <summary>
    /// 
    /// </summary>
    public bool FileExist()
    {
        if (System.IO.File.Exists(m_sFileName))
            return true;

        return false;
    }

    #endregion
}
#endregion