﻿#region 어셈블리 Esol.ShareTrx, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareTrx.dll
// Decompiled with ICSharpCode.Decompiler 7.1.0.6543
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Esol.CIM;
using Esol.ShareMemory;
using static Esol.LogControl.Log;

namespace Esol.ShareTrxControl
{
    public class Logger
    {
        public class LogMessage
        {
            public string Text;

            public DateTime Time;

            public LogMessageType Type;
        }

        [Flags]
        public enum LogMessageType
        {
            Info = 0x1,
            Data = 0x2
        }

        public delegate void SendLogData(StringBuilder sb);

        public interface IPlcDriverLogLeecher
        {
            string PlcLogLeecherId { get; }

            void HandleLogMessage(LogMessage message);
        }

        protected const int DEF_SAVE_FILE_INTERVAL = 5000;

        public const string DEF_SHARE_MEMORY = "Share";

        public const string DEF_MES = "Mes";

        public string LogPath;

        private ShareTrx m_ShareTrx;

        private object m_syncShareObject;

        private object m_syncMesObject;

        protected Timer m_timerSaveFile;

        protected Timer m_timerMesSaveFile;

        private Dictionary<IPlcDriverLogLeecher, LogMessageType> m_hashLeechers;

        private Dictionary<IPlcDriverLogLeecher, LogMessageType> m_hashMesLeechers;

        private Queue<LogMessage> m_queueShareMessages;

        private Queue<LogMessage> m_queueShareMessagesFileSaveCache;

        private Queue<LogMessage> m_queueMesMessages;

        private Queue<LogMessage> m_queueMesMessagesFileSaveCache;

        private Thread m_threadShareLogMessageProcessor;

        private Thread m_threadMesLogMessageProcessor;

        private bool m_bSimulator;

        public bool bSimulator
        {
            get
            {
                return m_bSimulator;
            }
            set
            {
                m_bSimulator = value;
            }
        }

        public void Initialize(string path, ShareTrx shareTrx)
        {
            try
            {
                LogPath = path;
                m_ShareTrx = shareTrx;
                m_syncShareObject = new object();
                m_syncMesObject = new object();
                m_bSimulator = false;
                m_queueShareMessages = new Queue<LogMessage>();
                m_queueShareMessagesFileSaveCache = new Queue<LogMessage>();
                m_queueMesMessages = new Queue<LogMessage>();
                m_queueMesMessagesFileSaveCache = new Queue<LogMessage>();
                m_hashLeechers = new Dictionary<IPlcDriverLogLeecher, LogMessageType>();
                m_hashMesLeechers = new Dictionary<IPlcDriverLogLeecher, LogMessageType>();
                TimerCallback callback = OnSaveFileShareMemoryTimerElapsed;
                m_timerSaveFile = new Timer(callback, null, -1, -1);
                TimerCallback callback2 = OnSaveFileMesMemoryTimerElapsed;
                m_timerMesSaveFile = new Timer(callback2, null, -1, -1);
            }
            catch
            {
            }
        }

        public void Start()
        {
            lock (m_syncShareObject)
            {
                if (m_ShareTrx.bRun)
                {
                    m_queueShareMessages.Clear();
                    m_queueShareMessagesFileSaveCache.Clear();
                    m_queueMesMessages.Clear();
                    m_queueMesMessagesFileSaveCache.Clear();
                    CreateLogMessageProcessorThread();
                    m_threadShareLogMessageProcessor.Start();
                    m_threadMesLogMessageProcessor.Start();
                    RechargeTimer("Share");
                    RechargeTimer("Mes");
                }
            }
        }

        private void CreateLogMessageProcessorThread()
        {
            ThreadStart start = OnLogShareMessageProcessorThread;
            m_threadShareLogMessageProcessor = new Thread(start);
            m_threadShareLogMessageProcessor.IsBackground = true;
            ThreadStart start2 = OnLogMesMessageProcessorThread;
            m_threadMesLogMessageProcessor = new Thread(start2);
            m_threadMesLogMessageProcessor.IsBackground = true;
        }

        public void OnStop()
        {
            if (m_threadMesLogMessageProcessor != null)
            {
                m_threadMesLogMessageProcessor.Abort();
            }

            if (m_threadShareLogMessageProcessor != null)
            {
                m_threadShareLogMessageProcessor.Abort();
            }
        }

        public StringBuilder LogWrite(PlcAddr PlcMessage)
        {
            try
            {
                string key = string.Format("{0},{1}", PlcMessage.AddrName, PlcMessage.vBit ? "1" : "0");
                StringBuilder stringBuilder = new StringBuilder();
                if (!m_ShareTrx.hashShareTrx.ContainsKey(key))
                {
                    string text = GenerateCurrentTime();
                    object obj = PlcTypeSwitch(PlcMessage);
                    string text2 = PlcMessage.Addr.ToString();
                    if (PlcMessage.ValueType == PlcValueType.BIT)
                    {
                        text2 = $"{PlcMessage.Addr}.{PlcMessage.Bit}";
                    }

                    stringBuilder.AppendLine($"{text} : [{text2}]{PlcMessage.AddrName} = {obj.ToString()}");
                    WriteMemoryLogWrite("Share", $"{PlcMessage.AddrName} = {obj.ToString()}");
                    return stringBuilder;
                }

                Trx trx = m_ShareTrx.hashShareTrx[key];
                ShareAddLog(trx);
                foreach (string key2 in trx.WriteItem.Keys)
                {
                    PlcAddr addr = m_ShareTrx.hashShareKeyToShareData[key2];
                    m_ShareTrx.ShareMem.SetBit(addr, trx.WriteItem[key2]);
                }

                return stringBuilder;
            }
            catch
            {
                return new StringBuilder();
            }
        }

        public StringBuilder SimulatorLogWrite(PlcAddr PlcMessage)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                string text = GenerateCurrentTime();
                object obj = PlcTypeSwitch(PlcMessage);
                string text2 = PlcMessage.Addr.ToString();
                if (PlcMessage.ValueType == PlcValueType.BIT)
                {
                    text2 = $"{PlcMessage.Addr}.{PlcMessage.Bit}";
                }

                stringBuilder.AppendLine($"{text} : [{text2}]{PlcMessage.AddrName} = {obj.ToString()}");
                WriteMemoryLogWrite("Share", $"[{text2}]{PlcMessage.AddrName} = {obj.ToString()}");
                return stringBuilder;
            }
            catch
            {
                return new StringBuilder();
            }
        }

        public StringBuilder StructLogWrite(string sStructName, List<PlcAddr> listPlcMessage)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                string text = GenerateCurrentTime();
                stringBuilder.AppendLine(sStructName);
                foreach (PlcAddr item in listPlcMessage)
                {
                    object obj = PlcTypeSwitch(item);
                    stringBuilder.AppendLine($"{text} : [{item.Addr}]{item.AddrName} = {obj.ToString()}");
                    string arg = item.Addr.ToString();
                    if (item.ValueType == PlcValueType.BIT)
                    {
                        arg = $"{item.Addr}.{item.Bit}";
                    }

                    WriteMemoryLogWrite("Share", $"[{arg}]{item.AddrName} = {obj.ToString()}");
                }

                return stringBuilder;
            }
            catch
            {
                return new StringBuilder();
            }
        }

        private StringBuilder ShareAddLog(Trx trx)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string text = GenerateCurrentTime();
            stringBuilder.AppendLine($"{text} : {trx.sTrxId}");
            WriteMemoryLogWrite("Share", trx.sTrxId);
            PlcAddr plcAddr = m_ShareTrx.hashShareKeyToShareData[trx.sTriggerId];
            stringBuilder.AppendLine($"{text} : [{plcAddr.Addr}] {trx.sTriggerId} = {trx.bTriggerValue.ToString()}");
            WriteMemoryLogWrite("Share", $"{trx.sTriggerId} = {trx.bTriggerValue.ToString()}");
            foreach (string item in trx.ReadItem)
            {
                if (m_ShareTrx.hashShareKeyToShareData.ContainsKey(item))
                {
                    PlcAddr plcAddr2 = m_ShareTrx.hashShareKeyToShareData[item];
                    object obj = PlcTypeSwitch(plcAddr2);
                    stringBuilder.AppendLine($"{text} : [{plcAddr2.Addr}]{plcAddr2.AddrName} = {obj.ToString()}");
                    WriteMemoryLogWrite("Share", $"{plcAddr2.AddrName} = {obj.ToString()}");
                }

                if (!m_ShareTrx.hashShareStruct.ContainsKey(item))
                {
                    continue;
                }

                List<PlcAddr> list = m_ShareTrx.hashShareStruct[item];
                stringBuilder.AppendLine($"{text} : {item}");
                WriteMemoryLogWrite("Share", $"{item}");
                foreach (PlcAddr item2 in list)
                {
                    object obj2 = PlcTypeSwitch(item2);
                    stringBuilder.AppendLine($"{text} : [{item2.Addr}]{item2.AddrName} = {obj2.ToString()}");
                    WriteMemoryLogWrite("Share", $"{item2.AddrName} = {obj2.ToString()}");
                }
            }

            string[] array = new string[trx.WriteItem.Keys.Count];
            trx.WriteItem.Keys.CopyTo(array, 0);
            return stringBuilder;
        }

        private object PlcTypeSwitch(PlcAddr plcaddr)
        {
            switch (plcaddr.ValueType)
            {
                case PlcValueType.ASCII: return m_ShareTrx.ShareMem.GetAscii(plcaddr);
                case PlcValueType.BYTE: return m_ShareTrx.ShareMem.GetByte(plcaddr);
                case PlcValueType.SHORT: return m_ShareTrx.ShareMem.GetShort(plcaddr);
                case PlcValueType.INT32: return m_ShareTrx.ShareMem.GetInt32(plcaddr);
                case PlcValueType.INT64: return m_ShareTrx.ShareMem.GetInt64(plcaddr);
                case PlcValueType.USHORT: return m_ShareTrx.ShareMem.GetUShort(plcaddr);
                case PlcValueType.UINT32: return m_ShareTrx.ShareMem.GetUInt32(plcaddr);
                case PlcValueType.UINT64: return m_ShareTrx.ShareMem.GetUInt64(plcaddr);
                case PlcValueType.BIT: return m_ShareTrx.ShareMem.GetBit(plcaddr);
                case PlcValueType.FLOAT: return m_ShareTrx.ShareMem.GetFloat(plcaddr);
                default: return new object();
            }
        }

        private void WriteMemoryLogWrite(string sKey, string sMessage)
        {
            LogMessage logMessage = GetLogMessage(sMessage);
            PrepareMessageForHandling(sKey, logMessage);
        }

        private LogMessage GetLogMessage(string sMessage)
        {
            return new LogMessage
            {
                Type = LogMessageType.Data,
                Text = sMessage,
                Time = DateTime.Now
            };
        }

        private static string GenerateCurrentTime()
        {
            return string.Format("[{0:yyyy}-{0:MM}-{0:dd}] [{0:HH}:{0:mm}:{0:ss}.{0:fff}]", DateTime.Now);
        }

        protected void PrepareMessageForHandling(string sKey, LogMessage message)
        {
            if (sKey == "Share")
            {
                lock (m_syncShareObject)
                {
                    m_queueShareMessages.Enqueue(message);
                    m_queueShareMessagesFileSaveCache.Enqueue(message);
                    Monitor.Pulse(m_syncShareObject);
                }
            }
            else
            {
                lock (m_syncMesObject)
                {
                    m_queueMesMessages.Enqueue(message);
                    m_queueMesMessagesFileSaveCache.Enqueue(message);
                    Monitor.Pulse(m_syncMesObject);
                }
            }
        }

        private void OnLogShareMessageProcessorThread()
        {
            while (true)
            {
                LogMessage logMessage = null;
                lock (m_syncShareObject)
                {
                    if (!m_ShareTrx.bRun && m_queueShareMessages.Count == 0)
                    {
                        return;
                    }

                    if (m_queueShareMessages.Count > 0)
                    {
                        logMessage = m_queueShareMessages.Dequeue();
                    }
                    else
                    {
                        Monitor.Wait(m_syncShareObject);
                    }
                }

                if (logMessage != null)
                {
                    HandleLogMessage(logMessage);
                }
            }
        }

        private void OnLogMesMessageProcessorThread()
        {
            while (true)
            {
                LogMessage logMessage = null;
                lock (m_syncMesObject)
                {
                    if (!m_ShareTrx.bRun && m_queueMesMessages.Count == 0)
                    {
                        return;
                    }

                    if (m_queueMesMessages.Count > 0)
                    {
                        logMessage = m_queueMesMessages.Dequeue();
                    }
                    else
                    {
                        Monitor.Wait(m_syncMesObject);
                    }
                }

                if (logMessage != null)
                {
                    HandleMesLogMessage(logMessage);
                }
            }
        }

        private void HandleLogMessage(LogMessage message)
        {
            IDictionaryEnumerator dictionaryEnumerator = m_hashLeechers.GetEnumerator();
            while (dictionaryEnumerator.MoveNext())
            {
                IPlcDriverLogLeecher plcDriverLogLeecher = (IPlcDriverLogLeecher)dictionaryEnumerator.Key;
                LogMessageType logMessageType = (LogMessageType)dictionaryEnumerator.Value;
                if ((message.Type & logMessageType) != 0)
                {
                    try
                    {
                        plcDriverLogLeecher.HandleLogMessage(message);
                    }
                    catch
                    {
                        _ = $"Exception has been thrown by [{plcDriverLogLeecher.PlcLogLeecherId}] within [Logger.HandleLogMessage]";
                    }
                }
            }
        }

        private void HandleMesLogMessage(LogMessage message)
        {
            IDictionaryEnumerator dictionaryEnumerator = m_hashMesLeechers.GetEnumerator();
            while (dictionaryEnumerator.MoveNext())
            {
                IPlcDriverLogLeecher plcDriverLogLeecher = (IPlcDriverLogLeecher)dictionaryEnumerator.Key;
                LogMessageType logMessageType = (LogMessageType)dictionaryEnumerator.Value;
                if ((message.Type & logMessageType) != 0)
                {
                    try
                    {
                        plcDriverLogLeecher.HandleLogMessage(message);
                    }
                    catch
                    {
                        _ = $"Exception has been thrown by [{plcDriverLogLeecher.PlcLogLeecherId}] within [Logger.HandleLogMessage]";
                    }
                }
            }
        }

        private void OnSaveFileShareMemoryTimerElapsed(object state)
        {
            try
            {
                LogMessage[] array = null;
                lock (m_syncShareObject)
                {
                    if (!m_ShareTrx.bRun)
                    {
                        return;
                    }

                    array = new LogMessage[m_queueShareMessagesFileSaveCache.Count];
                    m_queueShareMessagesFileSaveCache.CopyTo(array, 0);
                    m_queueShareMessagesFileSaveCache.Clear();
                }

                SaveLogMessagesToFile("ShareMemory", array);
            }
            catch
            {
                LogMessage[] array2 = null;
                lock (m_syncShareObject)
                {
                    if (!m_ShareTrx.bRun)
                    {
                        return;
                    }

                    array2 = new LogMessage[m_queueShareMessagesFileSaveCache.Count];
                    m_queueShareMessagesFileSaveCache.CopyTo(array2, 0);
                    m_queueShareMessagesFileSaveCache.Clear();
                }

                SaveLogMessagesToFile("ShareMemory", array2);
            }
        }

        private void OnSaveFileMesMemoryTimerElapsed(object state)
        {
            try
            {
                LogMessage[] array = null;
                lock (m_syncShareObject)
                {
                    if (!m_ShareTrx.bRun)
                    {
                        return;
                    }

                    array = new LogMessage[m_queueMesMessagesFileSaveCache.Count];
                    m_queueMesMessagesFileSaveCache.CopyTo(array, 0);
                    m_queueMesMessagesFileSaveCache.Clear();
                }

                SaveLogMessagesToFile("Mes", array);
            }
            catch
            {
                LogMessage[] array2 = null;
                lock (m_syncShareObject)
                {
                    if (!m_ShareTrx.bRun)
                    {
                        return;
                    }

                    array2 = new LogMessage[m_queueMesMessagesFileSaveCache.Count];
                    m_queueMesMessagesFileSaveCache.CopyTo(array2, 0);
                    m_queueMesMessagesFileSaveCache.Clear();
                }

                SaveLogMessagesToFile("Mes", array2);
            }
        }

        protected void SaveLogMessagesToFile(string sKey, LogMessage[] listMessages)
        {
            try
            {
                if (listMessages == null || listMessages.Length == 0)
                {
                    return;
                }
                
                StreamWriter streamWriter = File.AppendText(GetCurrentLogFile(sKey));
                int i = 0;
                for (int num = listMessages.Length; i < num; i++)
                {
                    LogMessage logMessage = listMessages[i];
                    string arg = $"{logMessage.Time.Hour:d2}:{logMessage.Time.Minute:d2}:{logMessage.Time.Second:d2}.{logMessage.Time.Millisecond:d3}";
                    string value = $"[{arg}]  {logMessage.Text}";
                    streamWriter.WriteLine(value);
                }
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch
            {
            }
            finally
            {
                RechargeTimer(sKey);
            }
        }

        private void RechargeTimer(string sKey)
        {
            if (sKey != "Mes")
            {
                m_timerSaveFile.Change(5000, -1);
            }
            else
            {
                m_timerMesSaveFile.Change(5000, -1);
            }
        }

        private string GetCurrentLogFile(string sKey)
        {
            DateTime now = DateTime.Now;
            string empty = string.Empty;
            if (m_bSimulator)
            {
                string text = $"Simulator{sKey}";
                empty = string.Format("{0}\\{1}\\{2}_{3:00}_{4:00}", LogPath, text, now.Year, now.Month, now.Day);
            }
            else
            {
                empty = string.Format("{0}\\{1}\\{2}_{3:00}_{4:00}", LogPath, sKey, now.Year, now.Month, now.Day);
            }

            if (!Directory.Exists(empty))
            {
                Directory.CreateDirectory(empty);
            }

            return $"{empty}\\{now.Hour:00}.txt";
        }
    }
}
#if false // 디컴파일 로그
캐시의 '27'개 항목
------------------
확인: 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\mscorlib.dll'
------------------
확인: 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.dll'
------------------
확인: 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
단일 어셈블리를 찾았습니다. 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
로드 위치: 'C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareMemory.dll'
------------------
확인: 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.Xml.dll'
#endif
