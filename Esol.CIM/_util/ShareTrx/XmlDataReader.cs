﻿#region 어셈블리 Esol.ShareTrx, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareTrx.dll
// Decompiled with ICSharpCode.Decompiler 7.1.0.6543
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.XPath;

namespace Esol.ShareTrxControl
{
    public class XmlDataReader : XmlText
    {
        public delegate void XmlDataChangeEventHandler();

        public const string DEF_INNER_TEXT = "InnerText";

        private object m_syncObject;

        private string m_sXmlDataReadFilePath;

        private bool m_bFileMapping;

        private Timer m_timefw;

        private DateTime m_timeLastWrite;

        private FileStream m_fs;

        private XPathDocument m_xPath;

        public event XmlDataChangeEventHandler XmlDataChanged;

        public XmlDataReader(string sPath, bool bFileMapping)
            : base(sPath)
        {
            m_fs = base.FileStream;
            m_bFileMapping = bFileMapping;
            m_syncObject = new object();
            m_sXmlDataReadFilePath = sPath;
            m_xPath = new XPathDocument(base.FileStream);
            InitializeFileWatcher(bFileMapping, NotifyFilters.LastWrite);
            this.XmlDataChanged = OnFileReload;
            if (!File.Exists(m_sXmlDataReadFilePath))
            {
                throw new Exception("파일이 없습니다.");
            }
        }

        private void InitializeFileWatcher(bool bFileMapping, NotifyFilters watcherNotify)
        {
            if (bFileMapping)
            {
                _ = m_timeLastWrite.Ticks;
                m_timefw = new Timer(OnFileDataChanged);
                ScanTimer();
            }
        }

        private void OnFileDataChanged(object sender)
        {
            try
            {
                if (File.Exists(m_fs.Name))
                {
                    FileInfo fileInfo = new FileInfo(m_fs.Name);
                    if (m_timeLastWrite.Ticks == 0L)
                    {
                        m_timeLastWrite = fileInfo.LastWriteTime;
                    }

                    if (m_timeLastWrite != fileInfo.LastWriteTime && this.XmlDataChanged != null)
                    {
                        this.XmlDataChanged();
                        m_timeLastWrite = fileInfo.LastWriteTime;
                    }
                }

                ScanTimer();
            }
            catch
            {
            }
        }

        private void ScanTimer()
        {
            m_timefw.Change(500, -1);
        }

        private void OnFileReload()
        {
            m_xPath = new XPathDocument(m_fs.Name);
        }

        private string XmlReadValue(string sTotalKey, string sInterLockKey, string sInterLockValue, string sAttributeKey, string sAttributeValue)
        {
            lock (m_syncObject)
            {
                try
                {
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    if (xPathNodeIterator.Count < 1)
                    {
                        return string.Empty;
                    }

                    while (xPathNodeIterator.MoveNext())
                    {
                        if (string.IsNullOrEmpty(sInterLockKey) && string.IsNullOrEmpty(sInterLockValue) && string.IsNullOrEmpty(sAttributeKey) && string.IsNullOrEmpty(sAttributeValue))
                        {
                            return xPathNodeIterator.Current.Value;
                        }

                        if (!(xPathNodeIterator.Current.GetAttribute(sInterLockKey, "") == sInterLockValue))
                        {
                            continue;
                        }

                        xPathNodeIterator.Current.MoveToFirstChild();
                        string attribute = xPathNodeIterator.Current.GetAttribute(sAttributeKey, "");
                        if (string.IsNullOrEmpty(sAttributeKey) || string.IsNullOrEmpty(sAttributeValue) || attribute == sAttributeValue)
                        {
                            return xPathNodeIterator.Current.Value;
                        }

                        while (xPathNodeIterator.Current.MoveToNext())
                        {
                            attribute = xPathNodeIterator.Current.GetAttribute(sAttributeKey, "");
                            if (attribute == sAttributeValue)
                            {
                                return xPathNodeIterator.Current.Value;
                            }
                        }
                    }

                    return string.Empty;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        private string[] XmlReadArrayValue(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sReadAttributeId)
        {
            lock (m_syncObject)
            {
                try
                {
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    if (xPathNodeIterator.Count < 1)
                    {
                        return null;
                    }

                    while (xPathNodeIterator.MoveNext())
                    {
                        if (string.IsNullOrEmpty(sInterlockKey) && string.IsNullOrEmpty(sInterlockValue) && string.IsNullOrEmpty(sAttributeKey) && string.IsNullOrEmpty(sAttributeValue))
                        {
                            xPathNodeIterator.Current.MoveToFirstChild();
                            return GetAttributeArryString(xPathNodeIterator, sReadAttributeId);
                        }

                        string attribute = xPathNodeIterator.Current.GetAttribute(sInterlockKey, "");
                        if (!(sInterlockValue == attribute))
                        {
                            continue;
                        }

                        xPathNodeIterator.Current.MoveToFirstChild();
                        string attribute2 = xPathNodeIterator.Current.GetAttribute(sAttributeKey, "");
                        if (string.IsNullOrEmpty(sAttributeKey) || string.IsNullOrEmpty(sAttributeValue) || attribute2 == sAttributeValue)
                        {
                            return GetAttributeArryString(xPathNodeIterator, sReadAttributeId);
                        }

                        while (xPathNodeIterator.Current.MoveToNext())
                        {
                            attribute2 = xPathNodeIterator.Current.GetAttribute(sAttributeKey, "");
                            if (attribute2 == sAttributeValue)
                            {
                                return GetAttributeArryString(xPathNodeIterator, sReadAttributeId);
                            }
                        }
                    }

                    return null;
                }
                catch
                {
                    return null;
                }
            }
        }

        private string XmlReadAttributeValue(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string sReadAttributeKey)
        {
            lock (m_syncObject)
            {
                try
                {
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    if (xPathNodeIterator.Count < 1)
                    {
                        return string.Empty;
                    }

                    while (xPathNodeIterator.MoveNext())
                    {
                        if (string.IsNullOrEmpty(sInterlockKey) && string.IsNullOrEmpty(sInterlockValue) && string.IsNullOrEmpty(sAttributeKey) && string.IsNullOrEmpty(sInterlockValue))
                        {
                            return xPathNodeIterator.Current.GetAttribute(sReadAttributeKey, "");
                        }

                        if (!(xPathNodeIterator.Current.GetAttribute(sInterlockKey, "") == sInterlockValue))
                        {
                            continue;
                        }

                        xPathNodeIterator.Current.MoveToFirstChild();
                        if (xPathNodeIterator.Current.GetAttribute(sAttributeKey, "") == sAttributeValue || string.IsNullOrEmpty(sAttributeKey) || string.IsNullOrEmpty(sAttributeValue))
                        {
                            return xPathNodeIterator.Current.GetAttribute(sReadAttributeKey, "");
                        }

                        while (xPathNodeIterator.Current.MoveToNext())
                        {
                            if (xPathNodeIterator.Current.GetAttribute(sAttributeKey, "") == sAttributeValue)
                            {
                                return xPathNodeIterator.Current.GetAttribute(sReadAttributeKey, "");
                            }
                        }
                    }

                    return string.Empty;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        private List<string[]> XmlReadListValue(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributekey, string sAttributeValue, string sAttribute, string[] sReadAttributeKey)
        {
            lock (m_syncObject)
            {
                try
                {
                    List<string[]> list = new List<string[]>();
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    if (xPathNodeIterator.Count < 1)
                    {
                        return list;
                    }

                    while (xPathNodeIterator.MoveNext())
                    {
                        if (string.IsNullOrEmpty(sInterlockKey) && string.IsNullOrEmpty(sInterlockValue))
                        {
                            if (!xPathNodeIterator.Current.MoveToFirstChild())
                            {
                                return list;
                            }

                            list.Add(GetAttributeArryString(xPathNodeIterator, sReadAttributeKey));
                            while (xPathNodeIterator.Current.MoveToNext())
                            {
                                list.Add(GetAttributeArryString(xPathNodeIterator, sReadAttributeKey));
                            }

                            return list;
                        }

                        if (string.IsNullOrEmpty(sAttributekey) || string.IsNullOrEmpty(sAttributeValue))
                        {
                            if (xPathNodeIterator.Current.GetAttribute(sInterlockKey, "") == sInterlockValue)
                            {
                                xPathNodeIterator.Current.MoveToFirstChild();
                                list.Add(GetAttributeArryString(xPathNodeIterator, sReadAttributeKey));
                                while (xPathNodeIterator.Current.MoveToNext())
                                {
                                    list.Add(GetAttributeArryString(xPathNodeIterator, sReadAttributeKey));
                                }
                            }
                        }
                        else
                        {
                            if (!(xPathNodeIterator.Current.GetAttribute(sInterlockKey, "") == sInterlockValue))
                            {
                                continue;
                            }

                            xPathNodeIterator.Current.MoveToFirstChild();
                            if (sAttribute == xPathNodeIterator.Current.Name)
                            {
                                list.Add(GetAttributeArryString(xPathNodeIterator, sReadAttributeKey));
                            }

                            while (xPathNodeIterator.Current.MoveToNext())
                            {
                                if (sAttribute == xPathNodeIterator.Current.Name)
                                {
                                    list.Add(GetAttributeArryString(xPathNodeIterator, sReadAttributeKey));
                                }
                            }
                        }
                    }

                    return list;
                }
                catch
                {
                    return new List<string[]>();
                }
            }
        }

        private Dictionary<string, string[]> XmlReadDictionaryValue(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sReadAttributeKey)
        {
            lock (m_syncObject)
            {
                try
                {
                    Dictionary<string, string[]> dictionary = new Dictionary<string, string[]>();
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    while (xPathNodeIterator.MoveNext())
                    {
                        if (string.IsNullOrEmpty(sInterlockKey) && string.IsNullOrEmpty(sInterlockValue))
                        {
                            if (!xPathNodeIterator.Current.MoveToFirstChild())
                            {
                                return dictionary;
                            }

                            string[] attributeArryString = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                            dictionary.Add(attributeArryString[0], attributeArryString);
                            while (xPathNodeIterator.Current.MoveToNext())
                            {
                                attributeArryString = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                dictionary.Add(attributeArryString[0], attributeArryString);
                            }

                            return dictionary;
                        }

                        if (string.IsNullOrEmpty(sAttributeKey) || string.IsNullOrEmpty(sAttributeValue))
                        {
                            if (xPathNodeIterator.Current.GetAttribute(sInterlockKey, "") == sInterlockValue)
                            {
                                xPathNodeIterator.Current.MoveToFirstChild();
                                string[] attributeArryString2 = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                dictionary.Add(attributeArryString2[0], attributeArryString2);
                                while (xPathNodeIterator.Current.MoveToNext())
                                {
                                    attributeArryString2 = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                    dictionary.Add(attributeArryString2[0], attributeArryString2);
                                }

                                return dictionary;
                            }
                        }
                        else
                        {
                            if (!(xPathNodeIterator.Current.GetAttribute(sInterlockKey, "") == sInterlockValue))
                            {
                                continue;
                            }

                            xPathNodeIterator.Current.MoveToFirstChild();
                            if (xPathNodeIterator.Current.GetAttribute(sAttributeKey, "") == sAttributeValue)
                            {
                                xPathNodeIterator.Current.MoveToFirstChild();
                                string[] attributeArryString3 = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                dictionary.Add(attributeArryString3[0], attributeArryString3);
                                while (xPathNodeIterator.Current.MoveToNext())
                                {
                                    attributeArryString3 = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                    dictionary.Add(attributeArryString3[0], attributeArryString3);
                                }

                                continue;
                            }

                            while (xPathNodeIterator.Current.MoveToNext())
                            {
                                if (xPathNodeIterator.Current.GetAttribute(sAttributeKey, "") == sAttributeValue)
                                {
                                    xPathNodeIterator.Current.MoveToFirstChild();
                                    string[] attributeArryString3 = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                    dictionary.Add(attributeArryString3[0], attributeArryString3);
                                    while (xPathNodeIterator.Current.MoveToNext())
                                    {
                                        attributeArryString3 = GetAttributeArryString(xPathNodeIterator, sReadAttributeKey);
                                        dictionary.Add(attributeArryString3[0], attributeArryString3);
                                    }
                                }
                            }
                        }
                    }

                    return dictionary;
                }
                catch
                {
                    return new Dictionary<string, string[]>();
                }
            }
        }

        public string XmlRead(string sTotalKey)
        {
            return XmlReadValue(sTotalKey, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        public string XmlRead(string sTotalKey, string sInterLcokKey, string sInterLockValue)
        {
            return XmlReadValue(sTotalKey, sInterLcokKey, sInterLockValue, string.Empty, string.Empty);
        }

        public string XmlRead(string sTotalKey, string sInterLcokKey, string sInterLockValue, string sAttributeKey, string sAttributeValue)
        {
            return XmlReadValue(sTotalKey, sInterLcokKey, sInterLockValue, sAttributeKey, sAttributeValue);
        }

        public string[] XmlReadArray(string sTotalKey, string[] sReadAttributeId)
        {
            return XmlReadArrayValue(sTotalKey, string.Empty, string.Empty, string.Empty, string.Empty, sReadAttributeId);
        }

        public string[] XmlReadArray(string sTotalKey, string sInterlockKey, string sInterlockValue, string[] sReadAttributeId)
        {
            return XmlReadArrayValue(sTotalKey, sInterlockKey, sInterlockValue, string.Empty, string.Empty, sReadAttributeId);
        }

        public string[] XmlReadArray(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sReadAttributeId)
        {
            return XmlReadArrayValue(sTotalKey, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sReadAttributeId);
        }

        public string XmlReadAttribute(string sTotalKey, string sReadAttributeKey)
        {
            return XmlReadAttributeValue(sTotalKey, string.Empty, string.Empty, string.Empty, string.Empty, sReadAttributeKey);
        }

        public string XmlReadAttribute(string sTotalKey, string sInterlockKey, string sInterlockValue, string sReadAttributeKey)
        {
            return XmlReadAttributeValue(sTotalKey, sInterlockKey, sInterlockValue, string.Empty, string.Empty, sReadAttributeKey);
        }

        public string XmlReadAttribute(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string sReadAttributeKey)
        {
            return XmlReadAttributeValue(sTotalKey, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sReadAttributeKey);
        }

        public List<string[]> XmlReadList(string sTotalKey, string[] sReadAttributeKey)
        {
            return XmlReadListValue(sTotalKey, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, sReadAttributeKey);
        }

        public List<string[]> XmlReadList(string sTotalKey, string sInterlockKey, string sInterlockValue, string[] sReadAttributeKey)
        {
            return XmlReadListValue(sTotalKey, sInterlockKey, sInterlockValue, string.Empty, string.Empty, string.Empty, sReadAttributeKey);
        }

        public List<string[]> XmlReadList(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributekey, string sAttributeValue, string sAttribute, string[] sReadAttributeKey)
        {
            return XmlReadListValue(sTotalKey, sInterlockKey, sInterlockValue, sAttributekey, sAttributeValue, sAttribute, sReadAttributeKey);
        }

        public Dictionary<string, string[]> XmlReadDictionary(string sTotalKey, string[] sReadAttributeKey)
        {
            return XmlReadDictionaryValue(sTotalKey, string.Empty, string.Empty, string.Empty, string.Empty, sReadAttributeKey);
        }

        public Dictionary<string, string[]> XmlReadDictionary(string sTotalKey, string sInterlockKey, string sInterlockValue, string[] sReadAttributeKey)
        {
            return XmlReadDictionaryValue(sTotalKey, sInterlockKey, sInterlockValue, string.Empty, string.Empty, sReadAttributeKey);
        }

        public Dictionary<string, string[]> XmlReadDictionary(string sTotalKey, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sReadAttributeKey)
        {
            return XmlReadDictionaryValue(sTotalKey, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sReadAttributeKey);
        }

        private Dictionary<string, string> GetReadAttributekey(string[] sReadAttributeKey)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string key in sReadAttributeKey)
            {
                dictionary.Add(key, string.Empty);
            }

            dictionary.Add("InnerText", string.Empty);
            return dictionary;
        }

        private string[] GetAttributeArryString(XPathNodeIterator xPathNodeIterator, string[] sAttributeKey)
        {
            string[] array = new string[sAttributeKey.Length + 1];
            for (int i = 0; i < sAttributeKey.Length; i++)
            {
                array[i] = xPathNodeIterator.Current.GetAttribute(sAttributeKey[i], "");
            }

            array[sAttributeKey.Length] = xPathNodeIterator.Current.Value;
            return array;
        }

        public string[] ReadArryAttribute(string sTotalKey, string sAttributeKey)
        {
            lock (m_syncObject)
            {
                try
                {
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    List<string> list = new List<string>();
                    if (xPathNodeIterator.Count < 1)
                    {
                        return null;
                    }

                    while (xPathNodeIterator.MoveNext())
                    {
                        list.Add(xPathNodeIterator.Current.GetAttribute(sAttributeKey, ""));
                    }

                    string[] array = new string[list.Count];
                    list.CopyTo(array, 0);
                    return array;
                }
                catch
                {
                    return null;
                }
            }
        }

        public string ReadAttribute(string sTotalKey, string sAttributeKey)
        {
            lock (m_syncObject)
            {
                try
                {
                    XPathNodeIterator xPathNodeIterator = m_xPath.CreateNavigator().Select(sTotalKey);
                    string result = string.Empty;
                    if (xPathNodeIterator.Count < 1)
                    {
                        return string.Empty;
                    }

                    while (xPathNodeIterator.MoveNext())
                    {
                        result = xPathNodeIterator.Current.GetAttribute(sAttributeKey, "");
                    }

                    return result;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }
    }
}
#if false // 디컴파일 로그
캐시의 '27'개 항목
------------------
확인: 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\mscorlib.dll'
------------------
확인: 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.dll'
------------------
확인: 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
단일 어셈블리를 찾았습니다. 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
로드 위치: 'C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareMemory.dll'
------------------
확인: 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.Xml.dll'
#endif
