﻿#region 어셈블리 Esol.ShareTrx, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareTrx.dll
// Decompiled with ICSharpCode.Decompiler 7.1.0.6543
#endregion

using System.Collections.Generic;

namespace Esol.ShareTrxControl
{
    public class Trx
    {
        private string m_sTrxId;

        private string m_striggerId;

        private bool m_btriggerValue;

        private List<string> m_listReadItem;

        private Dictionary<string, bool> m_hashWriteItem;

        public string sTrxId
        {
            get
            {
                return m_sTrxId;
            }
            set
            {
                m_sTrxId = value;
            }
        }

        public string sTriggerId
        {
            get
            {
                return m_striggerId;
            }
            set
            {
                m_striggerId = value;
            }
        }

        public bool bTriggerValue
        {
            get
            {
                return m_btriggerValue;
            }
            set
            {
                m_btriggerValue = value;
            }
        }

        public List<string> ReadItem
        {
            get
            {
                return m_listReadItem;
            }
            set
            {
                m_listReadItem = value;
            }
        }

        public Dictionary<string, bool> WriteItem
        {
            get
            {
                return m_hashWriteItem;
            }
            set
            {
                m_hashWriteItem = value;
            }
        }

        public object Clone()
        {
            return this;
        }
    }
}
#if false // 디컴파일 로그
캐시의 '27'개 항목
------------------
확인: 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\mscorlib.dll'
------------------
확인: 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.dll'
------------------
확인: 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
단일 어셈블리를 찾았습니다. 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
로드 위치: 'C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareMemory.dll'
------------------
확인: 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.Xml.dll'
#endif
