﻿#region 어셈블리 Esol.ShareTrx, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareTrx.dll
// Decompiled with ICSharpCode.Decompiler 7.1.0.6543
#endregion

using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Esol.ShareTrxControl
{
    public class XmlDataWriter : XmlText
    {
        private const string DEF_INNER_TEXT = "InnerText";

        private object m_syncObject;

        private string m_sXmlDataWriteFilePath;

        private bool m_bOverWrite;

        public bool bOverWrite
        {
            get
            {
                return m_bOverWrite;
            }
            set
            {
                m_bOverWrite = value;
            }
        }

        public XmlDataWriter(string sPath)
        {
            m_sXmlDataWriteFilePath = sPath;
            m_syncObject = new object();
        }

        private void WriteXmlData(XmlDocument xmldoc)
        {
            XmlTextWriter xmlTextWriter = new XmlTextWriter(m_sXmlDataWriteFilePath, null);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmldoc.WriteContentTo(xmlTextWriter);
            xmlTextWriter.Close();
        }

        private bool XmlWriteValue(string sParentElement, string sWriteElement, string sValue, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, bool bOverWrite)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    if (elementsByTagName.Count < 1 || elementsByTagName == null)
                    {
                        return false;
                    }

                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    if (xmlNode == null)
                    {
                        return false;
                    }

                    if (bOverWrite)
                    {
                        XmlNodeList childNodes = xmlNode.ChildNodes;
                        XmlNode xmlNode2 = ((string.IsNullOrEmpty(sAttributeKey) || string.IsNullOrEmpty(sAttributeValue)) ? CompareInnerName(childNodes, sWriteElement, "", bAttribute: false) : CompareInnerName(childNodes, sAttributeKey, sAttributeValue, bAttribute: true));
                        if (xmlNode2 != null)
                        {
                            xmlNode2.InnerText = sValue;
                            WriteXmlData(xmlDocument);
                            return true;
                        }
                    }

                    XmlElement newChild = CreateParameter(xmlDocument, sWriteElement, sValue);
                    xmlNode.AppendChild(newChild);
                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private bool XmlAttributeWriteValue(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string sWriteAttributeKey, string sWriteAttributeValue, string sValue)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    if (xmlNode == null)
                    {
                        return false;
                    }

                    XmlNodeList childNodes = xmlNode.ChildNodes;
                    if (bOverWrite && string.IsNullOrEmpty(sAttributeKey))
                    {
                        sAttributeKey = sWriteAttributeKey;
                        sAttributeValue = sWriteAttributeValue;
                    }

                    XmlNode xmlNode2 = CompareInnerName(childNodes, sAttributeKey, sAttributeValue, bAttribute: true);
                    if (xmlNode2 != null)
                    {
                        XmlElement xmlElement = (XmlElement)xmlNode2;
                        xmlElement.SetAttribute(sWriteAttributeKey, sWriteAttributeValue);
                        if (!string.IsNullOrEmpty(sValue))
                        {
                            xmlElement.InnerText = sValue;
                        }

                        xmlNode.ReplaceChild(xmlElement, xmlNode2);
                    }
                    else
                    {
                        XmlElement xmlElement2 = xmlDocument.CreateElement(sWriteElement);
                        xmlElement2.SetAttribute(sWriteAttributeKey, sWriteAttributeValue);
                        if (!string.IsNullOrEmpty(sValue))
                        {
                            xmlElement2.InnerText = sValue;
                        }

                        xmlNode.AppendChild(xmlElement2);
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private bool XmlAttributeWriteValue(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue, bool bHasValue)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    if (elementsByTagName.Count < 1 || elementsByTagName == null || sWriteAttributeKey.Length != sWriteAttributeValue.Length)
                    {
                        return false;
                    }

                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    if (xmlNode == null)
                    {
                        return false;
                    }

                    XmlNodeList childNodes = xmlNode.ChildNodes;
                    if (bOverWrite && string.IsNullOrEmpty(sAttributeKey))
                    {
                        sAttributeKey = sWriteAttributeKey[0];
                        sAttributeValue = sWriteAttributeValue[0];
                    }

                    XmlNode xmlNode2 = CompareInnerName(childNodes, sAttributeKey, sAttributeValue, bAttribute: true);
                    if (xmlNode2 != null)
                    {
                        XmlElement newChild = SetAttributeToElement((XmlElement)xmlNode2, sWriteAttributeKey, sWriteAttributeValue, bHasValue);
                        xmlNode.ReplaceChild(newChild, xmlNode2);
                    }
                    else
                    {
                        XmlElement newChild2 = SetAttributeToElement(xmlDocument.CreateElement(sWriteElement), sWriteAttributeKey, sWriteAttributeValue, bHasValue);
                        xmlNode.AppendChild(newChild2);
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private bool XmlAttributeWriteValue(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string[]> hashValue, string sFixedId)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    if (elementsByTagName.Count < 1 || elementsByTagName == null)
                    {
                        return false;
                    }

                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    if (xmlNode == null)
                    {
                        return false;
                    }

                    XmlNode xmlNode2 = CompareInnerName(xmlNode.ChildNodes, sAttributeKey, sAttributeValue, bAttribute: true);
                    string[] array = new string[hashValue.Count];
                    hashValue.Keys.CopyTo(array, 0);
                    string[] array2 = array;
                    foreach (string text in array2)
                    {
                        XmlElement xmlElement = ((xmlNode2 != null) ? ((XmlElement)xmlNode2) : xmlDocument.CreateElement(sWriteElement));
                        for (int j = 0; j < hashValue[text].Length - 1; j++)
                        {
                            xmlElement.SetAttribute(string.IsNullOrEmpty(sFixedId) ? text : sFixedId, hashValue[text][j]);
                        }

                        if (!string.IsNullOrEmpty(hashValue[text][hashValue[text].Length - 1]))
                        {
                            xmlElement.InnerText = hashValue[text][hashValue[text].Length - 1];
                        }

                        if (xmlNode2 != null)
                        {
                            xmlNode.ReplaceChild(xmlElement, xmlNode2);
                        }
                        else
                        {
                            xmlNode.AppendChild(xmlElement);
                        }
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private bool XmlAttributeWriteValue(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string> hashAttribute, bool bHasValue)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    if (elementsByTagName.Count < 1 || elementsByTagName == null)
                    {
                        return false;
                    }

                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    if (xmlNode == null)
                    {
                        return false;
                    }

                    XmlNode xmlNode2 = CompareInnerName(xmlNode.ChildNodes, sAttributeKey, sAttributeValue, bAttribute: true);
                    if (xmlNode2 != null)
                    {
                        XmlElement newChild = SetAttributeToElement((XmlElement)xmlNode2, hashAttribute, bHasValue);
                        xmlNode.ReplaceChild(newChild, xmlNode2);
                    }
                    else
                    {
                        XmlElement newChild2 = SetAttributeToElement(xmlDocument.CreateElement(sWriteElement), hashAttribute, bHasValue);
                        xmlNode.AppendChild(newChild2);
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool XmlWrite(string sParentElement, string sWriteElement, string sValue)
        {
            return XmlWriteValue(sParentElement, sWriteElement, sValue, null, null, null, null, bOverWrite);
        }

        public bool XmlWrite(string sParnentElement, string sInterlockKey, string sInterlockValue, string sWriteElement, string sValue)
        {
            return XmlWriteValue(sParnentElement, sWriteElement, sValue, sInterlockKey, sInterlockValue, null, null, bOverWrite);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sWriteAttributeKey, string sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, string.Empty, string.Empty, string.Empty, string.Empty, sWriteAttributeKey, sWriteAttributeValue, string.Empty);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sInterLockKey, string sInterLcokValue, string sWriteAttributeKey, string sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterLockKey, sInterLcokValue, string.Empty, string.Empty, sWriteAttributeKey, sWriteAttributeValue, string.Empty);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sAttributeKey, string sAttributeValue, string sWriteAttributeKey, string sWriteAttributeValue, bool bAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, sAttributeKey, sAttributeValue, sWriteAttributeKey, sWriteAttributeValue, string.Empty);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string sWtireAttributeKey, string sWirteAttirbuteValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sWtireAttributeKey, sWirteAttirbuteValue, null);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string[] sWriteAttributeKey, string[] sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, null, null, sWriteAttributeKey, sWriteAttributeValue, bHasValue: false);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, sWriteAttributeKey, sWriteAttributeValue, bHasValue: false);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sAttributeKey, string sAttributeValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue, bool bAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, sAttributeKey, sAttributeValue, sWriteAttributeKey, sWriteAttributeValue, bHasValue: false);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sWriteAttributeKey, sWriteAttributeValue, bHasValue: false);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, Dictionary<string, string> hashAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, hashAttribute, bHasValue: false);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sAttributeKey, string sAttributeValue, Dictionary<string, string> hashAttribute, bool bAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, sAttributeKey, sAttributeValue, hashAttribute, bHasValue: false);
        }

        public bool XmlAttributeWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string> hashAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, hashAttribute, bHasValue: false);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sWriteAttributeKey, string sWriteAttributeValue, string sValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, null, null, sWriteAttributeKey, sWriteAttributeValue, sValue);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sWriteAttributeKey, string sWriteAttributeValue, string sValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, sWriteAttributeKey, sWriteAttributeValue, sValue);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string sWriteAttributeKey, string sWriteAttributeValue, string sValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sWriteAttributeKey, sWriteAttributeValue, sValue);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string[] sWriteAttributeKey, string[] sWriteAttributeValue, string sValue)
        {
            string[] array = new string[sWriteAttributeKey.Length + 1];
            string[] array2 = new string[sWriteAttributeValue.Length + 1];
            array[sWriteAttributeKey.Length] = "InnerText";
            array2[sWriteAttributeValue.Length] = sValue;
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, null, null, array, array2, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string[] sWriteAttributeKey, string[] sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, null, null, sWriteAttributeKey, sWriteAttributeValue, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue, string sValue)
        {
            string[] array = new string[sWriteAttributeKey.Length + 1];
            string[] array2 = new string[sWriteAttributeValue.Length + 1];
            array[sWriteAttributeKey.Length] = "InnerText";
            array2[sWriteAttributeValue.Length] = sValue;
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, array, array2, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, sWriteAttributeKey, sWriteAttributeValue, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue, string sValue)
        {
            string[] array = new string[sWriteAttributeKey.Length + 1];
            string[] array2 = new string[sWriteAttributeValue.Length + 1];
            array[sWriteAttributeKey.Length] = "InnerText";
            array2[sWriteAttributeValue.Length] = sValue;
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sWriteAttributeKey, sWriteAttributeValue, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, string[] sWriteAttributeKey, string[] sWriteAttributeValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, sWriteAttributeKey, sWriteAttributeValue, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, Dictionary<string, string> hashAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, null, null, hashAttribute, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, Dictionary<string, string> hashAttribute, string sValue)
        {
            hashAttribute.Add("InnerText", sValue);
            return XmlAttributeWriteValue(sParentElement, sWriteElement, null, null, null, null, hashAttribute, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, Dictionary<string, string> hashAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, hashAttribute, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, Dictionary<string, string> hashAttribute, string sValue)
        {
            hashAttribute.Add("InnerText", sValue);
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, hashAttribute, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string> hashAttribute)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, hashAttribute, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string> hashAttribute, string sValue)
        {
            hashAttribute.Add("InnerText", sValue);
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, hashAttribute, bHasValue: true);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, Dictionary<string, string[]> hashValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, hashValue, null);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string[]> hashValue)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, hashValue, null);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, Dictionary<string, string[]> hashValue, string sFixedId)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, null, null, hashValue, sFixedId);
        }

        public bool XmlAttributeElementWrite(string sParentElement, string sWriteElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue, Dictionary<string, string[]> hashValue, string sFixedId)
        {
            return XmlAttributeWriteValue(sParentElement, sWriteElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue, hashValue, sFixedId);
        }

        public bool XmlElementDelete(string sParentElement)
        {
            return XmlElementDeleteValue(sParentElement, null, null, null, null);
        }

        public bool XmlElementDelete(string sParentElement, string sInterlockKey, string sInterlockValue)
        {
            return XmlElementDeleteValue(sParentElement, sInterlockKey, sInterlockValue, null, null);
        }

        public bool XmlElementDelete(string sParentElement, string sAttributeKey, string sAttributeValue, bool bAttribute)
        {
            return XmlElementDeleteValue(sParentElement, null, null, sAttributeKey, sAttributeValue);
        }

        public bool XmlElementDelete(string sParentElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue)
        {
            return XmlElementDeleteValue(sParentElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue);
        }

        public bool XmlAllElementDelete(string sParentElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue)
        {
            return XmlElementDeleteValue(sParentElement, sInterlockKey, sInterlockValue, sAttributeKey, sAttributeValue);
        }

        public bool XmlElementDelete(string sParentElement, string sInnerName)
        {
            return XmlElementDeleteValue(sParentElement, null, null, sInnerName);
        }

        public bool XmlElementDelete(string sParentElement, string sInterlockKey, string sInterlockValue, string sInnerName)
        {
            return XmlElementDeleteValue(sParentElement, sInterlockKey, sInterlockValue, sInnerName);
        }

        private bool XmlElementDeleteValue(string sParentElement, string sInterlockKey, string sInterlockValue, string sAttributeKey, string sAttributeValue)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
                    {
                        XmlNode xmlNode2 = xmlNode.ChildNodes[i];
                        if (string.Compare(((XmlElement)xmlNode2).GetAttribute(sAttributeKey, ""), sAttributeValue) == 0)
                        {
                            xmlNode.RemoveChild(xmlNode2);
                            break;
                        }
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private bool XmlElementDeleteValue(string sParentElement, string sInterlockKey, string sInterlockValue, string sInnerName)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
                    {
                        XmlNode xmlNode2 = xmlNode.ChildNodes[i];
                        if (string.Compare(xmlNode2.Name, sInnerName) == 0)
                        {
                            xmlNode.RemoveChild(xmlNode2);
                            break;
                        }
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool XmlAllElementDelete(string sParentElement)
        {
            return XmlAllElementDeleteValue(sParentElement, null, null);
        }

        public bool XmlAllElementDelete(string sParentElement, string sInterlockKey, string sInterlockValue)
        {
            return XmlAllElementDeleteValue(sParentElement, sInterlockKey, sInterlockValue);
        }

        private bool XmlAllElementDeleteValue(string sParentElement, string sInterlockKey, string sInterlockValue)
        {
            lock (m_syncObject)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(m_sXmlDataWriteFilePath);
                    XmlNodeList elementsByTagName = xmlDocument.DocumentElement.GetElementsByTagName(sParentElement);
                    XmlNode xmlNode = ((string.IsNullOrEmpty(sInterlockKey) || string.IsNullOrEmpty(sInterlockValue)) ? elementsByTagName[0] : CompareInnerName(elementsByTagName, sInterlockKey, sInterlockValue, bAttribute: true));
                    int count = xmlNode.ChildNodes.Count;
                    for (int i = 0; i < count; i++)
                    {
                        xmlNode.RemoveChild(xmlNode.ChildNodes[0]);
                    }

                    WriteXmlData(xmlDocument);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private void XmlFileCreate(string sElement, string sParentElement, string sAttributeKey, string[] sAryAttributeValue)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(m_sXmlDataWriteFilePath);
                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                StreamWriter streamWriter = new StreamWriter(m_sXmlDataWriteFilePath);
                if (string.IsNullOrEmpty(sAttributeKey))
                {
                    streamWriter.WriteLine($"<{sElement}>");
                    streamWriter.WriteLine(string.Format("<{0}></{0}>", sParentElement));
                    streamWriter.WriteLine($"</{sElement}>");
                }
                else
                {
                    streamWriter.WriteLine($"<{sElement}>");
                    foreach (string arg in sAryAttributeValue)
                    {
                        streamWriter.WriteLine(string.Format("\t<{0} {1}=\"{2}\"></{0}>", sParentElement, sAttributeKey, arg));
                    }

                    streamWriter.WriteLine($"</{sElement}>");
                }

                streamWriter.Close();
                streamWriter.Dispose();
            }
            catch
            {
            }
        }

        private XmlNode CompareInnerName(XmlNodeList listNode, string sInnerName, string sInnerValue, bool bAttribute)
        {
            XmlNode result = null;
            if (bAttribute)
            {
                for (int i = 0; i < listNode.Count; i++)
                {
                    XmlAttributeCollection attributes = listNode[i].Attributes;
                    for (int j = 0; j < attributes.Count; j++)
                    {
                        if (attributes[j].Name == sInnerName && attributes[j].InnerText == sInnerValue)
                        {
                            result = listNode[i];
                            break;
                        }
                    }
                }
            }
            else
            {
                for (int k = 0; k < listNode.Count; k++)
                {
                    if (listNode[k].Name == sInnerName)
                    {
                        result = listNode[k];
                        break;
                    }
                }
            }

            return result;
        }

        private XmlElement CreateParameter(XmlDocument xdoc, string sKey, string sValue)
        {
            XmlElement xmlElement = xdoc.CreateElement(sKey);
            xmlElement.InnerText = sValue;
            return xmlElement;
        }

        private XmlElement CreateParameter(XmlDocument xdoc, string sKey, string sValue, Dictionary<string, string> hashAttribute)
        {
            XmlElement xmlElement = xdoc.CreateElement(sKey);
            xmlElement.InnerText = sValue;
            string[] array = new string[hashAttribute.Count];
            hashAttribute.Keys.CopyTo(array, 0);
            for (int i = 0; i < hashAttribute.Count; i++)
            {
                xmlElement.SetAttribute(array[i], hashAttribute[array[i]]);
            }

            return xmlElement;
        }

        private XmlElement SetAttributeToElement(XmlElement element, string[] sKey, string[] sValue, bool bHasValue)
        {
            int num = (bHasValue ? (sKey.Length - 1) : sKey.Length);
            for (int i = 0; i < num; i++)
            {
                element.SetAttribute(sKey[i], sValue[i]);
            }

            if (bHasValue)
            {
                element.InnerText = sValue[sKey.Length - 1];
            }

            return element;
        }

        private XmlElement SetAttributeToElement(XmlElement element, Dictionary<string, string> hashAttribute, bool bHasValue)
        {
            string[] array = new string[hashAttribute.Count];
            hashAttribute.Keys.CopyTo(array, 0);
            int num = (bHasValue ? (array.Length - 1) : array.Length);
            for (int i = 0; i < num; i++)
            {
                element.SetAttribute(array[i], hashAttribute[array[i]]);
            }

            if (bHasValue)
            {
                element.InnerText = hashAttribute[array[array.Length - 1]];
            }

            return element;
        }
    }
}
#if false // 디컴파일 로그
캐시의 '27'개 항목
------------------
확인: 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\mscorlib.dll'
------------------
확인: 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.dll'
------------------
확인: 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
단일 어셈블리를 찾았습니다. 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
로드 위치: 'C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareMemory.dll'
------------------
확인: 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.Xml.dll'
#endif
