﻿#region 어셈블리 Esol.ShareTrx, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareTrx.dll
// Decompiled with ICSharpCode.Decompiler 7.1.0.6543
#endregion

using System;
using System.Collections.Generic;
using Esol.ShareMemory;

namespace Esol.ShareTrxControl
{
    public class ShareTrx
    {
        private enum ReadPlcBitKeys
        {
            id,
            address,
            length,
            type
        }

        private enum ReadPlcWordKeys
        {
            id,
            address,
            type,
            size
        }

        private enum ReadTrxAttributeItem
        {
            id,
            triggerId,
            triggerValue
        }

        private enum ReadTrxChildItem
        {
            id,
            value
        }

        private const string DEF_DATA_BIT_ELEMENT_NAME = "PlcBits";

        private const string DEF_DATA_WORD_ELEMENT_NAME = "PlcWords";

        private const string DEF_DATA_STRUCT_ELEMENT_NAME = "PlcStructs";

        private const string DEF_DATA_TRX_ELEMENT_NAME = "Transactions";

        private const string DEF_DATA_TRX_ATTRI_NAME = "Transaction";

        private const string DEF_DEVICE_NAME = "Device";

        private const string DEF_ATTRIBUTE_KEY = "code";

        private const string DEF_TRX_ATTRIBUTE_KEY = "id";

        private Dictionary<string, List<string>> m_hashPlcTypeToPlcKey;

        private Dictionary<string, PlcAddr> m_hashPlcKeyToPlcData;

        private Dictionary<string, List<PlcAddr>> m_hashPlcStructAddr;

        private Dictionary<string, Trx> m_hashPlcTrx;

        private List<string> m_hashPlcAddressOverlep;

        private VirtualMemory m_ShareMem;

        private Logger m_Logger;

        private bool m_bRun;

        public Dictionary<string, List<string>> hashShareTypeToShareKey => m_hashPlcTypeToPlcKey;

        public Dictionary<string, PlcAddr> hashShareKeyToShareData => m_hashPlcKeyToPlcData;

        public Dictionary<string, List<PlcAddr>> hashShareStruct => m_hashPlcStructAddr;

        public Dictionary<string, Trx> hashShareTrx => m_hashPlcTrx;

        public VirtualMemory ShareMem => m_ShareMem;

        public bool bRun
        {
            get
            {
                return m_bRun;
            }
            set
            {
                m_bRun = value;
            }
        }

        public ShareTrx(string path, VirtualMemory shareMoemoryControl)
        {
            m_ShareMem = shareMoemoryControl;
            m_hashPlcTypeToPlcKey = new Dictionary<string, List<string>>();
            m_hashPlcKeyToPlcData = new Dictionary<string, PlcAddr>();
            m_hashPlcStructAddr = new Dictionary<string, List<PlcAddr>>();
            m_hashPlcTrx = new Dictionary<string, Trx>();
            m_hashPlcAddressOverlep = new List<string>();
            m_Logger = new Logger();
            m_Logger.Initialize(path, this);
            m_bRun = true;
        }

        public void InitializeDataBit(string sConfigFilePath)
        {
            XmlDataReader xmlDataReader = new XmlDataReader(sConfigFilePath, bFileMapping: false);
            ReadXmlDataBit(xmlDataReader);
        }

        public void InitializeDataWord(string sConfigFilePath)
        {
            XmlDataReader xmlDataReader = new XmlDataReader(sConfigFilePath, bFileMapping: false);
            ReadXmlDataWord(xmlDataReader);
        }

        public void InitializeDataStruct(string sConfigFilePath)
        {
            XmlDataReader xmlDataReader = new XmlDataReader(sConfigFilePath, bFileMapping: false);
            ReadXmlDataStruct(xmlDataReader);
        }

        public void InitializeDataTrx(string sConfigFilePath)
        {
            XmlDataReader xmlDataReader = new XmlDataReader(sConfigFilePath, bFileMapping: false);
            ReadXmlDataTrx(xmlDataReader);
        }

        public void Stop()
        {
            m_Logger.OnStop();
        }

        private void ReadXmlDataBit(XmlDataReader xmlDataReader)
        {
            try
            {
                string sTotalKey = string.Format("{0}/{1}", "PlcBits", "Device");
                string[] array = xmlDataReader.ReadArryAttribute(sTotalKey, "code");
                string[] names = Enum.GetNames(typeof(ReadPlcBitKeys));
                string[] array2 = array;
                foreach (string text in array2)
                {
                    Dictionary<string, string[]> dictionary = xmlDataReader.XmlReadDictionary(sTotalKey, "code", text, names);
                    List<string> list = new List<string>();
                    foreach (string[] value in dictionary.Values)
                    {
                        PlcAddr plcAddr = new PlcAddr(ConvertPlcMemType(text), ConvertInt(text, value[1]), ConvertInt(text, value[2]), 1, PlcValueType.BIT, 81);
                        if (value[1].Split('.').Length == 2)
                        {
                            string[] array3 = value[1].Split('.');
                            ConvertInt(text, array3[1]);
                            plcAddr = new PlcAddr(ConvertPlcMemType(text), ConvertInt(text, array3[0]), ConvertInt(text, array3[1]));
                        }

                        plcAddr.SHARE = m_ShareMem;
                        plcAddr.AddrName = value[0];
                        string[] array4 = plcAddr.ToString().Split('-');
                        if (m_hashPlcAddressOverlep.Contains(array4[0]) || m_hashPlcKeyToPlcData.ContainsKey(value[0]))
                        {
                            throw new Exception("");
                        }

                        m_hashPlcAddressOverlep.Add(array4[0]);
                        if (plcAddr.AddrName.Contains("L1"))
                        {
                            m_ShareMem.SetBit(plcAddr, value: false);
                        }

                        plcAddr.vBit = m_ShareMem.GetBit(plcAddr);
                        m_hashPlcKeyToPlcData.Add(value[0], plcAddr);
                    }

                    foreach (string key in dictionary.Keys)
                    {
                        list.Add(key);
                    }

                    m_hashPlcTypeToPlcKey.Add(text, list);
                }
            }
            catch
            {
            }
        }

        private void ReadXmlDataWord(XmlDataReader xmlDataReader)
        {
            try
            {
                string sTotalKey = string.Format("{0}/{1}", "PlcWords", "Device");
                string[] array = xmlDataReader.ReadArryAttribute(sTotalKey, "code");
                string[] names = Enum.GetNames(typeof(ReadPlcWordKeys));
                string[] array2 = array;
                foreach (string text in array2)
                {
                    Dictionary<string, string[]> dictionary = xmlDataReader.XmlReadDictionary(sTotalKey, "code", text, names);
                    foreach (string[] value in dictionary.Values)
                    {
                        PlcAddr plcAddr = new PlcAddr(ConvertPlcMemType(text), ConvertInt(text, value[1]), 0, WordSize(text, value), GetPlcValueType(value[2]), 81);
                        plcAddr.SHARE = m_ShareMem;
                        plcAddr.AddrName = value[0];
                        string[] array3 = plcAddr.ToString().Split('-');
                        if (m_hashPlcAddressOverlep.Contains(array3[0]) || m_hashPlcKeyToPlcData.ContainsKey(value[0]))
                        {
                            throw new Exception("Overlap Address");
                        }

                        m_hashPlcAddressOverlep.Add(array3[0]);
                        m_hashPlcKeyToPlcData.Add(value[0], plcAddr);
                    }

                    List<string> list = new List<string>();
                    foreach (string key in dictionary.Keys)
                    {
                        list.Add(key);
                        m_hashPlcTypeToPlcKey[text].Add(key);
                    }
                }
            }
            catch
            {
            }
        }

        private void ReadXmlDataStruct(XmlDataReader xmlDataReader)
        {
            try
            {
                string sTotalKey = string.Format("{0}/{1}", "PlcStructs", "Device");
                string[] array = xmlDataReader.ReadArryAttribute(sTotalKey, "code");
                string[] names = Enum.GetNames(typeof(ReadPlcWordKeys));
                string[] array2 = array;
                foreach (string text in array2)
                {
                    Dictionary<string, string[]> dictionary = xmlDataReader.XmlReadDictionary(sTotalKey, "code", text, names);
                    foreach (string key in dictionary.Keys)
                    {
                        Dictionary<string, string[]> dictionary2 = xmlDataReader.XmlReadDictionary(sTotalKey, "code", text, "id", key, names);
                        List<PlcAddr> list = new List<PlcAddr>();
                        int num = 0;
                        int num2 = Convert.ToInt32(dictionary[key][1]);
                        foreach (string key2 in dictionary2.Keys)
                        {
                            dictionary2[key2][3] = WordSize(text, dictionary2[key2]).ToString();
                            PlcAddr plcAddr = new PlcAddr(ConvertPlcMemType(text), (ConvertInt(text, dictionary2[key2][3]) + num != 0) ? num2 : 0, 0, WordSize(text, dictionary2[key2]), GetPlcValueType(dictionary2[key2][2]), 81);
                            plcAddr.SHARE = m_ShareMem;
                            plcAddr.AddrName = key2;
                            string[] array3 = plcAddr.ToString().Split('-');
                            if (m_hashPlcAddressOverlep.Contains(array3[0]))
                            {
                                throw new Exception("Overlap Address");
                            }

                            m_hashPlcAddressOverlep.Add(array3[0]);
                            list.Add(plcAddr);
                            num2 += WordSize(text, dictionary2[key2]);
                            num++;
                        }

                        m_hashPlcStructAddr.Add(key, list);
                    }
                }
            }
            catch
            {
            }
        }

        private void ReadXmlDataTrx(XmlDataReader xmlDataReader)
        {
            try
            {
                string sTotalKey = string.Format("{0}/{1}", "Transactions", "Transaction");
                string[] array = xmlDataReader.ReadArryAttribute(sTotalKey, "id");
                string[] names = Enum.GetNames(typeof(ReadTrxAttributeItem));
                Dictionary<string, string[]> dictionary = xmlDataReader.XmlReadDictionary("Transactions", names);
                string[] names2 = Enum.GetNames(typeof(ReadTrxChildItem));
                string[] array2 = array;
                foreach (string text in array2)
                {
                    List<string[]> list = xmlDataReader.XmlReadList(sTotalKey, "id", text, "id", text, "Read", names2);
                    List<string[]> list2 = xmlDataReader.XmlReadList(sTotalKey, "id", text, "id", text, "Write", names2);
                    List<string> list3 = new List<string>();
                    Dictionary<string, bool> dictionary2 = new Dictionary<string, bool>();
                    foreach (string[] item in list)
                    {
                        list3.Add(item[0]);
                    }

                    foreach (string[] item2 in list2)
                    {
                        dictionary2.Add(item2[0], (item2[1] == "1") ? true : false);
                    }

                    Trx trx = new Trx();
                    trx.sTrxId = text;
                    trx.sTriggerId = dictionary[text][1];
                    trx.bTriggerValue = ((dictionary[text][2] == "1") ? true : false);
                    trx.ReadItem = list3;
                    trx.WriteItem = dictionary2;
                    string key = $"{dictionary[text][1]},{dictionary[text][2]}";
                    m_hashPlcTrx.Add(key, trx);
                }
            }
            catch
            {
            }
        }

        private PlcValueType GetPlcValueType(string sKey)
        {
            try
            {
                PlcValueType plcValueType = PlcValueType.ASCII;

                switch (sKey)
                {
                    case "A": return PlcValueType.ASCII;
                    case "I1": return PlcValueType.BYTE;
                    case "I2": return PlcValueType.SHORT;
                    case "I4": return PlcValueType.INT32;
                    case "I8": return PlcValueType.INT64;
                    case "U2": return PlcValueType.USHORT;
                    case "U4": return PlcValueType.UINT32;
                    case "U8": return PlcValueType.UINT64;
                    case "F": return PlcValueType.FLOAT;
                    case "D": return PlcValueType.DOUBLE;
                    default: return plcValueType;
                }
            }
            catch
            {
                return PlcValueType.ASCII;
            }
        }

        private int WordSize(string sKey, string[] sValue)
        {
            try
            {
                int num = 0;

                switch (sValue[2])
                {
                    case "A": return Convert.ToInt32(sValue[3]);
                    case "I1": return num = 1;
                    case "I2": return num = 2;
                    case "I4": return num = 4;
                    case "U1": return num = 1;
                    case "U2": return num = 2;
                    case "U4": return num = 4;
                    case "U8": return num = 8;
                    case "I8": return num = 8;
                    case "F": return num = 4;
                    case "D": return num = 8;
                    default: return num;
                }         
            }
            catch
            {
                return 0;
            }
        }

        private PlcMemType ConvertPlcMemType(string sKey)
        {
            try
            {
                PlcMemType result = PlcMemType.EM;
                switch (sKey)
                {
                    case "B":
                        result = PlcMemType.B;
                        break;
                    case "S":
                        result = PlcMemType.S;
                        break;
                    case "SB":
                        result = PlcMemType.SB;
                        break;
                    case "SW":
                        result = PlcMemType.SW;
                        break;
                    case "W":
                        result = PlcMemType.W;
                        break;
                }

                return result;
            }
            catch
            {
                return PlcMemType.EM;
            }
        }

        private int ConvertInt(string sType, string sValue)
        {
            try
            {
                switch (sType)
                {
                    case "W":
                    case "B":
                    case "SW":
                    case "SB":
                        return Convert.ToInt32(sValue, 16);
                    default:
                        return Convert.ToInt32(sValue);
                }
            }
            catch
            {
                return 0;
            }
        }
    }
}
#if false // 디컴파일 로그
캐시의 '27'개 항목
------------------
확인: 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\mscorlib.dll'
------------------
확인: 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.dll'
------------------
확인: 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
단일 어셈블리를 찾았습니다. 'Esol.ShareMemory, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'
로드 위치: 'C:\Users\user\Desktop\archive\Esol.CIM\Lib\Esol.ShareMemory.dll'
------------------
확인: 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
단일 어셈블리를 찾았습니다. 'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
로드 위치: 'C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1\System.Xml.dll'
#endif
