﻿#region Class Using
using System;
using System.Drawing;
using Esol.Components;
using System.IO;
using Esol.LogControl;
using GEM_XGem300Pro;
#endregion

namespace Esol.CIM
{
    public partial class FormControl : RoundChildForm
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private MainControl m_MainControl;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public MainControl MainControl
        {
            get
            {
                return m_MainControl;
            }
            set
            {
                m_MainControl = value;
            }
        }
        public XGem300ProNet Xgem
        {
            get
            {
                return m_MainControl.Xgem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public CimConfigurator CimConfigurator
        {
            get
            {
                return m_MainControl.CimConfigurator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return CimConfigurator.LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public CimMessageProcessor CimMessageProcessor
        {
            get
            {
                return MainControl.CimMessageProcessor;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public JobControl JobControl
        {
            get
            {
                return MainControl.JobControl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public RecipeControl RecipeControl
        {
            get
            {
                return m_MainControl.RecipeControl;
            }
        }
        #endregion

        #region Class initialization
        /// <summary>
        /// 
        /// </summary>
        public FormControl()
        {
            InitializeComponent();
        }
        #endregion

        #region Class Utility Methods
        /// <summary>
        /// 
        /// </summary>
        protected Color GetStateColor(STATE state)
        {
            switch (state)
            {
                case STATE.Done:
                    return Color.LightGray;

                case STATE.Running:
                    return Color.Peru;

                case STATE.Waiting:
                    return Color.YellowGreen;

                default:
                    return Color.YellowGreen;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void FileDeleteInFolder(string sPath, int iDays)
        {
            try
            {
                DirectoryInfo diLog = new DirectoryInfo(sPath);
                FileDeleteInDirectory(diLog, iDays);

                try
                {
                    DirectoryInfo[] diDateLog = diLog.GetDirectories();
                    foreach (DirectoryInfo diSub in diDateLog)
                    {
                        if (DirectoryDelete(diSub, iDays))
                            continue;

                        DirectoryInfo[] diMCCLog = diSub.GetDirectories();
                        foreach (DirectoryInfo diMCC in diMCCLog)
                        {
                            if (DirectoryDelete(diMCC, iDays))
                                continue;
                        }
                    }
                }
                catch { }
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        private void FileDeleteInDirectory(DirectoryInfo di, int iDays)
        {
            FileInfo[] fiLog = di.GetFiles();
            for (int j = 0; j < fiLog.Length; j++)
            {
                TimeSpan ts = DateTime.Now.Subtract(fiLog[j].CreationTime);

                if (iDays <= Math.Abs(ts.Days))
                    fiLog[j].Delete();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private bool DirectoryDelete(DirectoryInfo di, int iDays)
        {
            TimeSpan ts = DateTime.Now.Subtract(di.CreationTime);

            if (iDays <= Math.Abs(ts.Days))
            {
                di.Delete(true);
                return true;
            }
            else
            {
                FileDeleteInDirectory(di, iDays);
                return false;
            }
        }
        #endregion

        #region Class Public Methods
        /// <summary>
        /// 
        /// </summary>
        public new void Show()
        {
            this.Visible = true;

            base.Show();
        }
        /// <summary>
        /// 
        /// </summary>
        public new void Hide()
        {
            this.Visible = false;

            base.Hide();
        }
        #endregion
    }
}
