﻿#region Class Using
using System;
#endregion

namespace Esol.CIM
{
    /// <summary>
    /// 
    /// </summary>
    public enum ControlState
    {
        Offline,
        Local,
        Remote,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum Terminal
    {
        id,
        text,
        time,
        tick,
    }
    /// <summary>			
    /// 			
    /// </summary>			
    public enum LogType
    {
        ERROR,
        BUTTON,
        EVENT,
        ALARM,
        TERMINAL,
        ECID,
        XGEM,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum LoadingStep
    {
        INITIALIZE = 1,
        CREATE_FORM,
        CREATE_TITLE,
        SHAREMEM_DRIVER,
        HSMS_DRIVER,
        INITIAL_FORM,
        CONFIG,
        THREAD,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum STATE
    {
        Waiting = 1,
        Running,
        Done,
    }
    /// <summary>
    /// Processing state for sample(equipment dependent)
    /// </summary>
    public enum ProcessingState
    {
        None = -1,
        SystemPowerUp = 1,
        Empty,
        Idle,
        Standby,
        Processing,
        HoldProcessing
    }
    /// <summary>
    /// 
    /// </summary>
    [Flags]
    public enum LogMessageType
    {
        Info = 0x1,
        Data = 0x2,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum ConfigItems
    {
        Device_ID,
        IP_Adress,
        Port_No,
        Active_Mode,
        Link_Test_Interval,
        T3_TimeOut,
        T5_TimeOut,
        T6_TimeOut,
        T7_TimeOut,
        T8_TimeOut,
        ScanNo,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum SVID
    {
        key,
        plcChannelName,
        Type,
        Unit,
        Range,
        ModuleID,
        Point,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum Alarm
    {
        id,
        code,
        text,
        use,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum AlarmHistory
    {
        id,
        text,
        time,
        code,
        siSet,
        tick,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum PRJobState
    {
        Pj_NoState = -1,
        Pj_JobQueued,			//0
        Pj_SettingUp,			//1 
        Pj_WaintingForStart,	//2
        Pj_Processing,			//3
        Pj_ProcessComplete,		//4
        Pj_Reserved,			//5
        Pj_Pausing,				//6
        Pj_Paused,				//7
        Pj_Stopping,			//8
        Pj_Aborting,			//9
        Pj_Stopped,				//10
        Pj_Aborted,				//11
        Pj_JobCanceled,			//12
        Pj_JobActive,			//13
        Pj_NotPaused,			//14
        Pj_NotStopping,			//15
        Pj_NotAbroting,			//16
        Pj_JobComplete,			//17
    };

    public enum PortTransferState
    {
        OutOfService = 0,
        TransferBlocked = 1,
        ReadyToLoad = 2,
        ReadyToUnload = 3,
    };

    public enum EmAccessMode
    {
        NoState = -1,
        Manual = 0,
        Auto = 1,
    }
}
