﻿#region Class Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Esol.XmlControl;
using Esol.ShareMemory;
#endregion 

namespace Esol.CIM
{
    public class RffSummary
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_HEADER_NAME = "Recipe";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_ATTRIBUTE_KEY = "Parameters";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_READ_ATTRIBUTE_VALUE = "PARAM";
        /// <summary>
        /// 
        /// </summary>
        private readonly string DEF_INTERLOCK_KEY = "id";

        private readonly double X_Min_Standard = -19600; // 0 - LB X
        private readonly double X_Max_Standard = 154200 - 19600; // MASK WIDTH SIZE - LB X
        private readonly double Y_Min_Standard = -19600; // 0 - LB Y
        private readonly double Y_Max_Standard = 154200 - 19600; // MASK HEIGHT SIZE - LB Y
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private readonly string m_sProcessRecipePath;
        /// <summary>
        /// 
        /// </summary>
        private CimConfigurator m_CimConfigurator;
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public RffSummary(string sProcessRecipePath, CimConfigurator cimConfigurator)
        {
            m_sProcessRecipePath = sProcessRecipePath;
            m_CimConfigurator = cimConfigurator;
        }
        public ProcessRecipeData SummaryRecipeToData(string sRecipeFilePath, string sRecipeName, string sRffFilePath, string sLotId, string sDateTime, EmAccessMode mode)
        {
            try
            {
                // Daily Monitoring
                if (Path.GetFileNameWithoutExtension(sRffFilePath) == m_CimConfigurator.DailyMonitoringFlag.ToUpper() || sRecipeName == m_CimConfigurator.DailyMonitoringFlag.ToUpper())
                {
                    FileInfo ProcessRecipe = new FileInfo(m_CimConfigurator.DailyMonitoringRecipePath);

                    ProcessRecipeData prd = m_CimConfigurator.RecipeManager.ReadProcessRecipe(ProcessRecipe);
                    List<PlcAddr> listProcessRecipeStruct = m_CimConfigurator.HashShareStruct["L1_PROCESS_RECIPE_CMD_STRUCT"];
                    m_CimConfigurator.ShareMem.SetAscii(listProcessRecipeStruct[0], prd.RecipeName);
                    m_CimConfigurator.ShareMem.SetByte(listProcessRecipeStruct[1], 1);

                    m_CimConfigurator.ShareMem.SetBit(m_CimConfigurator.HashShareKeyToShareData["L1_B_RECIPE_CMD_COMMAND"], true);

                    return prd;
                }

                if (Path.GetExtension(sRffFilePath).ToUpper() == ".RFF")
                    return SummaryRecipeToRff(sRecipeFilePath, sRecipeName, sRffFilePath, sLotId, sDateTime, mode);
                else // .POS
                    return SummaryRecipeToPos(sRecipeFilePath, sRecipeName, sRffFilePath, sLotId, sDateTime, mode);
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] SummaryRecipeToData Code Error");
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private ProcessRecipeData SummaryRecipeToRff(string sRecipeFilePath, string sRecipeName, string sRffFilePath, string sLotId, string sDateTime, EmAccessMode mode)
        {
            try
            {
                bool bCheck = false;

                double dCOORDINATE_X = 0;
                double dCOORDINATE_Y = 0;

                string sRead = string.Empty;
                string sTempMaskId = string.Empty;
                 
                Dictionary<string, string> hashSubState = new Dictionary<string, string>();

                StreamReader sr = new StreamReader(sRffFilePath, Encoding.Default);

                while ((sRead = sr.ReadLine()) != null)
                {
                    string[] sArrayReadDatas = sRead.Split(' ');

                    switch (sArrayReadDatas[0])
                    {
                        case "WaferID":
                            sTempMaskId = sArrayReadDatas[1].Replace('"', ' ').Replace('/', ' ').Trim();
                            hashSubState.Add("SUBSTRATE_ID", sTempMaskId.Replace(';', ' '));
                            bCheck = true;
                            break;
                        case "SampleSize":
                            hashSubState.Add("SUBSTRATE_SIZE_X", sArrayReadDatas[2].Replace(';', ' '));
                            hashSubState.Add("SUBSTRATE_SIZE_Y", sArrayReadDatas[3].Replace(';', ' '));
                            break;
                    }
                    if (bCheck) break;
                }
                
                XmlDataReader xmlDataReader = new XmlDataReader(sRecipeFilePath, false);

                string sTotalKey = string.Format("{0}/{1}", DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY);

                Dictionary<string, string[]> hashReadData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, DEF_READ_ATTRIBUTE_VALUE, m_CimConfigurator.AttriButeId);

                string sProcessRecipeName = string.Empty;
                string sProcessRecipeFilePath = string.Empty;
                string tempRecipeName = string.Format("{0}.xml", sLotId);
                for (int _iPos = 1; ;_iPos++ )
                {
                    if (File.Exists(string.Format(@"{0}\{1}", m_sProcessRecipePath, tempRecipeName)))
                    {
                        tempRecipeName = string.Format("{0}_{1}.xml", sLotId, _iPos);
                    }
                    else
                    {
                        sProcessRecipeName = tempRecipeName;
                        sProcessRecipeFilePath = string.Format(@"{0}\{1}", m_sProcessRecipePath, sProcessRecipeName);   
                        break;
                    }
                }

                XmlDataWriter xmlDataWriter = new XmlDataWriter(sProcessRecipeFilePath, "Recipe");

                Dictionary<string, string> hashTime = new Dictionary<string, string>()
                {
                    { "Time",Common.GenerateGUITime(DateTime.Now) },
                    { "Version", "1"  },
                    { "Type", mode.ToString() }
                };

                if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Info", hashTime))
                {
                    m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Info)", sProcessRecipeFilePath);

                    return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = "Rff File Access Fail (Param : Info)" };
                }

                ProcessRecipeData prd = new ProcessRecipeData()
                {
                    RecipeTime = hashTime["Time"],
                    RecipeVersion = hashTime["Version"]
                };

                foreach (string sKey in hashSubState.Keys)
                    prd.ParameterNameToValue.Add(sKey, hashSubState[sKey]);

                if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Substrate", hashSubState))
                {
                    m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Substrate)", sProcessRecipeFilePath);
                    return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = "Rff File Access Fail (Param : Substrate)" };
                }

                List<string> _classLookUp = new List<string>();
                string sReadData = string.Empty;

                while ((sReadData = sr.ReadLine()) != null)
                {
                    string[] sArrayReadDatas = sReadData.Split(' ');

                    switch (sArrayReadDatas[0])
                    {
                        case "ClassLookup":
                            {
                                int _listCount = 0;
                                int.TryParse(sArrayReadDatas[1], out _listCount);

                                for (int iPos = 0; iPos < _listCount; iPos++)
                                {
                                    if ((sReadData = sr.ReadLine()) != null)
                                        _classLookUp.Add(sReadData.Split('\"')[1]);
                                }
                            }
                            break;
                        case "AlignmentPoints":
                            int iAlignmentCount = Convert.ToInt16(sArrayReadDatas[1]);

                            Dictionary<string, string> hashTempAlg = new Dictionary<string, string>()
                            {
                                { "ALIGNMENT_POINT_NUMBER", iAlignmentCount.ToString() }
                            };

                            //for (int i = 0; i < iAlignmentCount; i++)
                            //{
                            //    string[] sArratAlignment = sr.ReadLine().TrimStart().Split(' ');
                            //
                            //    hashTempAlg.Add(string.Format("ALIGNMENT_POINT_X_{0}", i + 1), sArratAlignment[2]);
                            //    hashTempAlg.Add(string.Format("ALIGNMENT_POINT_Y_{0}", i + 1), sArratAlignment[3]);
                            //    prd.ParameterNameToValue.Add(string.Format("ALIGNMENT_POINT_X_{0}", i + 1), sArratAlignment[2]);
                            //    prd.ParameterNameToValue.Add(string.Format("ALIGNMENT_POINT_Y_{0}", i + 1), sArratAlignment[3]);
                            //}

                            //Mask 90도 일때
                            hashTempAlg.Add("ALIGNMENT_POINT_X_1", "19600");
                            hashTempAlg.Add("ALIGNMENT_POINT_Y_1", "19600");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_1", "19600");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_1", "19600");

                            hashTempAlg.Add("ALIGNMENT_POINT_X_2", "19600");
                            hashTempAlg.Add("ALIGNMENT_POINT_Y_2", "133300");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_2", "19600");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_2", "133300");

                            hashTempAlg.Add("ALIGNMENT_POINT_X_3", "132800");
                            hashTempAlg.Add("ALIGNMENT_POINT_Y_3", "133300");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_3", "132800");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_3", "133300");

                            hashTempAlg.Add("ALIGNMENT_POINT_X_4", "132800");
                            hashTempAlg.Add("ALIGNMENT_POINT_Y_4", "19600");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_4", "132800");
                            prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_4", "19600");

                            //Alignment 정보 Check
                            if (hashTempAlg.Count == 0)
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] Aligment Point Value Error (Count 0)");
                                return new ProcessRecipeData() { RecipeTime = "201", RecipeVersion = "Aligment Point Value Error (Count 0)" };
                            }

                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Alignment", hashTempAlg))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Alignment)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = "Rff File Access Fail (Param : Alignment)" };
                            }

                            Dictionary<string, string> hashTemp = new Dictionary<string, string>();

                            Dictionary<string, string[]> hashReadCalibrationData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, "Calibration", m_CimConfigurator.AttriButeId);

                            foreach (string sKey in hashReadCalibrationData.Keys)
                            {
                                string[] sValue = hashReadCalibrationData[sKey];
                                hashTemp.Add(sValue[0], sValue[1]);
                                prd.ParameterNameToValue.Add(sValue[0], sValue[1]);
                            }
                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Calibration", hashTemp))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Calibration)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = "Rff File Access Fail (Param : Calibration)" };
                            }

                            //230202 ProcessRecipe 변경 사항 적용
                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Monitoring", m_CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].HashMonitoringNameToValue))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Monitoring)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = "Rff File Access Fail (Param : Monitoring)" };
                            }

                            hashTemp = new Dictionary<string, string>();

                            Dictionary<string, string[]> hashReadContaminationData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, "Contamination", m_CimConfigurator.AttriButeId);

                            foreach (string sKey in hashReadContaminationData.Keys)
                            {
                                string[] sValue = hashReadContaminationData[sKey];
                                hashTemp.Add(sValue[0], sValue[1]);
                                prd.ParameterNameToValue.Add(sValue[0], sValue[1]);
                            }
                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Contamination", hashTemp))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Contamination)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = "Rff File Access Fail (Param : Contamination)" };
                            }
                            break;
                        case "DefectRecordSpec":
                            int columnCount;
                            int.TryParse(sReadData.Split(' ').Where(x => x != string.Empty).ToArray()[1], out columnCount);
                            sr.ReadLine();//DefectList Text Pass
                            int iCount = 1;
                            Dictionary<string, string> hashTempMeasureMent = new Dictionary<string, string>();
                            while (true)
                            {
                                string sDefectData = sr.ReadLine();

                                //230717 jsOh 공백 다섯칸 아니어도 파싱 가능하도록 수정
                                //string[] sArrayDefects = sDefectData.TrimStart().Split(new string[] { "     " }, StringSplitOptions.None);
                                string[] sArrayDefects = sDefectData.Split(' ').Where(x => x != string.Empty).ToArray();

                                if (string.IsNullOrEmpty(sDefectData.FirstOrDefault().ToString()) || sArrayDefects.Length < columnCount)
                                {
                                    iCount--;
                                    break;
                                }


                                //if (sArrayDefects[0].Contains("00000"))
                                //    continue;


                                hashTempMeasureMent.Add(string.Format("MEASUREMENT_DEFECT_ID_{0}", iCount), sArrayDefects[0].ToString());
                                double dMeasuremnet_X = 0;
                                double dMeasuremnet_Y = 0;

                                if (!double.TryParse(sArrayDefects[1], out dMeasuremnet_X) || !double.TryParse(sArrayDefects[2], out dMeasuremnet_Y))
                                {
                                    m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] Defect Point Value Error");
                                    return new ProcessRecipeData() { RecipeTime = "202", RecipeVersion = "Defect Point Value Error" };
                                }

                                double.TryParse(sArrayDefects[1], out dMeasuremnet_X);
                                double.TryParse(sArrayDefects[2], out dMeasuremnet_Y);
                                hashTempMeasureMent.Add(string.Format("MEASUREMENT_REF_X_POS_{0}", iCount), dMeasuremnet_X.ToString());//(dMeasuremnet_X - dCOORDINATE_X).ToString());
                                hashTempMeasureMent.Add(string.Format("MEASUREMENT_REF_Y_POS_{0}", iCount), dMeasuremnet_Y.ToString());//(dMeasuremnet_Y - dCOORDINATE_Y).ToString());

                                if (X_Min_Standard > dMeasuremnet_X || X_Max_Standard < dMeasuremnet_X)
                                {
                                    return new ProcessRecipeData() { RecipeTime = "203", RecipeVersion = "Point X Position Error!" };
                                }
                                if (Y_Min_Standard > dMeasuremnet_Y || Y_Max_Standard < dMeasuremnet_Y)
                                {
                                    return new ProcessRecipeData() { RecipeTime = "204", RecipeVersion = "Point Y Position Error!" };
                                }


                                int iFov = ConvertFov(sArrayDefects[8]);

                                //20220927 JHKIM FOV 0.0 : Auto, Etc : 1.0, 1.5, 2.0, 3.0, 5.0, 8.0, 10.0
                                if (iFov == 0)
                                {
                                    m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] Fov Value Error (Fov is {0} -> 0)", sArrayDefects[8]);
                                    return new ProcessRecipeData() { RecipeTime = "205", RecipeVersion = string.Format(" Fov Value Error (Fov is {0} -> 0)", sArrayDefects[8]) };
                                }

                                //hashTempMeasureMent.Add(string.Format("MEASUREMENT_FOV_SIZE_{0}", iCount), iFov.ToString());

                                Dictionary<string, string[]> hashReadMeasurementData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, "Measurement", m_CimConfigurator.AttriButeId);

                                foreach (string sKey in hashReadMeasurementData.Keys)
                                {
                                    string[] sValue = hashReadMeasurementData[sKey];
                                    if (sKey.Contains("FOV"))
                                        hashTempMeasureMent.Add(string.Format("{0}_{1}", sValue[0], iCount), sValue[1] == "0" ? iFov.ToString() : sValue[1]);
                                    else if (sKey.Contains("SCAN_NO"))
                                        hashTempMeasureMent.Add(string.Format("{0}_{1}", sValue[0], iCount), m_CimConfigurator.ScanNoFlag.Contains(_classLookUp[int.Parse(sArrayDefects[9]) - 1]) ? m_CimConfigurator.ScanNo : sValue[1]);
                                    else
                                        hashTempMeasureMent.Add(string.Format("{0}_{1}", sValue[0], iCount), sValue[1]);
                                }

                                iCount++;
                            }

                            Dictionary<string, string> hashMeasureMent = new Dictionary<string, string>
                            {
                                { "MEASUREMENT_POINT_NUMBER", iCount.ToString() },
                            };
                            prd.ParameterNameToValue.Add("MEASUREMENT_POINT_NUMBER", iCount.ToString());

                            foreach (string sKey in hashTempMeasureMent.Keys)
                            {
                                hashMeasureMent.Add(sKey, hashTempMeasureMent[sKey]);
                                prd.ParameterNameToValue.Add(sKey, hashTempMeasureMent[sKey]);
                            }

                            //Defcet 정보 유무 Check
                            //20230630 jkseo, 측정 포인트가 0인것도 Run될수 있음...
                            //if (hashTempMeasureMent.Count == 0)
                            //    return null;

                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Measurement", hashMeasureMent))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Measurement)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = string.Format("{0} File Access Failed (Param : Measurement)", sProcessRecipeFilePath) };
                            }
                            break;
                        case "SampleCenterLocation":
                            Dictionary<string, string> hashOrigin = new Dictionary<string, string>()
                            {
                                { "ORIGIN_COORDINATE_X", sArrayReadDatas[1].Replace(';', ' ') },
                                { "ORIGIN_COORDINATE_Y", sArrayReadDatas[2].Replace(';', ' ') }
                            };

                            prd.ParameterNameToValue.Add("ORIGIN_COORDINATE_X", sArrayReadDatas[1].Replace(';', ' '));
                            prd.ParameterNameToValue.Add("ORIGIN_COORDINATE_Y", sArrayReadDatas[2].Replace(';', ' '));


                            double.TryParse(sArrayReadDatas[1].Replace(';', ' '), out dCOORDINATE_X);
                            double.TryParse(sArrayReadDatas[2].Replace(';', ' '), out dCOORDINATE_Y);

                            if (dCOORDINATE_X == 0 || dCOORDINATE_Y == 0)
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] ORIGIN_COORDINATE_X / ORIGIN_COORDINATE_Y Value Error");
                                return new ProcessRecipeData() { RecipeTime = "206", RecipeVersion = "ORIGIN_COORDINATE_X / ORIGIN_COORDINATE_Y Value Error" };
                            }

                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Origin", hashOrigin))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Origin)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = string.Format("{0} File Access Failed (Param : Origin)", sProcessRecipeFilePath) };
                            }

                            //230202 ProcessRecipe 변경 사항 적용
                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Inspection", m_CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].HashInspectionNameToValue))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Inspection)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = string.Format("{0} File Access Failed (Param : Inspection)", sProcessRecipeFilePath) };
                            }
                            //230202 ProcessRecipe 변경 사항 적용
                            if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Aperture", m_CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].HashApertureNameToValue))
                            {
                                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Aperture)", sProcessRecipeFilePath);
                                return new ProcessRecipeData() { RecipeTime = "200", RecipeVersion = string.Format("{0} File Access Failed (Param : Aperture)", sProcessRecipeFilePath) };
                            }
                            break;
                    }
                }

                prd.RecipeName = sProcessRecipeName.Replace(".xml", string.Empty);
                prd.RecipeFilePath = sProcessRecipeFilePath;
                m_CimConfigurator.RecipeManager.RecipeCreate(1, prd.RecipeName, null, prd);

                List<PlcAddr> listProcessRecipeStruct = m_CimConfigurator.HashShareStruct["L1_PROCESS_RECIPE_CMD_STRUCT"];
                m_CimConfigurator.ShareMem.SetAscii(listProcessRecipeStruct[0], prd.RecipeName);
                m_CimConfigurator.ShareMem.SetByte(listProcessRecipeStruct[1], 1);

                m_CimConfigurator.ShareMem.SetBit(m_CimConfigurator.HashShareKeyToShareData["L1_B_RECIPE_CMD_COMMAND"], true);

                return prd;
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
                m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] SummaryRecipeToRff Code Error");
                return null;
            }
        }
        private ProcessRecipeData SummaryRecipeToPos(string sRecipeFilePath, string sRecipeName, string sRffFilePath, string sLotId, string sDateTime, EmAccessMode mode)
        {
            try
            {
                string sRead = string.Empty;
                string sTempMaskId = string.Empty;

                Dictionary<string, string> hashSubState = new Dictionary<string, string>();

                StreamReader sr = new StreamReader(sRffFilePath, Encoding.Default);

                hashSubState.Add("SUBSTRATE_SIZE_X", "152400");
                hashSubState.Add("SUBSTRATE_SIZE_Y", "152400");
                hashSubState.Add("SUBSTRATE_ID", "TestMaskID");

                XmlDataReader xmlDataReader = new XmlDataReader(sRecipeFilePath, false);

                string sTotalKey = string.Format("{0}/{1}", DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY);

                Dictionary<string, string[]> hashReadData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, DEF_READ_ATTRIBUTE_VALUE, m_CimConfigurator.AttriButeId);

                string sProcessRecipeName = string.Empty;
                string sProcessRecipeFilePath = string.Empty;
                string tempRecipeName = string.Format("{0}.xml", sLotId);
                for (int _iPos = 1; ; _iPos++)
                {
                    if (File.Exists(string.Format(@"{0}\{1}", m_sProcessRecipePath, tempRecipeName)))
                    {
                        tempRecipeName = string.Format("{0}_{1}.xml", sLotId, _iPos);
                    }
                    else
                    {
                        sProcessRecipeName = tempRecipeName;
                        sProcessRecipeFilePath = string.Format(@"{0}\{1}", m_sProcessRecipePath, sProcessRecipeName);
                        break;
                    }
                }

                XmlDataWriter xmlDataWriter = new XmlDataWriter(sProcessRecipeFilePath, "Recipe");

                Dictionary<string, string> hashTime = new Dictionary<string, string>()
                {
                    { "Time",Common.GenerateGUITime(DateTime.Now) },
                    { "Version", "1"  },
                    { "Type", mode.ToString() }
                };

                if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Info", hashTime))
                {
                    m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] {0} File Access Failed (Param : Info)", sProcessRecipeFilePath);
                    return null;
                }

                ProcessRecipeData prd = new ProcessRecipeData()
                {
                    RecipeTime = hashTime["Time"],
                    RecipeVersion = hashTime["Version"]
                };

                foreach (string sKey in hashSubState.Keys)
                    prd.ParameterNameToValue.Add(sKey, hashSubState[sKey]);

                if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Substrate", hashSubState))
                {
                    m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] {0} File Access Failed (Param : Substrate)", sProcessRecipeFilePath);
                    return null;
                }

                string sReadData = string.Empty;

                // SampleCenterLocation
                {
                    Dictionary<string, string> hashOrigin = new Dictionary<string, string>(){
                                { "ORIGIN_COORDINATE_X", "76200.0" },
                                { "ORIGIN_COORDINATE_Y", "76200.0" }};
                    prd.ParameterNameToValue.Add("ORIGIN_COORDINATE_X", "76200.0");
                    prd.ParameterNameToValue.Add("ORIGIN_COORDINATE_Y", "76200.0");


                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Origin", hashOrigin))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] {0} File Access Failed (Param : Origin)", sProcessRecipeFilePath);
                        return null;
                    }
                    //230202 ProcessRecipe 변경 사항 적용
                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Inspection", m_CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].HashInspectionNameToValue))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] {0} File Access Failed (Param : Inspection)", sProcessRecipeFilePath);
                        return null;
                    }
                    //230202 ProcessRecipe 변경 사항 적용
                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Aperture", m_CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].HashApertureNameToValue))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] {0} File Access Failed (Param : Aperture)", sProcessRecipeFilePath);
                        return null;
                    }
                }

                // AlignmentPoints
                {
                    Dictionary<string, string> hashTemp = new Dictionary<string, string>();
                    Dictionary<string, string> hashTempAlg = new Dictionary<string, string>(){
                                { "ALIGNMENT_POINT_NUMBER", "4" }};

                    //Mask 90도 일때
                    hashTempAlg.Add("ALIGNMENT_POINT_X_1", "19600");
                    hashTempAlg.Add("ALIGNMENT_POINT_Y_1", "19600");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_1", "19600");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_1", "19600");

                    hashTempAlg.Add("ALIGNMENT_POINT_X_2", "19600");
                    hashTempAlg.Add("ALIGNMENT_POINT_Y_2", "133300");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_2", "19600");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_2", "133300");

                    hashTempAlg.Add("ALIGNMENT_POINT_X_3", "132800");
                    hashTempAlg.Add("ALIGNMENT_POINT_Y_3", "133300");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_3", "132800");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_3", "133300");

                    hashTempAlg.Add("ALIGNMENT_POINT_X_4", "132800");
                    hashTempAlg.Add("ALIGNMENT_POINT_Y_4", "19600");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_X_4", "132800");
                    prd.ParameterNameToValue.Add("ALIGNMENT_POINT_Y_4", "19600");

                    //Alignment 정보 Check
                    if (hashTempAlg.Count == 0)
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] Aligment Point Value Error (Count 0)");
                        return null;
                    }

                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Alignment", hashTempAlg))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Alignment)", sProcessRecipeFilePath);
                        return null;
                    }


                    Dictionary<string, string[]> hashReadCalibrationData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, "Calibration", m_CimConfigurator.AttriButeId);

                    foreach (string sKey in hashReadCalibrationData.Keys)
                    {
                        string[] sValue = hashReadCalibrationData[sKey];
                        hashTemp.Add(sValue[0], sValue[1]);
                        prd.ParameterNameToValue.Add(sValue[0], sValue[1]);
                    }
                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Calibration", hashTemp))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Calibration)", sProcessRecipeFilePath);
                        return null;
                    }

                    //230202 ProcessRecipe 변경 사항 적용
                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Monitoring", m_CimConfigurator.RecipeManager.BasicRecipeNameToData[sRecipeName].HashMonitoringNameToValue))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Monitoring)", sProcessRecipeFilePath);
                        return null;
                    }

                    hashTemp = new Dictionary<string, string>();

                    Dictionary<string, string[]> hashReadContaminationData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, "Contamination", m_CimConfigurator.AttriButeId);

                    foreach (string sKey in hashReadContaminationData.Keys)
                    {
                        string[] sValue = hashReadContaminationData[sKey];
                        hashTemp.Add(sValue[0], sValue[1]);
                        prd.ParameterNameToValue.Add(sValue[0], sValue[1]);
                    }
                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Contamination", hashTemp))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] {0} File Access Failed (Param : Contamination)", sProcessRecipeFilePath);
                        return null;
                    }
                }

                // DefectRecordSpec (.POS File Read Part)
                {
                    int iCount = 0;
                    Dictionary<string, string> hashTempMeasureMent = new Dictionary<string, string>();
                    while ((sReadData = sr.ReadLine()) != null)
                    {
                        if (sReadData.Length < 4)
                            break;
                        iCount++;


                        string[] sArrayReadDatas = sReadData.Split(',');

                        hashTempMeasureMent.Add(string.Format("MEASUREMENT_DEFECT_ID_{0}", iCount), sArrayReadDatas[3].ToString().Replace("\"", ""));
                        double dMeasuremnet_X = 0;
                        double dMeasuremnet_Y = 0;

                        if (!double.TryParse(sArrayReadDatas[0], out dMeasuremnet_X) || !double.TryParse(sArrayReadDatas[1], out dMeasuremnet_Y))
                        {
                            m_CimConfigurator.HsmsLogControl.AddMessage("[RFF Fail] Defect Point Value Error");
                            return null;
                        }

                        double.TryParse(sArrayReadDatas[0], out dMeasuremnet_X);
                        double.TryParse(sArrayReadDatas[1], out dMeasuremnet_Y);
                        hashTempMeasureMent.Add(string.Format("MEASUREMENT_REF_X_POS_{0}", iCount), dMeasuremnet_X.ToString());//(dMeasuremnet_X - dCOORDINATE_X).ToString());
                        hashTempMeasureMent.Add(string.Format("MEASUREMENT_REF_Y_POS_{0}", iCount), dMeasuremnet_Y.ToString());//(dMeasuremnet_Y - dCOORDINATE_Y).ToString());

                        int iFov = 0;
                        int.TryParse(sArrayReadDatas[2], out iFov);
                        //20220927 JHKIM FOV 0.0 : Auto, Etc : 1.0, 1.5, 2.0, 3.0, 5.0, 8.0, 10.0
                        //20230720 JSOH Fov 0일 경우 3 Fix 사용  변재연님 요청
                        if (iFov == 0)
                            iFov = 3;

                        Dictionary<string, string[]> hashReadMeasurementData = xmlDataReader.XmlReadDictionary(sTotalKey, DEF_INTERLOCK_KEY, "Measurement", m_CimConfigurator.AttriButeId);

                        foreach (string sKey in hashReadMeasurementData.Keys)
                        {
                            string[] sValue = hashReadMeasurementData[sKey];
                            if (sKey.Contains("FOV"))
                                hashTempMeasureMent.Add(string.Format("{0}_{1}", sValue[0], iCount), sValue[1] == "0" ? iFov.ToString() : sValue[1]);
                            else
                                hashTempMeasureMent.Add(string.Format("{0}_{1}", sValue[0], iCount), sValue[1]);
                        }
                    }

                    Dictionary<string, string> hashMeasureMent = new Dictionary<string, string>
                        {
                            { "MEASUREMENT_POINT_NUMBER", iCount.ToString() },
                        };
                    prd.ParameterNameToValue.Add("MEASUREMENT_POINT_NUMBER", iCount.ToString());

                    foreach (string sKey in hashTempMeasureMent.Keys)
                    {
                        hashMeasureMent.Add(sKey, hashTempMeasureMent[sKey]);
                        prd.ParameterNameToValue.Add(sKey, hashTempMeasureMent[sKey]);
                    }

                    //Defcet 정보 유무 Check
                    if (hashTempMeasureMent.Count == 0)
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] Defect Point Value Error (Count 0)");
                        return null;
                    }

                    if (!xmlDataWriter.EsolXmlElement(DEF_HEADER_NAME, DEF_ATTRIBUTE_KEY, DEF_INTERLOCK_KEY, "Measurement", hashMeasureMent))
                    {
                        m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] {0} File Access Failed (Param : Measurement)", sProcessRecipeFilePath);
                        return null;
                    }
                }

                prd.RecipeName = sProcessRecipeName.Replace(".xml", string.Empty);
                prd.RecipeFilePath = sProcessRecipeFilePath;
                m_CimConfigurator.RecipeManager.RecipeCreate(1, prd.RecipeName, null, prd);

                List<PlcAddr> listProcessRecipeStruct = m_CimConfigurator.HashShareStruct["L1_PROCESS_RECIPE_CMD_STRUCT"];
                m_CimConfigurator.ShareMem.SetAscii(listProcessRecipeStruct[0], prd.RecipeName);
                m_CimConfigurator.ShareMem.SetByte(listProcessRecipeStruct[1], 1);

                m_CimConfigurator.ShareMem.SetBit(m_CimConfigurator.HashShareKeyToShareData["L1_B_RECIPE_CMD_COMMAND"], true);

                return prd;
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
                m_CimConfigurator.HsmsLogControl.AddMessage("[POS Fail] SummaryRecipeToPos Code Error");
                return null;
            }
        }
        #endregion

        #region Class Private Methdos
        /// <summary>
        /// 2023-02-06 (월) 오후 6:47 장준태 프로 내용 적용
        /// </summary>
        private int ConvertFov(string sValue)
        {
            try
            {
                if (float.TryParse(sValue, out float fValue))
                {
                    if (fValue < 0.5)
                        return 1;
                    else if (fValue < 2)
                        return 3;
                    else
                        return 8;
                }

                return 0;
            }
            catch (Exception ex)
            {
                m_CimConfigurator.LogManager.ErrorWriteLog(ex.ToString());
                return 0;
            }
        }
        #endregion
    }
}
