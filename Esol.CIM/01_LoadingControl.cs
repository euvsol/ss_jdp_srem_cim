﻿#region Class Using
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
#endregion

namespace Esol.CIM
{
    public partial class LoadingControl : FormControl
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadProgressValue(ProgressBar pro, int iValue);
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadThisClose();
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public LoadingStep LoadingStep
        {
            set
            {
                try
                {
                    int iValue = (int)value * 13 > 100 ? 100 : (int)value * 13;
                    ProgressBarCrossThread(m_progressBar, iValue);
                    GetPictureBoxObject((int)value).BackgroundImage = GetImage("Loading.png");

                    if (iValue == m_progressBar.Maximum)
                    {
                        System.Threading.Thread.Sleep(1000);
                        FormCloseCrossThread();
                    }
                }
                catch (Exception ex)
                {
                    MainControl.LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        #endregion

        #region Class Initialization
        /// <summary>
        /// 
        /// </summary>
        public LoadingControl()
        {
            InitializeComponent();

            this.DoubleBuffered = true;

            Initialize();
        }
        /// <summary>
        /// 
        /// </summary>
        private void Initialize()
        {
            try
            {
                for (int i = 1; i < 9; i++)
                {
                    GetPictureBoxObject(i).BackgroundImage = GetImage("InitiLoading.png");
                    GetLabelObject(i).Text = ((LoadingStep)(i)).ToString();
                }

                m_pbCompany.BackgroundImage = GetImage("Esol.png");
            }
            catch (Exception ex)
            {
                MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Utility Methods
        /// <summary>
        /// 
        /// </summary>
        public PictureBox GetPictureBoxObject(int iValue)
        {
            try
            {
                FieldInfo finfo = this.GetType().GetField(string.Format("pictureBox{0}", iValue), BindingFlags.GetField | BindingFlags.IgnoreCase
                 | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

                return (PictureBox)finfo.GetValue(this);
            }
            catch (Exception ex)
            {
                MainControl.LogManager.ErrorWriteLog(ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private Label GetLabelObject(int iValue)
        {
            try
            {
                FieldInfo finfo = this.GetType().GetField(string.Format("label{0}", iValue), BindingFlags.GetField | BindingFlags.IgnoreCase
                 | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

                return (Label)finfo.GetValue(this);
            }
            catch (Exception ex)
            {
                MainControl.LogManager.ErrorWriteLog(ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ProgressBarCrossThread(ProgressBar pro, int iValue)
        {
            try
            {
                if (pro.InvokeRequired)
                {
                    ReloadProgressValue reload = new ReloadProgressValue(ProgressBarCrossThread);
                    this.Invoke(reload, new object[] { pro, iValue });
                }
                else
                    pro.Value = iValue;
            }
            catch (Exception ex)
            {
                MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void FormCloseCrossThread()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    ReloadThisClose reload = new ReloadThisClose(FormCloseCrossThread);
                    this.Invoke(reload);
                }
                else
                    this.Close();
            }
            catch (Exception ex)
            {
                MainControl.LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Image GetImage(string sImageSource)
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();

                Stream imageStream = assembly.GetManifestResourceStream("Esol.CIM.Resources.Icons." + sImageSource);

                return Bitmap.FromStream(imageStream);
            }
            catch (Exception ex)
            {
                MainControl.LogManager.ErrorWriteLog(ex.ToString());
                return null;
            }
        }
        #endregion
    }
}
