﻿namespace Esol.CIM
{
    partial class MainControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Esol.Components.ColorPanel.ColorizerPanel colorizerPanel1 = new Esol.Components.ColorPanel.ColorizerPanel();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainControl));
            Esol.Components.ColorPanel.ColorizerPanel colorizerPanel2 = new Esol.Components.ColorPanel.ColorizerPanel();
            this.m_AliveTimer = new System.Windows.Forms.Timer(this.components);
            this.m_DataTimer = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.m_panelNotification = new Esol.Components.RoundWindow();
            this.m_roundWindow = new Esol.Components.RoundWindow();
            this.colorPanel2 = new Esol.Components.ColorPanel();
            this.m_picXgemTransfer = new Esol.Components.RoundPictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_lbXgemConnectState = new System.Windows.Forms.Label();
            this.roundTitleButton6 = new Esol.Components.RoundTitleButton();
            this.colorPanel1 = new Esol.Components.ColorPanel();
            this.m_picHsmsTransfer = new Esol.Components.RoundPictureBox();
            this.m_lbIpAdress = new System.Windows.Forms.Label();
            this.m_lbHsmsConnectState = new System.Windows.Forms.Label();
            this.roundTitleButton5 = new Esol.Components.RoundTitleButton();
            this.roundTitleButton1 = new Esol.Components.RoundTitleButton();
            this.roundTitleButton4 = new Esol.Components.RoundTitleButton();
            this.m_roundTitleButton2 = new Esol.Components.RoundTitleButton();
            this.roundTitleButton7 = new Esol.Components.RoundTitleButton();
            this.roundTitleButton2 = new Esol.Components.RoundTitleButton();
            this.roundTitleButton3 = new Esol.Components.RoundTitleButton();
            this.m_roundTitleBar = new Esol.Components.RoundTitleBar();
            this.tableLayoutPanel1.SuspendLayout();
            this.colorPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_picXgemTransfer)).BeginInit();
            this.colorPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_picHsmsTransfer)).BeginInit();
            this.SuspendLayout();
            // 
            // m_AliveTimer
            // 
            this.m_AliveTimer.Interval = 500;
            this.m_AliveTimer.Tick += new System.EventHandler(this.OnAliveTick);
            // 
            // m_DataTimer
            // 
            this.m_DataTimer.Interval = 1000;
            this.m_DataTimer.Tick += new System.EventHandler(this.OnDataTimer);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(683, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 26);
            this.button1.TabIndex = 27;
            this.button1.Text = "JobClear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 12;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.599604F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.101764F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.81312F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.64414F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.64414F));
            this.tableLayoutPanel1.Controls.Add(this.m_panelNotification, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.m_roundWindow, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.colorPanel2, 10, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton6, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.colorPanel1, 11, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton5, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton4, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.m_roundTitleButton2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton7, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.roundTitleButton3, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 54);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 862F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1520, 959);
            this.tableLayoutPanel1.TabIndex = 28;
            // 
            // m_panelNotification
            // 
            this.m_panelNotification._Round_DockStyle = System.Windows.Forms.DockStyle.Fill;
            this.m_panelNotification._Round_Shape = Esol.Components.eShape.Round;
            this.m_panelNotification._Round_Theme = Esol.Components.eTheme.Black;
            this.m_panelNotification._Round_VisibleLine = true;
            this.m_panelNotification.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.m_panelNotification.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_panelNotification.Location = new System.Drawing.Point(757, 0);
            this.m_panelNotification.Margin = new System.Windows.Forms.Padding(0);
            this.m_panelNotification.Name = "m_panelNotification";
            this.m_panelNotification.Size = new System.Drawing.Size(316, 97);
            this.m_panelNotification.TabIndex = 26;
            // 
            // m_roundWindow
            // 
            this.m_roundWindow._Round_DockStyle = System.Windows.Forms.DockStyle.Fill;
            this.m_roundWindow._Round_Shape = Esol.Components.eShape.Round;
            this.m_roundWindow._Round_Theme = Esol.Components.eTheme.Black;
            this.m_roundWindow._Round_VisibleLine = true;
            this.m_roundWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.tableLayoutPanel1.SetColumnSpan(this.m_roundWindow, 13);
            this.m_roundWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_roundWindow.Location = new System.Drawing.Point(0, 102);
            this.m_roundWindow.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.m_roundWindow.Name = "m_roundWindow";
            this.m_roundWindow.Size = new System.Drawing.Size(1520, 857);
            this.m_roundWindow.TabIndex = 2;
            // 
            // colorPanel2
            // 
            this.colorPanel2._BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.colorPanel2.BackColor = System.Drawing.Color.White;
            this.colorPanel2.Controls.Add(this.m_picXgemTransfer);
            this.colorPanel2.Controls.Add(this.label1);
            this.colorPanel2.Controls.Add(this.m_lbXgemConnectState);
            this.colorPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorPanel2.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.colorPanel2.ForeColor = System.Drawing.Color.White;
            this.colorPanel2.IsColorizerEnabled = false;
            this.colorPanel2.IsTransparencyEnabled = false;
            this.colorPanel2.Location = new System.Drawing.Point(1076, 3);
            this.colorPanel2.Name = "colorPanel2";
            this.colorPanel2.PanelColorizer = colorizerPanel1;
            this.colorPanel2.Size = new System.Drawing.Size(216, 91);
            this.colorPanel2.TabIndex = 13;
            this.colorPanel2.Title = "Xgem";
            // 
            // m_picXgemTransfer
            // 
            this.m_picXgemTransfer.BackColor = System.Drawing.Color.Transparent;
            this.m_picXgemTransfer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("m_picXgemTransfer.BackgroundImage")));
            this.m_picXgemTransfer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.m_picXgemTransfer.Location = new System.Drawing.Point(17, 40);
            this.m_picXgemTransfer.Name = "m_picXgemTransfer";
            this.m_picXgemTransfer.Size = new System.Drawing.Size(46, 44);
            this.m_picXgemTransfer.TabIndex = 14;
            this.m_picXgemTransfer.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(75, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 28);
            this.label1.TabIndex = 13;
            this.label1.Text = "127.0.0.1:2001";
            // 
            // m_lbXgemConnectState
            // 
            this.m_lbXgemConnectState.AutoSize = true;
            this.m_lbXgemConnectState.BackColor = System.Drawing.Color.White;
            this.m_lbXgemConnectState.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.m_lbXgemConnectState.ForeColor = System.Drawing.Color.Black;
            this.m_lbXgemConnectState.Location = new System.Drawing.Point(75, 39);
            this.m_lbXgemConnectState.Name = "m_lbXgemConnectState";
            this.m_lbXgemConnectState.Size = new System.Drawing.Size(74, 28);
            this.m_lbXgemConnectState.TabIndex = 13;
            this.m_lbXgemConnectState.Text = "Closed";
            this.m_lbXgemConnectState.DoubleClick += new System.EventHandler(this.OnXgemStartStop);
            this.m_lbXgemConnectState.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnXgemMouseDown);
            // 
            // roundTitleButton6
            // 
            this.roundTitleButton6._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton6._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton6._Round_Index = 7;
            this.roundTitleButton6._Round_Selected = true;
            this.roundTitleButton6._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton6._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton6._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton6._Round_TitleImage")));
            this.roundTitleButton6._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton6._Round_TitleLocation = new System.Drawing.Point(-3, 56);
            this.roundTitleButton6._Round_TitleText = "Config";
            this.roundTitleButton6._Round_TitleTextShow = true;
            this.roundTitleButton6._Round_UnselectLineVisible = false;
            this.roundTitleButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton6.Location = new System.Drawing.Point(595, 0);
            this.roundTitleButton6.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton6.Name = "roundTitleButton6";
            this.roundTitleButton6.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton6.TabIndex = 10;
            this.roundTitleButton6.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // colorPanel1
            // 
            this.colorPanel1._BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.colorPanel1.BackColor = System.Drawing.Color.White;
            this.colorPanel1.Controls.Add(this.m_picHsmsTransfer);
            this.colorPanel1.Controls.Add(this.m_lbIpAdress);
            this.colorPanel1.Controls.Add(this.m_lbHsmsConnectState);
            this.colorPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorPanel1.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.colorPanel1.ForeColor = System.Drawing.Color.White;
            this.colorPanel1.IsColorizerEnabled = false;
            this.colorPanel1.IsTransparencyEnabled = false;
            this.colorPanel1.Location = new System.Drawing.Point(1298, 3);
            this.colorPanel1.Name = "colorPanel1";
            this.colorPanel1.PanelColorizer = colorizerPanel2;
            this.colorPanel1.Size = new System.Drawing.Size(219, 91);
            this.colorPanel1.TabIndex = 13;
            this.colorPanel1.Title = "HSMS";
            // 
            // m_picHsmsTransfer
            // 
            this.m_picHsmsTransfer.BackColor = System.Drawing.Color.Transparent;
            this.m_picHsmsTransfer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("m_picHsmsTransfer.BackgroundImage")));
            this.m_picHsmsTransfer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.m_picHsmsTransfer.Location = new System.Drawing.Point(17, 40);
            this.m_picHsmsTransfer.Name = "m_picHsmsTransfer";
            this.m_picHsmsTransfer.Size = new System.Drawing.Size(46, 44);
            this.m_picHsmsTransfer.TabIndex = 14;
            this.m_picHsmsTransfer.TabStop = false;
            // 
            // m_lbIpAdress
            // 
            this.m_lbIpAdress.AutoSize = true;
            this.m_lbIpAdress.BackColor = System.Drawing.Color.White;
            this.m_lbIpAdress.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.m_lbIpAdress.ForeColor = System.Drawing.Color.Black;
            this.m_lbIpAdress.Location = new System.Drawing.Point(69, 67);
            this.m_lbIpAdress.Name = "m_lbIpAdress";
            this.m_lbIpAdress.Size = new System.Drawing.Size(138, 28);
            this.m_lbIpAdress.TabIndex = 13;
            this.m_lbIpAdress.Text = "127.0.0.1:8000";
            // 
            // m_lbHsmsConnectState
            // 
            this.m_lbHsmsConnectState.AutoSize = true;
            this.m_lbHsmsConnectState.BackColor = System.Drawing.Color.White;
            this.m_lbHsmsConnectState.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.m_lbHsmsConnectState.ForeColor = System.Drawing.Color.Black;
            this.m_lbHsmsConnectState.Location = new System.Drawing.Point(69, 39);
            this.m_lbHsmsConnectState.Name = "m_lbHsmsConnectState";
            this.m_lbHsmsConnectState.Size = new System.Drawing.Size(112, 28);
            this.m_lbHsmsConnectState.TabIndex = 13;
            this.m_lbHsmsConnectState.Text = "Disconnect";
            // 
            // roundTitleButton5
            // 
            this.roundTitleButton5._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton5._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton5._Round_Index = 6;
            this.roundTitleButton5._Round_Selected = true;
            this.roundTitleButton5._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton5._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton5._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton5._Round_TitleImage")));
            this.roundTitleButton5._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton5._Round_TitleLocation = new System.Drawing.Point(-6, 56);
            this.roundTitleButton5._Round_TitleText = "History";
            this.roundTitleButton5._Round_TitleTextShow = true;
            this.roundTitleButton5._Round_UnselectLineVisible = false;
            this.roundTitleButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton5.Location = new System.Drawing.Point(510, 0);
            this.roundTitleButton5.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton5.Name = "roundTitleButton5";
            this.roundTitleButton5.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton5.TabIndex = 10;
            this.roundTitleButton5.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // roundTitleButton1
            // 
            this.roundTitleButton1._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton1._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton1._Round_Index = 1;
            this.roundTitleButton1._Round_Selected = true;
            this.roundTitleButton1._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton1._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton1._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton1._Round_TitleImage")));
            this.roundTitleButton1._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton1._Round_TitleLocation = new System.Drawing.Point(-3, 56);
            this.roundTitleButton1._Round_TitleText = "Recipe";
            this.roundTitleButton1._Round_TitleTextShow = true;
            this.roundTitleButton1._Round_UnselectLineVisible = false;
            this.roundTitleButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton1.Location = new System.Drawing.Point(85, 0);
            this.roundTitleButton1.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton1.Name = "roundTitleButton1";
            this.roundTitleButton1.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton1.TabIndex = 9;
            this.roundTitleButton1.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // roundTitleButton4
            // 
            this.roundTitleButton4._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton4._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton4._Round_Index = 5;
            this.roundTitleButton4._Round_Selected = true;
            this.roundTitleButton4._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton4._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton4._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton4._Round_TitleImage")));
            this.roundTitleButton4._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton4._Round_TitleLocation = new System.Drawing.Point(1, 56);
            this.roundTitleButton4._Round_TitleText = "Alarm";
            this.roundTitleButton4._Round_TitleTextShow = true;
            this.roundTitleButton4._Round_UnselectLineVisible = false;
            this.roundTitleButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton4.Location = new System.Drawing.Point(425, 0);
            this.roundTitleButton4.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton4.Name = "roundTitleButton4";
            this.roundTitleButton4.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton4.TabIndex = 10;
            this.roundTitleButton4.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // m_roundTitleButton2
            // 
            this.m_roundTitleButton2._Round_Dependence = Esol.Components.eDependence.Independence;
            this.m_roundTitleButton2._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.m_roundTitleButton2._Round_Index = 0;
            this.m_roundTitleButton2._Round_Selected = true;
            this.m_roundTitleButton2._Round_Shape = Esol.Components.eShape.Round;
            this.m_roundTitleButton2._Round_Theme = Esol.Components.eTheme.Black;
            this.m_roundTitleButton2._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("m_roundTitleButton2._Round_TitleImage")));
            this.m_roundTitleButton2._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.m_roundTitleButton2._Round_TitleLocation = new System.Drawing.Point(2, 56);
            this.m_roundTitleButton2._Round_TitleText = "Hsms";
            this.m_roundTitleButton2._Round_TitleTextShow = true;
            this.m_roundTitleButton2._Round_UnselectLineVisible = false;
            this.m_roundTitleButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.m_roundTitleButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.m_roundTitleButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_roundTitleButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_roundTitleButton2.Location = new System.Drawing.Point(0, 0);
            this.m_roundTitleButton2.Margin = new System.Windows.Forms.Padding(0);
            this.m_roundTitleButton2.Name = "m_roundTitleButton2";
            this.m_roundTitleButton2.Size = new System.Drawing.Size(80, 80);
            this.m_roundTitleButton2.TabIndex = 8;
            this.m_roundTitleButton2.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // roundTitleButton7
            // 
            this.roundTitleButton7._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton7._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton7._Round_Index = 4;
            this.roundTitleButton7._Round_Selected = true;
            this.roundTitleButton7._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton7._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton7._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton7._Round_TitleImage")));
            this.roundTitleButton7._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton7._Round_TitleLocation = new System.Drawing.Point(3, 56);
            this.roundTitleButton7._Round_TitleText = "CJ/PJ";
            this.roundTitleButton7._Round_TitleTextShow = true;
            this.roundTitleButton7._Round_UnselectLineVisible = false;
            this.roundTitleButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton7.Location = new System.Drawing.Point(340, 0);
            this.roundTitleButton7.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton7.Name = "roundTitleButton7";
            this.roundTitleButton7.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton7.TabIndex = 10;
            this.roundTitleButton7.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // roundTitleButton2
            // 
            this.roundTitleButton2._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton2._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton2._Round_Index = 2;
            this.roundTitleButton2._Round_Selected = true;
            this.roundTitleButton2._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton2._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton2._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton2._Round_TitleImage")));
            this.roundTitleButton2._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton2._Round_TitleLocation = new System.Drawing.Point(7, 56);
            this.roundTitleButton2._Round_TitleText = "ECID";
            this.roundTitleButton2._Round_TitleTextShow = true;
            this.roundTitleButton2._Round_UnselectLineVisible = false;
            this.roundTitleButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton2.Location = new System.Drawing.Point(170, 0);
            this.roundTitleButton2.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton2.Name = "roundTitleButton2";
            this.roundTitleButton2.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton2.TabIndex = 10;
            this.roundTitleButton2.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // roundTitleButton3
            // 
            this.roundTitleButton3._Round_Dependence = Esol.Components.eDependence.Independence;
            this.roundTitleButton3._Round_Direction = Esol.Components.eButtonDirection.Top;
            this.roundTitleButton3._Round_Index = 3;
            this.roundTitleButton3._Round_Selected = true;
            this.roundTitleButton3._Round_Shape = Esol.Components.eShape.Round;
            this.roundTitleButton3._Round_Theme = Esol.Components.eTheme.Black;
            this.roundTitleButton3._Round_TitleImage = ((System.Drawing.Image)(resources.GetObject("roundTitleButton3._Round_TitleImage")));
            this.roundTitleButton3._Round_TitleImageSize = Esol.Components.eTitleImageSize.Large;
            this.roundTitleButton3._Round_TitleLocation = new System.Drawing.Point(11, 56);
            this.roundTitleButton3._Round_TitleText = "FDC";
            this.roundTitleButton3._Round_TitleTextShow = true;
            this.roundTitleButton3._Round_UnselectLineVisible = false;
            this.roundTitleButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.roundTitleButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.roundTitleButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundTitleButton3.Location = new System.Drawing.Point(255, 0);
            this.roundTitleButton3.Margin = new System.Windows.Forms.Padding(0);
            this.roundTitleButton3.Name = "roundTitleButton3";
            this.roundTitleButton3.Size = new System.Drawing.Size(80, 80);
            this.roundTitleButton3.TabIndex = 10;
            this.roundTitleButton3.ButtonClicked += new Esol.Components.RoundTitleButton.ButtonClickEventHandler(this.OnButtonClick);
            // 
            // m_roundTitleBar
            // 
            this.m_roundTitleBar._Round_Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.m_roundTitleBar._Round_BottomBarVisible = true;
            this.m_roundTitleBar._Round_CloseEnable = false;
            this.m_roundTitleBar._Round_ControlBox = true;
            this.m_roundTitleBar._Round_Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.m_roundTitleBar._Round_ManualClose = true;
            this.m_roundTitleBar._Round_MaximumEnable = false;
            this.m_roundTitleBar._Round_MinimumEnable = true;
            this.m_roundTitleBar._Round_Shape = Esol.Components.eShape.Round;
            this.m_roundTitleBar._Round_Theme = Esol.Components.eTheme.Black;
            this.m_roundTitleBar._Round_Title = "ESOL.CIM";
            this.m_roundTitleBar._Round_TitleImage = global::Esol.CIM.Properties.Resources.CIM;
            this.m_roundTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.m_roundTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_roundTitleBar.Location = new System.Drawing.Point(0, 0);
            this.m_roundTitleBar.Margin = new System.Windows.Forms.Padding(4);
            this.m_roundTitleBar.Name = "m_roundTitleBar";
            this.m_roundTitleBar.Size = new System.Drawing.Size(1520, 54);
            this.m_roundTitleBar.TabIndex = 0;
            this.m_roundTitleBar.MouseDownChaged += new Esol.Components.RoundTitleBar.MouseDownEventHandler(this.OnMouseDownChaged);
            this.m_roundTitleBar.MouseMoveChaged += new Esol.Components.RoundTitleBar.MouseMoveEventHandler(this.OnMouseMoveChaged);
            // 
            // MainControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1520, 1013);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.m_roundTitleBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Esol.Cim";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.colorPanel2.ResumeLayout(false);
            this.colorPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_picXgemTransfer)).EndInit();
            this.colorPanel1.ResumeLayout(false);
            this.colorPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_picHsmsTransfer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Components.RoundTitleBar m_roundTitleBar;
        private Components.RoundWindow m_roundWindow;
        private Components.RoundTitleButton m_roundTitleButton2;
        private Components.RoundTitleButton roundTitleButton1;
        private Components.RoundTitleButton roundTitleButton2;
        private Components.RoundTitleButton roundTitleButton3;
        private Components.RoundTitleButton roundTitleButton4;
        private Components.RoundTitleButton roundTitleButton5;
        private Components.RoundTitleButton roundTitleButton6;
        private Components.ColorPanel colorPanel1;
        private System.Windows.Forms.Label m_lbIpAdress;
        private System.Windows.Forms.Label m_lbHsmsConnectState;
        private Components.RoundPictureBox m_picHsmsTransfer;
        private Components.ColorPanel colorPanel2;
        private Components.RoundPictureBox m_picXgemTransfer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_lbXgemConnectState;
        private Components.RoundWindow m_panelNotification;
        private System.Windows.Forms.Timer m_AliveTimer;
        private Components.RoundTitleButton roundTitleButton7;
        private System.Windows.Forms.Timer m_DataTimer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}